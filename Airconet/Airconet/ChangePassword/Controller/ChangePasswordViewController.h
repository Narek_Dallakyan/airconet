//
//  ChangePasswordViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/24/20.
//  Copyright © 2020Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChangePasswordViewController : BaseViewController

@end

NS_ASSUME_NONNULL_END
