//
//  ChangePasswordViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/24/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "ChangePasswordView.h"
#import "RequestManager.h"
#import "ACommonTools.h"

@interface ChangePasswordViewController ()<CustomAlertDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityInd;
@property (strong, nonatomic) IBOutlet ChangePasswordView *mChangePasswordV;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self preferredStatusBarStyle];

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated {
    [self setBackground:self];

}

-(BOOL)isValidFields {
    if (self.mChangePasswordV.mNiewPasswordTxtFl.text.length == 0 || self.mChangePasswordV.mCodeTxtFl.text.length == 0) {
        [self showAlert:NSLocalizedString(@"fill_info_note", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        return NO;

    } else if (self.mChangePasswordV.mNiewPasswordTxtFl.text.length < 7) {
        [self showAlert:NSLocalizedString(@"pass_valid", nil) ok:NSLocalizedString(@"ok", nil) cencel:@""delegate:self];
        return NO;
    }
    return YES;
}

- (IBAction)changePassword:(UIButton *)sender {
    if ([self isValidFields]) {
        
        /*
         Object = {
             attachment = "system error";
             reason = "";
             result = "-12";
         }
         */
        
        [self.mActivityInd startAnimating];
        NSDictionary *dict = @{@"email": (NSString *)[ACommonTools getObjectValueForKey: AKeyCurrentUserName],
                               @"password": self.mChangePasswordV.mNiewPasswordTxtFl.text,
                               @"verifyCode": self.mChangePasswordV.mCodeTxtFl.text};
        [[RequestManager sheredMenage] postJsonDataWithUrl:AGetuserresetPassword andParameters:dict success:^(id response) {
            [ACommonTools removeValueForKey:AKeyCurrentUserName];
            if ([[response valueForKey:@"result"] isEqual:[NSNumber numberWithInt:0]]) {
                dispatch_async(dispatch_get_main_queue(),^{
                    [self.mActivityInd stopAnimating];
                    [self presentViewControllerWithIdentifier:@"Login" controller:self];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(),^{
                    [self.mActivityInd stopAnimating];
                    [self showAlert:NSLocalizedString(@"fill_email_again", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
                });
            }
            

        } failure:^(NSError *error) {
            [ACommonTools removeValueForKey:AKeyCurrentUserName];
            dispatch_async(dispatch_get_main_queue(),^{
                [self.mActivityInd stopAnimating];
                [self showAlert:NSLocalizedString(@"fill_email_again", nil) ok:NSLocalizedString(@"ok", nil) cencel:@""delegate:self];

            });
        }];
    }
    
}
- (IBAction)cancel:(UIButton *)sender {
    [self presentViewControllerWithIdentifier:@"Login" controller:self];
}

#pragma mark : Custom Alert View delegate
-(void)handelOk {
    
}
-(void) handelOkWithMessage:(NSString *)message {
    [self presentViewControllerWithIdentifier:@"ResetPassword" controller:self];
}
@end
