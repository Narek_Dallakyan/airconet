//
//  ChangePasswordView.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/24/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "ChangePasswordView.h"
#import "APublicDefine.h"
#import "UITextField+Content.h"

@implementation ChangePasswordView

-(void)awakeFromNib {
    [self.mCodeTxtFl setBlueBorderStyle];
    [self.mNiewPasswordTxtFl setBlueBorderStyle];
    
    [self.mCodeTxtFl setPlaceholderColor:PlaceholderColor text:self.mCodeTxtFl.placeholder];
    [self.mNiewPasswordTxtFl setPlaceholderColor:PlaceholderColor text:self.mNiewPasswordTxtFl.placeholder];
    
    [self.mCodeTxtFl setImageInLeftView:[UIImage imageNamed:@"code"]];
    [self.mNiewPasswordTxtFl setImageInLeftView:[UIImage imageNamed:@"password"]];
    [self.mCodeTxtFl setRightPadding];
    [self addButtonToRightViewTextField];
    [super awakeFromNib];
}

-(void)addButtonToRightViewTextField {
    UIButton * secureBtn = [[UIButton alloc] init];
       [secureBtn addTarget:self action:@selector(secureBtnAction:) forControlEvents:UIControlEventTouchUpInside];
       
       [self.mNiewPasswordTxtFl setButtonInRightView:secureBtn buttonImg:[UIImage imageNamed: @"not_visible"]];
}

-(void)secureBtnAction:(UIButton *)btn {
    if ([btn.imageView.image isEqual:[UIImage imageNamed:@"not_visible"]]) {
        [btn setImage:[UIImage imageNamed:@"visible"] forState:UIControlStateNormal];
        [self.mNiewPasswordTxtFl setSecureTextEntry:NO];
    } else {
        [btn setImage:[UIImage imageNamed:@"not_visible"] forState:UIControlStateNormal];
        [self.mNiewPasswordTxtFl setSecureTextEntry:YES];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.mCodeTxtFl) {
        textField.returnKeyType = UIReturnKeyNext;
    } else if (textField == self.mNiewPasswordTxtFl) {
        textField.returnKeyType = UIReturnKeyDone;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.mCodeTxtFl) {
        [self.mNiewPasswordTxtFl becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

-(void)textFieldDidChangeSelection:(UITextField *)textField {
    if ([textField isEqual:self.mCodeTxtFl]) {
        if (textField.text.length == 0) {
            [textField setRightPadding];
        } else
            [textField deletRightPadding];
    }
}


@end
