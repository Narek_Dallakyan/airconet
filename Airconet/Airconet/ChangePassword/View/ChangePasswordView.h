//
//  ChangePasswordView.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/24/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChangePasswordView : UIView <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *mCodeTxtFl;
@property (weak, nonatomic) IBOutlet UITextField *mNiewPasswordTxtFl;
@property (strong, nonatomic) IBOutlet UIButton *mCancelBtn;
@property (strong, nonatomic) IBOutlet UILabel *mResetPassLb;

@end

NS_ASSUME_NONNULL_END
