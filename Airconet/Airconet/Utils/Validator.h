//
//  Validator.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/24/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Validator : UIView
+ (BOOL)isValidEmail: (NSString *)email;

@end

NS_ASSUME_NONNULL_END
