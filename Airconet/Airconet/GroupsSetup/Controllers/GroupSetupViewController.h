//
//  GroupSetupViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/16/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"
#import "Group.h"

NS_ASSUME_NONNULL_BEGIN

@interface GroupSetupViewController : BaseViewController
@property (strong, nonatomic) Group * currentGroup;
@property (assign, nonatomic) BOOL isDefaultSetting;
@end

NS_ASSUME_NONNULL_END
