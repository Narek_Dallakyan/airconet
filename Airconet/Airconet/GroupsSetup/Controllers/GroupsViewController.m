//
//  GroupsViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/16/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "GroupsViewController.h"
#import "GroupSetupViewController.h"
#import "DevicesTableViewCell.h"
#import "Group.h"

@interface GroupsViewController ()<DevicesTableViewCellDelegate, PopupViewDelegate, CustomAlertDelegate> {
    NSMutableArray * allGroups;
}
@property (weak, nonatomic) IBOutlet UITableView *mGroupsTbV;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityV;
@property (weak, nonatomic) IBOutlet UILabel *mAddGroupLb;
@property (weak,nonatomic) NSIndexPath * clickedIndexPath;
@property (strong, nonatomic)UIButton * rightBarBtn;
@property (strong, nonatomic)UIButton * leftBarBtn;
@end

@implementation GroupsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: YES];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: YES];

    [self.navigationController.topViewController setTitle:NSLocalizedString(@"groups", nil)];
    [self setBackground:self];
    [self setupView];

}

#pragma mark: SET
-(void)setupView {
    self.mGroupsTbV.separatorColor = [UIColor clearColor];
    [self setBarButton];
    [self setNavigationStyle];
    [_mActivityV startAnimating];
    [self getGroup];

}
-(void)setNavigationStyle {
   // self.navigationController.navigationBar.topItem.title = @"";
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self setNavigationStyle:BaseColor tintColor:[UIColor whiteColor] titleText:NSLocalizedString(@"groups", nil) titleColor:NavTitleColor];
      for (UIView *view in self.navigationController.navigationBar.subviews) {
            if ([view isKindOfClass:[DevicesNavigationBar class]]) {
                [view removeFromSuperview];
            }
        }
}
-(void)setBarButton {
    UIImage* addImage = [UIImage imageNamed:@"plus"];
    _rightBarBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, addImage.size.width, addImage.size.height)];
    [_rightBarBtn setBackgroundImage:addImage forState:UIControlStateNormal];
    [_rightBarBtn addTarget:self action:@selector(addGroup)
           forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithCustomView:_rightBarBtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    UIImage* backImg = [UIImage imageNamed:@"ic_left"];
    if ([BaseViewController isDeviceLanguageRTL]) {
        backImg = [UIImage imageNamed:@"right_direction"];
    }
    _leftBarBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, backImg.size.width, backImg.size.height)];
    [_leftBarBtn setBackgroundImage:backImg forState:UIControlStateNormal];
    [_leftBarBtn addTarget:self action:@selector(back)
          forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:_leftBarBtn];
    self.navigationItem.leftBarButtonItem = leftButton;
}
-(void)setGroups:(NSArray *)result {
    allGroups = [NSMutableArray array];
    for (NSDictionary * group in result) {
        if (![self isHiddenGroupsDevice:[group valueForKey:@"macList"]]) {
            [allGroups addObject:[[[Group alloc] init] getGroupObj:[group valueForKey:@"group"]]];
        }
    }
    if (allGroups.count > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mGroupsTbV setHidden:NO];
            [self.mGroupsTbV reloadData];
        });
    } else {
        [self.mAddGroupLb setHidden:NO];
    }
    [_mActivityV stopAnimating];
    
}
#pragma mark: GET
-(void)getGroup {
    [[RequestManager sheredMenage] getJsonDataWithUrl:AGetgroup
                                        andParameters:@{}
                                         isNeedCookie:NO
                                              success:^(id  _Nonnull response) {
       // NSLog(@"getGroup respons@ = %@\n", response);
        [self.mActivityV stopAnimating];
        if (response) {
            [self setGroups:[response valueForKey:@"attachment"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [self.mActivityV stopAnimating];
        //show error
    }];
}
#pragma mark: Delete Group
-(void)deleteGroup:(Group *)currGroup {
    NSDictionary * param = @{ @"id": @(currGroup.GrId)};
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostgroupremove andParameters:param success:^(id _Nonnull response) {
        //NSLog(@"deleteGroup response = %@\n", response);
        [self getGroup];
        
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"deleteGroup error = %@\n", error.localizedDescription);
    }];
}

-(void)orderGroup {
     NSString * ids = @"";
       for (Group * group in allGroups) {
               ids = [ids stringByAppendingString:[NSString stringWithFormat:@"%ld|",(long)group.GrId]];
       }
       NSDictionary * param = @{@"ids": ids};
       [[RequestManager sheredMenage] postJsonDataWithUrl:APostGroupOrder andParameters:param success:^(id _Nonnull response) {
              // NSLog(@"Group order response  = %i\n",response);
       } failure:^(NSError * _Nonnull erroe) {
              // NSLog(@"Group order response error  = %i\n",erroe.localizedDescription);
           [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
       }];
}

-(void)addGroup {
    [self goToAddGroupPage:YES currentGroup:nil];
}
-(void)back {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)goToAddGroupPage:(BOOL)isDefault currentGroup:(Group *)currentGroup {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    GroupSetupViewController * vc = [storyboard   instantiateViewControllerWithIdentifier:@"GroupSetupViewController"];
    vc.isDefaultSetting = isDefault;
    if (currentGroup) {
        vc.currentGroup = currentGroup;
    }
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return allGroups.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DevicesTableViewCell * groupsCell = [tableView dequeueReusableCellWithIdentifier:@"DevicesCell"];
    groupsCell.delegate = self;
    Group * group = [allGroups objectAtIndex:indexPath.row];
    groupsCell.mDeviceNameLb.text = group.name;
    UILongPressGestureRecognizer * longPressGest = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(orderlongPressGesture:)];
    [groupsCell.mOrderBtn addGestureRecognizer:longPressGest];
    return groupsCell;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self goToAddGroupPage:NO currentGroup:[allGroups objectAtIndex:indexPath.row]];
}

#pragma mark: DeviesCell Delegate
-(void)handlerDelete:(UIButton *)clickedBtn {
     [self popupAlert:NSLocalizedString(@"delete_this_group", nil)
                  frame:CGRectMake(0, -44, 230, 170) ok:NSLocalizedString(@"yes", nil)
               cencel:NSLocalizedString(@"no", nil) delegate:self];
    self.navigationController.topViewController.title = NSLocalizedString(@"delete_group", nil);

    CGPoint buttonPosition = [clickedBtn convertPoint:CGPointZero toView:self.mGroupsTbV];
    _clickedIndexPath = [self.mGroupsTbV indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"clickedIndexPath  = %i\n", _clickedIndexPath.row);
}

-(void)handlerOrder:(UIButton *)clickedBtn {
  CGPoint buttonPosition = [clickedBtn convertPoint:CGPointZero toView:self.mGroupsTbV];
     _clickedIndexPath = [self.mGroupsTbV indexPathForRowAtPoint:buttonPosition];
    NSString * ids = [NSString stringWithFormat:@"%li|",(long)[[allGroups objectAtIndex:_clickedIndexPath.row] GrId]];
    for (Group * group in allGroups) {
        if (![group isEqual:[allGroups objectAtIndex:_clickedIndexPath.row]]) {
            ids = [ids stringByAppendingString:[NSString stringWithFormat:@"%ld|",(long)group.GrId]];
        }
    }
    NSDictionary * param = @{@"ids": ids};
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostGroupOrder andParameters:param success:^(id _Nonnull response) {
           // NSLog(@"Group order response  = %i\n",response);
        if ([[response valueForKey:@"resutlt"] integerValue] == 0) {
            [self.mGroupsTbV moveRowAtIndexPath:self->_clickedIndexPath toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }

    } failure:^(NSError * _Nonnull erroe) {
           // NSLog(@"Group order response error  = %i\n",erroe.localizedDescription);
        [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];

    }];
    
}

#pragma mark: AlertView Delegate
-(void)handlerYes {
    [self deleteGroup:[allGroups objectAtIndex:_clickedIndexPath.row]];
}
#pragma mark: CustomAlertDelegate
-(void)handelOk {

}


- (IBAction)orderlongPressGesture:(UIGestureRecognizer *)sender {
    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
    UIGestureRecognizerState state = longPress.state;
    CGPoint location = [longPress locationInView:_mGroupsTbV];
    NSIndexPath *indexPath = [_mGroupsTbV indexPathForRowAtPoint:location];
    static UIView       *snapshot = nil;        ///< A snapshot of the row user is moving.
    static NSIndexPath  *sourceIndexPath = nil; ///< Initial index path, where gesture begins.
    switch (state) {
        case UIGestureRecognizerStateBegan: {
            if (indexPath) {
                sourceIndexPath = indexPath;
                UITableViewCell *cell = [_mGroupsTbV cellForRowAtIndexPath:indexPath];
                // Take a snapshot of the selected row using helper method.
                snapshot = [self customSnapshoFromView:cell];
                // Add the snapshot as subview, centered at cell's center...
                __block CGPoint center = cell.center;
                snapshot.center = center;
                snapshot.alpha = 0.0;
                [_mGroupsTbV addSubview:snapshot];
                [UIView animateWithDuration:0.25 animations:^{
                    // Offset for gesture location.
                    center.y = location.y;
                    snapshot.center = center;
                    snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                    snapshot.alpha = 0.98;
                    cell.alpha = 0.0;
                    cell.hidden = YES;
                }];
            }
            break;
        }
        case UIGestureRecognizerStateChanged: {
            CGPoint center = snapshot.center;
            center.y = location.y;
            snapshot.center = center;
            // Is destination valid and is it different from source?
            if (indexPath && ![indexPath isEqual:sourceIndexPath]) {
                // ... update data source.
                [allGroups exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                
                // ... move the rows.
                [_mGroupsTbV moveRowAtIndexPath:sourceIndexPath toIndexPath:indexPath];
                // ... and update source so it is in sync with UI changes.
                sourceIndexPath = indexPath;
            }
            break;
        }
        default: {
            // Clean up.
            UITableViewCell *cell = [_mGroupsTbV cellForRowAtIndexPath:sourceIndexPath];
            cell.alpha = 0.0;
            [UIView animateWithDuration:0.25 animations:^{
                snapshot.center = cell.center;
                snapshot.transform = CGAffineTransformIdentity;
                snapshot.alpha = 0.0;
                cell.alpha = 1.0;
            } completion:^(BOOL finished) {
                cell.hidden = NO;
                sourceIndexPath = nil;
                [snapshot removeFromSuperview];
                snapshot = nil;
            }];
            [self orderGroup];
            break;
        }
    }
}


/** @brief Returns a customized snapshot of a given view. */
- (UIView *)customSnapshoFromView:(UIView *)inputView {
    // Make an image from the input view.
    UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
    [inputView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    // Create an image view.
    UIView *snapshot = [[UIImageView alloc] initWithImage:image];
    snapshot.layer.masksToBounds = NO;
    snapshot.layer.cornerRadius = 0.0;
    snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
    snapshot.layer.shadowRadius = 5.0;
    snapshot.layer.shadowOpacity = 0.4;
    return snapshot;
}


@end
