//
//  GroupSetupViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/16/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "GroupSetupViewController.h"
#import "AddTimerDevicesTableViewCell.h"
#import "UITextField+Content.h"
#import "Devices.h"

@interface GroupSetupViewController ()<CustomAlertDelegate> {
    NSMutableArray * allDevices;
}
@property (weak, nonatomic) IBOutlet UITableView *mGroupTbV;
@property (weak, nonatomic) IBOutlet UITextField *mGroupNameTextFl;
@property (weak, nonatomic) IBOutlet UILabel *mSelectDevToAddLb;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityV;

@end

@implementation GroupSetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];   
}
-(void)viewWillAppear:(BOOL)animated {
    [self setBackground:self];
}

#pragma mark: SET
-(void)setupView {
    [self setCurrentGroup];
    [self setBarButton];
    [self.mGroupNameTextFl setBlueBorderStyle];
    [self.mGroupNameTextFl addEgdeInsetsOnTextFields];
    [self.mGroupNameTextFl setImageInRightView:[UIImage imageNamed:@"edit"]];
    self.navigationItem.titleView = [self setNavigationTitle:@"group_setup"];

    self.mGroupTbV.separatorColor = [UIColor clearColor];
    [self.mGroupNameTextFl setleftPadding];
    _mGroupNameTextFl.leftViewMode = UITextFieldViewModeAlways;
    UIView * paddingView=  [[UIView alloc]initWithFrame:CGRectMake(0, 0, 60, 30)];
    _mGroupNameTextFl.leftView = paddingView;
    if ([BaseViewController isDeviceLanguageRTL]) {
        _mSelectDevToAddLb.textAlignment = NSTextAlignmentCenter;
    }

}


-(void)setBarButton {
    UIButton * rightbtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
    [rightbtn setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    [rightbtn addTarget:self action:@selector(handlerCancel)
         forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithCustomView:rightbtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    UIButton * leftbtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
       [leftbtn setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
       [leftbtn addTarget:self action:@selector(handlerSave)
            forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftbtn];
       self.navigationItem.leftBarButtonItem = leftButton;
}

-(void)setCurrentGroup {
    [self getAllDevices];
    [self.mGroupNameTextFl setText:_currentGroup.name];
}
-(void)setDefaulCheckDeviceInfo {
    for (int i = 0; i < allDevices.count; i++) {
        [[allDevices objectAtIndex:i] setIsCheckDevice:NO];
    }
}

-(void)setAllDevises:(NSArray *)result {
    allDevices = [NSMutableArray array];
    for (NSDictionary * device in result) {
        if (![self isHiddenDevice:[device valueForKey:@"mac"]]) {
              if (![[device valueForKey:@"deviceType"] isEqual:@"PM"]) {
                      [allDevices addObject:[[[Devices alloc]init] getDeviceObj:device]];
                  }
        }
      
    }
    if (allDevices.count > 0) {
        if (_currentGroup) {
            [self getCurrentGroupDevices];
        } else {
            [self setDefaulCheckDeviceInfo];
            [_mGroupTbV reloadData];
            [_mActivityV stopAnimating];
        }
    } else {
        [_mActivityV stopAnimating];
    }
}

#pragma mark : Get
- (void)getAllDevices {
    [[RequestManager sheredMenage] getJsonDataWithUrl:AGetdevdevices andParameters:@{}
                                         isNeedCookie:NO success:^(id response) {
        
        NSNumber *code = [response objectForKey:AResultCode];
        if (code.intValue == 0) {
            NSArray *array = [response objectForKey:@"values"];
            [self setAllDevises:array];
        }
    } failure:^(NSError *error) {
        [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
    }];
}
-(void)getCurrentGroupDevices {
    NSString *formatUrl = [NSString stringWithFormat:@"%@/%@",AGetgroupdevices,@(_currentGroup.GrId)];
    [[RequestManager sheredMenage] getJsonDataWithUrl:formatUrl
                                        andParameters:@{}
                                         isNeedCookie:NO
                                              success:^(id  _Nonnull response) {
      //  NSLog(@"%@ response = %@\n", formatUrl, response);
        NSNumber *code = [response objectForKey:AResultCode];
               if (code.intValue == 0) {
                   NSArray *array = [response objectForKey:AAttachment];
                   [self getCurrentDevices:array];
                  }
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"%@ error = %@\n", formatUrl, error.localizedDescription);
        [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];

    }];
}

-(void)getCurrentDevices:(NSArray *)dataArr {
    for (NSDictionary *dict in dataArr) {
        NSString *mac = [dict objectForKey:@"mac"];
        for (Devices * currDevice  in allDevices) {
            if ([currDevice.mac caseInsensitiveCompare:mac] == NSOrderedSame ) {
                NSInteger index = [allDevices indexOfObject:currDevice];
                 if (index >= 0 && index <= allDevices.count-1 ) {
                     [[allDevices objectAtIndex:index] setIsCheckDevice:YES];
                 }
            }
        }
    }
    [self.mGroupTbV reloadData];
    [_mActivityV stopAnimating];
}
#pragma mark: Actions
-(void)handlerCancel {
    [self.navigationController.topViewController setTitle:NSLocalizedString(@"groups", nil)];
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)handlerSave {
    if ([self isValidFields]) {
         __block NSString *groupMac = @"";
              [allDevices enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                  Devices * dev = obj;
                  if (dev.isCheckDevice) {
                     // NSDictionary *dict = [self.allDataAry objectAtIndex:idx];
                      groupMac = [NSString stringWithFormat:@"%@%@|",groupMac,dev.mac];
                  }
              }];
        NSDictionary * param = @{@"id":@(_currentGroup.GrId), @"groupName": self.mGroupNameTextFl.text, @"macs": groupMac};
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostgroupdevice andParameters:param success:^(id _Nonnull resopnse) {
           // NSLog(@"Save group edit resopnse = %@\n", resopnse);
              NSString *result = [resopnse objectForKey:@"result"];
                      if (result.intValue == 0) {
                          [self.navigationController.topViewController setTitle:NSLocalizedString(@"groups", nil)];
                          [self.navigationController popViewControllerAnimated:YES];
                      } else if (result.intValue == -25) {
                          [self showAlert:NSLocalizedString(@"gr_name_exist", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
                      } else {
                          [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
                      }
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"Save group edit error = %@\n", error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
    }
}

-(BOOL)isValidFields {
    BOOL isCheck = NO;
    for (Devices * dev in allDevices) {
        if (dev.isCheckDevice) {
            isCheck = YES;
            break;
        }
    }
    if (!isCheck) {
        [self showAlert:NSLocalizedString(@"select_one", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        return NO;
    }
    
    if (IsStrEmpty(self.mGroupNameTextFl.text)) {
        [self showAlert:NSLocalizedString(@"eneter_gr_name", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        // [self.view makeToast:NSLocalizedString(,@"")];
        return NO;
    }
    return YES;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return allDevices.count;
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AddTimerDevicesTableViewCell * groupCell = [tableView dequeueReusableCellWithIdentifier:@"AddTimerDevicesCell"];
    Devices * device = [allDevices objectAtIndex:indexPath.row];
    groupCell.isCheck = device.isCheckDevice;
    groupCell.mTitleLb.text = device.name;
    if ([device.deviceType isEqual:@"SW"]) {
        [groupCell.mModeImgV setImage:[UIImage imageNamed:@"ic_grey_lamp"]];
    } else if ([device.model caseInsensitiveCompare:@"Sensor"] == NSOrderedSame) {
        [groupCell.mModeImgV setImage:[UIImage imageNamed:@"sys_sensor"]];
    } else {
        [groupCell.mModeImgV setImage:[UIImage imageNamed:@"light_cold"]];
    }
    return  groupCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Devices * currDevice = [allDevices objectAtIndex:indexPath.row];
    if (currDevice.isCheckDevice) {
        [[allDevices objectAtIndex:indexPath.row] setIsCheckDevice:NO];
        } else {
            [[allDevices objectAtIndex:indexPath.row] setIsCheckDevice:YES];
        }
    [tableView reloadData];
}

#pragma mark: UITextFiled Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


#pragma mark: CustomAlert Delegate
-(void)handelOk {
}
@end
