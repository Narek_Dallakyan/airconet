//
//  GroupsViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/16/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GroupsViewController : BaseViewController

@end

NS_ASSUME_NONNULL_END
