//
//  SignUpViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/23/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "SignUpViewController.h"
#import "RequestManager.h"
#import "SignUpView.h"
#import "Validator.h"
#import <CoreLocation/CoreLocation.h>


@interface SignUpViewController ()<CustomAlertDelegate, CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    NSString * region;
     NSString * country;
}
@property (strong, nonatomic) IBOutlet SignUpView *mSignupV;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityInd;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mScrollContenierHeight;

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
   // [self setScroll];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self setupLocation];
    [self setBackground:self];

}
-(void)viewDidAppear:(BOOL)animated  {
    [super viewDidAppear:YES];
    self.mScrollView.contentOffset = CGPointMake(0, 0);
    self.mScrollView.scrollEnabled = YES;
}
-(void)viewDidLayoutSubviews {
    self.mScrollContenierHeight.constant = self.mScrollView.frame.size.height;
    self.mScrollView.contentSize = CGSizeMake(self.view.frame.size.width,self.mSignupV.mLoginBtn.frame.origin.y + self.mSignupV.mLoginBtn.frame.size.height+20);
    
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void) setup {
    [self preferredStatusBarStyle];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)setupLocation {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];

}
-(BOOL) isValidFields {
    if (self.mSignupV.mEmailTxtFl.text.length == 0 || self.mSignupV.mPasswordTxtFl.text.length == 0 || self.mSignupV.mNameTxtFl.text.length == 0 || self.mSignupV.mPhoneTxtFl.text.length == 0 || self.mSignupV.mAddressTxtFl.text.length == 0) {
        [self showAlert:NSLocalizedString(@"fill_info_note", nil) ok:NSLocalizedString(@"ok", nil)  cencel:@"" delegate:self];
        return NO;
    } else if (![Validator isValidEmail:self.mSignupV.mEmailTxtFl.text]) {
        [self showAlert:NSLocalizedString(@"correct_email_note", nil)ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        return NO;
    } else if (self.mSignupV.mPasswordTxtFl.text.length < 7) {
        [self showAlert:NSLocalizedString(@"pass_valid", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        return NO;
    } else if (self.mSignupV.mPasswordTxtFl.text.length < 7) {
           [self showAlert:NSLocalizedString(@"pass_valid", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
           return NO;
       }
        //Account already exist in the system. Please
       // try another email
    return YES;
}

- (void)signUpClick {
   
    [self.mActivityInd startAnimating];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:self.mSignupV.mNameTxtFl.text forKey:@"username"];
    [dict setValue:self.mSignupV.mPasswordTxtFl.text forKey:@"password"];
    [dict setValue:self.mSignupV.mAddressTxtFl.text forKey:@"address"];
    [dict setValue:self.mSignupV.mPhoneTxtFl.text forKey:@"phone"];
    [dict setValue:self.mSignupV.mEmailTxtFl.text forKey:@"email"];
    [dict setValue:self.mSignupV.mNameTxtFl.text forKey:@"fullname"];
    [dict setValue:@"111" forKey:@"adminPassword"];
    [dict setValue:@"" forKey:@"token"];
    if (country.length > 0 && region.length > 0 ) {
        [dict setValue:country forKey:@"country"];
        [dict setValue:region forKey:@"countryState"];
    }
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostusersignup
                                         andParameters:dict
                                               success:^(id response) {
        if ([[response valueForKey:@"result"] isEqual:[NSNumber numberWithInt:0]]) {
            dispatch_async(dispatch_get_main_queue(),^{
                [self showAlert:NSLocalizedString(@"comfirm_email", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
            });
           
        } else if ([[response valueForKey:@"attachment"] isEqual:@"could not execute statement"] || [[response valueForKey:@"attachment"] isEqual:@"failed"] ) {
            [self showAlert:NSLocalizedString(@"account_exist", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }
        else {
            [self showAlert:NSLocalizedString(@"try_again_later", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }
       // NSLog(@"response dic = %@ \n", dic);
        dispatch_async(dispatch_get_main_queue(),^{
            [self.mActivityInd stopAnimating];
        });
    } failure:^(NSError *error) {
        [self showAlert:NSLocalizedString(@"system_err", nil)ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        
    }];
}


- (IBAction)signup:(UIButton *)sender {
    if ([self isValidFields]) {
        [self signUpClick];
    }
}
- (IBAction)login:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -- Keyboard Fubctions
- (void)keyboardWillShow:(NSNotification *)sender {
    CGFloat height = [[sender.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, height, 0);
    _mScrollView.contentInset = edgeInsets;
    _mScrollView.scrollIndicatorInsets = edgeInsets;
}

- (void)keyboardWillHide:(NSNotification *)sender {
    UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
    _mScrollView.contentInset = edgeInsets;
    _mScrollView.scrollIndicatorInsets = edgeInsets;
}

#pragma mark: Custom Alert view Delegate
-(void)handelOk {
}

-(void)handelOkWithMessage:(NSString *)message {
    [self presentViewControllerWithIdentifier:@"Login" controller:self];
}

#pragma mark: CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *currentLocation = locations.lastObject;
    if (currentLocation != nil)
    // stop updating location in order to save battery power
    [locationManager stopUpdatingLocation];
    // Reverse Geocoding
     [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:currentLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if (error == nil && [placemarks count] > 0) {
           CLPlacemark * placemark = [placemarks lastObject];
            if ([placemark.administrativeArea length] != 0) {
                NSString * reg = [placemark administrativeArea];
               reg = [reg stringByReplacingOccurrencesOfString:@"Region" withString:@""];
               reg = [reg stringByReplacingOccurrencesOfString:@" " withString:@""];
                self->region = reg;
                NSLog(@"region = %@\n",  [placemark administrativeArea]);
            }
            if ([placemark.country length] != 0) {
                self->country = [placemark country];
                NSLog(@"Country = %@\n", [placemark country]);
            }
        }
        [[NSUserDefaults standardUserDefaults] removeObjectForKey: @"AppleLanguages"];
        }];
    }
@end
