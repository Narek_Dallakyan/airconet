//
//  SignUpViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/23/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SignUpViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIScrollView *mScrollView;

@end

NS_ASSUME_NONNULL_END
