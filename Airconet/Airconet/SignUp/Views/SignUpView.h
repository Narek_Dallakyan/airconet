//
//  SignUpView.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/23/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SignUpView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *mUserImgV;
@property (weak, nonatomic) IBOutlet UITextField *mEmailTxtFl;
@property (weak, nonatomic) IBOutlet UITextField *mPasswordTxtFl;
@property (weak, nonatomic) IBOutlet UITextField *mNameTxtFl;
@property (weak, nonatomic) IBOutlet UITextField *mPhoneTxtFl;
@property (weak, nonatomic) IBOutlet UITextField *mAddressTxtFl;
@property (weak, nonatomic) IBOutlet UIButton *mSignupBtn;
@property (weak, nonatomic) IBOutlet UIButton *mLoginBtn;
@property (strong, nonatomic) IBOutlet UILabel *mSignupLb;

@end

NS_ASSUME_NONNULL_END
