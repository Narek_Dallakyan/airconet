//
//  SignUpView.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/23/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "SignUpView.h"
#import "APublicDefine.h"
#import "UITextField+Content.h"


@implementation SignUpView

-(void)awakeFromNib {
   // [self setLocalizedLang];
    [self.mEmailTxtFl setBlueBorderStyle];
    [self.mPasswordTxtFl setBlueBorderStyle];
    [self.mNameTxtFl setBlueBorderStyle];
    [self.mPhoneTxtFl setBlueBorderStyle];
    [self.mAddressTxtFl setBlueBorderStyle];

    [self.mEmailTxtFl setImageInLeftView:[UIImage imageNamed:@"email"]];
    [self.mPasswordTxtFl setImageInLeftView:[UIImage imageNamed:@"password"]];
    [self.mNameTxtFl setImageInLeftView:[UIImage imageNamed:@"user_name"]];
    [self.mPhoneTxtFl setImageInLeftView:[UIImage imageNamed:@"phone"]];
    [self.mAddressTxtFl setImageInLeftView:[UIImage imageNamed:@"address"]];
    
    [self.mEmailTxtFl setPlaceholderColor:PlaceholderColor text:self.mEmailTxtFl.placeholder];
    [self.mPasswordTxtFl setPlaceholderColor:PlaceholderColor text:self.mPasswordTxtFl.placeholder];
    [self.mNameTxtFl setPlaceholderColor:PlaceholderColor text:self.mNameTxtFl.placeholder];
    [self.mPhoneTxtFl setPlaceholderColor:PlaceholderColor text:self.mPhoneTxtFl.placeholder];
    [self.mAddressTxtFl setPlaceholderColor:PlaceholderColor text:self.mAddressTxtFl.placeholder];
    
    [self.mEmailTxtFl setRightPadding];
    [self.mNameTxtFl setRightPadding];
    [self.mPhoneTxtFl setRightPadding];
    [self.mAddressTxtFl setRightPadding];

    [self addButtonToRightViewTextField];
    [super awakeFromNib];
}

//-(void)setLocalizedLang {
//    self.mSignupLb.text = NSLocalizedString(@"signup", nil);
//    self.mEmailTxtFl.placeholder = NSLocalizedString(@"e_mail", nil);
//    self.mPasswordTxtFl.placeholder = NSLocalizedString(@"pass", nil);
//    self.mNameTxtFl.placeholder = NSLocalizedString(@"name", nil);
//    self.mPhoneTxtFl.placeholder = NSLocalizedString(@"phone", nil);
//    self.mAddressTxtFl.placeholder = NSLocalizedString(@"address", nil);
//    [self.mSignupBtn setTitle:NSLocalizedString(@"signup", nil) forState:UIControlStateNormal];
//    [self.mLoginBtn setTitle:NSLocalizedString(@"login", nil) forState:UIControlStateNormal];
//}

-(void)addButtonToRightViewTextField {
    UIButton * secureBtn = [[UIButton alloc] init];
       [secureBtn addTarget:self action:@selector(secureBtnAction:) forControlEvents:UIControlEventTouchUpInside];
       
       [self.mPasswordTxtFl setButtonInRightView:secureBtn buttonImg:[UIImage imageNamed: @"not_visible"]];
}

-(void)secureBtnAction:(UIButton *)btn {
    if ([btn.imageView.image isEqual:[UIImage imageNamed:@"not_visible"]]) {
        [btn setImage:[UIImage imageNamed:@"visible"] forState:UIControlStateNormal];
        [self.mPasswordTxtFl setSecureTextEntry:NO];
    } else {
        [btn setImage:[UIImage imageNamed:@"not_visible"] forState:UIControlStateNormal];
        [self.mPasswordTxtFl setSecureTextEntry:YES];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
     if (textField == self.mAddressTxtFl) {
        textField.returnKeyType = UIReturnKeyDone;
     } else {
         textField.returnKeyType = UIReturnKeyNext;
     }
}

-(void)textFieldDidChangeSelection:(UITextField *)textField {
    if (![textField isEqual:self.mPasswordTxtFl]) {
        if (textField.text.length == 0) {
            [textField setRightPadding];
        } else
            [textField deletRightPadding];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.mEmailTxtFl) {
        [self.mPasswordTxtFl becomeFirstResponder];
    } else if (textField == self.mPasswordTxtFl) {
        [self.mNameTxtFl becomeFirstResponder];
    } else if (textField == self.mNameTxtFl) {
        [self.mPhoneTxtFl becomeFirstResponder];
    } else  if (textField == self.mPhoneTxtFl) {
        [self.mAddressTxtFl becomeFirstResponder];
    } else {
        [self.mAddressTxtFl resignFirstResponder];
    }
    return YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
