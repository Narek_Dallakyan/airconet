//
//  AKeysDefine.h
//  iAirCon
//
//  Created by Elvis on 16/6/30.
//  Copyright © 2016年 Elvis. All rights reserved.
//

#ifndef AKeysDefine_h
#define AKeysDefine_h

static NSString* const AGetExistDevice    = @"dev/exist-devices";

static NSString* const AGetuserrequestRecoverPassword    = @"user/requestRecoverPassword";

static NSString* const AGetuserresetPassword       = @"user/resetPassword";

static NSString* const AGetuserssid       = @"user/ssid";

// register
static NSString* const APostusersignup                = @"user/signup";

// login

static NSString* const AGetCurrentUserId       = @"user/currentUserId";

static NSString* const APostjspringsecuritycheck        = @"j_spring_security_check";

static NSString* const AGetdevdevices                = @"dev/devices";

static NSString* const AGetdevdeviceStatuses         = @"dev/deviceStatuses";

static NSString* const AGetdevmac                = @"dev/mac";

static NSString* const AGetalert                 = @"alert/exist";

static NSString* const APostdevsaveDevice                = @"dev/saveDevice";

static NSString* const APostUpdateDevice                =@"dev/updateDevice";

static NSString* const AGetgroup                       = @"group";

static NSString* const AGetgroupdevices               = @"group/devices/"; // 分组下的所有设备

static NSString* const APostgroupdevice                = @"group/device"; // 保存分组

static NSString* const APostgroupremove                = @"group/remove"; // 删除分组

static NSString* const AGetdevremoteDevice               = @"dev/removeDevice";

static NSString* const AGetdevswitchHvacs              = @"dev/switchHvacs";

static NSString* const AGetdevswitchHvac             = @"dev/switchHvac";

static NSString* const AGetdevqueryHvac            = @"dev/queryHvac";

static NSString* const APostdevsetHvac                = @"dev/setHvac";

static NSString* const APostdeviceorder               = @"dev/order";
//add new vrf device
static NSString* const APostVrfAddress               = @"dev/vrfAddress";
//add new model device
static NSString* const APostDeviceModel               = @"dev/updateDeviceModel";
//add new timer or update timer
static NSString* const APostTimerUpdate            = @"timer/app/save-update";

static NSString* const APostTimerIsExist            = @"timer/app/isExist";

//AIR Quality
static NSString* const APostAirQuality                 = @"air-quality";

/* 分组下的操作 */
static NSString* const APostgroupswitchGroup               = @"group/switchGroup";

static NSString* const APostgroupsetGroupFan               = @"group/setGroupFan";

static NSString* const APostgroupsetGroupMode               = @"group/setGroupMode";

static NSString* const APostgroupsetGroupTgtTemp               = @"group/setGroupTgtTemp";

static NSString * const APostGroupOrder                      = @"group/order";

//get timer witch added in device
static NSString * const AGetTimer                     = @"timer/load/timers";//"@"/timer/timers";;

static NSString * const AGetIsThereAnyTimer                     = @"timer/isThereAnyTimer";
static NSString * const AGetAllTimer              =@"timer/isThereAnyTimer/all";
static NSString * const APostDeleteTimer                     = @"timer/delete";

static NSString * const APostEnabledOrDisabled                    =@"timer/enabledOrDisabled";

/* end */

static NSString* const APostusercheckAdminPassword               = @"user/checkAdminPassword";

static NSString* const AGetscheduletimeroff            = @"schedule/timeroff";

static NSString* const APostscheduletimeroffdelete            = @"schedule/timeroff/delete";

static NSString* const APostcallme                       = @"callme";

static NSString* const AGetschedulequerytimer            = @"schedule/querytimer";

static NSString* const APostschedulesavetimer            = @"schedule/savetimer";

static NSString* const APostscheduleremovetimer            = @"schedule/removetimer";

static NSString* const AGetoncetimeroff            = @"oncetimeroff";

static NSString* const APostoncetimeroffdisable            = @"oncetimeroff/disable";

static NSString* const AGetsetpoint            = @"setpoint";

static NSString* const AGetlogout                 = @"logout";

static NSString* const AGeth250reset                 = @"h250/reset";

static NSString* const AGeterrorCode                = @"errorCode";

static NSString* const AGetnotificationquery     =@"notification/query";



static NSString* const AGeterrorCodesure    =    @"errorCode/sure";

static NSString* const AGetnotificationcount    =    @"notification/count";

static NSString* const AGetnotificationqueryhours     =    @"notification/query/hours";

static NSString* const AGetscheduleisHasTimer     =    @"schedule/isHasTimer";

static NSString* const AGetdevallStatus     =    @"dev/allStatus";

static NSString* const AGetweathertemperature     =    @"weather/temperature";

static NSString* const AGetnotificationqueryturnoff     =    @"notification/query/turnoff";

static NSString* const AGetwifichangedisNeedNotify     =    @"wifichanged/isNeedNotify";

static NSString* const AGetwifichangeddeviceautoffsettings     =    @"wifichanged/deviceautoffsettings";

static NSString* const APostmobiletoken     =    @"mobile/token";

static NSString* const AGetnotificationturnoffdelete =  @"notification/turnoff/delete";

static NSString* const AGetnotificationerrorCodedelete = @"notification/errorCode/delete";

static NSString* const APostDeletAllError             =@"alert/deleteAll";

static NSString* const AGetnotificationturnoffconfirm = @"notification/turnoff/confirm";


//Device setup
static NSString* const AGetWorkingHour              = @"dev/working-hour";
static NSString* const AGetSetupPoint               =@"setpoint/dev";
static NSString* const APostResetWorkingHours       = @"level3-info/restart-device-hours";
static NSString* const APostSetupPoint              = @"setpoint/dev";
static NSString* const APostLink                    =@"pm/udp/link";
static NSString* const APostDeleteLink              =@"dev/deletePM/";
static NSString* const APostUpdateBasewatt          =@"dev/update-pm-watt/app";
static NSString* const APostRefreshBasewatt         =@"pm/udp/";
static NSString* const APostSetMaster               =@"pm/app/";
static NSString* const AGetMaster                   =@"pm/app/master";
static NSString* const AGetMasterLink               =@"dev/by/pm";
static NSString* const APostUpdatePowerMeterName    =@"pm/update/name";
static NSString* const AGetDevice                   =@"dev/mac/";
static NSString* const AGetRefreshBasewattSensor    =@"pm/watt";
static NSString* const APostRefreshBasewattSensor   =@"pm/update-pm-watt/app";
static NSString* const APostUpdateMotionSensor      =@"dev/updateMotionSensor";

//System setup
static NSString* const AGetHazardRoomTemp           =@"alertConfig/hazard-room-temperature";
static NSString* const APostHazardRoomTemp          =@"alertConfig/hazard-room-temperature";
static NSString* const AGetSystemWorkingHour        =@"user/workingHour";
static NSString* const APostSystemWorkingHour       =@"user/workingHour";
static NSString* const AGetCountry                  =@"country";
static NSString* const APostUpdateCountry           =@"system/update-country";
static NSString* const AGetBaseBill                 =@"electric-bill/baseBill/app";
static NSString* const APostDayBaseBill             =@"electric-bill/base-bill/update-day";
static NSString* const APostPriceBaseBill           =@"electric-bill/base-bill/update-price";
static NSString* const AGetMaxPower                 =@"user/peak-power/app";
static NSString* const APostMaxPower                =@"pm/change-peak-power/app";
static NSString* const APostRefreshMaxPower         =@"pm/udp/send-master-status-request";
static NSString* const AGetVoltage                  =@"user/sensor-pm-consumption";
static NSString* const APostVoltage                 =@"user/change-sensor-pm-consumption/app";


//Holiday
static NSString* const AGetHoliday                  =@"holiday/dates";
static NSString* const APostHolidayAddRemove        =@"holiday/addOrRemove/app";

//Alerts
static NSString* const AGetAlertAll                 =@"alert/all";
static NSString* const AGetTotalAlert               =@"alert/total-alerts/app";
static NSString* const AGetLastAlert                =@"alert/last/byDevice";
static NSString* const APostResetBadge              =@"user/restart-ios-badge-number";

//Online Offline
static NSString* const AGetIsOnline               =@"control/checkDevicesCurrentStatus/";

//Menu bar
static NSString* const AGetEfficiencyRateData       =@"level3-info/efficiency-rate-data-of-PM/app";
static NSString* const AGetAllOnOff      =@"dev/checkAllDevicesPower";
//graph in livingRoom
static NSString* const AGetTotalEnergyGraph         =@"level3-info/total-energy-for-single-device/app";
static NSString* const AGetGroupTotalEnergyGraph    =@"level3-info/total-energy-for-group-devices/app";
static NSString* const AGetNavigationDeviceMode     =@"level3-info/device-mode/app";
static NSString* const AGetGroupDeviceMode          =@"level3-info/device-mode/group/app";
static NSString* const AGetPmMaster                 =@"pm/app/master";
static NSString* const AGetPmInfo                   =@"pm/info";
static NSString* const AGetGroupPmInfo              =@"pm/info/group";


//Power meter
static NSString* const AGetPmElectric               =@"level3-info/power-meter-electric-parameters/app"; //Gauge hamar
static NSString* const AGetEnergyUsagePerDevice     =@"/level3-info/energy-usage-per-device";
static NSString* const AGetProportionEnergy         =@"level3-info/proportion-energy/and/efficiency-rate-data";
static NSString* const AGetTotalEnergy              =@"level3-info/total-energy/app";
static NSString* const AGetBaseBillGraph            =@"electric-bill/baseBill-graph/app";


//Motion Sensor
static NSString* const AGetMotionSensorConfig        =@"motion-sensor/setup-config";
static NSString* const APostMotionSensorConfig       =@"motion-sensor/setup-config";
static NSString* const AGetMotionSensorAway          =@"motion-sensor/away-config";
static NSString* const APostMotionSensorAway         =@"motion-sensor/away-config";
static NSString* const AGetMotionSensorArrive        =@"motion-sensor/arrive-config";
static NSString* const APostMotionSensorArrive       =@"motion-sensor/arrive-config";
static NSString* const AGetMotionSensorByDeviceMac   =@"motion-sensor/byDeviceMac";

#endif /* AKeysDefine_h */

/*
jox jan es urlneri prefix@ poxvel e "/level3-info" es urli poxaren@ es mekn e  "/electric-bill"
/level3-info/baseBill
/level3-info/baseBill-graph/app
/level3-info/baseBill/app
/level3-info/base-bill/update-day
/level3-info/base-bill/update-price
/level3-info/base-bill/updateAll
 */
