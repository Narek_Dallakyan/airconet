//
//  APublicDefine.h
//  iAirCon
//
//  Created by Elvis on 16/6/30.
//  Copyright © 2016年 Elvis. All rights reserved.
//

#ifndef APublicDefine_h
#define APublicDefine_h


#define CommonAlphaColor [UIColor colorWithRed:0 green:0 blue:0 alpha:0.75]
#define BaseColor [UIColor colorWithRed:51.0f/255.0f green:153.0f/255.0f blue:255.0f/255.0f alpha:1.0]
#define GrayBarColor [UIColor colorWithRed:38.0f/255.0f green:38.0f/255.0f blue:38.0f/255.0f alpha:1.0]
#define NavTitleColor [UIColor colorWithRed:27.0f/255.0f green:58.0f/255.0f blue:99.0f/255.0f alpha:1.0]
#define PlaceholderColor [UIColor colorWithRed:127.0f/255.0f green: 127.0f/255.0f blue:127.0f/255.0f alpha:1.0]
#define RedColor [UIColor colorWithRed:192.0f/255.0f green: 0/255.0f blue:0.0f/255.0f alpha:1.0]
#define GreenColor [UIColor colorWithRed:48.0f/255.0f green: 204/255.0f blue:114.0f/255.0f alpha:1.0]
#define GreenAlertColor [UIColor colorWithRed:74.0f/255.0f green: 111/255.0f blue:51.0f/255.0f alpha:1.0]

#define GrayTitleColor [UIColor colorWithRed:117.0f/255.0f green:117.0f/255.0f blue:117.0f/255.0f alpha:1.0]

#define ColdColor [UIColor colorWithRed:0.0f/255.0f green:112.0f/255.0f blue:192.0f/255.0f alpha:1.0]
#define FanColor [UIColor colorWithRed:56.0f/255.0f green:87.0f/255.0f blue:35.0f/255.0f alpha:1.0]
#define DryColor [UIColor colorWithRed:191.0f/255.0f green:144.0f/255.0f blue:0.0f/255.0f alpha:1.0]
#define HotColor [UIColor colorWithRed:192.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0]
#define AutoColor [UIColor colorWithRed:84.0f/255.0f green:64.0f/255.0f blue:113.0f/255.0f alpha:1.0]
#define SwitchColor [UIColor colorWithRed:255.0f/255.0f green:192.0f/255.0f blue:0.0f/255.0f alpha:1.0]
#define GrayDevColor [UIColor colorWithRed:89.0f/255.0f green:89.0f/255.0f blue:89.0f/255.0f alpha:1.0]
#define YellowLightDevColor [UIColor colorWithRed:253.0f/255.0f green:188.0f/255.0f blue:41.0f/255.0f alpha:1.0]


#define TemperatureList @[@"16°",@"17°",@"18°",@"19°",@"20°",@"21°",@"22°",@"23°",@"24°", @"25°",@"26°",@"27°", @"28°",@"29°", @"30°", @"31°", @"32°"]
#define FanModesList @[@"1",@"2",@"3",@"A"]



#define ApiPriceList @[@"kWh", @"$", @"Hour"]
#define ApiPriceHourList @[@"Hour"]
#define ApiPriceWithoutHList @[@"kWh", @"$"]



#define TimerList @[NSLocalizedString(@"online", nil), NSLocalizedString(@"day", nil), NSLocalizedString(@"week", nil), NSLocalizedString(@"month", nil)]
#define TimerWithoutOnlineList  @[ NSLocalizedString(@"day", nil), NSLocalizedString(@"week", nil), NSLocalizedString(@"month", nil), NSLocalizedString(@"year", nil)]
#define TimerForRequestList @[@"REALTIME", @"DAY", @"WEEK", @"MONTH", @"YEAR"]
#define TimerForRequestWithoutOnlineList @[@"DAY", @"WEEK", @"MONTH", @"YEAR"]


#define PriceList @[NSLocalizedString(@"kWh", nil), NSLocalizedString(@"$", nil), NSLocalizedString(@"hour", nil)]
#define PriceHourList @[NSLocalizedString(@"hour", nil)]

#define PriceWithoutHList @[NSLocalizedString(@"kWh", nil), NSLocalizedString(@"$", nil)]
#define PriceWithoutHRequestList @[@"KWH", @"COST"]
#define PriceHRequestList @[@"HOURS"]

#define PriceForRequestList @[@"KWH", @"COST", @"HOURS"]

#define VoltageList @[@"100V", @"105V", @"110V", @"115V", @"120V", @"125V", @"130V", @"200V", @"205V", @"210V", @"215V", @"220V", @"225V", @"230V", @"235V", @"240V"]



#define DeviceTypeList @[ NSLocalizedString(@"ac", nil), NSLocalizedString(@"ac_sensor", nil), NSLocalizedString(@"thermostat", nil), NSLocalizedString(@"switch", nil), NSLocalizedString(@"power_meter", nil)]
  //@[@"air conditioner", @"thermostat / VRF", @"sensor", @"switch", @"power meter"]
#define DevicesNameList @[@"Amana",@"Aux",@"Bosch",@"Carrier",@"Changhong",@"Chigo",@"Daikin",@"Electrolux",@"Friedrich",@"Frigidaire",@"Fujitsu",@"GE",@"Goodman",@"Gree",@"Haier",@"Hisense",@"Hitachi",@"Kingsfin",@"Klimaire",@"LG",@"Midea",@"Mitsubishi",@"Panasonic",@"Phoenix",@"Pioneer",@"Samsung",@"Sanyo",@"Senville",@"Sharp",@"Siemens",@"Skyworth",@"SuperAir",@"TCL",@"Toshiba",@"Whirlpool",@"Yair",@"York",@"Other"]
#define VrfAddressList @[@"01-04", @"05-08", @"09-12", @"13-16", @"17-20", @"21-24", @"25-28", @"29-32", @"33-36", @"37-40", @"41-44", @"45-48", @"49-52", @"53-56", @"57-60", @"61-64", @"65-68", @"69-72", @"73-76", @"77-80", @"81-84", @"85-88", @"89-92", @"93-96", @"97-100", @"101-104", @"105-108", @"109-112", @"113-116", @"117-120", @"121-124", @"125-128"]
#define TimeDropDownList @[@"01:00", @"02:00",@"03:00",@"04:00",@"05:00",@"06:00",@"07:00",@"08:00",@"09:00",@"10:00",@"11:00",@"12:00",@"13:00",@"14:00",@"15:00",@"16:00",@"17:00",@"18:00",@"19:00",@"20:00",@"21:00",@"22:00",@"23:00", @"24:00"]
#define TemperatureDropDownList @[@"16°C",@"17°C",@"18°C",@"19°C",@"20°C",@"21°C",@"22°C",@"23°C",@"24°C", @"25°C",@"26°C",@"27°C", @"28°C",@"29°C", @"30°C", @"31°C", @"32°C"]
#define HoursDropDownList @[NSLocalizedString(@"forever", nil), [NSString stringWithFormat:@"5%@",NSLocalizedString(@"hours", nil) ], [NSString stringWithFormat:@"10%@",NSLocalizedString(@"hours", nil) ], [NSString stringWithFormat:@"15%@",NSLocalizedString(@"hours", nil) ], [NSString stringWithFormat:@"20%@",NSLocalizedString(@"hours", nil) ], [NSString stringWithFormat:@"24%@",NSLocalizedString(@"hours", nil) ]]


#define ModeDropDownList @[NSLocalizedString(@"auto", nil), NSLocalizedString(@"cold", nil), NSLocalizedString(@"dry", nil), NSLocalizedString(@"hot", nil),   NSLocalizedString(@"fan", nil)]
#define FanDropDownList @[NSLocalizedString(@"fan1", nil), NSLocalizedString(@"fan2", nil), NSLocalizedString(@"fan3", nil), NSLocalizedString(@"fanA", nil)]
#define ElectricBillCycleList @[@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28"]
#define DaysList  @[@"01", @"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23", @"24", @"25", @"26", @"27", @"28", @"29", @"30",@"31"]



#define MonthNameList @[NSLocalizedString(@"jan", nil), NSLocalizedString(@"feb", nil), NSLocalizedString(@"mar", nil), NSLocalizedString(@"aprn", nil), NSLocalizedString(@"may", nil), NSLocalizedString(@"jun", nil), NSLocalizedString(@"jul", nil), NSLocalizedString(@"aug", nil), NSLocalizedString(@"sep", nil), NSLocalizedString(@"oct", nil), NSLocalizedString(@"nov", nil), NSLocalizedString(@"dic", nil)]

#define DevisesModeImgList @[[UIImage imageNamed:@"auto_A"], [UIImage imageNamed:@"cold"], [UIImage imageNamed:@"dry"], [UIImage imageNamed:@"hot"], [UIImage imageNamed:@"green_fan_up"]]
#define DevisesModeBigImgList @[[UIImage imageNamed:@"auto_big"], [UIImage imageNamed:@"cold_big"], [UIImage imageNamed:@"dry_big"], [UIImage imageNamed:@"hot_big"], [UIImage imageNamed:@"green_fan_up"]]

#define RGB(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define Image(imageName) [UIImage imageNamed:imageName]


#define AReturnCode   @"returnCode"
#define AResultCode   @"result"
#define AAttachment   @"attachment"
#define DeviceStatus @"deviceStatus"
#define RequestBytes @"requestBytes"


// 登录相关
#define AKeyCurrentUserName      @"AKeyCurrentUserName"
#define AKeyCurrentUserPwd       @"AKeyCurrentUserPwd"
#define AKeyLastLoginSuccess     @"AKeyLastLoginSuccess"
#define KeyRememberAdmin         @"KeyRememberAdmin"
#define KeyIsUnLockMenu          @"KeyIsUnLockMenu"
#define KeyIsAdminLock           @"KeyIsAdminLock"
#define KeyRememberAdminCheck    @"KeyRememberAdminChcek"



#define AKeyRemotedeviceToken @"AKeyRemotedeviceToken"
#define AKeyGetDeviceTokenNoti @"AKeyGetDeviceTokenNoti"

#define AKeyPersistentCookie @"AKeyPersistentCookie"
#define URLGetCurrentuserId @"currentUserId"

// 条形码扫描
#define AQRViewScanResult  @"AQRViewScanResult"

//Notification
#define NotificationDeviceSetup             @"DeviceSetupNotification"
#define NotificationLivingRoom             @"LivingRoomNotification"
#define NotificationAirQuality             @"AirQualityNotification"
#define NotificationReloadMenu             @"ReloadMenuNotification"
#define NotificationReloadMenuHoliday      @"ReloadMenuHolidayNotification"
#define NotificationAdminLock              @"AdminLockNotification"
#define NotificationBackgroundMode         @"BackgroundModeNotification"
#define NotificationPowerMeter             @"PowerMeterNotification"
#define NotificationGeneralSetup           @"GeneralSetupNotification"


/* 区分屏幕 */
#define IPHONE5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define IPHONE4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define IPHONE6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
#define IPHONE6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)

#define IsStrEmpty(_ref)    (((_ref) == nil) || ([(_ref) isEqual:[NSNull null]]) ||([(_ref)isEqualToString:@""]) || ([(_ref) isKindOfClass:[NSNull class]]))

#define HexRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define AIMAGE(_name)  [UIImage imageNamed:_name]

// 颜色
#define ACommonGreenClolor [UIColor colorWithRed:0.42 green:0.82 blue:0.35 alpha:1.00]
#define ACommonBlueClolor  [UIColor colorWithRed:0.20 green:0.74 blue:0.87 alpha:1.00]
#define ACommonCancelClolor  HexRGB(0x767a90)
#define ACommonOKClolor      HexRGB(0x148cb8)

#define SCREEN_Width    ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_Height   ([UIScreen mainScreen].bounds.size.height)

#endif /* APublicDefine_h */
