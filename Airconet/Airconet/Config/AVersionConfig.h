//
//  AVersionConfig.h
//  iAirCon
//
//  Created by Elvis on 16/6/30.
//  Copyright © 2016年 Elvis. All rights reserved.
//

#ifndef AVersionConfig_h
#define AVersionConfig_h


/*测试服务器地址*/

#define ServerIP               "3.137.73.173"//"18.220.84.216"//

#define ABaseURLString        @"https://activate-ac.com/"// @"https://www.activate-test.ml"//
#define WebInfoUrl             @"https://www.activate-ac.com"
#define ABaseWSURLString      @"https://activate-ac.com/v2"// @"https://www.activate-test.ml/v2"//

#define ABaseHostString        @"255.255.255.255" 
#define ABasePort              1025
#define ATimeOut               5000

#define AServerTCPPort         @"443"
#define AServerUDPPort         @"6343"

// 测试标识
#define ATESTFLAG              0


#endif /* AVersionConfig_h */
