//
//  DelayAwayCountdownCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "DelayAwayCountdownCell.h"
#import "VSDropdown.h"
#import "APublicDefine.h"
#import "ACommonTools.h"
#import "BaseViewController.h"

@interface DelayAwayCountdownCell () <VSDropdownDelegate > {
    VSDropdown *_dropdown;
    BOOL isDropDownOpen;
    NSArray * minutesDropDownList;
}
@end
@implementation DelayAwayCountdownCell

- (void)awakeFromNib {
    [super awakeFromNib];
    minutesDropDownList =  @[[NSString stringWithFormat:@"5%@", NSLocalizedString(@"minut", nil)], [NSString stringWithFormat:@"10%@", NSLocalizedString(@"minut", nil)], [NSString stringWithFormat:@"15%@", NSLocalizedString(@"minut", nil)], [NSString stringWithFormat:@"20%@", NSLocalizedString(@"minut", nil)], [NSString stringWithFormat:@"25%@", NSLocalizedString(@"minut", nil)], [NSString stringWithFormat:@"30%@", NSLocalizedString(@"minut", nil)], [NSString stringWithFormat:@"35%@", NSLocalizedString(@"minut", nil)], [NSString stringWithFormat:@"40%@", NSLocalizedString(@"minut", nil)], [NSString stringWithFormat:@"45%@", NSLocalizedString(@"minut", nil)], [NSString stringWithFormat:@"50%@", NSLocalizedString(@"minut", nil)], [NSString stringWithFormat:@"55%@", NSLocalizedString(@"minut", nil)], [NSString stringWithFormat:@"60%@", NSLocalizedString(@"minut", nil)]];
    if ([BaseViewController isDeviceLanguageRTL]) { //Arabic, Hebrew
        [self.mDelayAwayLb setTextAlignment:NSTextAlignmentRight];
        self.mDropDownBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    isDropDownOpen = NO;
    if ([ACommonTools getBoolValueForKey:@"CloseDropDown"]) {
           [_dropdown remove];
           [_dropdown.tableView removeFromSuperview];
       } else{
           [self dropDownSetup];
       }
}

-(void) dropDownSetup {
    if (_dropdown) {
          [_dropdown remove];
          [_dropdown.tableView removeFromSuperview];
      }
    _dropdown = [[VSDropdown alloc]initWithDelegate:self];
    [_dropdown setAdoptParentTheme:YES];
    [_dropdown setShouldSortItems:YES];
    _dropdown.allowMultipleSelection = NO;
    _dropdown.textColor = PlaceholderColor;
    _dropdown.separatorColor = PlaceholderColor;

    _dropdown.tableView.backgroundColor = self.dropDownColor;
    [self.mTriangleBtn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    [ACommonTools setObjectValue:@0 forKey:@"DropDownX"];
    [ACommonTools setObjectValue:@100 forKey:@"DropDownWidth"];
}
-(void)dropDownHandler:(UIButton *)triangle  dropDown:(UIButton *)dropDown{
    [ACommonTools setBoolValue:NO forKey:@"CloseDropDown"];
    [self dropDownSetup];
    if ([triangle.currentImage isEqual:[UIImage imageNamed:@"syst_drop_down_img"] ]) {
        [self showDropDownForButton:dropDown  adContents:minutesDropDownList];
        [triangle setImage:[UIImage imageNamed:@"syst_drop_up_img"] forState:UIControlStateNormal];
    } else {
        [triangle setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
        [_dropdown remove];
    }
}
-(void)showDropDownForButton:(UIButton *)sender adContents:(NSArray *)contents {
    [_dropdown setDrodownAnimation:rand()%2];
    [_dropdown setupDropdownForView:sender];
    [_dropdown setSeparatorColor:sender.titleLabel.textColor];
    [_dropdown reloadDropdownWithContents:contents andSelectedItems:@[[sender titleForState:UIControlStateNormal]]];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
   // NSLog(@"touchesMoved");
    [self closeDropDown];
}
-(void)closeDropDown {
    [self.mTriangleBtn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    [_dropdown remove];
}
- (IBAction)dropDown:(UIButton *)sender {
    [self dropDownHandler:self.mTriangleBtn dropDown:sender];

}

- (IBAction)triangle:(UIButton *)sender {
    [self dropDownHandler:sender dropDown:self.mDropDownBtn];
}

#pragma mark - VSDropdown Delegate methods.
- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected {
    NSString * min = @"";
    if (selected) {
        UIButton *btn = (UIButton *)dropDown.dropDownView;
        NSString *allSelectedItems = nil;
        allSelectedItems = [dropDown.selectedItems firstObject];
        [btn setTitle:allSelectedItems forState:UIControlStateNormal];
        min = allSelectedItems;
    }
    [self.mTriangleBtn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    min = [min stringByReplacingOccurrencesOfString:NSLocalizedString(@"minut", nil) withString:@""];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *value = [f numberFromString:min];
    [self.delegate handlerDelay:value];
}
@end
