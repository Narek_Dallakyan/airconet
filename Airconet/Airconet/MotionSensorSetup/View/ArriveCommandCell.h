//
//  ArriveCommandCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ArriveCommandDelegate <NSObject>
-(void)handlerMotionSensorArriveCommand:(NSString *)command;
-(void)handlerTemp:(NSNumber *)temp;
-(void)handlerMode:(NSNumber *)mode;
-(void)handlerFan:(NSNumber *)fan;
@end

@interface ArriveCommandCell : UITableViewCell
@property (strong, nonatomic) id<ArriveCommandDelegate> delegate;
@property(strong, nonatomic) UIColor * dropDownColor;


@property (weak, nonatomic) IBOutlet UILabel *mArriveCommLb;
@property (weak, nonatomic) IBOutlet UIButton *mCheckBox1Btn;
@property (weak, nonatomic) IBOutlet UIButton *mCheckBox2Btn;
@property (weak, nonatomic) IBOutlet UIButton *mCheckBox3Btn;
@property (weak, nonatomic) IBOutlet UILabel *mLastACSettLb;
@property (weak, nonatomic) IBOutlet UILabel *mNoChangeLb;
@property (weak, nonatomic) IBOutlet UILabel *mOnLb;
@property (weak, nonatomic) IBOutlet UILabel *mInfoLb;
@property (weak, nonatomic) IBOutlet UIButton *mTemperatureDropDownBtn;
@property (weak, nonatomic) IBOutlet UIButton *mModeDropDownBtn;
@property (weak, nonatomic) IBOutlet UIButton *mFanDropDownBtn;
@property (weak, nonatomic) IBOutlet UIButton *mTriangle1Btn;
@property (weak, nonatomic) IBOutlet UIButton *mTriangle2Btn;
@property (weak, nonatomic) IBOutlet UIButton *mTriangle3Btn;



- (IBAction)checkBox1:(UIButton *)sender;
- (IBAction)checkBox2:(UIButton *)sender;
- (IBAction)checkBox3:(UIButton *)sender;
- (IBAction)triangle1:(UIButton *)sender;
- (IBAction)triangle2:(UIButton *)sender;
- (IBAction)triangle3:(UIButton *)sender;
- (IBAction)temperatureDropDown:(UIButton *)sender;
- (IBAction)modeDropDown:(UIButton *)sender;
- (IBAction)fanDropDown:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
