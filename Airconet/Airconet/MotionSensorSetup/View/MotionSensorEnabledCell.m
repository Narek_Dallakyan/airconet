//
//  MotionSensorEnabledCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "MotionSensorEnabledCell.h"
#import "APublicDefine.h"
#import "BaseViewController.h"

@implementation MotionSensorEnabledCell

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([BaseViewController isDeviceLanguageRTL]) { //Arabic, Hebrew
        [self.mMotionSensorEnabledLb setTextAlignment:NSTextAlignmentRight];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
        if (self.enabled) {
            [self.mCheckBoxBtn setImage:Image(@"circle_check") forState:UIControlStateNormal];
        } else {
            [self.mCheckBoxBtn setImage:Image(@"uncheck") forState:UIControlStateNormal];
        }
}

- (IBAction)enable:(UIButton *)sender {
    if (self.enabled) {
        [sender setImage:Image(@"uncheck") forState:UIControlStateNormal];
    } else {
        [sender setImage:Image(@"circle_check") forState:UIControlStateNormal];
        
    }
    self.enabled = !self.enabled;
    [self.delegate handlerEnabled:self.enabled];
}
@end
