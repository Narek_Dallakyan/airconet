//
//  AwayCommandCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AwayCommandDelegate <NSObject>
-(void)handlerMotionSensorCommand:(NSString *)command;
-(void)handlerAwayFrom:(NSNumber *)temp;
-(void)handlerAwayTo:(NSNumber *)temp;
-(void)handlerAwayFor:(NSNumber *)time;


@end

@interface AwayCommandCell : UITableViewCell
@property (strong, nonatomic) id<AwayCommandDelegate> delegate;
@property(strong, nonatomic) UIColor * dropDownColor;

@property (weak, nonatomic) IBOutlet UILabel *mAwayCommandLb;
@property (weak, nonatomic) IBOutlet UIButton *mCheckBox1Btn;
@property (weak, nonatomic) IBOutlet UIButton *mCheckBox2Btn;
@property (weak, nonatomic) IBOutlet UIButton *mCheckBox3Btn;
@property (weak, nonatomic) IBOutlet UILabel *mOffAcSwLb;
@property (weak, nonatomic) IBOutlet UILabel *mNoChangeLb;
@property (weak, nonatomic) IBOutlet UILabel *mInfoLb;
@property (weak, nonatomic) IBOutlet UILabel *mConservationLb;
@property (weak, nonatomic) IBOutlet UIButton *mDropDown1Btn;
@property (weak, nonatomic) IBOutlet UIButton *mDropDown2Btn;
@property (weak, nonatomic) IBOutlet UIButton *mDropDown3Btn;
@property (weak, nonatomic) IBOutlet UIButton *mTriangle1Btn;
@property (weak, nonatomic) IBOutlet UIButton *mTriangle2Btn;
@property (weak, nonatomic) IBOutlet UIButton *mTriangle3Btn;
@property (weak, nonatomic) IBOutlet UILabel *mToLb;
@property (weak, nonatomic) IBOutlet UILabel *mFromLb;

- (IBAction)checkBox1:(UIButton *)sender;
- (IBAction)checkBox2:(UIButton *)sender;
- (IBAction)checkBox3:(UIButton *)sender;
- (IBAction)dropDown1:(UIButton *)sender;
- (IBAction)dropDown2:(UIButton *)sender;
- (IBAction)dropDown3:(UIButton *)sender;
- (IBAction)triangle1:(UIButton *)sender;
- (IBAction)triangle2:(UIButton *)sender;
- (IBAction)triangle3:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
