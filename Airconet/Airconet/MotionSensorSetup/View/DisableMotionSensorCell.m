//
//  DisableMotionSensorCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "DisableMotionSensorCell.h"
#import "VSDropdown.H"
#import "APublicDefine.h"
#import "ACommonTools.h"
#import "BaseViewController.h"


@interface DisableMotionSensorCell () <VSDropdownDelegate > {
    VSDropdown *_dropdown;
    UIButton * currTriangleBtn;
}
@end
@implementation DisableMotionSensorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([BaseViewController isDeviceLanguageRTL]) { //Arabic, Hebrew
        [self.mDisableMotionLb setTextAlignment:NSTextAlignmentRight];
        [self.mFirtsDropDownBtn.titleLabel setTextAlignment:NSTextAlignmentRight];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if ([ACommonTools getBoolValueForKey:@"CloseDropDown"]) {
        [_dropdown remove];
        [_dropdown.tableView removeFromSuperview];
    } else{
        [self dropDownSetup];
    }
    [self setupView];
}

-(void)setupView {
    if (self.disabled) {
        [self.mCheckBoxBtn setImage:Image(@"circle_check") forState:UIControlStateNormal];
    } else {
        [self.mCheckBoxBtn setImage:Image(@"uncheck") forState:UIControlStateNormal];
    }
    [self.mFirstReiangleBtn setImage:Image(@"syst_drop_down_img") forState:UIControlStateNormal];
    [self.mSecondTriangleBtn setImage:Image(@"syst_drop_down_img") forState:UIControlStateNormal];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    //NSLog(@"touchesMoved");
    [self closeDropDown];
}
-(void)closeDropDown {
    [self.mFirstReiangleBtn setImage:Image(@"syst_drop_down_img") forState:UIControlStateNormal];
    [self.mSecondTriangleBtn setImage:Image(@"syst_drop_down_img") forState:UIControlStateNormal];
    [_dropdown remove];
}

-(void) dropDownSetup {
    if (_dropdown) {
        [_dropdown remove];
        [_dropdown.tableView removeFromSuperview];
    }
    _dropdown = [[VSDropdown alloc]initWithDelegate:self];
    [_dropdown setAdoptParentTheme:YES];
    [_dropdown setShouldSortItems:YES];
    _dropdown.allowMultipleSelection = NO;
    _dropdown.textColor = PlaceholderColor;
    _dropdown.separatorColor = PlaceholderColor;
    _dropdown.tableView.backgroundColor = self.dropDownColor;
    [ACommonTools setObjectValue:@0 forKey:@"DropDownX"];
    [ACommonTools setObjectValue:@80 forKey:@"DropDownWidth"];
    [self.mFirstReiangleBtn setImage:Image(@"syst_drop_down_img") forState:UIControlStateNormal];
    [self.mSecondTriangleBtn setImage:Image(@"syst_drop_down_img") forState:UIControlStateNormal];
}
-(void)dropDownHandler:(UIButton *)triangle  dropDown:(UIButton *)dropDown {
    currTriangleBtn = triangle;
    [ACommonTools setBoolValue:NO forKey:@"CloseDropDown"];
    [self dropDownSetup];
    if ([triangle.currentImage isEqual:[UIImage imageNamed:@"syst_drop_down_img"]]) {
        [self showDropDownForButton:dropDown  adContents:TimeDropDownList];
        [triangle setImage:[UIImage imageNamed:@"syst_drop_up_img"] forState:UIControlStateNormal];
    } else {
        [triangle setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
        [_dropdown remove];
    }
}
-(void)showDropDownForButton:(UIButton *)sender adContents:(NSArray *)contents {
    [_dropdown setDrodownAnimation:rand()%2];
    [_dropdown setupDropdownForView:sender];
    [_dropdown setSeparatorColor:sender.titleLabel.textColor];
    [_dropdown reloadDropdownWithContents:contents andSelectedItems:@[[sender titleForState:UIControlStateNormal]]];

}

- (IBAction)check:(UIButton *)sender {
    if (self.disabled) {
        [sender setImage:Image(@"uncheck") forState:UIControlStateNormal];
    } else {
        [sender setImage:Image(@"circle_check") forState:UIControlStateNormal];
    }
    self.disabled = ! self.disabled;
    [self.delegate handlerDisable: self.disabled];

}

- (IBAction)firstDropDown:(UIButton *)sender {
    [self dropDownHandler:self.mFirstReiangleBtn dropDown:sender];
}

- (IBAction)firstTriangle:(UIButton *)sender {
    [self dropDownHandler:sender dropDown:self.mFirtsDropDownBtn];
}

- (IBAction)secondDropDown:(UIButton *)sender {
    [self dropDownHandler:self.mSecondTriangleBtn dropDown:sender];
}

- (IBAction)secondTriangle:(UIButton *)sender {
    [self dropDownHandler:sender dropDown:self.mSecondDropDownBtn];
}

#pragma mark - VSDropdown Delegate methods.
- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected {
    NSString *allSelectedItems = nil;
    if (selected) {
        UIButton *btn = (UIButton *)dropDown.dropDownView;
        allSelectedItems = [dropDown.selectedItems firstObject];
        [btn setTitle:allSelectedItems forState:UIControlStateNormal];
    }
    [currTriangleBtn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    if (currTriangleBtn.tag == 0) {
        [self.delegate handlerFrom:allSelectedItems];
    } else {
        [self.delegate handlerTo:allSelectedItems];
    }
}
@end
