//
//  ArriveCommandCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "ArriveCommandCell.h"
#import "VSDropdown.h"
#import "APublicDefine.h"
#import "ACommonTools.h"
#import "BaseViewController.h"

@interface ArriveCommandCell () <VSDropdownDelegate > {
    VSDropdown *_dropdown;
    UIButton * currTriangleBtn;
}
@end

@implementation ArriveCommandCell

- (void)awakeFromNib {
    [super awakeFromNib];
       if ([BaseViewController isDeviceLanguageRTL]) { //arabic
            [self.mArriveCommLb setTextAlignment:NSTextAlignmentRight];
            [self.mLastACSettLb setTextAlignment:NSTextAlignmentRight];
            [self.mNoChangeLb setTextAlignment:NSTextAlignmentRight];
            [self.mOnLb setTextAlignment:NSTextAlignmentRight];
        }
    }


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if ([ACommonTools getBoolValueForKey:@"CloseDropDown"]) {
        [self closeDropDown];
              [_dropdown.tableView removeFromSuperview];
          } else{
              [self dropDownSetup];
          }
    // Configure the view for the selected state
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
   // NSLog(@"touchesMoved");
    [self closeDropDown];
}
-(void) dropDownSetup {
    if (_dropdown) {
        [_dropdown remove];
        [_dropdown.tableView removeFromSuperview];

    }
    _dropdown = [[VSDropdown alloc]initWithDelegate:self];
    [_dropdown setAdoptParentTheme:YES];
    [_dropdown setShouldSortItems:YES];
    _dropdown.allowMultipleSelection = NO;
    _dropdown.textColor = PlaceholderColor;
    _dropdown.separatorColor = PlaceholderColor;
    _dropdown.tableView.backgroundColor = self.dropDownColor;
    [self.mTriangle1Btn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    [self.mTriangle2Btn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    [self.mTriangle3Btn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];

    [ACommonTools setObjectValue:@0 forKey:@"DropDownX"];
}
-(void)closeDropDown {
    [_dropdown remove];
    [self.mTriangle1Btn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    [self.mTriangle2Btn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    [self.mTriangle3Btn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];

}
-(void)dropDownHandler:(UIButton *)triangle  dropDown:(UIButton *)dropDown adContents:(NSArray *)contents {
    currTriangleBtn = triangle;
    [ACommonTools setBoolValue:NO forKey:@"CloseDropDown"];
    [self dropDownSetup];
    if ([triangle.currentImage isEqual:[UIImage imageNamed:@"syst_drop_down_img"] ]) {
        [self showDropDownForButton:dropDown  adContents:contents];
        [triangle setImage:[UIImage imageNamed:@"syst_drop_up_img"] forState:UIControlStateNormal];
    } else {
        [triangle setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
        [_dropdown remove];
    }
}
-(void)showDropDownForButton:(UIButton *)sender adContents:(NSArray *)contents {
    [_dropdown setDrodownAnimation:rand()%2];
    [_dropdown setupDropdownForView:sender];
    [_dropdown setSeparatorColor:sender.titleLabel.textColor];
    [_dropdown reloadDropdownWithContents:contents andSelectedItems:@[[sender titleForState:UIControlStateNormal]]];
}

-(void)handlerCheckBox:(UIButton*)other1Btn other2Btn:(UIButton*)other2Btn other1Lb:(UILabel*)other1Lb other2Lb:(UILabel*)other2Lb {
    [other1Btn setImage:[UIImage imageNamed:@"uncheck_radioBtn"] forState:UIControlStateNormal];
    [other2Btn setImage:[UIImage imageNamed:@"uncheck_radioBtn"] forState:UIControlStateNormal];
    [other1Lb setTextColor:PlaceholderColor];
    [other2Lb setTextColor:PlaceholderColor];
}

- (IBAction)checkBox3:(UIButton *)sender {
    [sender setImage:[UIImage imageNamed:@"check_radioBtn"] forState:UIControlStateNormal];
    [self.mOnLb setTextColor:ColdColor];
    [self handlerCheckBox:self.mCheckBox1Btn other2Btn:self.mCheckBox2Btn other1Lb:self.mNoChangeLb other2Lb:self.mLastACSettLb];
    [self.delegate handlerMotionSensorArriveCommand:@"FUNC_MODE"];
}

- (IBAction)triangle1:(UIButton *)sender {
    [ACommonTools setObjectValue:@80 forKey:@"DropDownWidth"];
       [self dropDownHandler:sender dropDown:self.mTemperatureDropDownBtn adContents:TemperatureDropDownList];
}

- (IBAction)triangle2:(UIButton *)sender {
    [ACommonTools setObjectValue:@100 forKey:@"DropDownWidth"];
       [self dropDownHandler:sender dropDown:self.mModeDropDownBtn adContents:ModeDropDownList];
}

- (IBAction)triangle3:(UIButton *)sender {
    [ACommonTools setObjectValue:@70 forKey:@"DropDownWidth"];
       [self dropDownHandler:sender dropDown:self.mFanDropDownBtn adContents:FanDropDownList];
}

- (IBAction)temperatureDropDown:(UIButton *)sender {
    [ACommonTools setObjectValue:@80 forKey:@"DropDownWidth"];
    [self dropDownHandler:self.mTriangle1Btn dropDown:sender adContents:TemperatureDropDownList];
}

- (IBAction)modeDropDown:(UIButton *)sender {
    [ACommonTools setObjectValue:@100 forKey:@"DropDownWidth"];
    [self dropDownHandler:self.mTriangle2Btn dropDown:sender adContents:ModeDropDownList];
}

- (IBAction)fanDropDown:(UIButton *)sender {
    [ACommonTools setObjectValue:@80 forKey:@"DropDownWidth"];
    [self dropDownHandler:self.mTriangle3Btn dropDown:sender adContents:FanDropDownList];
    
}
- (IBAction)checkBox2:(UIButton *)sender {
    [sender setImage:[UIImage imageNamed:@"check_radioBtn"] forState:UIControlStateNormal];
    [self.mNoChangeLb setTextColor:ColdColor];
    [self handlerCheckBox:self.mCheckBox1Btn other2Btn:self.mCheckBox3Btn other1Lb:self.mLastACSettLb other2Lb:self.mOnLb];
    [self.delegate handlerMotionSensorArriveCommand:@"NO_CHANGE"];
}
- (IBAction)checkBox1:(UIButton *)sender {
    [sender setImage:[UIImage imageNamed:@"check_radioBtn"] forState:UIControlStateNormal];
    [self.mLastACSettLb setTextColor:ColdColor];
    [self handlerCheckBox:self.mCheckBox2Btn other2Btn:self.mCheckBox3Btn other1Lb:self.mNoChangeLb other2Lb:self.mOnLb];
   [self.delegate handlerMotionSensorArriveCommand:@"ON"];
}

#pragma mark - VSDropdown Delegate methods.
- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected {
    NSString *allSelectedItems = nil;
    if (selected) {
        UIButton *btn = (UIButton *)dropDown.dropDownView;
        allSelectedItems = [dropDown.selectedItems firstObject];
        [btn setTitle:allSelectedItems forState:UIControlStateNormal];
    }
    [currTriangleBtn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    if (currTriangleBtn.tag == 0) {
        [self.delegate handlerTemp:[NSNumber numberWithInteger:[allSelectedItems integerValue]]];
    } else if (currTriangleBtn.tag == 1) {
        NSInteger i = [ModeDropDownList indexOfObject:allSelectedItems];
        NSInteger j = [BaseViewController getModeValue:(int)i];
        [self.delegate handlerMode:[NSNumber numberWithLong:j]];
    } else {
        [self.delegate handlerFan:[NSNumber numberWithInteger:[FanDropDownList indexOfObject:allSelectedItems]+1]];
    }
        
}
@end
