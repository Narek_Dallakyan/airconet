//
//  AwayCommandCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "AwayCommandCell.h"
#import "VSDropdown.h"
#import "APublicDefine.h"
#import "ACommonTools.h"
#import "BaseViewController.h"

@interface AwayCommandCell () <VSDropdownDelegate > {
    VSDropdown *_dropdown;
    UIButton * currTriangleBtn;

}
@end
@implementation AwayCommandCell

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([BaseViewController isDeviceLanguageRTL]) { //Arabic, Hebrew
        [self.mAwayCommandLb setTextAlignment:NSTextAlignmentRight];
        [self.mOffAcSwLb setTextAlignment:NSTextAlignmentRight];
        [self.mNoChangeLb setTextAlignment:NSTextAlignmentRight];
        [self.mConservationLb setTextAlignment:NSTextAlignmentRight];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if ([ACommonTools getBoolValueForKey:@"CloseDropDown"]) {
              [self closeDropDown];
              [_dropdown.tableView removeFromSuperview];
          } else{
              [self dropDownSetup];
          }
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
   // NSLog(@"touchesMoved");
    [self closeDropDown];
}
-(void)closeDropDown {
    [self.mTriangle1Btn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    [self.mTriangle2Btn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    [self.mTriangle3Btn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];

    [_dropdown remove];
}
-(void) dropDownSetup {
    if (_dropdown) {
         [_dropdown remove];
        [_dropdown.tableView removeFromSuperview];
    }
    _dropdown = [[VSDropdown alloc]initWithDelegate:self];
    [_dropdown setAdoptParentTheme:YES];
    [_dropdown setShouldSortItems:YES];
    _dropdown.allowMultipleSelection = NO;
    _dropdown.textColor = PlaceholderColor;
    _dropdown.separatorColor = PlaceholderColor;
    _dropdown.tableView.backgroundColor = self.dropDownColor;
    [self.mTriangle1Btn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    [self.mTriangle2Btn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    [self.mTriangle3Btn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];

    [ACommonTools setObjectValue:@0 forKey:@"DropDownX"];
}
-(void)dropDownHandler:(UIButton *)triangle  dropDown:(UIButton *)dropDown adContents:(NSArray *)contents {
    currTriangleBtn = triangle;
    [ACommonTools setBoolValue:NO forKey:@"CloseDropDown"];
    [self dropDownSetup];
    if ([triangle.currentImage isEqual:[UIImage imageNamed:@"syst_drop_down_img"] ]) {
        [self showDropDownForButton:dropDown  adContents:contents];
        [triangle setImage:[UIImage imageNamed:@"syst_drop_up_img"] forState:UIControlStateNormal];
    } else {
        [triangle setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
        [_dropdown remove];
    }
}
-(void)showDropDownForButton:(UIButton *)sender adContents:(NSArray *)contents {
    [_dropdown setDrodownAnimation:rand()%2];
    [_dropdown setupDropdownForView:sender];
    [_dropdown setSeparatorColor:sender.titleLabel.textColor];
    [_dropdown reloadDropdownWithContents:contents andSelectedItems:@[[sender titleForState:UIControlStateNormal]]];

}
-(void)handlerCheckBox:(UIButton*)other1Btn other2Btn:(UIButton*)other2Btn
              other1Lb:(UILabel*)other1Lb other2Lb:(UILabel*)other2Lb {
    [other1Btn setImage:[UIImage imageNamed:@"uncheck_radioBtn"] forState:UIControlStateNormal];
    [other2Btn setImage:[UIImage imageNamed:@"uncheck_radioBtn"] forState:UIControlStateNormal];
    [other1Lb setTextColor:PlaceholderColor];
    [other2Lb setTextColor:PlaceholderColor];
}

- (IBAction)checkBox1:(UIButton *)sender {
    [sender setImage:[UIImage imageNamed:@"check_radioBtn"] forState:UIControlStateNormal];
    [self.mOffAcSwLb setTextColor:ColdColor];
    [self handlerCheckBox:self.mCheckBox2Btn other2Btn:self.mCheckBox3Btn other1Lb:self.mNoChangeLb other2Lb:self.mConservationLb];
    [self.delegate handlerMotionSensorCommand:@"OFF"];
}

- (IBAction)checkBox2:(UIButton *)sender {
    [sender setImage:[UIImage imageNamed:@"check_radioBtn"] forState:UIControlStateNormal];
    [self.mNoChangeLb setTextColor:ColdColor];
    [self handlerCheckBox:self.mCheckBox1Btn other2Btn:self.mCheckBox3Btn other1Lb:self.mOffAcSwLb other2Lb:self.mConservationLb];
    [self.delegate handlerMotionSensorCommand:@"NO_CHANGE"];
}

- (IBAction)checkBox3:(UIButton *)sender {
    [sender setImage:[UIImage imageNamed:@"check_radioBtn"] forState:UIControlStateNormal];
    [self.mConservationLb setTextColor:ColdColor];
    [self handlerCheckBox:self.mCheckBox1Btn other2Btn:self.mCheckBox2Btn other1Lb:self.mNoChangeLb other2Lb:self.mOffAcSwLb];
    [self.delegate handlerMotionSensorCommand:@"FUNC_MODE"];
}

- (IBAction)dropDown1:(UIButton *)sender {
    [ACommonTools setObjectValue:@70 forKey:@"DropDownWidth"];
    [self dropDownHandler:self.mTriangle1Btn dropDown:sender adContents:TemperatureDropDownList];
}

- (IBAction)dropDown2:(UIButton *)sender {
    [ACommonTools setObjectValue:@70 forKey:@"DropDownWidth"];
    [self dropDownHandler:self.mTriangle2Btn dropDown:sender adContents:TemperatureDropDownList];
}

- (IBAction)dropDown3:(UIButton *)sender {
    [ACommonTools setObjectValue:@120 forKey:@"DropDownWidth"];
    [self dropDownHandler:self.mTriangle3Btn dropDown:sender adContents:HoursDropDownList];
}

- (IBAction)triangle1:(UIButton *)sender {
    [ACommonTools setObjectValue:@70 forKey:@"DropDownWidth"];
    [self dropDownHandler:sender dropDown:self.mDropDown1Btn adContents:TemperatureDropDownList];
}

- (IBAction)triangle2:(UIButton *)sender {
    [ACommonTools setObjectValue:@70 forKey:@"DropDownWidth"];
    [self dropDownHandler:sender dropDown:self.mDropDown2Btn adContents:TemperatureDropDownList];
}

- (IBAction)triangle3:(UIButton *)sender {
    [ACommonTools setObjectValue:@120 forKey:@"DropDownWidth"];
    [self dropDownHandler:sender dropDown:self.mDropDown3Btn adContents:HoursDropDownList];
}

#pragma mark - VSDropdown Delegate methods.
- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected {
    NSString *allSelectedItems = nil;
    if (selected) {
        UIButton *btn = (UIButton *)dropDown.dropDownView;
        allSelectedItems = [dropDown.selectedItems firstObject];
        [btn setTitle:allSelectedItems forState:UIControlStateNormal];
    }
    [currTriangleBtn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    if ([allSelectedItems isEqualToString:NSLocalizedString(@"forever", nil)]) {
        allSelectedItems = 0;
    }
    if (currTriangleBtn.tag == 0) {
        [self.delegate handlerAwayFrom:[NSNumber numberWithInt:[allSelectedItems intValue]] ];
    } else if (currTriangleBtn.tag == 1) {
        [self.delegate handlerAwayTo:[NSNumber numberWithInt:[allSelectedItems intValue]] ];
    } else {
        [self.delegate handlerAwayFor:[NSNumber numberWithInt:[allSelectedItems intValue]] ];
    }
}
@end
