//
//  MotionSensorEnabledCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol MotionSensorEnabledDelegate <NSObject>
-(void)handlerEnabled:(BOOL)enabled;
@end

@interface MotionSensorEnabledCell : UITableViewCell
@property (strong, nonatomic) id<MotionSensorEnabledDelegate> delegate;
@property (assign, nonatomic) BOOL enabled;

@property (weak, nonatomic) IBOutlet UILabel *mMotionSensorEnabledLb;
@property (weak, nonatomic) IBOutlet UIButton *mCheckBoxBtn;
- (IBAction)enable:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
