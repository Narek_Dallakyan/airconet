//
//  DelayAwayCountdownCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol DelayAwayCountdownDelegate <NSObject>
-(void)handlerDelay:(NSNumber *)min;
@end
@interface DelayAwayCountdownCell : UITableViewCell
@property (strong, nonatomic) id<DelayAwayCountdownDelegate> delegate;
@property (strong, nonatomic) UIColor * dropDownColor;

@property (weak, nonatomic) IBOutlet UILabel *mDelayAwayLb;
@property (weak, nonatomic) IBOutlet UILabel *mInfoLb;
@property (weak, nonatomic) IBOutlet UIButton *mTriangleBtn;
@property (weak, nonatomic) IBOutlet UIButton *mDropDownBtn;
- (IBAction)dropDown:(UIButton *)sender;
- (IBAction)triangle:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
