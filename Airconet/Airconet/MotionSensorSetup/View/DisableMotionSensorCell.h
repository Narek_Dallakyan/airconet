//
//  DisableMotionSensorCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol DisableMotionSensorDelegate <NSObject>

-(void)handlerDisable:(BOOL)isEnable;
-(void)handlerFrom:(NSString *)timer;
-(void)handlerTo:(NSString *)timer;

@end


@interface DisableMotionSensorCell : UITableViewCell

@property (strong, nonatomic)id <DisableMotionSensorDelegate> delegate;
@property (strong, nonatomic) UIColor * dropDownColor;
@property (assign, nonatomic) BOOL disabled;
@property (weak, nonatomic) IBOutlet UILabel *mDisableMotionLb;
@property (weak, nonatomic) IBOutlet UIButton *mCheckBoxBtn;
@property (weak, nonatomic) IBOutlet UIButton *mFirtsDropDownBtn;
@property (weak, nonatomic) IBOutlet UIButton *mFirstReiangleBtn;
@property (weak, nonatomic) IBOutlet UILabel *mToLb;
@property (weak, nonatomic) IBOutlet UIButton *mSecondDropDownBtn;
@property (weak, nonatomic) IBOutlet UIButton *mSecondTriangleBtn;
@property (weak, nonatomic) IBOutlet UILabel *mInfoLb;



- (IBAction)check:(UIButton *)sender;
- (IBAction)firstDropDown:(UIButton *)sender;
- (IBAction)firstTriangle:(UIButton *)sender;
-(IBAction)secondDropDown:(UIButton *)sender;
-(IBAction)secondTriangle:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
