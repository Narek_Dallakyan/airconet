//
//  MotionSensorSettingsViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "MotionSensorSettingsViewController.h"
#import "MotionSensorEnabledCell.h"
#import "DisableMotionSensorCell.h"
#import "AwayCommandCell.h"
#import "ArriveCommandCell.h"
#import "DelayAwayCountdownCell.h"
#import "ACommonTools.h"
#import "Motionsensor.h"

@interface MotionSensorSettingsViewController () <MotionSensorEnabledDelegate, DelayAwayCountdownDelegate, DisableMotionSensorDelegate, ArriveCommandDelegate, AwayCommandDelegate> {
    Motionsensor * motionSensor;
    BOOL isUpdateArriveConfig;
    BOOL isUpdateAwayConfig;
    BOOL isUpdateSetupConfig;
    
    
}
@property (strong, nonatomic) IBOutlet UITableView *mMotionSensorSettingsTbv;

@end

@implementation MotionSensorSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    [ACommonTools setBoolValue:NO forKey:@"CloseDropDown"];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated {
    [self setBackground:self];
    
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [ACommonTools setBoolValue:YES forKey:@"CloseDropDown"];
    [self.mMotionSensorSettingsTbv reloadData];
}

#pragma mark: SET
-(void)setupView {
    [self setNavigationStyle];
    [self setBarButton];
    [self getSetupConfig];
}
-(void)setNavigationStyle {
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self setNavigationStyle:BaseColor tintColor:[UIColor whiteColor] titleText:NSLocalizedString(@"motion_sensor_setup", nil) titleColor:NavTitleColor];
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if ([view isKindOfClass:[DevicesNavigationBar class]]) {
            [view removeFromSuperview];
        }
    }
    self.navigationItem.titleView = [self setNavigationTitle:@"motion_sensor_setup"];
}

-(void)setBarButton {
    UIButton * rightbtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
    [rightbtn setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    [rightbtn addTarget:self action:@selector(handlerCancel)
       forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithCustomView:rightbtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    UIButton * leftbtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
    [leftbtn setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
    [leftbtn addTarget:self action:@selector(handlerSave)
      forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftbtn];
    self.navigationItem.leftBarButtonItem = leftButton;
}

-(void)handlerSave {
    [ACommonTools setBoolValue:YES forKey:@"CloseDropDown"];
    [self.mMotionSensorSettingsTbv reloadData];
    if (isUpdateSetupConfig) {
        [self updateSetupConfig];
    }
    if (isUpdateAwayConfig) {
        [self updateAwayConfig];
    }
    if (isUpdateArriveConfig) {
        [self updateArriveConfig];
    }
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
           [self.navigationController popViewControllerAnimated:YES];
    });
}

-(void)handlerCancel {
    [ACommonTools setBoolValue:YES forKey:@"CloseDropDown"];
    [self.mMotionSensorSettingsTbv reloadData];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark-- GET
-(void)getSetupConfig {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[RequestManager sheredMenage] getJsonDataWithUrl:AGetMotionSensorConfig
                                            andParameters:@{}
                                             isNeedCookie:NO
                                                  success:^(id  _Nonnull response) {
           // NSLog(@"AGetMotionSensorConfig response  = %@\n", response);
            self->motionSensor = [[[Motionsensor alloc] init] getMotionSensorObj:response];
            [self.mMotionSensorSettingsTbv reloadData];
            [self getAwayConfig];
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"AGetMotionSensorConfig response  error = %@\n", error.localizedDescription);
        }];
    });
}

-(void)getAwayConfig {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[RequestManager sheredMenage] getJsonDataWithUrl:AGetMotionSensorAway
                                            andParameters:@{}
                                             isNeedCookie:NO
                                                  success:^(id  _Nonnull response) {
            //NSLog(@"AGetMotionSensorAway response  = %@\n", response);
            self->motionSensor.awayConfig = [[AwayConfig alloc] init];
            self->motionSensor.awayConfig.roomTmpFrom = [response valueForKey:@"roomTmpFrom"];
            self->motionSensor.awayConfig.roomTmpTo = [response valueForKey:@"roomTmpTo"];
            self->motionSensor.awayConfig.lastingHour = [response valueForKey:@"lastingHour"];
            self->motionSensor.awayConfig.ayId = [response valueForKey:@"id"];
            self->motionSensor.awayConfig.motionSensorCommand = [response valueForKey:@"motionSensorCommand"];
            [self getArriveConfig];
            [self.mMotionSensorSettingsTbv reloadData];
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"AGetMotionSensorAway response  error = %@\n", error.localizedDescription);
        }];
    });
}


-(void)getArriveConfig {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[RequestManager sheredMenage] getJsonDataWithUrl:AGetMotionSensorArrive
                                            andParameters:@{}
                                             isNeedCookie:NO
                                                  success:^(id  _Nonnull response) {
           // NSLog(@"AGetMotionSensorArrive response  = %@\n", response);
            self->motionSensor.arriveConfig = [[ArriveConfig alloc] init];
            self->motionSensor.arriveConfig.arrId = [response valueForKey:@"id"];
            self->motionSensor.arriveConfig.tmp = [response valueForKey:@"tmp"];
            self->motionSensor.arriveConfig.mode = [response valueForKey:@"mode"];
            self->motionSensor.arriveConfig.fan = [response valueForKey:@"fan"];
            self->motionSensor.arriveConfig.motionSensorCommand = [response valueForKey:@"motionSensorCommand"];
            [self.mMotionSensorSettingsTbv reloadData];
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"AGetMotionSensorArrive response  error = %@\n", error.localizedDescription);
        }];
    });
}

#pragma mark -- Update
-(void)updateSetupConfig {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary * param = [Motionsensor getSetupConfigParam:self->motionSensor];
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostMotionSensorConfig andParameters:param
                                                   success:^(id _Nonnull response) {
            //NSLog(@"APostUpdateCountry response  = %@\n", response);
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"APostUpdateCountry response  = %@\n", error.localizedDescription);
            // [self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
        }];
    });
}

-(void)updateAwayConfig {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary * param = [Motionsensor getAwayConfigParam:self->motionSensor.awayConfig];
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostMotionSensorAway andParameters:param
                                                   success:^(id _Nonnull response) {
           // NSLog(@"APostMotionSensorAway response  = %@\n", response);
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"APostMotionSensorAway response  = %@\n", error.localizedDescription);
            // [self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
        }];
    });
}

-(void)updateArriveConfig {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary * param = [Motionsensor getArriveConfigParam:self->motionSensor.arriveConfig];
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostMotionSensorArrive andParameters:param
                                                   success:^(id _Nonnull response) {
            //NSLog(@"APostMotionSensorArrive response  = %@\n", response);
        } failure:^(NSError * _Nonnull error) {
            //NSLog(@"APostMotionSensorArrive response  = %@\n", error.localizedDescription);
        }];
    });
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.separatorColor = [UIColor clearColor];
    switch (indexPath.row) {
        case 0: {
            MotionSensorEnabledCell * motionSensorEnabledCell = [tableView dequeueReusableCellWithIdentifier:@"MotionSensorEnabledCell"];
            motionSensorEnabledCell.delegate = self;
            [self setMsEnabled:motionSensorEnabledCell];
            return motionSensorEnabledCell;
        }
        case 1: {
            DelayAwayCountdownCell * delayAwayCountdownCell = [tableView dequeueReusableCellWithIdentifier:@"DelayAwayCountdownCell"];
            delayAwayCountdownCell.delegate = self;
            [self setDelayAwayCountdown:delayAwayCountdownCell];
            return delayAwayCountdownCell;
        }
        case 2: {
            DisableMotionSensorCell * disableMotionSensorCell = [tableView dequeueReusableCellWithIdentifier:@"DisableMotionSensorCell"];
            disableMotionSensorCell.delegate = self;
            [self setDisableMotionSensor:disableMotionSensorCell];
            return disableMotionSensorCell;
        }
        case 3: {
            AwayCommandCell * awayCommandCell = [tableView dequeueReusableCellWithIdentifier:@"AwayCommandCell"];
            awayCommandCell.delegate = self;
            [self setAwayCommand:awayCommandCell];
            return awayCommandCell;
        }
        default: {
            ArriveCommandCell * arriveCommandCell = [tableView dequeueReusableCellWithIdentifier:@"ArriveCommandCell"];
            arriveCommandCell.delegate = self;
            [self setArriveCommand:arriveCommandCell];
            return arriveCommandCell;
        }
    }
    
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // [self goToAddDevicePage:NO];
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (![ACommonTools getBoolValueForKey:@"CloseDropDown"]) {
        [ACommonTools setBoolValue:YES forKey:@"CloseDropDown"];
        [self.mMotionSensorSettingsTbv reloadData];
    }
    
}

-(void)setMsEnabled:(MotionSensorEnabledCell *)msEnabledCell {
    msEnabledCell.enabled = motionSensor.enabled;
}

-(void)setDelayAwayCountdown:(DelayAwayCountdownCell*)delayAwayCountdownCell {
    [delayAwayCountdownCell.mDropDownBtn setTitle:[NSString  stringWithFormat:@"%@%@", [motionSensor.delayAwayCountdown stringValue], NSLocalizedString(@"minut", nil)] forState:UIControlStateNormal];
    delayAwayCountdownCell.dropDownColor = [BaseViewController setDropDownColor:self];
    [delayAwayCountdownCell.mTriangleBtn setImage:[UIImage imageNamed:@"syst_drop_down_img"] forState:UIControlStateNormal];
    
}

-(void)setDisableMotionSensor:(DisableMotionSensorCell *)disableMsCell {
    disableMsCell.dropDownColor = [BaseViewController setDropDownColor:self];
    [disableMsCell.mFirtsDropDownBtn setTitle:motionSensor.sleepTimeFrom forState:UIControlStateNormal];
    [disableMsCell.mSecondDropDownBtn setTitle:motionSensor.sleepTimeTo forState:UIControlStateNormal];
    disableMsCell.disabled = motionSensor.disabledSleepTime;
}

-(void)setAwayCommand:(AwayCommandCell *)awayCommandCell {
    awayCommandCell.dropDownColor = [BaseViewController setDropDownColor:self];
    if ([motionSensor.awayConfig.lastingHour integerValue] == 0) {
        [awayCommandCell.mDropDown3Btn setTitle:NSLocalizedString(@"forever", nil) forState:UIControlStateNormal];
    } else {
        [awayCommandCell.mDropDown3Btn setTitle:[NSString stringWithFormat:@"%li%@", (long)[motionSensor.awayConfig.lastingHour integerValue], NSLocalizedString(@"hours", nil)] forState:UIControlStateNormal];
    }
    [awayCommandCell.mDropDown1Btn setTitle:[NSString stringWithFormat:@"%li°C", (long)[motionSensor.awayConfig.roomTmpFrom integerValue]] forState:UIControlStateNormal];
    [awayCommandCell.mDropDown2Btn setTitle:[NSString stringWithFormat:@"%li°C", (long)[motionSensor.awayConfig.roomTmpTo integerValue]] forState:UIControlStateNormal];
    
    [awayCommandCell.mCheckBox1Btn setImage:Image(@"uncheck_radioBtn") forState:UIControlStateNormal];
    [awayCommandCell.mCheckBox2Btn setImage:Image(@"uncheck_radioBtn") forState:UIControlStateNormal];
    [awayCommandCell.mCheckBox3Btn setImage:Image(@"uncheck_radioBtn") forState:UIControlStateNormal];
    [awayCommandCell.mOffAcSwLb setTextColor:PlaceholderColor];
    [awayCommandCell.mNoChangeLb setTextColor:PlaceholderColor];
    [awayCommandCell.mConservationLb setTextColor:PlaceholderColor];
    if ([motionSensor.awayConfig.motionSensorCommand isEqualToString:@"OFF"]) {
        [awayCommandCell.mCheckBox1Btn setImage:Image(@"check_radioBtn") forState:UIControlStateNormal];
        [awayCommandCell.mCheckBox1Btn setTitleColor:ColdColor forState:UIControlStateNormal];
    } else if ([motionSensor.awayConfig.motionSensorCommand isEqualToString:@"NO_CHANGE"] ) {
        [awayCommandCell.mCheckBox2Btn setImage:Image(@"check_radioBtn") forState:UIControlStateNormal];
        [awayCommandCell.mNoChangeLb setTextColor:ColdColor];
    } else if ([motionSensor.awayConfig.motionSensorCommand isEqualToString:@"FUNC_MODE"] ) {
        [awayCommandCell.mCheckBox3Btn setImage:Image(@"check_radioBtn") forState:UIControlStateNormal];
        [awayCommandCell.mConservationLb setTextColor:ColdColor];
    }
}

-(void) setArriveCommand:(ArriveCommandCell *)arriveCommandCell {
    arriveCommandCell.dropDownColor = [BaseViewController setDropDownColor:self];
    [arriveCommandCell.mCheckBox1Btn setImage:Image(@"uncheck_radioBtn") forState:UIControlStateNormal];
    [arriveCommandCell.mCheckBox2Btn setImage:Image(@"uncheck_radioBtn") forState:UIControlStateNormal];
    [arriveCommandCell.mCheckBox3Btn setImage:Image(@"uncheck_radioBtn") forState:UIControlStateNormal];
    [arriveCommandCell.mLastACSettLb setTextColor:PlaceholderColor];
    [arriveCommandCell.mNoChangeLb setTextColor:PlaceholderColor];
    [arriveCommandCell.mOnLb setTextColor:PlaceholderColor];
    // OFF, ON, NO_CHANGE, FUNC_MODE
    //FUNC_MODE = conservation mode
    if ([motionSensor.arriveConfig.motionSensorCommand isEqualToString:@"ON"]) {
        [arriveCommandCell.mCheckBox1Btn setImage:Image(@"check_radioBtn") forState:UIControlStateNormal];
        [arriveCommandCell.mLastACSettLb setTextColor:ColdColor];
    } else if ([motionSensor.arriveConfig.motionSensorCommand isEqualToString:@"NO_CHANGE"] ) {
        [arriveCommandCell.mCheckBox2Btn setImage:Image(@"check_radioBtn") forState:UIControlStateNormal];
        [arriveCommandCell.mNoChangeLb setTextColor:ColdColor];
    } else if ([motionSensor.arriveConfig.motionSensorCommand isEqualToString:@"FUNC_MODE"] ) {
        [arriveCommandCell.mCheckBox3Btn setImage:Image(@"check_radioBtn") forState:UIControlStateNormal];
        [arriveCommandCell.mOnLb setTextColor:ColdColor];
    }
    NSString *preferredLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([motionSensor.arriveConfig.fan intValue] == 4) {
        if ([preferredLanguage containsString:@"ru"]) {
            [arriveCommandCell.mFanDropDownBtn setTitle:[NSString stringWithFormat:@"Fan%@", @"A"] forState:UIControlStateNormal];
        } else {
            [arriveCommandCell.mFanDropDownBtn setTitle:[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"fan", nil), @"A"] forState:UIControlStateNormal];
        }
    } else {
        if ([preferredLanguage containsString:@"ru"]) {
            [arriveCommandCell.mFanDropDownBtn setTitle:[NSString stringWithFormat:@"Fan%@", @"A"] forState:UIControlStateNormal];
        } else {
            [arriveCommandCell.mFanDropDownBtn setTitle:[NSString stringWithFormat:@"%@%i",NSLocalizedString(@"fan", nil), [motionSensor.arriveConfig.fan intValue]] forState:UIControlStateNormal];
        }
    }
    [arriveCommandCell.mModeDropDownBtn setTitle:[BaseViewController getModeName:motionSensor.arriveConfig.mode] forState:UIControlStateNormal];
     [arriveCommandCell.mTemperatureDropDownBtn setTitle:[NSString stringWithFormat:@"%li°C", (long)[motionSensor.arriveConfig.tmp integerValue]] forState:UIControlStateNormal];
}
#pragma mark-- MotionSensor Enabled Delegate
-(void)handlerEnabled:(BOOL)enabled {
    if (motionSensor.enabled != enabled) {
        motionSensor.enabled = enabled;
        isUpdateSetupConfig = YES;
    }
}

#pragma mark -- DelayAwayCountdown Delegate
-(void)handlerDelay:(NSNumber *)min {
    if (![motionSensor.delayAwayCountdown isEqual:min]) {
        motionSensor.delayAwayCountdown = min;
        isUpdateSetupConfig = YES;
    }
}

#pragma mark -- DisableMotionSensor Delegate {
-(void)handlerDisable:(BOOL)isEnable {
    if (motionSensor.disabledSleepTime != isEnable) {
        motionSensor.disabledSleepTime = isEnable;
        isUpdateSetupConfig = YES;
    }
}
-(void)handlerTo:(NSString *)timer {
    if (![motionSensor.sleepTimeTo isEqual:timer]) {
        motionSensor.sleepTimeTo = timer;
        isUpdateSetupConfig = YES;
    }
}

-(void)handlerFrom:(NSString *)timer {
    if (![motionSensor.sleepTimeFrom isEqual:timer]) {
           motionSensor.sleepTimeFrom = timer;
           isUpdateSetupConfig = YES;
       }
}

#pragma mark: AwayCommand Delegate
-(void)handlerMotionSensorCommand:(NSString *)command {
    if (![motionSensor.awayConfig.motionSensorCommand isEqual:command]) {
        motionSensor.awayConfig.motionSensorCommand = command;
        isUpdateAwayConfig = YES;
    }
}
-(void) handlerAwayFrom:(NSNumber *)temp {
    if (![motionSensor.awayConfig.roomTmpFrom isEqual:temp]) {
        motionSensor.awayConfig.roomTmpFrom = temp;
        isUpdateAwayConfig = YES;
    }
}

-(void)handlerAwayTo:(NSNumber *)temp {
    if (![motionSensor.awayConfig.roomTmpTo isEqual:temp]) {
        motionSensor.awayConfig.roomTmpTo = temp;
        isUpdateAwayConfig = YES;
    }
}
-(void)handlerAwayFor:(NSNumber *)time {
    if (![motionSensor.awayConfig.lastingHour isEqual:time]) {
        motionSensor.awayConfig.lastingHour = time;
        isUpdateAwayConfig = YES;
    }
}

#pragma mark: ArriveCommand Delegate
-(void)handlerMotionSensorArriveCommand:(NSString *)command {
    if (![motionSensor.arriveConfig.motionSensorCommand isEqual:command]) {
        motionSensor.arriveConfig.motionSensorCommand = command;
        isUpdateArriveConfig = YES;
    }
}
-(void)handlerTemp:(NSNumber *)temp {
    if (![motionSensor.arriveConfig.tmp isEqual:temp]) {
        motionSensor.arriveConfig.tmp = temp;
        isUpdateArriveConfig = YES;
    }
}
-(void)handlerMode:(NSNumber *)mode {
    if (![motionSensor.arriveConfig.mode isEqual:mode]) {
        motionSensor.arriveConfig.mode = mode;
        isUpdateArriveConfig = YES;
    }
}
-(void)handlerFan:(NSNumber *)fan {
    if (![motionSensor.arriveConfig.fan isEqual:fan]) {
        motionSensor.arriveConfig.fan = fan;
        isUpdateArriveConfig = YES;
    }
}
@end
