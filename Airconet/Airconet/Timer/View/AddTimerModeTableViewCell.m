//
//  AddTimerModeTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/13/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "AddTimerModeTableViewCell.h"
#import "BaseViewController.h"
#import "APublicDefine.h"

@implementation AddTimerModeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)temperature:(UIButton *)sender {
    [self.mTemperatureBtn setHidden:YES];
    [self.mTemperaturePickV setHidden:NO];
     if ([TemperatureList indexOfObject:self.mTemperatureBtn.titleLabel.text] >= 0 && [TemperatureList indexOfObject:self.mTemperatureBtn.titleLabel.text] <= TemperatureList.count-1) {
         [self.mTemperaturePickV selectRow:[TemperatureList indexOfObject:self.mTemperatureBtn.titleLabel.text] inComponent:0 animated:NO];
     }
    
    [self.mModeBtn setHidden:NO];
    [self.mModePickV setHidden:YES];
    [self.mFanPickV setHidden:YES];
    [self.mFanBtn setHidden:NO];
}

- (IBAction)fan:(UIButton *)sender {
    [self.mFanBtn setHidden:YES];
    [self.mFanPickV setHidden:NO];
    if ([FanModesList indexOfObject:self.mFanBtn.titleLabel.text] >= 0 && [FanModesList indexOfObject:self.mFanBtn.titleLabel.text] <= FanModesList.count-1 ) {
        [self.mFanPickV selectRow:[FanModesList indexOfObject:self.mFanBtn.titleLabel.text] inComponent:0 animated:NO];
    }
    
    [self.mTemperatureBtn setHidden:NO];
    [self.mTemperaturePickV setHidden:YES];
    [self.mModeBtn setHidden:NO];
    [self.mModePickV setHidden:YES];
}

- (IBAction)mode:(UIButton *)sender {
    [self.mModeBtn setHidden:YES];
    [self.mModePickV setHidden:NO];
    if ([DevisesModeImgList indexOfObject:self.mModeBtn.imageView.image] >= 0 && [DevisesModeImgList indexOfObject:self.mModeBtn.imageView.image] <= DevisesModeImgList.count-1 ) {
     [self.mModePickV selectRow:[DevisesModeImgList indexOfObject:self.mModeBtn.imageView.image] inComponent:0 animated:NO];
    }
    
    
    [self.mFanPickV setHidden:YES];
    [self.mFanBtn setHidden:NO];
    
    [self.mTemperatureBtn setHidden:NO];
    [self.mTemperaturePickV setHidden:YES];
}

#pragma mark: PickerView delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    [pickerView.subviews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
        subview.hidden = (CGRectGetHeight(subview.frame) < 1.0);
    }];
    return 1;
    
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if ([pickerView isEqual: self.mTemperaturePickV]) {
        return [TemperatureList count];
    } else if ([pickerView isEqual: self.mFanPickV]) {
        return [FanModesList count];
    }
    return [DevisesModeImgList count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if ([pickerView isEqual: self.mTemperaturePickV]) {
        return [TemperatureList objectAtIndex:row];
    } else if ([pickerView isEqual: self.mFanPickV]) {
        return [FanModesList objectAtIndex:row];
    }
    return @"";
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    NSInteger currentRow = [pickerView selectedRowInComponent:0];
    if ([pickerView isEqual: self.mTemperaturePickV]) {
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 35)];
        label.font = [UIFont fontWithName:@"Calibri" size:31.f];
        label.textColor = GrayTitleColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.text = [TemperatureList objectAtIndex:row];
        if (row == currentRow) {
            [label setTextColor: _mTemperatureBtn.titleLabel.textColor];
        }
        return label;
    } else if ([pickerView isEqual: self.mFanPickV]) {
        UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 35.f)];
        [btn setTitle:FanModesList[row] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"gray_fan"] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:@"Calibri" size:31.f];
        [btn setTitleColor:GrayDevColor forState:UIControlStateNormal];
        [pickerView addSubview:btn];
        if (row == currentRow) {
            [btn setTitleColor:self.mFanBtn.titleLabel.textColor forState:UIControlStateNormal];
            [btn setImage:self.mFanBtn.imageView.image forState:UIControlStateNormal];
        }
        return btn;
    } else {
        UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 35.f)];
        [btn setImage:[DevisesModeImgList objectAtIndex:row] forState:UIControlStateNormal];
        [pickerView addSubview:btn];
        if (row == currentRow) {
            if (row == 0) {
                [btn setImage:[UIImage imageNamed:@"auto_A"] forState:UIControlStateNormal];
            } else if (row == 2) {
                [btn setImage:[UIImage imageNamed:@"dry"] forState:UIControlStateNormal];
            }else {
                [btn setImage:self.mModeBtn.imageView.image forState:UIControlStateNormal];
            }
        }
        return btn;
    }
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return  35;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
   // NSLog(@"CALL didSelectRow!!!!!\n");
    
    if ([pickerView isEqual: self.mTemperaturePickV]) {
        UILabel *labelSelected = (UILabel*)[pickerView viewForRow:row forComponent:component];
        [labelSelected setTextColor: _mTemperatureBtn.titleLabel.textColor];
        [self.mTemperatureBtn setHidden:NO];
        [self.mTemperaturePickV setHidden:YES];
        [self.mTemperatureBtn setTitle:[TemperatureList objectAtIndex:row] forState:UIControlStateNormal];
        [self.delegate handlerTemperature:[[TemperatureList objectAtIndex:row] integerValue]];
        
    } else if ([pickerView isEqual: self.mFanPickV]) {
        UIButton *btnSelected = (UIButton*)[pickerView viewForRow:row forComponent:component];
        [btnSelected setTitleColor:self.mFanBtn.titleLabel.textColor forState:UIControlStateNormal];
        [btnSelected setImage:self.mFanBtn.imageView.image forState:UIControlStateNormal];
        [self.mFanBtn setHidden:NO];
        [self.mFanPickV setHidden:YES];
        NSString * fan = [FanModesList objectAtIndex:row];
        [self.mFanBtn setTitle:fan forState:UIControlStateNormal];
        
        NSInteger fanValue;
        if ([FanModesList[row] isEqualToString:@"A"]) {
            fanValue = 4;
        } else {
            fanValue = [FanModesList[row] integerValue];
        }
        [self.delegate handlerFan:fanValue];
        
    } else {
        UIButton *btnSelected = (UIButton*)[pickerView viewForRow:row forComponent:component];
        [btnSelected setImage:[DevisesModeImgList objectAtIndex:row] forState:UIControlStateNormal];
        [self.mModeBtn setHidden:NO];
        [self.mModePickV setHidden:YES];
        [self.mModeBtn setImage:[DevisesModeImgList objectAtIndex:row] forState:UIControlStateNormal];
        [self.delegate handlerMode:[BaseViewController getModeValue:(int)row]];
    }
}

- (void)selectRow:(NSInteger)row inComponent:(NSInteger)component animated:(BOOL)animated {
   // NSLog(@"CALL selectRow!!!!!\n");
}

@end
