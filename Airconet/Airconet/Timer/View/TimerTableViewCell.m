//
//  TimerTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/12/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "TimerTableViewCell.h"
#import "APublicDefine.h"

@implementation TimerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setupView];

    // Configure the view for the selected state
}

-(void)setupView{
    [self.mMondayBtn setTitle:NSLocalizedString(@"mon", nil) forState:UIControlStateNormal];
    [self.mTuesdayBtn setTitle:NSLocalizedString(@"tue", nil) forState:UIControlStateNormal];
    [self.mWednesdayBtn setTitle:NSLocalizedString(@"wed", nil) forState:UIControlStateNormal];
    [self.mThursdayBtnh setTitle:NSLocalizedString(@"thu", nil) forState:UIControlStateNormal];
    [self.mFridayBtn setTitle:NSLocalizedString(@"fri", nil) forState:UIControlStateNormal];
    [self.mSundayBtn setTitle:NSLocalizedString(@"sun", nil) forState:UIControlStateNormal];
    [self.mSaturdayBtn setTitle:NSLocalizedString(@"sat", nil) forState:UIControlStateNormal];
    [self setDefaultDaysColor];
    [self setDays];
    [self setOnOff];
}

-(void)setDefaultDaysColor {
    [_mMondayBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
     [_mTuesdayBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
    [_mWednesdayBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
     [_mThursdayBtnh setTitleColor:GrayTitleColor forState:UIControlStateNormal];
     [_mFridayBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
     [_mSundayBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
     [_mSaturdayBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
}
-(void)setDays {
    for (int i = 0; i < _daysArr.count; i++) {
        switch ([_daysArr[i]integerValue]) {
            case 1:
                [_mMondayBtn setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
            case 2:
                [_mTuesdayBtn setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
            case 4:
                [_mWednesdayBtn setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
            case 8:
                [_mThursdayBtnh setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
            case 16:
                [_mFridayBtn setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
            case 32:
                [_mSaturdayBtn setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
            default:
                [_mSundayBtn setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
        }
    }
    
}


-(void)setOnOff {
    if (_onOff == 86 || _onOff == 170) {
        [_mGraduseLb setHidden:YES];
        [_mFanLb setHidden:YES];
        [_mModeImg setHidden:YES];
        _mTimerLbTopLayout.constant = self.frame.size.height/2 - self.mTimeLb.frame.size.height/2;
    } else {
        [_mGraduseLb setHidden:NO];
        [_mFanLb setHidden:NO];
        [_mModeImg setHidden:NO];
        _mTimerLbTopLayout.constant = 10;
    }
}
#pragma mark:ACTIONS
- (IBAction)checkBox:(UIButton *)sender {
    if ([sender.imageView.image isEqual:[UIImage imageNamed:@"uncheck"]]) {
        [_mCheckBocBtn setImage:[UIImage imageNamed:@"circle_check"] forState:UIControlStateNormal];
        [self.delegate handlerCheckBox:sender isOn:@"true"];
    } else {
        [_mCheckBocBtn setImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
        [self.delegate handlerCheckBox:sender isOn:@"false"];

    }
}

- (IBAction)deleteCell:(UIButton *)sender {
    [self.delegate handlerDelete:sender];
}
- (IBAction)monday:(UIButton *)sender {
    if ([sender.titleLabel.textColor isEqual:GrayTitleColor]) {
        [sender setTitleColor:ColdColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:1 isTurnOn:YES];
    } else {
        [sender setTitleColor:GrayTitleColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:1 isTurnOn:NO];
        
    }
}

- (IBAction)tuesday:(UIButton *)sender {
   if ([sender.titleLabel.textColor isEqual:GrayTitleColor]) {
        [sender setTitleColor:ColdColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:2 isTurnOn:YES];
    } else {
        [sender setTitleColor:GrayTitleColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:2 isTurnOn:NO];
        
    }
}

- (IBAction)wednesday:(UIButton *)sender {
    if ([sender.titleLabel.textColor isEqual:GrayTitleColor]) {
        [sender setTitleColor:ColdColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:3 isTurnOn:YES];
    } else {
        [sender setTitleColor:GrayTitleColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:3 isTurnOn:NO];
        
    }
}

- (IBAction)thursday:(UIButton *)sender {
    if ([sender.titleLabel.textColor isEqual:GrayTitleColor]) {
        [sender setTitleColor:ColdColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:4 isTurnOn:YES];
    } else {
        [sender setTitleColor:GrayTitleColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:4 isTurnOn:NO];
        
    }
}

- (IBAction)friday:(UIButton *)sender {
    if ([sender.titleLabel.textColor isEqual:GrayTitleColor]) {
        [sender setTitleColor:ColdColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:5 isTurnOn:YES];
    } else {
        [sender setTitleColor:GrayTitleColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:5 isTurnOn:NO];
        
    }
}

- (IBAction)saturday:(UIButton *)sender {
   if ([sender.titleLabel.textColor isEqual:GrayTitleColor]) {
        [sender setTitleColor:ColdColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:6 isTurnOn:YES];
    } else {
        [sender setTitleColor:GrayTitleColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:6 isTurnOn:NO];
        
    }
}

- (IBAction)sunday:(UIButton *)sender {
    if ([sender.titleLabel.textColor isEqual:GrayTitleColor]) {
        [sender setTitleColor:ColdColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:7 isTurnOn:YES];
    } else {
        [sender setTitleColor:GrayTitleColor forState:UIControlStateNormal];
        [self.delegate handlerWeekDays:7 isTurnOn:NO];
        
    }
}
@end
