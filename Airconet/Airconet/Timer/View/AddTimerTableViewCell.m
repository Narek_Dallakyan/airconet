//
//  AddTimerTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/13/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "AddTimerTableViewCell.h"
#import "APublicDefine.h"

UIDatePicker * datePicker;
@implementation AddTimerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.mDatePickerV.layer.borderWidth = 0;
       [self.mDatePickerV setValue:BaseColor forKeyPath:@"textColor"];
       SEL selector = NSSelectorFromString( @"setHighlightsToday:" );
       NSInvocation *invocation = [NSInvocation invocationWithMethodSignature :
                                   [UIDatePicker
                                    instanceMethodSignatureForSelector:selector]];
       BOOL no = NO;
       [invocation setSelector:selector];
       [invocation setArgument:&no atIndex:2];
       [invocation invokeWithTarget:self.mDatePickerV];
       NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"da_DK"];
         [self.mDatePickerV setLocale:locale];
   }

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setupView];
}

-(void)setupView {
    [self.mMondayBtn setTitle:NSLocalizedString(@"mon", nil) forState:UIControlStateNormal];
    [self.mTuesdayBtn setTitle:NSLocalizedString(@"tue", nil) forState:UIControlStateNormal];
    [self.mWednesdayBtn setTitle:NSLocalizedString(@"wed", nil) forState:UIControlStateNormal];
    [self.mThursdayBtn setTitle:NSLocalizedString(@"thu", nil) forState:UIControlStateNormal];
    [self.mFridayBtn setTitle:NSLocalizedString(@"fri", nil) forState:UIControlStateNormal];
    [self.mSundayBtn setTitle:NSLocalizedString(@"sun", nil) forState:UIControlStateNormal];
    [self.mSaturdayBtn setTitle:NSLocalizedString(@"sat", nil) forState:UIControlStateNormal];
    [_mMondayBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
    [_mTuesdayBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
    [_mWednesdayBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
    [_mThursdayBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
    [_mFridayBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
    [_mSundayBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
    [_mSaturdayBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
    [self setDays];
    [self setOnOff];
}

-(void)setOnOff {
    if(_onOff == 86 || _onOff == 170) {
        [_mOnBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
        [_mOffBtn setTitleColor:BaseColor forState:UIControlStateNormal];
        
    }else {
        [_mOnBtn setTitleColor:BaseColor forState:UIControlStateNormal];
        [_mOffBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
    }
}

-(void)setDays {
    for (int i = 0; i < _daysArr.count; i++) {
        switch ([_daysArr[i]integerValue]) {
            case 1:
                [_mMondayBtn setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
            case 2:
                [_mTuesdayBtn setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
            case 4:
                [_mWednesdayBtn setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
            case 8:
                [_mThursdayBtn setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
            case 16:
                [_mFridayBtn setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
            case 32:
                [_mSaturdayBtn setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
            default:
                [_mSundayBtn setTitleColor:BaseColor forState:UIControlStateNormal];
                break;
        }
    }
}
- (IBAction)on:(UIButton *)sender {
    [self.delegate handlerOn:YES];
    [sender setTitleColor:BaseColor forState:UIControlStateNormal];
    [self.mOffBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
}

- (IBAction)off:(UIButton *)sender {
    [self.delegate handlerOn:NO];
    [sender setTitleColor:BaseColor forState:UIControlStateNormal];
    [self.mOnBtn setTitleColor:GrayTitleColor forState:UIControlStateNormal];
}

- (IBAction)weekDays:(UIButton *)sender {
    if ([sender.titleLabel.textColor isEqual:GrayTitleColor]) {
           [sender setTitleColor:BaseColor forState:UIControlStateNormal];
           [self.delegate handlerWeekDays:sender.tag isTurnOn:YES];
        
       } else {
           [sender setTitleColor:GrayTitleColor forState:UIControlStateNormal];
           [self.delegate handlerWeekDays:sender.tag isTurnOn:NO];
           
       }
}

- (IBAction)timerPicker:(UIDatePicker *)sender {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    dateFormat.dateStyle = kCFDateFormatterShortStyle;
    [dateFormat setDateFormat:@"HH:mm"];
    NSString *str = [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:sender.date]];
    [self.mTimerBtn setTitle:str forState:UIControlStateNormal];
    sender.hidden = YES;
    self.mTimerBtn.hidden = NO;
    [self.delegate handlerTimer:str];
}

- (IBAction)timer:(UIButton *)sender {
    NSString *dateString = self.mTimerBtn.titleLabel.text;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
     NSDate *date = [dateFormatter dateFromString:dateString];
    [self.mDatePickerV setDate:date];
      self.mDatePickerV.hidden = NO;
    self.mTimerBtn.hidden = YES;
}
-(void)pickerLabelTitle:(id)sender
{
    
}
@end
