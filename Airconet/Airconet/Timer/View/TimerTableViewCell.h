//
//  TimerTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/12/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//




#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol TimerTableViewCellDelegate <NSObject>

-(void)handlerDelete:(UIButton *)clickedBtn;
-(void)handlerCheckBox:(UIButton *)clickedBtn isOn:(NSString *)isOn;
-(void)handlerWeekDays:(int)day isTurnOn:(BOOL)isTurnOn;

@end

@interface TimerTableViewCell : UITableViewCell

@property(weak, nonatomic) id<TimerTableViewCellDelegate> delegate;
@property (weak, nonatomic) NSArray * daysArr;
@property (assign, nonatomic) NSInteger onOff;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mTimerLbTopLayout;

@property (weak, nonatomic) IBOutlet UIStackView *mDeviceStackV;

@property (weak, nonatomic) IBOutlet UIView *mBlackLineV;
@property (weak, nonatomic) IBOutlet UIButton *mCheckBocBtn;
@property (weak, nonatomic) IBOutlet UILabel *mTimeLb;
@property (weak, nonatomic) IBOutlet UILabel *mGraduseLb;
@property (weak, nonatomic) IBOutlet UILabel *mFanLb;
@property (weak, nonatomic) IBOutlet UIImageView *mModeImg;
@property (weak, nonatomic) IBOutlet UIButton *mDeleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *mMondayBtn;
@property (weak, nonatomic) IBOutlet UIButton *mTuesdayBtn;
@property (weak, nonatomic) IBOutlet UIButton *mWednesdayBtn;
@property (weak, nonatomic) IBOutlet UIButton *mThursdayBtnh;
@property (weak, nonatomic) IBOutlet UIButton *mFridayBtn;
@property (weak, nonatomic) IBOutlet UIButton *mSaturdayBtn;
@property (weak, nonatomic) IBOutlet UIButton *mSundayBtn;

- (IBAction)checkBox:(UIButton *)sender;
- (IBAction)deleteCell:(UIButton *)sender;
- (IBAction)monday:(UIButton *)sender;
- (IBAction)tuesday:(UIButton *)sender;
- (IBAction)wednesday:(UIButton *)sender;
- (IBAction)thursday:(UIButton *)sender;
- (IBAction)friday:(UIButton *)sender;
- (IBAction)saturday:(UIButton *)sender;
- (IBAction)sunday:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
