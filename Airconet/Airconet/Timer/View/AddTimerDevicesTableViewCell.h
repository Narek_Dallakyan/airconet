//
//  AddTimerDevicesTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/13/20.
//  Copyright © 2020Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
//@protocol AddTimerDevicesDelegate <NSObject>
//
//-(void)handlerHiddenDviece:()
//
//@end
@interface AddTimerDevicesTableViewCell : UITableViewCell
//@property (weak, nonatomic) id<AddTimerDevicesDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *mCheckImgV;
@property (weak, nonatomic) IBOutlet UIImageView *mModeImgV;
@property (weak, nonatomic) IBOutlet UILabel *mTitleLb;
@property (assign, nonatomic) BOOL isCheck;


@end

NS_ASSUME_NONNULL_END
