//
//  AddTimerDevicesTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/13/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "AddTimerDevicesTableViewCell.h"
#import "APublicDefine.h"

@implementation AddTimerDevicesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setupView];
    // Configure the view for the selected state
}
-(void)setupView {
    if (_isCheck) {
        [_mCheckImgV setImage:Image(@"light_check")];
    } else {
        [_mCheckImgV setImage:Image(@"light_uncheck")];
    }
}
@end
