//
//  AlertView.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/13/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AlertViewDelegate <NSObject>

-(void)handlerCancel:(BOOL)isCancel ;

@end
@interface AlertView : UIView
@property (weak, nonatomic) id<AlertViewDelegate> delegate;
@property(weak, nonatomic) IBOutlet UIImageView * mErrorImg;
@property(weak, nonatomic) IBOutlet UILabel * mAlertLb;
@property(weak, nonatomic) IBOutlet UILabel * mMessageLb;
@property(weak, nonatomic) IBOutlet UILabel * mPhoneConnectrdLb;
@property(weak, nonatomic) IBOutlet UILabel * mRouterWifiNameLb;

@property(weak, nonatomic) IBOutlet UIButton * mNoBtn;
@property(weak, nonatomic) IBOutlet UIButton * mYesBtn;
- (IBAction)no:(UIButton *)sender;
- (IBAction)yes:(UIButton *)sender;
@end

NS_ASSUME_NONNULL_END
