//
//  AlertView.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/13/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "AlertView.h"

@implementation AlertView

-(void)awakeFromNib {
    [super awakeFromNib];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)no:(UIButton *)sender {
    [self.delegate handlerCancel:YES];
}
-(IBAction)yes:(UIButton *)sender {
    [self.delegate handlerCancel:NO];

}
@end
