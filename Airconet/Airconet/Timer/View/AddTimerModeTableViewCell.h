//
//  AddTimerModeTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/13/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol AddTimerModeDelegate <NSObject>

-(void)handlerTemperature:(NSInteger)temp;
-(void)handlerFan:(NSInteger)fan;
-(void)handlerMode:(NSInteger)mode;


@end
@interface AddTimerModeTableViewCell : UITableViewCell

@property (weak, nonatomic) id<AddTimerModeDelegate> delegate;
@property (weak, nonatomic ) UIColor * modeColor;

@property (weak, nonatomic) IBOutlet UIView *mBlackLineV;
@property (weak, nonatomic) IBOutlet UIView *mSecondBlackLineV;
@property (weak, nonatomic) IBOutlet UIPickerView *mTemperaturePickV;
@property (weak, nonatomic) IBOutlet UIPickerView *mFanPickV;
@property (weak, nonatomic) IBOutlet UIPickerView *mModePickV;
@property (weak, nonatomic) IBOutlet UIButton *mTemperatureBtn;
@property (weak, nonatomic) IBOutlet UIButton *mFanBtn;
@property (weak, nonatomic) IBOutlet UIButton *mModeBtn;
- (IBAction)temperature:(UIButton *)sender;
- (IBAction)fan:(UIButton *)sender;
- (IBAction)mode:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
