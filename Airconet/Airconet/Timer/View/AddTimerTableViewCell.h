//
//  AddTimerTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/13/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol AddTimerCellDelegate <NSObject>

-(void)handlerOn:(BOOL)isOn;
-(void)handlerWeekDays:(NSInteger)day isTurnOn:(BOOL)isTurnOn;
-(void)handlerTimer:(NSString *)time;


@end
@interface AddTimerTableViewCell : UITableViewCell
@property (weak, nonatomic) id<AddTimerCellDelegate> delegate;
@property (weak, nonatomic) NSArray * daysArr;
@property (assign, nonatomic) NSInteger onOff;

@property (weak, nonatomic) IBOutlet UIButton *mOnBtn;
@property (weak, nonatomic) IBOutlet UIButton *mOffBtn;
@property (weak, nonatomic) IBOutlet UIView *mBlackLineV;
@property (weak, nonatomic) IBOutlet UIDatePicker *mDatePickerV;
@property (weak, nonatomic) IBOutlet UILabel *mDaysToRepeatLb;
@property (weak, nonatomic) IBOutlet UIButton *mMondayBtn;
@property (weak, nonatomic) IBOutlet UIButton *mTuesdayBtn;
@property (weak, nonatomic) IBOutlet UIButton *mWednesdayBtn;
@property (weak, nonatomic) IBOutlet UIButton *mThursdayBtn;
@property (weak, nonatomic) IBOutlet UIButton *mFridayBtn;
@property (weak, nonatomic) IBOutlet UIButton *mSaturdayBtn;
@property (weak, nonatomic) IBOutlet UIButton *mSundayBtn;
@property (weak, nonatomic) IBOutlet UIButton *mTimerBtn;
- (IBAction)on:(UIButton *)sender;
- (IBAction)off:(UIButton *)sender;
- (IBAction)weekDays:(UIButton *)sender;
- (IBAction)timerPicker:(UIDatePicker *)sender;
- (IBAction)timer:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
