//
//  TimerViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/28/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "TimerViewController.h"
#import "TimerTableViewCell.h"
#import "AddTimerViewController.h"
#import "PopupView.h"
#import "Timer.h"


@interface TimerViewController ()<TimerTableViewCellDelegate, PopupViewDelegate,CustomAlertDelegate> {
    NSMutableArray * allTimerDevices;
}

@property (weak, nonatomic) IBOutlet UITableView *mTimerTableV;
@property (weak, nonatomic) IBOutlet UILabel *mAddInfoLb;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityV;
@property (weak, nonatomic) NSIndexPath* clickedIndexPath;
@property (strong, nonatomic)UIButton * rightBarBtn;
@property (strong, nonatomic)UIButton * leftBarBtn;

@end

@implementation TimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void) viewWillAppear:(BOOL)animated {
    [self setBackground:self];
    [_mActivityV startAnimating];
   [self setNavigationStyle];
    [self setupView];
}

-(void)setupView {
    allTimerDevices = [NSMutableArray array];
    [self getTimer];
    [self setBarButton];
    self.mTimerTableV.separatorColor = [UIColor clearColor];
    [self setNavigationStyle];


}
-(void)setNavigationStyle {
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self setNavigationStyle:BaseColor tintColor:[UIColor whiteColor] titleText:NSLocalizedString(@"timer", nil) titleColor:NavTitleColor];
      for (UIView *view in self.navigationController.navigationBar.subviews) {
            if ([view isKindOfClass:[DevicesNavigationBar class]]) {
                [view removeFromSuperview];
            }
        }
}
-(void)setBarButton {
    UIImage* addImage = [UIImage imageNamed:@"plus"];
    _rightBarBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, addImage.size.width, addImage.size.height)];
    [_rightBarBtn setBackgroundImage:addImage forState:UIControlStateNormal];
    [_rightBarBtn addTarget:self action:@selector(addTimer)
           forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithCustomView:_rightBarBtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    UIImage* backImg = [UIImage imageNamed:@"ic_left"];
    if ([BaseViewController isDeviceLanguageRTL]) {
        backImg = [UIImage imageNamed:@"right_direction"];
    }
    _leftBarBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, backImg.size.width, backImg.size.height)];
    [_leftBarBtn setBackgroundImage:backImg forState:UIControlStateNormal];
    [_leftBarBtn addTarget:self action:@selector(back)
          forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:_leftBarBtn];
    self.navigationItem.leftBarButtonItem = leftButton;
}
-(void)setTimer:(NSArray *)arr {
    for (NSDictionary * dic in arr) {
        [allTimerDevices addObject:[[[Timer alloc]init] getTimerObj:dic]];
    }
    if (allTimerDevices.count == 0) {
        [self.mTimerTableV setHidden: YES];
    } else {
        [_mAddInfoLb setHidden:YES];
        [_mTimerTableV setHidden:NO];
    }
    [_mActivityV stopAnimating];
    [_mTimerTableV reloadData];
}

#pragma mark: GET
-(void)getTimer {
   // AGetTimer
    [[RequestManager sheredMenage] getJsonDataWithUrl:AGetTimer andParameters:@{} isNeedCookie:NO success:^(id  _Nonnull response) {
       // NSLog(@"Get Timer respoinse  = %@\n", response);
        [self setTimer:response];
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"Get Timer respoinse error  = %@\n", error.localizedDescription);
        [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];

    }];
}

-(void)showAlert {
    [self popupAlert:NSLocalizedString(@"delet_timer", nil)
               frame:CGRectMake(0, -44, 230, 170) ok:NSLocalizedString(@"yes", nil)
              cencel:NSLocalizedString(@"no", nil) delegate:self];
}

-(void)back {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)addTimer {
    [self goToAddTimerPage:nil];
}

-(void)goToAddTimerPage:(Timer *)timer {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddTimerViewController * vc = [storyboard   instantiateViewControllerWithIdentifier:@"AddTimerViewController"];
    vc.timer = timer;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return allTimerDevices.count;//timer count
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TimerTableViewCell * timerCell = [tableView dequeueReusableCellWithIdentifier:@"TimerCell"];
    timerCell.delegate = self;
    Timer * timer = [allTimerDevices objectAtIndex:indexPath.row];
    if (timer.enabled) {
        [timerCell.mCheckBocBtn setImage:Image(@"circle_check") forState:UIControlStateNormal ];
    } else {
        [timerCell.mCheckBocBtn setImage:Image(@"uncheck") forState:UIControlStateNormal ];
    }
    timerCell.mTimeLb.text = timer.startTime;
    timerCell.daysArr = timer.daysArr;
    timerCell.onOff = timer.onOff; 
    timerCell.mModeImg.image = timer.deviceVisualComp.modeImage;
    timerCell.mGraduseLb.text = [NSString stringWithFormat:@"%li°", timer.temperature];
    timerCell.mFanLb.text = [NSString stringWithFormat:@"F%li", timer.fan];
    return timerCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (allTimerDevices.count > 0) {
        [self goToAddTimerPage:[allTimerDevices objectAtIndex:indexPath.row]];
    }
}

#pragma mark: TimerCell Delegate
-(void)handlerDelete:(UIButton *)clickedBtn {
    CGPoint buttonPosition = [clickedBtn convertPoint:CGPointZero toView:self.mTimerTableV];
_clickedIndexPath = [self.mTimerTableV indexPathForRowAtPoint:buttonPosition];
    [self showAlert];
   // NSLog(@"clickedIndexPath  = %i\n",_clickedIndexPath.row);
}

-(void)handlerWeekDays:(int)day isTurnOn:(BOOL)isTurnOn {
    switch (day) {
        case 1:
            break;
        case 2:
            break;
        case 3:
            break;
        case 4:
            break;
        case 5:
            break;
        case 6:
            break;
        case 7:
            break;
        default:
            break;
    }
}
-(void)handlerCheckBox:(UIButton *)clickedBtn isOn:(NSString *)isOn {
      CGPoint buttonPosition = [clickedBtn convertPoint:CGPointZero toView:self.mTimerTableV];
    _clickedIndexPath = [self.mTimerTableV indexPathForRowAtPoint:buttonPosition];
    Timer * currTimer = [allTimerDevices objectAtIndex:_clickedIndexPath.row];
    NSString * onOff = [NSString stringWithFormat:@"%li", currTimer.onOff];
    if ( currTimer.onOff == 86 ||  currTimer.onOff == 170) {
        onOff = @"170";
    }
    NSDictionary * param = @{ @"onOff": onOff,
                            @"timerId": [NSString stringWithFormat:@"%li", currTimer.tId],
                            @"enabled": isOn };
        [[RequestManager sheredMenage] postJsonWithContentType:APostEnabledOrDisabled andParameters:param success:^(id _Nonnull response) {
           // NSLog(@"Timer APostEnabledOrDisabled response = %@\n",response);
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"Timer APostEnabledOrDisabled response error = %@\n",error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];

        }];
}
#pragma mark: Popup  Delegate
-(void)handlerYes {
    //NSLog(@"Popup  Delegate Popup  Delegate\n");
        Timer * currTimer = [allTimerDevices objectAtIndex:_clickedIndexPath.row];
    if (currTimer.onOff == 86) {
        currTimer.onOff = 170;
    }
    NSDictionary * param = @{@"timerId": @(currTimer.tId), @"onOff" : @(currTimer.onOff)};
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostDeleteTimer andParameters:param success:^(id _Nonnull response) {
       // NSLog(@"Timer delete response = %@\n",response);
        [self->allTimerDevices removeObjectAtIndex:self->_clickedIndexPath.row];
        [self.mTimerTableV beginUpdates];
        [self.mTimerTableV deleteRowsAtIndexPaths:@[self->_clickedIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.mTimerTableV endUpdates];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self->allTimerDevices.count == 0) {
                [self.mAddInfoLb setHidden:NO];
            }
        });
       
    } failure:^(NSError * _Nonnull error) {
               // NSLog(@"Timer delete response error = %@\n",error.localizedDescription);
        [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];

    }];
}
-(void)handlerCancel {
}
#pragma mark: CustomAlert Delegate
-(void)handlerOk {
}
//-(void)handlerCancel {
//}
@end
