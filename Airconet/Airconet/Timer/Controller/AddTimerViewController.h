//
//  AddTimerViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/13/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"
#import "Timer.h"


NS_ASSUME_NONNULL_BEGIN

@interface AddTimerViewController : BaseViewController
@property (strong, nonatomic) Timer * timer;
@property (assign, nonatomic) BOOL isDefaultSettings;
@end

NS_ASSUME_NONNULL_END
