//
//  AddTimerViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/13/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "AddTimerViewController.h"
#import "AddTimerTableViewCell.h"
#import "AddTimerModeTableViewCell.h"
#import "AddTimerDevicesTableViewCell.h"
#import "Devices.h"

@interface AddTimerViewController ()<AddTimerCellDelegate, AddTimerModeDelegate, CustomAlertDelegate, AddTimerModeDelegate> {
    NSMutableArray * allTimerDevices;
    NSMutableArray * addDevicesArr;
    NSInteger daysSum;
    NSInteger rowCount;
    NSInteger oldOnOffValue;
    BOOL isAdd;
}
@property (weak, nonatomic) IBOutlet UITableView *mTimerSettingsTbv;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityV;

@end

@implementation AddTimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // [self popupRemoteControllAlert:CGRectMake(0, -44, 280, 230) ok:NSLocalizedString(@"ok", nil) delegate:self];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [self setupView];
    [self setBackground:self];
}
#pragma mark: SETER
-(void)setupView {
    rowCount = 0;
    daysSum = 0;
    self.navigationItem.titleView = [self setNavigationTitle:@"add_imer"];
    self.mTimerSettingsTbv.separatorColor = [UIColor clearColor];
    addDevicesArr = [NSMutableArray array];
    [self setBarButton];
    [self getAllDevices];
    
  
}

-(void)setBarButton {
    UIButton * rightbtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
    [rightbtn setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    [rightbtn addTarget:self action:@selector(handlerCancel)
         forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithCustomView:rightbtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    UIButton * leftbtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
       [leftbtn setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
       [leftbtn addTarget:self action:@selector(handlerSave)
            forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftbtn];
       self.navigationItem.leftBarButtonItem = leftButton;
}

-(void)setAddDeviceArr {
    for (NSDictionary * dic in _timer.airConDevices) {
        for (Devices *dev in allTimerDevices) {
            if ([dev.mac isEqualToString:[dic valueForKey:@"mac"]]) {
                [addDevicesArr addObject:dev];
            }
        }
        
    }
}
-(void)setAirConDevice {
    [_timer.airConDevices removeAllObjects];
    for (Devices *dev in addDevicesArr) {
        NSDictionary * dic = [Devices getDeviceJson:dev];
        [_timer.airConDevices addObject:dic];
    }
}
-(void)setAllDevises:(NSArray *)result {
    allTimerDevices = [NSMutableArray array];
    for (NSDictionary * device in result) {
        //check if the feedback device is in the list of hidden devices or not
        if (![self isHiddenDevice:[device objectForKey:@"mac"]]) {
            if (![[device valueForKey:@"deviceType"] isEqual:@"PM"]) {
                [allTimerDevices addObject:[[[Devices alloc]init] getDeviceObj:device]];
            }
        }
    }
   if (!_timer) { //set default value
          [self setDefaultTimer];
   }
    [self countDays];
    rowCount = allTimerDevices.count + 3;
    [self setAddDeviceArr];
    [_mTimerSettingsTbv reloadData];
    [_mActivityV stopAnimating];
}
-(void)setDefaultTimer {
    NSDictionary * defaultDic = [[[Timer alloc] init] defaultTimer];
    _timer = [[[Timer alloc]init] getTimerObj:defaultDic] ;
    isAdd = YES;
}
-(void)setDefaultDevice {
    _timer.fan = 2;
    _timer.temperature = 25;
    _timer.mode = 17;
    _timer.deviceVisualComp.fanImage = [UIImage imageNamed:@"blue_fan"];
    _timer.deviceVisualComp.modeImage = [UIImage imageNamed:@"cold"];
    _timer.deviceVisualComp.modeColor = ColdColor;
}


#pragma mark: GET
- (void)getAllDevices {
    [[RequestManager sheredMenage] getJsonDataWithUrl:AGetdevdevices andParameters:@{}
                                         isNeedCookie:NO success:^(id response) {
        
        NSNumber *code = [response objectForKey:AResultCode];
        if (code.intValue == 0) {
            NSArray *array = [response objectForKey:@"values"];
            [self setAllDevises:array];
        }
    } failure:^(NSError *error) {
        [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
    }];
}

-(void)sendSave{
    if ([self isChangeOnOff]) {
        [self deleteTimer];
    } else {
        [self saveTimer];
    }
}
#pragma mark: Timer update
-(void)isExitTimer:(NSString *)mac success:(void (^)(id))success {
    NSTimeZone *defaultTimeZone = [NSTimeZone defaultTimeZone];
    NSDictionary * param;
    if (isAdd) {
        param = @{@"mac":mac, @"startTime":_timer.startTime, @"daysList":_timer.daysArr, @"onOff":@(_timer.onOff), @"timeZone":defaultTimeZone.name};
    }else {
        param = @{@"mac":mac, @"startTime":_timer.startTime, @"daysList":_timer.daysArr, @"onOff":@(_timer.onOff), @"timeZone":defaultTimeZone.name, @"timerId": @(_timer.tId)};
    }
    
    [[RequestManager sheredMenage] postJsonWithContentType:APostTimerIsExist andParameters:param success:^(id _Nonnull response) {
        //NSLog(@"IS Exist Timer  response = %@\n", response);
        success(response);
    } failure:^(NSError * _Nonnull error) {
        //NSLog(@"IS Exist Timer response  error = %@\n", error.localizedDescription);
        [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
    }];
}

-(void)saveTimer {
    if (isAdd ) {
        _timer.tId = 0;
    }else if (_timer.onOff == 86) {
        _timer.onOff = 170;
    }
    [self setAirConDevice];
    _timer.days = daysSum;
    NSDictionary * param = [Timer getDictionaryByObj:_timer];
    [[RequestManager sheredMenage] postJsonWithContentType:APostTimerUpdate andParameters:param success:^(id _Nonnull response) {
        //NSLog(@"Timer update response = %@\n", response);
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"Timer update response  error = %@\n", error.localizedDescription);
        [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
    }];
}

-(void)deleteTimer {
    NSDictionary * param = @{@"timerId": @(_timer.tId), @"onOff" : @(oldOnOffValue)};
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostDeleteTimer andParameters:param success:^(id _Nonnull response) {
        //NSLog(@"Timer delete response = %@\n",response);
        self->isAdd = YES;
        [self saveTimer];
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"Timer delete response error = %@\n",error.localizedDescription);
        [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
    }];
}
#pragma mark: Chack functions
-(BOOL)isChangeOnOff {
    if (isAdd) {
        return NO;
    }
    if ((oldOnOffValue == 55 || oldOnOffValue == 85) && (_timer.onOff == 55 || _timer.onOff == 85)) {
        return NO;
    } else if ((oldOnOffValue == 170 || oldOnOffValue == 86) && (_timer.onOff == 170 || _timer.onOff == 86)) {
        return NO;
    }
    
    return YES;
}

-(BOOL)isValidFields {
    if (addDevicesArr.count == 0) {
        [self showAlert:NSLocalizedString(@"select_dev_to_Timer", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        return NO;
    }
    if (daysSum == 0) {
        [self showAlert:NSLocalizedString(@"select_dey_to_repeat", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        return NO;
    }
    return YES;
}
-(BOOL)isEnableDevice:(Devices *) device {
    for (NSDictionary * dic in addDevicesArr) {
        if ([device.mac isEqualToString:[dic valueForKey:@"mac"]]) {
            NSInteger index = [allTimerDevices indexOfObject:device];
             if (index >= 0 && index <= allTimerDevices.count-1 ) {
                 [[allTimerDevices  objectAtIndex:index] setIsCheckDevice:YES];
             }
            return YES;
        }
    }
    return NO;
}

-(void)deselectDevice:(Devices *)device {
    for (Devices * dev in addDevicesArr) {
        if ([device.mac isEqualToString:dev.mac]) {
            [addDevicesArr removeObject:dev];
            return;
        }
    }
}
-(void)selectDevice:(Devices *)device {
    for (Devices * dev in addDevicesArr) {
        if ([device.mac isEqualToString:dev.mac]) {
            NSInteger index = [addDevicesArr indexOfObject:device];
            if (index >= 0 && index <= addDevicesArr.count-1 ) {
                [addDevicesArr replaceObjectAtIndex:index withObject:device];
            }
            return;
        }
    }
    [addDevicesArr addObject:device];
}

-(void)countDays {
    for (NSNumber *num in _timer.daysArr) {
      daysSum += [num intValue];
    }
    if (!_timer.daysArr) {
        _timer.daysArr = [NSMutableArray array];
    }
    if (_timer.onOff == 86) {
        oldOnOffValue = 170;
    } else {
        oldOnOffValue = _timer.onOff;
    }
}
#pragma mark: ACTIONS
-(void)handlerCancel {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)handlerSave {
    __block BOOL stopChecking = NO;
    __block NSInteger isExitCounter = 0;
    
    if ([self isValidFields]) {
        if(isAdd) {
        for (Devices * dev in addDevicesArr) {
            if (!stopChecking) {
                [self isExitTimer:dev.mac success:^(id response) {
                    NSInteger timeCount = [response integerValue];
                    if (timeCount == 0) {
                        isExitCounter++;
                    } else if (timeCount > 0) {
                        stopChecking = YES;
                        [self showAlert:NSLocalizedString(@"timer_exist", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
                    }
                    if (isExitCounter == self->addDevicesArr.count) {
                        [self sendSave];
                    }
                }];
            }
        }
        } else {
           [self sendSave];
        }
    }
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  rowCount;
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0: {
            AddTimerTableViewCell * timerCell = [tableView dequeueReusableCellWithIdentifier:@"AddTimerCell"];
            timerCell.delegate = self;
            timerCell.onOff = _timer.onOff;
            [timerCell.mTimerBtn setTitle:_timer.startTime forState:UIControlStateNormal];
            timerCell.daysArr = _timer.daysArr;
            [self setTime:timerCell];
            return  timerCell;
        }
        case 1: {
            AddTimerModeTableViewCell * timerModeCell = [tableView dequeueReusableCellWithIdentifier:@"AddTimerModeCell"];
            timerModeCell.delegate = self;
            if (_timer.onOff == 85 || _timer.onOff == 55) {
                [self hideDeviceCell:NO timerModeCell:timerModeCell];
                [timerModeCell.mTemperatureBtn setTitle:[NSString stringWithFormat:@"%li°",_timer.temperature] forState:UIControlStateNormal];
                [timerModeCell.mFanBtn setImage:_timer.deviceVisualComp.fanImage forState:UIControlStateNormal];
                [timerModeCell.mModeBtn setImage:_timer.deviceVisualComp.modeImage  forState:UIControlStateNormal];
                [timerModeCell.mTemperatureBtn setTitleColor:_timer.deviceVisualComp.modeColor forState:UIControlStateNormal];
                [timerModeCell.mFanBtn setTitleColor:_timer.deviceVisualComp.modeColor forState:UIControlStateNormal];
                NSInteger index = [TemperatureList indexOfObject:[NSString stringWithFormat:@"%li°",_timer.temperature]];
                if (index >= 0 && index <= TemperatureList.count-1 ) {
                    [timerModeCell.mTemperaturePickV selectRow:index inComponent:0 animated:YES];
                }
                if ([FanModesList indexOfObject:timerModeCell.mFanBtn.titleLabel.text] >= 0 && [FanModesList indexOfObject:timerModeCell.mFanBtn.titleLabel.text] <= FanModesList.count-1 ) {
                    [timerModeCell.mFanPickV selectRow:[FanModesList indexOfObject:timerModeCell.mFanBtn.titleLabel.text] inComponent:0 animated:YES];
                }
                [timerModeCell.mModePickV selectRow:_timer.deviceVisualComp.modeImageIndex inComponent:0 animated:YES];
                [timerModeCell.mTemperaturePickV reloadAllComponents];
                [timerModeCell.mFanPickV reloadAllComponents];
                [timerModeCell.mModePickV reloadAllComponents];

                if (_timer.fan == 4) {
                    [timerModeCell.mFanBtn setTitle:@"A" forState:UIControlStateNormal];
                } else {
                    [timerModeCell.mFanBtn setTitle:[NSString stringWithFormat:@"%li",(long)_timer.fan] forState:UIControlStateNormal];
                }
            } else {
                [self hideDeviceCell:YES timerModeCell:timerModeCell];
            }
            return  timerModeCell;
        }
        case 2: {
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
            cell.textLabel.lineBreakMode = YES;
            cell.textLabel.numberOfLines = 5;
            cell.textLabel.text = NSLocalizedString(@"select_dev", nil);
            cell.textLabel.textColor = GrayTitleColor;
            return  cell;
        }
        default: {
            AddTimerDevicesTableViewCell * timerDevicesCell = [tableView dequeueReusableCellWithIdentifier:@"AddTimerDevicesCell"];
            Devices * device = [allTimerDevices objectAtIndex:indexPath.row -3];
            
            [timerDevicesCell.mTitleLb setText:device.name];
            [timerDevicesCell setIsCheck:[self isEnableDevice:device]];
            if ([device.deviceType isEqualToString:@"SW"]) {
                [timerDevicesCell.mModeImgV setImage:[UIImage imageNamed:@"ic_grey_lamp"]];
            } else {
                [timerDevicesCell.mModeImgV setImage:[UIImage imageNamed:@"light_cold"]];
            }
            return  timerDevicesCell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            return  170;
        case 1:
            return  120;
        default:
            return  50;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > 2) {
        Devices * currDevice = [allTimerDevices objectAtIndex:indexPath.row -3];
        //AddTimerDevicesTableViewCell *cell = (AddTimerDevicesTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        if (currDevice.isCheckDevice) {
            [[allTimerDevices  objectAtIndex:indexPath.row -3] setIsCheckDevice:NO];
            [self deselectDevice:currDevice];
        } else {
            [[allTimerDevices  objectAtIndex:indexPath.row -3] setIsCheckDevice:YES];
            [self selectDevice:currDevice];
        }
        [tableView reloadData];
    }
}

-(void)hideDeviceCell:(BOOL)hide timerModeCell:(AddTimerModeTableViewCell *)timerModeCell {
    [timerModeCell.mTemperatureBtn setHidden:hide];
    [timerModeCell.mFanBtn setHidden:hide];
    [timerModeCell.mFanBtn setHidden:hide];
    [timerModeCell.mModeBtn setHidden:hide];
    [timerModeCell.mSecondBlackLineV setHidden:hide];
    [timerModeCell.mBlackLineV setHidden:hide];
}
-(void)setTime:(AddTimerTableViewCell *)addTimerCell {
    NSString *dateString = _timer.startTime;
       NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
       [dateFormatter setDateFormat:@"HH:mm"];
        NSDate *date = [dateFormatter dateFromString:dateString];
       [addTimerCell.mDatePickerV setDate:date];
}
#pragma mark: AddTimerCell Delegate
-(void)handlerOn:(BOOL)isOn {
    if (isOn) {
        _timer.onOff = 85;
        if (_timer.fan == 0 || _timer.temperature == 0 || _timer.mode == 0) {
            [self setDefaultDevice];
        }
    } else {
        _timer.onOff = 170;
    }
    [self.mTimerSettingsTbv reloadData];
    
}

-(void)handlerWeekDays:(NSInteger)day isTurnOn:(BOOL)isTurnOn {
    //NSLog(@"start daysSum = %li\n day = %li\n", daysSum, day);
    daysSum = 0;
    if (isTurnOn) {
        [_timer.daysArr addObject:@(day)];
    } else {
        NSUInteger index = [_timer.daysArr indexOfObject:@(day)];
        if (index >= 0 && index <= _timer.daysArr.count-1 ) {
            [_timer.daysArr removeObjectAtIndex:index];
        }
    }
    [self countDays];
   // NSLog(@"finish daysSum = %li\n", daysSum);
}

-(void)handlerTimer:(NSString *)time {
    _timer.startTime = time;
}
#pragma mark: AddTimerModeDelegate
-(void)handlerTemperature:(NSInteger)temp {
    _timer.temperature = temp;
    [_mTimerSettingsTbv reloadData];
}
-(void)handlerFan:(NSInteger)fan {
    _timer.fan = fan;
    [_mTimerSettingsTbv reloadData];
}
-(void)handlerMode:(NSInteger)mode {
    _timer.mode = mode;
    _timer.deviceVisualComp = [Timer setDeviceVisualCompanent:_timer];
    [_mTimerSettingsTbv reloadData];

}
#pragma mark: CustomAlertDelegate
-(void)handelOk {
    
}
@end
