//
//  DevicesTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/14/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "DevicesTableViewCell.h"

@implementation DevicesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)deleteCell:(UIButton *)sender {
    [self.delegate handlerDelete:sender];
}

- (IBAction)order:(UIButton *)sender {
    [self.delegate handlerOrder:sender];
}

- (void) setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing: editing animated: YES];

    if (editing) {

        for (UIView * view in self.subviews) {
            if ([NSStringFromClass([view class]) rangeOfString: @"Reorder"].location != NSNotFound) {
                for (UIView * subview in view.subviews) {
                    if ([subview isKindOfClass: [UIImageView class]]) {
                        ((UIImageView *)subview).image = [UIImage imageNamed: @"gray_device_menu.png"];
                    }
                }
            }
        }
    }
}
@end
