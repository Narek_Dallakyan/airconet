//
//  SliderView.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/16/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTRangeSlider.h"

NS_ASSUME_NONNULL_BEGIN

@interface SliderView : UIView<TTRangeSliderDelegate>
@property (weak, nonatomic) IBOutlet TTRangeSlider *rangeSliderCurrency;
@property (weak, nonatomic) IBOutlet UILabel * mSliderLb;
@end

NS_ASSUME_NONNULL_END
