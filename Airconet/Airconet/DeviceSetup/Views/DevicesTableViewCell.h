//
//  DevicesTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/14/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol DevicesTableViewCellDelegate <NSObject>

@optional
-(void)handlerDelete:(UIButton *)clickedBtn;
-(void)handlerOrder:(UIButton *)clickedBtn;
@end
@interface DevicesTableViewCell : UITableViewCell
@property(weak, nonatomic)id<DevicesTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *mRightBtn;

@property (weak, nonatomic) IBOutlet UIView *mTopBlackLineV;
@property (weak, nonatomic) IBOutlet UIView *mButtomBlackLinev;
@property (weak, nonatomic) IBOutlet UIButton *mOrderBtn;
@property (weak, nonatomic) IBOutlet UIImageView *mModeImgV;
@property (weak, nonatomic) IBOutlet UILabel *mDeviceNameLb;
@property (weak, nonatomic) IBOutlet UIButton *mDeleteBtn;
- (IBAction)deleteCell:(UIButton *)sender;
- (IBAction)order:(UIButton *)sender;
- (void) setEditing:(BOOL)editing animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
