//
//  SliderView.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/16/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "SliderView.h"
#import "APublicDefine.h"

@implementation SliderView

-(void)awakeFromNib {
    [super awakeFromNib];
    //currency range slider
    self.rangeSliderCurrency.delegate = self;
   // self.rangeSliderCurrency.minValue = 16;
   // self.rangeSliderCurrency.maxValue = 32;
    self.rangeSliderCurrency.selectedMinimum = 16;
    self.rangeSliderCurrency.selectedMaximum = 32;
    self.rangeSliderCurrency.handleColor = [UIColor greenColor];
    self.rangeSliderCurrency.handleDiameter = 30;
    
    
    self.rangeSliderCurrency.minLabelColour = [UIColor whiteColor];
    self.rangeSliderCurrency.maxLabelColour = [UIColor whiteColor];
    self.rangeSliderCurrency.maxHandleColor = RGB(193, 0, 4);
    self.rangeSliderCurrency.minHandleColor = RGB(40, 88, 149);
    self.rangeSliderCurrency.tintColorBetweenHandles = [UIColor blackColor];
    self.rangeSliderCurrency.lineHeight = 7;
    self.rangeSliderCurrency.lineBorderWidth = 2;
    self.rangeSliderCurrency.lineBorderColor = [UIColor darkGrayColor];
    self.rangeSliderCurrency.tintColor = [UIColor blackColor];
    self.rangeSliderCurrency.maxLabelFont = [UIFont systemFontOfSize:20.f];
    self.rangeSliderCurrency.minLabelFont = [UIFont systemFontOfSize:20.f];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
