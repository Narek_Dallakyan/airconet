//
//  DevicesViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/14/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "DevicesViewController.h"
#import "DevicesTableViewCell.h"
#import "DeviceSetupViewController.h"
#import "AlertView.h"
#import "Devices.h"


#import "GCDAsyncUdpSocket.h"
#import "ASetDeviceSocket.h"
#import "ESPTouchTask.h"
#import "ESPTouchResult.h"
#import "ESP_NetUtil.h"
#import "ESPTouchDelegate.h"
#import "ESPAES.h"
#import "AFNetworking.h"
#import "Reachability.h"

#import "ESPTools.h"
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface DevicesViewController ()< GCDAsyncUdpSocketDelegate, DevicesTableViewCellDelegate, AlertViewDelegate, CustomAlertDelegate>

@property (weak, nonatomic) IBOutlet UITableView *mDevicesTbV;
@property (weak,nonatomic) NSIndexPath * clickedIndexPath;
@property (weak, nonatomic) IBOutlet AlertView *mAlertV;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityV;
@property (weak, nonatomic) IBOutlet UILabel *mAddDeviceLb;
@property (strong, nonatomic)UIButton * rightBarBtn;
@property (strong, nonatomic)UIButton * leftBarBtn;
@property (strong, nonatomic) NSMutableArray * allDevices;
@property (strong, nonatomic) NSMutableArray * allPowerMeters;

@property (assign, nonatomic)int cellCount;


@property (nonatomic, strong) GCDAsyncUdpSocket *udpSocket;
@property (nonatomic, strong) ASetDeviceSocket *setSocket;
//@property (nonatomic, strong)NSDictionary *netInfo;
@end

@implementation DevicesViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self setBackground:self];
    [self.mActivityV startAnimating];
    [self getAllDevices];
}

-(void)viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBar.topItem.title = NSLocalizedString(@"device_setup", nil);
}

#pragma mark: SET
-(void)setupView {
    _cellCount = 8;
    self.mAlertV.delegate = self;
    self.mDevicesTbV.contentInset = UIEdgeInsetsMake(-30.0f, 0.0f, 0.0f, 0.0f);
    
    [self setBarButton];
    [self setNavigationStyle];
}
-(void)setNavigationStyle {
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self setNavigationStyle:BaseColor tintColor:[UIColor whiteColor] titleText:NSLocalizedString(@"device_setup", nil) titleColor:NavTitleColor];
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if ([view isKindOfClass:[DevicesNavigationBar class]]) {
            [view removeFromSuperview];
        }
    }
    self.navigationItem.titleView = [self setNavigationTitle:@"device_setup"];

}
-(void)setBarButton {
    UIImage* addImage = [UIImage imageNamed:@"plus"];
    _rightBarBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, addImage.size.width, addImage.size.height)];
    [_rightBarBtn setBackgroundImage:addImage forState:UIControlStateNormal];
    [_rightBarBtn addTarget:self action:@selector(addDevice)
           forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithCustomView:_rightBarBtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    UIImage* backImg = [UIImage imageNamed:@"ic_left"];
    if ([BaseViewController isDeviceLanguageRTL]) {
        backImg = [UIImage imageNamed:@"right_direction"];
    }
    _leftBarBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, backImg.size.width, backImg.size.height)];
    [_leftBarBtn setBackgroundImage:backImg forState:UIControlStateNormal];
    [_leftBarBtn addTarget:self action:@selector(back)
          forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:_leftBarBtn];
    self.navigationItem.leftBarButtonItem = leftButton;
}
-(void)setAllTypeOfDevices:(NSArray *)arr {
    if (arr.count > 0) {
        self.mDevicesTbV.hidden = NO;
    } else {
        self.mAddDeviceLb.hidden = NO;
    }
    for ( NSDictionary * dic  in arr) {
        if (![self isHiddenDevice:[dic valueForKey:@"mac"]]) {
            
            NSString * type = [dic valueForKey:@"deviceType"];
            if (![type isKindOfClass:[NSNull class]]) {
                 if ([type isEqualToString:@"PM"]) {
                                [self.allPowerMeters addObject:[[[Devices alloc] init] getDeviceObj:dic] ];
                            } else {
                                [self.allDevices addObject:[[[Devices alloc] init] getDeviceObj:dic] ];
                #warning test! here we need to add PM obj if exist
                                //if on device exist PM it means that device already linked, else unlinked
                                //            [[[self.allDevices lastObject] powerMeter] setPhaseNumber:<#(NSNumber * _Nonnull)#>]
                                //            powerMeter = "<null>";
                                //            powerMeterPhaseNumber = 0;
                            }
            }
           
        }
    }
    
    
    if (self.allPowerMeters.count > 0) {
        [self getMasterPowerMeter];
    } else {
        [self.mDevicesTbV reloadData];
        [self.mActivityV stopAnimating];
    }
}

-(void)setMasterPM:(NSDictionary*) result {
    for (int i = 0; i < self.allPowerMeters.count; i++) {
        if ([[[self.allPowerMeters objectAtIndex:i] mac] isEqualToString:[result valueForKey:@"mac"]]) {
            [[self.allPowerMeters objectAtIndex:i] setMasterPm:YES];
        }
    }
    [self.mDevicesTbV reloadData];
    [self.mActivityV stopAnimating];
    
}
#pragma mark: GET
- (void)getAllDevices {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] getJsonDataWithUrl:AGetdevdevices
                                            andParameters:@{}  isNeedCookie:NO
                                                  success:^(id response) {
            self.allDevices = [NSMutableArray array];
            self.allPowerMeters = [NSMutableArray array];
            NSNumber *code = [response objectForKey:@"returnCode"];
            if (code.intValue == 0) {
                NSArray *array = [response objectForKey:@"values"];
               // NSLog(@"AGetdevdevices result->valies = %@ \n",array);
                [self setAllTypeOfDevices:array];
            }
        } failure:^(NSError *error) {
        }];
    });
}

-(void)getMasterPowerMeter {
    //AGetMaster
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] getJsonDataWithUrl:AGetMaster
                                            andParameters:@{}  isNeedCookie:NO
                                                  success:^(id response) {
            
           // NSLog(@"AGetMaster response= %@ \n",response);
            [self setMasterPM:response];
            
        } failure:^(NSError *error) {
            //show Alert
            [self.mDevicesTbV reloadData];
            [self.mActivityV stopAnimating];
            //NSLog(@"AGetMaster result->erropr = %@ \n",error.localizedDescription);
            
        }];
    });
}

-(void)addDevice {
    self.navigationController.navigationBar.topItem.title = @"";
    [self pushViewControllerWithIdentifier:@"AddDevice" controller:self];
}
-(void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)goToAddDevicePage:(Devices *)device identier:(NSString *) identier {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DeviceSetupViewController * vc = [storyboard   instantiateViewControllerWithIdentifier:identier];//@"DeviceSetupViewController"];
    vc.currDevice = device;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)hideAlert:(BOOL)hide {
    self.mAlertV.hidden = hide;
    _leftBarBtn.hidden = !hide;
    _rightBarBtn.hidden = !hide;
}

#pragma mark: UPDATE LIST
-(void)deleteDevice:(Devices *)currDevice currPM:(Devices *)currPM{
    
    NSString *formatUrl;
    if (currDevice) {
        formatUrl = [NSString stringWithFormat:@"%@/%@",AGetdevremoteDevice,currDevice.mac];
    } else {
        formatUrl = [NSString stringWithFormat:@"%@/%@",AGetdevremoteDevice,currPM.mac];
    }
    [[RequestManager sheredMenage] getJsonDataWithUrl:formatUrl andParameters:@{} isNeedCookie:NO success:^(id  _Nonnull response) {
        NSString * currIp;
        if (currDevice) {
            [self.allDevices removeObject:currDevice];
            currIp = currDevice.privateIp;
        } else {
            [self.allPowerMeters removeObject:currPM];
            currIp = currPM.privateIp;
        }
        [self.mDevicesTbV reloadData];
        ASetDeviceSocket *socket = [[ASetDeviceSocket alloc] init];
        socket.ip = currIp;
        socket.command = @"/config?command=restore";
        socket.configData = @"";
        self.setSocket = socket;
        [socket send];
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"deleteDevice error = %@\n", error.localizedDescription);
        //@"Operation Failure!" alert
    }];
}

-(void)orderDevices {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *macs = @"";
        for (Devices * device in self->_allDevices) {
            macs = [macs stringByAppendingString:[NSString stringWithFormat:@"%@|",device.mac]];
        }
        NSDictionary *args = [NSDictionary dictionaryWithObjectsAndKeys:macs,@"macs", nil];
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostdeviceorder andParameters:args success:^(id _Nonnull response) {
           //NSLog(@"Device order response = %@\n", response);
        } failure:^(NSError * _Nonnull error) {
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
    });
                
}
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.allDevices.count;
    }
    return self.allPowerMeters.count;//
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section == 1 && self.allPowerMeters.count > 0) {
        return NSLocalizedString(@"pms", nil);
    } else {
        return @" ";
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(20, 20, 320, 20);
    myLabel.textColor = GrayTitleColor;
    myLabel.font = [UIFont fontWithName:@"Calibri" size:22.f];
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:myLabel];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DevicesTableViewCell * devicesCell = [tableView dequeueReusableCellWithIdentifier:@"DevicesCell"];
    devicesCell.delegate = self;
    tableView.separatorColor = [UIColor clearColor];
    if (indexPath.section == 1) { //Power meter
        Devices * currPM = [self.allPowerMeters objectAtIndex:indexPath.row];
        [devicesCell.mOrderBtn setImage:[UIImage imageNamed:@"gray_gauge"] forState:UIControlStateNormal];
        [devicesCell.mDeviceNameLb setText:currPM.name];
        if (currPM.masterPm) {
            devicesCell.mModeImgV.hidden = NO;
            //[devicesCell.mModeImgV setImage:Image(@"crown")];
        }else {
            devicesCell.mModeImgV.hidden = YES;
        }
    } else { //device
        Devices * currDevice = [self.allDevices objectAtIndex:indexPath.row];
        [devicesCell.mDeviceNameLb setText:currDevice.name];
        devicesCell.mModeImgV.hidden = NO;
        
        if ([currDevice.model caseInsensitiveCompare: @"switch"] == NSOrderedSame) { //switch
            [devicesCell.mModeImgV setImage:[UIImage imageNamed:@"ic_grey_lamp"]];
        } else  if ([currDevice.model caseInsensitiveCompare: @"sensor"] == NSOrderedSame) { //switch
            [devicesCell.mModeImgV setImage:[UIImage imageNamed:@"sys_sensor"]];
        } else {
            [devicesCell.mModeImgV setImage:[UIImage imageNamed:@"light_cold"]];
        }
        UILongPressGestureRecognizer * longPressGest = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(orderlongPressGesture:)];
        [devicesCell.mOrderBtn addGestureRecognizer:longPressGest];
    }
    return devicesCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return  50.f;
    }
    return 0.f;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if ([[[_allDevices objectAtIndex:indexPath.row] model] caseInsensitiveCompare:@"Sensor"] == NSOrderedSame) {
            [self goToAddDevicePage:[_allDevices objectAtIndex:indexPath.row] identier:@"SensorSetupViewController"];
        } else {
            [self goToAddDevicePage:[_allDevices objectAtIndex:indexPath.row] identier:@"DeviceSetupViewController"];
        }
    } else {
        [self goToAddDevicePage:[_allPowerMeters objectAtIndex:indexPath.row] identier:@"DeviceSetupViewController"];
    }
}

- (NSDictionary *)fetchNetInfo {
    NSMutableDictionary *wifiDic = [NSMutableDictionary dictionaryWithCapacity:0];
    wifiDic[@"ssid"] = ESPTools.getCurrentWiFiSsid;
    wifiDic[@"bssid"] = ESPTools.getCurrentBSSID;
    return wifiDic;
}
- (IBAction)orderlongPressGesture:(UIGestureRecognizer *)sender {
    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
    UIGestureRecognizerState state = longPress.state;
    CGPoint location = [longPress locationInView:_mDevicesTbV];
    NSIndexPath *indexPath = [_mDevicesTbV indexPathForRowAtPoint:location];
    static UIView       *snapshot = nil;        ///< A snapshot of the row user is moving.
    static NSIndexPath  *sourceIndexPath = nil; ///< Initial index path, where gesture begins.
    switch (state) {
        case UIGestureRecognizerStateBegan: {
            if (indexPath) {
                sourceIndexPath = indexPath;
                UITableViewCell *cell = [_mDevicesTbV cellForRowAtIndexPath:indexPath];
                // Take a snapshot of the selected row using helper method.
                snapshot = [self customSnapshoFromView:cell];
                // Add the snapshot as subview, centered at cell's center...
                __block CGPoint center = cell.center;
                snapshot.center = center;
                snapshot.alpha = 0.0;
                [_mDevicesTbV addSubview:snapshot];
                [UIView animateWithDuration:0.25 animations:^{
                    // Offset for gesture location.
                    center.y = location.y;
                    snapshot.center = center;
                    snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                    snapshot.alpha = 0.98;
                    cell.alpha = 0.0;
                    cell.hidden = YES;
                }];
            }
            break;
        }
        case UIGestureRecognizerStateChanged: {
            CGPoint center = snapshot.center;
            center.y = location.y;
            snapshot.center = center;
            // Is destination valid and is it different from source?
            if (indexPath && ![indexPath isEqual:sourceIndexPath]) {
                // ... update data source.
                [_allDevices exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                
                // ... move the rows.
                [_mDevicesTbV moveRowAtIndexPath:sourceIndexPath toIndexPath:indexPath];
                // ... and update source so it is in sync with UI changes.
                sourceIndexPath = indexPath;
            }
            break;
        }
        default: {
            // Clean up.
            UITableViewCell *cell = [_mDevicesTbV cellForRowAtIndexPath:sourceIndexPath];
            cell.alpha = 0.0;
            [UIView animateWithDuration:0.25 animations:^{
                snapshot.center = cell.center;
                snapshot.transform = CGAffineTransformIdentity;
                snapshot.alpha = 0.0;
                cell.alpha = 1.0;
            } completion:^(BOOL finished) {
                cell.hidden = NO;
                sourceIndexPath = nil;
                [snapshot removeFromSuperview];
                snapshot = nil;
            }];
            [self orderDevices];
            break;
        }
    }
}


/** @brief Returns a customized snapshot of a given view. */
- (UIView *)customSnapshoFromView:(UIView *)inputView {
    // Make an image from the input view.
    UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
    [inputView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    // Create an image view.
    UIView *snapshot = [[UIImageView alloc] initWithImage:image];
    snapshot.layer.masksToBounds = NO;
    snapshot.layer.cornerRadius = 0.0;
    snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
    snapshot.layer.shadowRadius = 5.0;
    snapshot.layer.shadowOpacity = 0.4;
    return snapshot;
}



#pragma mark: DeviesCell Delegate
-(void)handlerDelete:(UIButton *)clickedBtn {
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status ==  AFNetworkReachabilityStatusReachableViaWiFi) {
            [self.mAlertV.mRouterWifiNameLb setText:[ [self fetchNetInfo] valueForKey:@"ssid"]];
        } else if (status ==  AFNetworkReachabilityStatusReachableViaWWAN) {
                   [self.mAlertV.mRouterWifiNameLb setText:NSLocalizedString(@"cellular_data", nil)];
               }
    }];
    [self hideAlert:NO];
    [self.mAlertV.mAlertLb setTextColor:[UIColor redColor]];
    
    [self animationPopup:self.mAlertV];
    self.navigationController.topViewController.title = NSLocalizedString(@"delete_dev", nil);
    
    CGPoint buttonPosition = [clickedBtn convertPoint:CGPointZero toView:self.mDevicesTbV];
    _clickedIndexPath = [self.mDevicesTbV indexPathForRowAtPoint:buttonPosition];
   // NSLog(@"clickedIndexPath  = %i\n", _clickedIndexPath.row);
}

-(void)handlerOrder:(UIButton *)clickedBtn {

    CGPoint buttonPosition = [clickedBtn convertPoint:CGPointZero toView:self.mDevicesTbV];
    _clickedIndexPath = [self.mDevicesTbV indexPathForRowAtPoint:buttonPosition];
    if (_clickedIndexPath.section == 0) {
         NSString *macs = [NSString stringWithFormat:@"%@|",[[_allDevices objectAtIndex:_clickedIndexPath.row] mac]];
           for (Devices * device in _allDevices) {
               if (![[[_allDevices objectAtIndex:_clickedIndexPath.row] mac] isEqualToString:device.mac]) {
                   macs = [macs stringByAppendingString:[NSString stringWithFormat:@"%@|",device.mac]];
               }
           }
           NSDictionary *args = [NSDictionary dictionaryWithObjectsAndKeys:macs,@"macs", nil];
           [[RequestManager sheredMenage] postJsonDataWithUrl:APostdeviceorder andParameters:args success:^(id _Nonnull response) {
               //NSLog(@"Device order response = %@\n", response);
               if ([[response valueForKey:@"resutlt"] integerValue] == 0) {
                   [self.mDevicesTbV moveRowAtIndexPath:self->_clickedIndexPath toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
               }
           } failure:^(NSError * _Nonnull error) {
               [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
           }];
    }
   
}
#pragma mark: AlertView Delegate
-(void)handlerCancel:(BOOL)isCancel {
    [self hideAlert:YES];
    if (!isCancel)  {
        if (_clickedIndexPath.section == 0) {
            [self deleteDevice:[self.allDevices objectAtIndex:_clickedIndexPath.row] currPM:nil];
        } else {
            [self deleteDevice:nil currPM:[self.allPowerMeters objectAtIndex:_clickedIndexPath.row]];
        }
    } else {
        self.navigationController.topViewController.title = NSLocalizedString(@"device_setup", nil);
        
    }
}

#pragma mark: Custom Alert
-(void)handelOk {
    
}

#pragma mark GCDAsyncUdpSocketDelegate
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didConnectToAddress:(NSData *)address {
    NSLog(@"didConnect to Address data = %@",address);
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotConnect:(NSError *)error {
    
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag {
    if (tag == 10) {
        [self.udpSocket close];
        return;
    }
    [self performSelector:@selector(udpSendData:) withObject:[NSNumber numberWithLong:tag] afterDelay:2];
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
    if (tag >= 10)
    {
        [self.udpSocket closeAfterSending];
        return;
    }
    
    [self performSelector:@selector(udpSendData:) withObject:[NSNumber numberWithLong:tag] afterDelay:2.5];
}

- (void)udpSendData:(NSNumber *)tag
{
    long t = tag.longValue + 1;
    NSData *data = [@"Are You AirM2M IOT Smart Device?" dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    [self.udpSocket sendData:data toHost:ABaseHostString port:ABasePort withTimeout:ATimeOut tag:t];
}
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext
{
    NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSArray *array = [msg componentsSeparatedByString:@" "];
    if (array.count < 3) {
        // [[[UIAlertView alloc] initWithTitle:@"Unrecognized module message. Ignore" message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"cancel",@"") otherButtonTitles:nil, nil] show];
        return;
    }
    NSString *str1 = [array objectAtIndex:1];
    NSString *privateIP = [array objectAtIndex:2];
    NSArray *ary2 = [str1 componentsSeparatedByString:@"."];
    NSString *type = [ary2 objectAtIndex:0];
    NSString *mac = [[ary2 objectAtIndex:1] stringByReplacingOccurrencesOfString:@":" withString:@""];
    BOOL found = NO;
    for (int i = 0; i < self.allDevices.count; ++i)
    {
        NSDictionary *dict = [self.allDevices objectAtIndex:i];
        NSString *hmac = [dict objectForKey:@"mac"];
        if ([hmac caseInsensitiveCompare:mac] == NSOrderedSame) {
            found = YES;
            NSMutableDictionary *mdict = [NSMutableDictionary dictionaryWithDictionary:dict];
            [mdict setObject:type forKey:@"type"];
            [mdict setObject:privateIP forKey:@"privateIp"];
            [self.allDevices replaceObjectAtIndex:i withObject:mdict];
            break;
        }
    }
    
    if (found) {
        // [self.tableView reloadData];
        
    } else {
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:type,@"type",privateIP,@"privateIp",mac,@"mac", nil];
        [self.allDevices addObject:[[[Devices alloc]init] getDeviceObj:dict]];
        //  [self.foldFlag addObject:@"YES"];
        //  [self.tableView reloadData];
        // [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.dataAry.count - 1 inSection:0]  atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
}


@end
