//
//  SensorSetupViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 12-05-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"
#import "Devices.h"

NS_ASSUME_NONNULL_BEGIN

@interface SensorSetupViewController : BaseViewController
@property (strong, nonatomic) Devices * currDevice;

@end

NS_ASSUME_NONNULL_END
