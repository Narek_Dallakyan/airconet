//
//  DeviceSetupViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/14/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "DeviceSetupViewController.h"
#import "UITextField+Content.h"
#import "TTRangeSlider.h"
#import "SliderView.h"
#import "VSDropdown.h"
#import "APublicDefine.h"
#import "ACommonTools.h"
#import "PopupView.h"
#import "DeviceSetup.h"
#import "AllDevises.h"

@interface DeviceSetupViewController () <TTRangeSliderDelegate, VSDropdownDelegate, PopupViewDelegate, UIScrollViewDelegate, CustomAlertDelegate> {
    
    DeviceSetup *deviceSetup;
    VSDropdown *_dropdown;
    AllDevises * currentDev;
    BOOL isVrf;
    BOOL isRefreshWorkHours;
    BOOL isResiveFeedback;
    BOOL isDeleteLink;
    UIButton * navRightBtn;
    UIButton * navLeftBtn;
}
@property (weak, nonatomic) IBOutlet UIScrollView *mScrollV;
@property (weak, nonatomic) IBOutlet UIView *MScrollContentV;
@property (weak, nonatomic) IBOutlet UITextField *mModifiableNameTetxFl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mLinkBackgvTopLayout;
@property (weak, nonatomic) IBOutlet UIView *mDeviceSetupSpecialBackV;
@property (weak, nonatomic) IBOutlet UIView *mSliderBackV;
@property (weak, nonatomic) IBOutlet UILabel *mWorkingHoursLb;
@property (weak, nonatomic) IBOutlet UIButton *mRefreshCounter;
@property (weak, nonatomic) IBOutlet UILabel *mHoursLb;
@property (weak, nonatomic) IBOutlet UILabel *mTemperatireLimitesLb;
@property (weak, nonatomic) IBOutlet UIButton *mKeepSetPointsBtn;
@property (weak, nonatomic) IBOutlet UILabel *mKeepSetPointLb;
@property (weak, nonatomic) IBOutlet UILabel *mLinkDeviceLb;
@property (weak, nonatomic) IBOutlet UIButton *mLinkBtn;
@property (weak, nonatomic) IBOutlet UILabel *mLinkedToPowerMeterLb;
@property (weak, nonatomic) IBOutlet UIButton *mDeleteLinkBtn;
@property (weak, nonatomic) IBOutlet UILabel *mBaseWattLb;
@property (weak, nonatomic) IBOutlet UITextField *mBaseWattTextFl;
@property (weak, nonatomic) IBOutlet UIButton *mBaseWattRefreshBtn;
@property (weak, nonatomic) IBOutlet UILabel *mVrfAddressLb;
@property (weak, nonatomic) IBOutlet UILabel *mVrfAddressNumber;
@property (weak, nonatomic) IBOutlet UIButton *mDropDownBtn;
@property (weak, nonatomic) IBOutlet UIButton *mTriangelBtn;
@property (weak, nonatomic) IBOutlet SliderView *mSliderV;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mLinkActivityV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mHideDevVTopLayout;
@property (weak, nonatomic) IBOutlet UIView *mLinkToPmBackgV;
@property (weak, nonatomic) IBOutlet UIView *mBaseWattBackgV;
@property (weak, nonatomic) IBOutlet UIView *mDropDownBackgV;
@property (weak, nonatomic) IBOutlet UIView *mLinkBackgV;
@property (weak, nonatomic) IBOutlet UIView *mMasterPmBackgV;
@property (weak, nonatomic) IBOutlet UILabel *mMasterPmLb;
@property (weak, nonatomic) IBOutlet UIButton *mMasterPmBtn;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *mBaseWettActivityV;


- (IBAction)refreshCounter:(UIButton *)sender;
- (IBAction)masterPm:(UIButton *)sender;
- (IBAction)keepSetPoint:(UIButton *)sender;
- (IBAction)link:(UIButton *)sender;
- (IBAction)deleteLink:(UIButton *)sender;
- (IBAction)baseWattRefresh:(UIButton *)sender;
- (IBAction)mDropDownBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mScrollsContentVHeight;

@end

@implementation DeviceSetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupView];

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated {
    [self setBackground:self];
}
-(void)viewDidAppear:(BOOL)animated  {
    [super viewDidAppear:YES];
    [[NSNotificationCenter defaultCenter]
    addObserver:self selector:@selector(updateByFeedback:) name:NotificationDeviceSetup object:nil];
    self.mScrollV.contentOffset = CGPointMake(0, 0);
    self.mScrollV.scrollEnabled = YES;
    
}
-(void)viewDidLayoutSubviews {
 self.mScrollV.contentSize = CGSizeMake(self.view.frame.size.width,self.mDropDownBtn.frame.origin.y + self.mDropDownBtn.frame.size.height+150);
    self.mScrollsContentVHeight.constant = self.mScrollV.frame.size.height;

}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self closeDropDown];
}

#pragma mark: SET
-(void)setupView {
    deviceSetup = [[DeviceSetup alloc] init];
    deviceSetup.baseWatt = _currDevice.baseWatt;
    deviceSetup.mac = _currDevice.mac;
    self.mScrollV.delegate = self;
    self.mSliderV.rangeSliderCurrency.delegate = self;
    [self.mBaseWattTextFl setBlueBorderStyle];
    [self.mModifiableNameTetxFl setBlueBorderStyle];
    [self.mModifiableNameTetxFl setImageInRightView:[UIImage imageNamed:@"edit"]];
    _mModifiableNameTetxFl.leftViewMode = UITextFieldViewModeAlways;
    UIView * paddingView=  [[UIView alloc]initWithFrame:CGRectMake(0, 0, 60, 30)];
    _mModifiableNameTetxFl.leftView = paddingView;
    [self.mTriangelBtn setImage:[UIImage imageNamed:@"down_triangle"] forState:UIControlStateNormal];
    [self setBarButton];
    isVrf = YES;
    
    _dropdown = [[VSDropdown alloc]initWithDelegate:self];
    [_dropdown setAdoptParentTheme:YES];
    [_dropdown setShouldSortItems:YES];
    _dropdown.allowMultipleSelection = NO;
    _dropdown.textColor = PlaceholderColor;
    _dropdown.separatorColor = PlaceholderColor;

    [ACommonTools setObjectValue:@(-10) forKey:@"DropDownX"];
    if (isVrf) {
        [ACommonTools setObjectValue:@90 forKey:@"DropDownWidth"];
    } else {
        [ACommonTools setObjectValue:@120 forKey:@"DropDownWidth"];
    }
    if ([_currDevice.deviceType isEqualToString:@"PM"]) {
           [self setPMView];
       } else {
           [self setAcOrSWView];
       }
    [self performSelectorInBackground:@selector(setDevInfo) withObject:nil];
}

-(void)setBarButton {
    navRightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
    [navRightBtn setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    [navRightBtn addTarget:self action:@selector(cancel)
         forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithCustomView:navRightBtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    navLeftBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
       [navLeftBtn setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
       [navLeftBtn addTarget:self action:@selector(handlerSave)
            forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:navLeftBtn];
       self.navigationItem.leftBarButtonItem = leftButton;
}

-(void)setDevInfo {
    currentDev = [[AllDevises alloc] init];
    for (AllDevises * dev in mainDevicesArr) {
        if ([dev.mac isEqualToString:_currDevice.mac]) {
            currentDev = dev;
        }
    }
}

-(void)updateAcOrSWView {
    [_mModifiableNameTetxFl setText:_currDevice.name];
    [_mBaseWattTextFl setText:[deviceSetup.baseWatt stringValue]];
    if (![_currDevice.deviceType isEqualToString:@"SW"]) {
        [_mSliderV.rangeSliderCurrency setMaxValue: [_currDevice.maxPoint floatValue]];
        [_mSliderV.rangeSliderCurrency setMinValue:[_currDevice.minPoint floatValue]];
        [_mSliderV.rangeSliderCurrency setSelectedMaximum:deviceSetup.maxPoint];
        [_mSliderV.rangeSliderCurrency setSelectedMinimum:deviceSetup.minPoint];
    } else {
        [self getHideDevFromApp];
    }
    if (deviceSetup.keepSetPointInRange == 1) {
     [_mKeepSetPointsBtn setImage:Image(@"circle_check") forState:UIControlStateNormal];
    } else {
        [_mKeepSetPointsBtn setImage:Image(@"uncheck") forState:UIControlStateNormal];
    }
}

-(void)setAcOrSWView {
    self.mLinkBackgvTopLayout.constant = 40;
    //_currDevice.deviceType = @"SW";
    if ([_currDevice.deviceType isEqualToString:@"SW"]) {
        self.navigationItem.titleView = [self setNavigationTitle:@"switch_setup"];
    } else {
        self.navigationItem.titleView = [self setNavigationTitle:@"aircon_setup"];
        if ([_currDevice.model caseInsensitiveCompare:@"Sensor"] == NSOrderedSame) {
            self.navigationItem.titleView = [self setNavigationTitle:@"sensor_setup"];
        } else if ([_currDevice.model caseInsensitiveCompare:@"Thermostat"] == NSOrderedSame){
            self.navigationItem.titleView = [self setNavigationTitle:@"thermostat_setup"];
        }
    }
    [_mBaseWattTextFl setText:[deviceSetup.baseWatt stringValue]];
    self.mBaseWattLb.hidden = NO;
    self.mBaseWattTextFl.hidden = NO;
    self.mBaseWattRefreshBtn.hidden = NO;
    self.mLinkBackgV.hidden = NO;
    self.mKeepSetPointsBtn.hidden = NO;
    self.mKeepSetPointLb.hidden = NO;
    self.mWorkingHoursLb.hidden = NO;
    self.mHoursLb.hidden = NO;
    self.mRefreshCounter.hidden = NO;
    self.mMasterPmBackgV.hidden = YES;
    self.mHideDevVTopLayout.constant = 20;
    self.mLinkDeviceLb.text = NSLocalizedString(@"link_dev", nil);
    [self getWorkingHour];
    
    if ([_currDevice.deviceType isEqualToString:@"SW"]) {
         self.mSliderV.hidden = YES;
         self.mKeepSetPointLb.text = NSLocalizedString(@"hide_dev_app", nil);
         self.mHideDevVTopLayout.constant = -80;
        [self updateAcOrSWView];
      } else {
          self.mSliderV.hidden = NO;
          self.mKeepSetPointLb.text = NSLocalizedString(@"keep_range", nil);
          [self getSetupPoint];
      }
}

-(void)setPMView {
    self.mLinkBackgvTopLayout.constant = -130;
    self.navigationItem.titleView = [self setNavigationTitle:@"pm_setup"];

    self.mLinkDeviceLb.text = NSLocalizedString(@"link_pm", nil);
    [self.mLinkBtn setTitle:NSLocalizedString(@"unlinked", nil) forState:UIControlStateNormal];
    [self.mLinkBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
    [self.mLinkBtn setUserInteractionEnabled:NO];

    self.mSliderV.hidden = YES;
    self.mKeepSetPointsBtn.hidden = YES;
    self.mKeepSetPointLb.hidden = YES;
    self.mWorkingHoursLb.hidden = YES;
    self.mHoursLb.hidden = YES;
    self.mRefreshCounter.hidden = YES;
    self.mMasterPmBackgV.hidden = NO;
    self.mLinkBtn.hidden = YES;
    self.mLinkToPmBackgV.hidden = YES;
    self.mLinkDeviceLb.hidden = YES;
    
    [_mModifiableNameTetxFl setText:_currDevice.name];
    if (self.currDevice.masterPm) {
        [self.mMasterPmBtn setImage:Image(@"square_check") forState:UIControlStateNormal];
        self.mLinkBackgV.hidden = NO;
    } else {
        [self.mMasterPmBtn setImage:Image(@"square_uncheck") forState:UIControlStateNormal];
        self.mLinkBackgV.hidden = YES;
    }
    [self getPMLink];
}


-(void)getHideDevFromApp {
    for (int i = 0; i < hiddenDevicesArr.count; i++) {
        if ([_currDevice.mac isEqualToString:[[hiddenDevicesArr objectAtIndex:i]mac]]) {
            deviceSetup.keepSetPointInRange = YES;
            return;
        }
    }
    deviceSetup.keepSetPointInRange = NO;
}
-(void)updatePmField {
    NSString * pmStr;
    if ([deviceSetup.powerMeter.phaseNumber integerValue] > 0) {
        pmStr = [NSString stringWithFormat:@"%@ %@ (%li)", NSLocalizedString(@"link_to", nil), deviceSetup.powerMeter.name, (long)[deviceSetup.powerMeter.phaseNumber integerValue] ];
    } else {
        pmStr = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"link_to", nil), deviceSetup.powerMeter.name ];
    }
    NSAttributedString * attString = [BaseViewController getAttributStringPossition:deviceSetup.powerMeter.name text:pmStr color:FanColor];
    [self.mLinkedToPowerMeterLb setAttributedText:attString];
}

//show when resive feedback
-(void)linkIsSeccess {
    [self.mLinkActivityV stopAnimating];
    self.mLinkBackgV.hidden = YES;
    [self.mLinkToPmBackgV setHidden: NO];
    [self.mBaseWattBackgV setHidden:NO];
}

//show when pw is offline, request is unseccesfully or when dont recive feedback
-(void)linkIsUnseccess {
    self.mLinkBackgV.hidden = NO;
    self.mLinkBtn.hidden = NO;
    [self.mLinkActivityV stopAnimating];
    [self popupAlert:NSLocalizedString(@"dev_are_conn", nil) frame:CGRectMake(0, -44, 230, 230) ok:@"" cencel:NSLocalizedString(@"ok", nil) delegate:self];
}

#pragma mark: Requests
-(void)getWorkingHour {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * urlFormar = [NSString stringWithFormat:@"%@?mac=%@",AGetWorkingHour, self.currDevice.mac];
        [[RequestManager sheredMenage] getDataWithUrl:urlFormar success:^(id  _Nonnull response) {
                  // NSLog(@"AGetWorkingHour response  = %@\n", response);
            
        dispatch_async(dispatch_get_main_queue(),  ^{
            [self.mHoursLb setText:[NSString stringWithFormat:@"%@%@",response, NSLocalizedString(@"h", nil)]];
            self->deviceSetup.workHour = [response integerValue];

        });
               } failure:^(NSError * _Nonnull error) {
                   //NSLog(@"AGetWorkingHour response error  = %@\n", error.localizedDescription);
                   [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
               }];
           });
}
-(void)getSetupPoint{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * urlFormar = [NSString stringWithFormat:@"%@?mac=%@",AGetSetupPoint, self.currDevice.mac];
        [[RequestManager sheredMenage] getDataWithUrl:urlFormar success:^(id  _Nonnull response) {
                  // NSLog(@"AGetSetupPoint response  = %@\n", response);
        dispatch_async(dispatch_get_main_queue(),  ^{
            self->deviceSetup.keepSetPointInRange = [[response valueForKey:@"keepSetPointInRange"] boolValue];
            self->deviceSetup.maxPoint = [[response valueForKey:@"maxPoint"] integerValue];
            self->deviceSetup.minPoint = [[response valueForKey:@"minPoint"] integerValue];
            [self updateAcOrSWView];
        });
               } failure:^(NSError * _Nonnull error) {
                  // NSLog(@"AGetSetupPoint response error  = %@\n", error.localizedDescription);
                   [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
               }];
           });
}

//it returns info if  about link, if exist show it if not show unlink
-(void)getPMLink {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * urlFormar = [NSString stringWithFormat:@"%@?mac=%@",AGetMasterLink, self.currDevice.mac];
        [[RequestManager sheredMenage] getDataWithUrl:urlFormar success:^(id  _Nonnull response) {
           // NSLog(@"AGetMasterLink response  = %@\n", response);
            dispatch_async(dispatch_get_main_queue(),  ^{
#warning Test! need read real
                self.mLinkToPmBackgV.hidden = NO;
                self.mDeleteLinkBtn.hidden = YES;
                self->deviceSetup.powerMeter = [[DevicePM alloc] init];
                self->deviceSetup.powerMeter.name = @"test 1";
                [self updatePmField];
                
            });
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"AGetMasterLink response error  = %@\n", error.localizedDescription);
            if ([error.localizedDescription containsString:@"404"]) {
                self.mLinkBackgV.hidden = NO;
                self.mLinkBtn.hidden = NO;
            }
        }];
    });
}

#pragma mark: Update Power Meter
-(void)updatePMName {
    self.currDevice.name = self.mModifiableNameTetxFl.text;
    NSDictionary * param = @{@"mac": self.currDevice.mac, @"name": self.currDevice.name};
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostUpdatePowerMeterName andParameters:param success:^(id _Nonnull response) {
              // NSLog(@"APostUpdateDevice response  = %@\n", response);
           } failure:^(NSError * _Nonnull error) {
               // NSLog(@"APostUpdateDevice response error  = %@\n", error.localizedDescription);
                [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
           }];
       });
}
-(void)updateMasterPM{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * formatUrl = [NSString stringWithFormat:@"%@%@/set-master", APostSetMaster, self.currDevice.mac ];
        NSString * value = @"false";
        if (self.currDevice.masterPm) {
            value = @"true";
        }
      [[RequestManager sheredMenage] postJsonDataWithUrl:formatUrl andParameters:@{@"enabled": value} success:^(id _Nonnull response) {
         // NSLog(@"APostSetMaster response  = %@\n", response);
          [self.navigationController popViewControllerAnimated:YES];
        
      } failure:^(NSError * _Nonnull error) {
          // NSLog(@"APostSetMaster response error  = %@\n", error.localizedDescription);
           [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
      }];
    });
}


#pragma mark: Update Device

-(void)updateDeviceName {
    self.currDevice.name = self.mModifiableNameTetxFl.text;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary * param = [Devices getDeviceJson:self.currDevice];
           [[RequestManager sheredMenage] postJsonDataWithUrl:APostUpdateDevice andParameters:param success:^(id _Nonnull response) {
               //NSLog(@"APostUpdateDevice response  = %@\n", response);
               [self.navigationController popViewControllerAnimated:YES];

           } failure:^(NSError * _Nonnull error) {
               // NSLog(@"APostUpdateDevice response error  = %@\n", error.localizedDescription);
                [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
           }];
       });
}

-(void)resetWorkingHour {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostResetWorkingHours andParameters:@{@"mac": self.currDevice.mac } success:^(id _Nonnull response) {
           // NSLog(@"APostResetWorkingHours response  = %@\n", response);
            if([response isEqual:@"1"]) {
                self->deviceSetup.workHour = 0;
                [self.mHoursLb setText:[NSString stringWithFormat:@"0%@", NSLocalizedString(@"h", nil)]];
                //is ok
            } else if ([response isEqual:@"-1"] ) {
                //in not ok
            }
        } failure:^(NSError * _Nonnull error) {
            // NSLog(@"APostResetWorkingHours response error  = %@\n", error.localizedDescription);
             [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
    });
}

-(void)updateSetupPoint {
    NSDictionary * param = [DeviceSetup getSetPointDic:deviceSetup];
    if (deviceSetup) {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostSetupPoint andParameters:param success:^(id _Nonnull response) {
           // NSLog(@"APostSetupPoint response  = %@\n", response);
            if (self->currentDev.tgtTemp < self->deviceSetup.minPoint) {
                self->currentDev.tgtTemp = self->deviceSetup.minPoint;
                [self updateGeneralInfo:self->currentDev];
            } else if (self->currentDev.tgtTemp > self->deviceSetup.maxPoint) {
                self->currentDev.tgtTemp = self->deviceSetup.maxPoint;
                [self updateGeneralInfo:self->currentDev];
            } else {
                [self updateDeviceName];
                 }
        } failure:^(NSError * _Nonnull error) {
            // NSLog(@"APostSetupPoint response error  = %@\n", error.localizedDescription);
             [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
    });
    }
}

-(void)updateGeneralInfo:(AllDevises *)currDevice  {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableDictionary *dict = [[AllDevises  dictionaryForDeviceUpdate:currDevice] mutableCopy];
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostdevsetHvac andParameters:dict success:^(id _Nonnull response) {
            [self updateDeviceName];
            // NSLog(@"response = %@\n", response);
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"error = %@\n", error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
    });
}

-(void)updateLink {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           [[RequestManager sheredMenage] postJsonDataWithUrl:APostLink andParameters:@{@"deviceMac": self.currDevice.mac } success:^(id _Nonnull response) {
              // NSLog(@"APostLink response  = %@\n", response);
               dispatch_async(dispatch_get_main_queue(),  ^{
                   if([response isEqual:@"0"]) {
                    //  [self linkIsSeccess];
                       //device is online
                   } else if ([response isEqual:@"-1"] ) {
                       //device is offline
                       [self linkIsUnseccess];
                   }
               });
           } failure:^(NSError * _Nonnull error) {
                //NSLog(@"APostLink response error  = %@\n", error.localizedDescription);
               self.mLinkBtn.hidden = NO;
               [self.mLinkActivityV stopAnimating];

                [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
           }];
       });
}

-(void)removeLink {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * formatUrl = [NSString stringWithFormat:@"%@%@",APostDeleteLink, self.currDevice.mac];
        [[RequestManager sheredMenage] postJsonDataWithUrl:formatUrl andParameters:@{} success:^(id _Nonnull response) {
           // NSLog(@"APostDeleteLink response  = %@\n", response);
            dispatch_async(dispatch_get_main_queue(),  ^{
                [self.mLinkToPmBackgV setHidden:YES];
                [self.mBaseWattBackgV setHidden:YES];
                [self.mLinkBackgV setHidden:NO];
                [self.mLinkBtn setHidden:NO];
               
            });
        } failure:^(NSError * _Nonnull error) {
            // NSLog(@"APostDeleteLink response error  = %@\n", error.localizedDescription);
             [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
    });
}
-(void)sendBaseWatt {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostUpdateBasewatt andParameters:@{@"mac": self->deviceSetup.mac, @"watt": self->deviceSetup.baseWatt,  } success:^(id _Nonnull response) {
           // NSLog(@"APostUpdateBasewatt response  = %@\n", response);
          
        } failure:^(NSError * _Nonnull error) {
            // NSLog(@"APostUpdateBasewatt response error  = %@\n", error.localizedDescription);
             [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
    });
}

-(void)refreshBaseWatt {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * urlFormar = [NSString stringWithFormat:@"%@?deviceMac=%@",AGetRefreshBasewattSensor, self.currDevice.mac];
        [[RequestManager sheredMenage] getDataWithUrl:urlFormar success:^(id  _Nonnull response) {
           // NSLog(@"AGettRefreshBasewattSensor response  = %@\n", response);
            [self.mBaseWattTextFl setHidden:NO];
            self->deviceSetup.baseWatt = response;
            [self.mBaseWattTextFl setText:[self->deviceSetup.baseWatt stringValue]];
        } failure:^(NSError * _Nonnull error) {
            [self.mBaseWattTextFl setHidden:NO];
           // NSLog(@"AGettRefreshBasewattSensor response error  = %@\n", error.localizedDescription);
        }];
    });
}

-(void)hideDeviceFromApp {
    BOOL isHideSw = NO;
    if (!hiddenDevicesArr) {
        hiddenDevicesArr = [NSMutableArray array];
    }
    for (int i = 0; i < hiddenDevicesArr.count; i++) {
        if ([_currDevice.mac isEqualToString:[[hiddenDevicesArr objectAtIndex:i]mac]] && !deviceSetup.keepSetPointInRange) {
            [hiddenDevicesArr removeObjectAtIndex:i];
            [ACommonTools setObjectValue:hiddenDevicesArr forKey:@"HiddenDevices"];
            break;
        } else if ([_currDevice.mac isEqualToString:[[hiddenDevicesArr objectAtIndex:i]mac]]) {
            isHideSw = YES;
            break;
        }
    }
    if (!isHideSw && deviceSetup.keepSetPointInRange) {
        [hiddenDevicesArr addObject: _currDevice.mac];
        [ACommonTools setObjectValue:hiddenDevicesArr forKey:@"HiddenDevices"];
    }
}




#pragma mark: Actions
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self closeDropDown];
}
-(void)cancel {
    [self closeDropDown];
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)handlerSave {
    [self closeDropDown];
    if ([_currDevice.deviceType isEqualToString:@"PM"]) {
        if (![self.currDevice.name isEqualToString:self.mModifiableNameTetxFl.text]) {
            [self updatePMName];
        } else {
            [self updateMasterPM];
        }
    } else if ([_currDevice.deviceType isEqualToString:@"SW"]) {
        if (![self.currDevice.name isEqualToString:self.mModifiableNameTetxFl.text]) {
            [self updatePMName];
        }
        [self hideDeviceFromApp];
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self updateSetupPoint];
    }
}
- (IBAction)refreshCounter:(UIButton *)sender {
    isRefreshWorkHours = YES;
    [self popupAlert:NSLocalizedString(@"reset_work_h", nil) frame:CGRectMake(0, -44, 230, 170) ok:NSLocalizedString(@"yes", nil) cencel:NSLocalizedString(@"no", nil) delegate:self];
}

- (IBAction)masterPm:(UIButton *)sender {
    if ([sender.currentImage isEqual:Image(@"square_check")]) {
        [sender setImage:Image(@"square_uncheck") forState:UIControlStateNormal];
        self.currDevice.masterPm = NO;
        self.mLinkBackgV.hidden = NO;

    } else {
        [sender setImage:Image(@"square_check") forState:UIControlStateNormal];
        self.currDevice.masterPm = YES;
        self.mLinkBackgV.hidden = YES;
       // self.mLinkToPmBackgV.hidden = NO;
        self.mDeleteLinkBtn.hidden = YES;
        self.mLinkBackgV.hidden = NO;
        
    }
}

- (IBAction)keepSetPoint:(UIButton *)sender {
    if ([sender.currentImage isEqual:Image(@"circle_check")]) {
        [sender setImage:Image(@"uncheck") forState:UIControlStateNormal];
        deviceSetup.keepSetPointInRange = NO;
    } else {
        [sender setImage:Image(@"circle_check") forState:UIControlStateNormal];
        deviceSetup.keepSetPointInRange = YES;
    }
}

- (IBAction)link:(UIButton *)sender {
    [sender setHidden:YES];
    [_mLinkActivityV startAnimating];
    [self updateLink];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self updateByFeedback:nil];
        [self linkIsSeccess];

    });
    
}

- (IBAction)deleteLink:(UIButton *)sender {
    isDeleteLink = YES;
    self.navigationItem.titleView = [self setNavigationTitle:@"device_setup"];

    [self popupAlert:NSLocalizedString(@"delete_link", nil)
               frame:CGRectMake(0, -44, 230, 170) ok:@"YES"
              cencel:NSLocalizedString(@"no", nil) delegate:self];
}

- (IBAction)baseWattRefresh:(UIButton *)sender {
     [self onDevice];
       [self.mBaseWettActivityV startAnimating];
       [self.mBaseWattTextFl setHidden:YES];
       dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC));
       dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
           [self.mBaseWettActivityV stopAnimating];
           [self refreshBaseWatt];
       });
}

- (IBAction)mDropDownBtn:(UIButton *)sender {
    
    if ([self.mTriangelBtn.imageView.image isEqual:[UIImage imageNamed:@"down_triangle"]]) {
        [self showDropDownForButton:self.mDropDownBtn adContents:DevicesNameList];
        [self.mTriangelBtn setImage:[UIImage imageNamed:@"up_triangle"] forState:UIControlStateNormal];
    } else {
        [self closeDropDown];
    }
}

-(void)onDevice {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] getDataWithUrl:[NSString stringWithFormat:@"%@/%@?on=true", AGetdevswitchHvac, self->deviceSetup.mac] success:^(id  _Nonnull response) {
            
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"error = %@\n", error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
            
        }];
    });
}
-(void)showDropDownForButton:(UIButton *)sender adContents:(NSArray *)contents {
    [_dropdown setDrodownAnimation:rand()%2];
    [_dropdown setupDropdownForView:sender];
    [_dropdown setSeparatorColor:sender.titleLabel.textColor];
    [_dropdown reloadDropdownWithContents:contents andSelectedItems:@[[sender titleForState:UIControlStateNormal]]];
}
-(void)closeDropDown {
    [self.mTriangelBtn setImage:[UIImage imageNamed:@"down_triangle"] forState:UIControlStateNormal];
    [_dropdown remove];
}
#pragma mark: UItextFiled Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.mBaseWattTextFl) {
        NSScanner *scanner = [NSScanner scannerWithString:textField.text];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        if (isNumeric) {
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber *baseWatt = [f numberFromString:textField.text];
            deviceSetup.baseWatt = baseWatt;
            [self sendBaseWatt];
            [textField resignFirstResponder];
        } else {
            [self showAlert:NSLocalizedString(@"enter_num", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }
    } else if (textField == self.mModifiableNameTetxFl) {
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
                                                       replacementString:(NSString *)string {
    if (textField == self.mBaseWattTextFl) {
           NSScanner *scanner = [NSScanner scannerWithString:textField.text];
           BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
           if (isNumeric) {
               NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
               f.numberStyle = NSNumberFormatterDecimalStyle;
               NSNumber *baseWatt = [f numberFromString:textField.text];
               deviceSetup.baseWatt = baseWatt;
               [self sendBaseWatt];
               [textField resignFirstResponder];
           } else {
               [self showAlert:NSLocalizedString(@"enter_num", nil)ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
           }
       } else if (textField == self.mModifiableNameTetxFl) {
           
       }
    return YES;
}
- (void)textFieldDidChangeSelection:(UITextField *)textField  {
}

#pragma mark TTRangeSliderViewDelegate
-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum{
        //NSLog(@"Currency slider updated. Min Value: %.1f Max Value: %.1f", selectedMinimum, selectedMaximum);
    deviceSetup.maxPoint = roundf(selectedMaximum);
    deviceSetup.minPoint = roundf(selectedMinimum);
}

#pragma mark - VSDropdown Delegate methods.
- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected {
    
    if (selected) {
        UIButton *btn = (UIButton *)dropDown.dropDownView;
        NSString *allSelectedItems = nil;
        allSelectedItems = [dropDown.selectedItems firstObject];
        [btn setTitle:allSelectedItems forState:UIControlStateNormal];
    }
    [self.mTriangelBtn setImage:[UIImage imageNamed:@"down_triangle"] forState:UIControlStateNormal];

}
#pragma mark: Popup  Delegate
-(void)handlerYes {
   // NSLog(@"Popup  Delegate Popup  Delegate\n");
    self.navigationItem.titleView = [self setNavigationTitle:@"aircon_setup"];

    if (isRefreshWorkHours){
        [self resetWorkingHour];
        isRefreshWorkHours = NO;
    } else if (isDeleteLink) {
        isDeleteLink = NO;
        [self removeLink];
    }

}
-(void)handlerCancel {
    self.navigationItem.titleView = [self setNavigationTitle:@"aircon_setup"];
    isDeleteLink = NO;
    isRefreshWorkHours = NO;
}
#pragma mark: Scroll Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self closeDropDown];

}

#pragma mark - Notification
-(void) updateByFeedback:(NSNotification *) notification
{
    NSDictionary *dict = notification.userInfo;
    if (dict != nil) {
        if ([[dict valueForKey:@"exist"] boolValue]) {
            deviceSetup.powerMeter = [[DevicePM alloc] init];
            deviceSetup.powerMeter.phaseNumber = [dict valueForKeyPath:@"phaseNumber"];
            deviceSetup.powerMeter.mac = [dict valueForKeyPath:@"powerMeter.mac"];
            deviceSetup.powerMeter.name = [dict valueForKeyPath:@"powerMeter.name"];
            deviceSetup.powerMeter.pmId = [dict valueForKeyPath:@"powerMeter.id"];
            [self updatePmField];
            
        }
        isResiveFeedback = YES;
        //NSLog(@"Power maeter Feedback = %@ \n", dict);
        [self linkIsSeccess];
    }
}

@end
