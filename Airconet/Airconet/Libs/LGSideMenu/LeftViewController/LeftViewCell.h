//
//  LeftViewCell.h
//  LGSideMenuControllerDemo
//






#import <UIKit/UIKit.h>

@interface LeftViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *titleLeadingLayoutv;

//@property (assign, nonatomic) IBOutlet UIView *separatorView;

@end
