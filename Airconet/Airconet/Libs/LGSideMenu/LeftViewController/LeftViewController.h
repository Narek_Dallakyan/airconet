//
//  LeftViewController.h
//  LGSideMenuControllerDemo
//

#import <UIKit/UIKit.h>

@interface LeftViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *mTableV;
@property (weak, nonatomic) IBOutlet UILabel *mAcountLb;
@property (weak, nonatomic) IBOutlet UIButton *mSignOutBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mVertionLbCenterLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mUserImgLeadingLayoutV;
@property (strong, nonatomic) IBOutlet UIView *mLeftFirstV;
@property (strong, nonatomic) IBOutlet UIView *mLeftSecondV;

@end
