//
//  LeftViewController.m
//  LGSideMenuControllerDemo
//

#import "LeftViewController.h"
#import "LeftViewCell.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "LoginViewController.h"
#import "RequestManager.h"
#import "ACommonTools.h"
#import "APublicDefine.h"
#import "BaseViewController.h"

@interface LeftViewController ()<PopupViewDelegate> {
    BOOL isPlane;
}

@property (strong, nonatomic) NSMutableArray *titlesArray;
@property (weak, nonatomic) IBOutlet UIView *firtsView;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondVBottomLayout;
@property (weak, nonatomic) IBOutlet UILabel *versionLb;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *titleLeadingLayoutV;

@end

@implementation LeftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setViewWithLang];

    [[NSNotificationCenter defaultCenter]
    addObserver:self selector:@selector(bacakhroundModeColor:) name:NotificationBackgroundMode object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenuList:) name:NotificationReloadMenu object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenuList:) name:NotificationReloadMenuHoliday object:nil];
    
    NSArray * arr =  @[NSLocalizedString(@"timer", nil),
                       NSLocalizedString(@"holiday", nil),
                       NSLocalizedString(@"messages", nil),
                       NSLocalizedString(@"tech_supp", nil),
                       NSLocalizedString(@"device_setup", nil),
                       NSLocalizedString(@"group_setup", nil),
                       NSLocalizedString(@"system_setup", nil),
                       NSLocalizedString(@"power_meter", nil),
                       NSLocalizedString(@"air_quality", nil),
                       NSLocalizedString(@"motion_sensor", nil),
                       @"",
                       NSLocalizedString(@"desktop", nil)];
    self.titlesArray = arr.mutableCopy;
    
    self.mAcountLb.text = [ACommonTools getObjectValueForKey:AKeyCurrentUserName];
    self.firtsView.layer.cornerRadius = 8.f;
    self.secondView.layer.cornerRadius = 8.f;
    self.firtsView.layer.borderWidth = 2.f;
    self.secondView.layer.borderWidth = 2.f;
    self.firtsView.layer.borderColor = RGB(127, 127, 127).CGColor;
    self.secondView.layer.borderColor = RGB(127, 127, 127).CGColor;
    
    self.mLeftFirstV.layer.cornerRadius = 8.f;
    self.mLeftSecondV.layer.cornerRadius = 8.f;
    self.mLeftFirstV.layer.borderWidth = 2.f;
    self.mLeftSecondV.layer.borderWidth = 2.f;
    self.mLeftFirstV.layer.borderColor = RGB(127, 127, 127).CGColor;
    self.mLeftSecondV.layer.borderColor = RGB(127, 127, 127).CGColor;
    self.mVertionLbCenterLayout.constant = 60;
}
-(void) viewWillAppear:(BOOL)animated {
    self.view.backgroundColor = [UIColor blackColor];
    [self setBackgroundModeColor];
    
}
-(void)viewWillLayoutSubviews {
//    if ([BaseViewController isDeviceLanguageRTL]) { //Arabic, Hebrew
//        self.mSignOutBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
//    }
}
-(void)setViewWithLang {
    if ([BaseViewController isDeviceLanguageRTL]) { //Arabic, Hebrew
        self.mSignOutBtn.titleLabel.textAlignment = NSTextAlignmentRight;
        self.mUserImgLeadingLayoutV.constant = 50;
        [self.mLeftFirstV setHidden:NO];
        [self.mLeftSecondV setHidden:NO];
        [self.firtsView setHidden:YES];
        [self.secondView setHidden:YES];
    } else {
        self.mUserImgLeadingLayoutV.constant = 10;
        [self.mLeftFirstV setHidden:YES];
        [self.mLeftSecondV setHidden:YES];
        [self.firtsView setHidden:NO];
        [self.secondView setHidden:NO];
    }

}
-(void) setBackgroundModeColor {
    if ([[[BaseViewController alloc] init] isDarkMode]) {
        _firtsView.backgroundColor = RGB(38, 38, 38);
        _secondView.backgroundColor = RGB(38, 38, 38);
        _mLeftFirstV.backgroundColor = RGB(38, 38, 38);
        _mLeftSecondV.backgroundColor = RGB(38, 38, 38);
    } else {
        _firtsView.backgroundColor = [UIColor whiteColor];
        _secondView.backgroundColor = [UIColor whiteColor];
        _mLeftFirstV.backgroundColor = [UIColor whiteColor];
        _mLeftSecondV.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark: BackgroundModeColor Notifivation
-(void) bacakhroundModeColor:(NSNotification *) notification {
     NSDictionary *dict = notification.userInfo;
       if (dict != nil) {
           if ([[dict valueForKey:@"BackgroundModeCheck"] boolValue]) {
               [self setBackgroundModeColor];
           }
       }
}

#pragma mark: Reload Menu Notifivation
-(void) updateMenuList:(NSNotification *) notification {
    NSDictionary *dict = notification.userInfo;
    if (dict != nil) {
        if ([dict valueForKey:@"messagesCount"] &&  ![[dict valueForKey:@"messagesCount"] isEqualToString:@"0"]) {
            [self.titlesArray replaceObjectAtIndex:2 withObject:[NSString stringWithFormat:@"%@ (%@)", NSLocalizedString(@"messages", nil), [dict valueForKey:@"messagesCount"]]];
            [self.mTableV reloadData];
        }else if ([dict valueForKey:@"messagesCount"] && [[dict valueForKey:@"messagesCount"] isEqualToString:@"0"]) {
            [self.titlesArray replaceObjectAtIndex:2 withObject:NSLocalizedString(@"messages", nil)];
            [self.mTableV reloadData];
        }
        else if ([[dict valueForKey:@"holidayCheckmark"] integerValue] > 0) {
            isPlane = YES;
           // [self.titlesArray replaceObjectAtIndex:1 withObject:@"Holiday √"];
            [self.mTableV reloadData];
        }  else if ([dict valueForKey:@"holidayCheckmark"] && [[dict valueForKey:@"holidayCheckmark"] integerValue] == 0) {
            isPlane = NO;
           // [self.titlesArray replaceObjectAtIndex:1 withObject:@"Holiday"];
            [self.mTableV reloadData];
        }
    }
}

- (IBAction)signOut:(UIButton *)sender {
    PopupView * customAlert = [[PopupView alloc] initWithFrame:CGRectMake(0, -44, 230, 170) message:NSLocalizedString(@"want_sign_out", nil) delegate:self];
       customAlert.mYesBtn.titleLabel.text = NSLocalizedString(@"yes", nil);
    customAlert.mAlertLb.text = NSLocalizedString(@"sign_out", nil);
    customAlert.mErrorImg.hidden = YES;
       customAlert.frame = CGRectMake(SCREEN_Width/2 - customAlert.frame.size.width/2, SCREEN_Height/2 - customAlert.frame.size.height/2 - 64, customAlert.frame.size.width, customAlert.frame.size.height);
       customAlert.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    UIWindow * keyWindow =  [BaseViewController keyWindow];

       [keyWindow addSubview:customAlert];
       for (UIView *view in keyWindow.subviews) {
           view.userInteractionEnabled=NO;
           if (![view isKindOfClass:[PopupView class]]) {
               [view setAlpha:0.5];
           }
       }
       customAlert.userInteractionEnabled=YES;
       [[[BaseViewController alloc] init] animationPopup:customAlert];

}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationFade;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titlesArray.count;
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LeftViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

    cell.userInteractionEnabled = (indexPath.row == 10) ?  NO : YES;
    NSAttributedString * attString = [BaseViewController getAttributStringPossition:@"√" text:self.titlesArray[indexPath.row] color:[UIColor redColor]];
    NSAttributedString * secondAttString = [BaseViewController getAttributStringPossition:@"(" text:self.titlesArray[indexPath.row] color:[UIColor redColor]];
    
    if (attString.length > 0) {
        [ cell.titleLabel setAttributedText: attString];
    } else if (secondAttString.length > 0) {
        [ cell.titleLabel setAttributedText: secondAttString];
    }else {
        cell.titleLabel.text = self.titlesArray[indexPath.row];

    }
    if (isPlane && indexPath.row == 1) {
        [cell.image setHidden:NO];
    } else {
        [cell.image setHidden:YES];
    }
    if ([BaseViewController isDeviceLanguageRTL]) { //Arabic, Hebrew
        cell.titleLeadingLayoutv.constant = 60;
    } else {
        cell.titleLeadingLayoutv.constant = 10;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 10) {
        return 30.0;
    } else if ([self.titlesArray[indexPath.row] length] > 15) {
        return 60.0;
    }
    return 37;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    UIViewController *viewController;

    switch (indexPath.row) {
        case 0:
            //Timer
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TimerViewController"];
            break;
        case 1:
            //Holiday
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HolidayViewController"];
            
            break;
        case 2:
            //Messages
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagesViewController"];
            break;
        case 3:
            //Tech support
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TechSupportViewController"];
            break;
        case 4:
              viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DevicesViewController"];
            //Device setup
            break;
        case 5:
            //Group setup
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"GroupsViewController"];
            break;
        case 6:
            //System setup
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"GeneralSetupViewController"];

            break;
        case 7:
            //Power meter
            [ACommonTools setBoolValue:YES forKey:@"isPm"];
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PowerMeterViewController"];

            break;
        case 8:
            //Air quality
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AirQualityViewController"];
            break;
        case 9:
            //Motion sensor
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MotionSensorSettingsViewController"];
            break;
        case 10:
            //empty
            break;
        case 11:
            //Desktop
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DesktopViewController"];
            break;
            
        default:
            break;
    }
     UINavigationController *navigationController = (UINavigationController *)mainViewController.rootViewController;
    [navigationController pushViewController:viewController animated:YES];
    [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
    if([[ACommonTools getObjectValueForKey:@"loginFirstTime"] isEqual:@"yes"] && ![ACommonTools getBoolValueForKey:KeyRememberAdmin]) {
              [ACommonTools setObjectValue:@"no" forKey:@"loginFirstTime"] ;
              [ACommonTools setBoolValue:NO forKey:KeyIsUnLockMenu];
          }
    
}

#pragma mark: Popup Alert
-(void)handlerYes {
    [[RequestManager sheredMenage] getJsonDataWithUrl:AGetlogout
                                               andParameters:@{}
                                                isNeedCookie:NO
                                                     success:^(id response) {
              
        if ([ACommonTools getBoolValueForKey:KeyIsAdminLock]) {
            [ACommonTools setBoolValue:NO forKey:KeyIsUnLockMenu];
        } else {
            [ACommonTools setBoolValue:YES forKey:KeyIsUnLockMenu];
        }
        [ACommonTools setBoolValue:NO forKey:AKeyLastLoginSuccess];
        [ACommonTools setBoolValue:NO forKey:KeyRememberAdmin];
        [ACommonTools removeValueForKey:AKeyRemotedeviceToken];
        [ACommonTools removeValueForKey:AKeyPersistentCookie];
        [ACommonTools removeValueForKey:AKeyCurrentUserPwd];
        [ACommonTools removeValueForKey:AKeyCurrentUserName];
        [ACommonTools removeValueForKey:AKeyCurrentUserName];

        

        UIStoryboard *mainSB= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController * loginVc = mainSB.instantiateInitialViewController;
        UIWindow * window = [BaseViewController keyWindow];
        window.rootViewController = loginVc;
    } failure:^(NSError *error) {
       // NSLog(@"logout error = %@\n", error.description);
        //error Alert
    }];
}
@end
