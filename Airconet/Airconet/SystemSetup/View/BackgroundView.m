//
//  BackgroundView.m
//  Airconet
//
//  Created by Karine Karapetyan on 07-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BackgroundView.h"
#import "APublicDefine.h"

@implementation BackgroundView

-(void)awakeFromNib {
    [super awakeFromNib];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setupView];
    // Configure the view for the selected state
}
-(void) setupView {
    switch ([_backgroundMode integerValue]) {
        case 0://Dark
            [_mLightBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
            [_mAutoBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
            [_mDarkBtn setTitleColor:BaseColor forState:UIControlStateNormal];

            break;
        case 1://Light
            [_mDarkBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
            [_mAutoBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
 [_mLightBtn setTitleColor:BaseColor forState:UIControlStateNormal];
            break;
        default://Auto
            [_mLightBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
            [_mDarkBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
[_mAutoBtn setTitleColor:BaseColor forState:UIControlStateNormal];
            break;
    }
}
- (IBAction)dark:(UIButton *)sender {
    [_mLightBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
    [_mAutoBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
    [_mDarkBtn setTitleColor:BaseColor forState:UIControlStateNormal];
    [self.delegate backgroundMode:@0];
    
}
- (IBAction)light:(UIButton *)sender {
    [_mDarkBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
    [_mAutoBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
    [_mLightBtn setTitleColor:BaseColor forState:UIControlStateNormal];
    [self.delegate backgroundMode:@1];

}
- (IBAction)autoDefault:(UIButton *)sender {
    [_mDarkBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
    [_mLightBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
    [_mAutoBtn setTitleColor:BaseColor forState:UIControlStateNormal];
    [self.delegate backgroundMode:@2];

}

@end
