//
//  HideDevicesView.h
//  Airconet
//
//  Created by Karine Karapetyan on 07-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol HideDevicesViewDelegate <NSObject>
-(void)handlerHideDevice;
@end
@interface HideDevicesView : UITableViewCell
@property(weak, nonatomic) id<HideDevicesViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel * mHideDeviceLb;
@property (weak, nonatomic) IBOutlet UIButton * mRightDurBtn;
@end

NS_ASSUME_NONNULL_END
