//
//  BackgroundView.h
//  Airconet
//
//  Created by Karine Karapetyan on 07-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol BackgroundViewDelegate <NSObject>
-(void)backgroundMode:(NSNumber *)mode;
@end
@interface BackgroundView : UITableViewCell
@property(weak, nonatomic) id<BackgroundViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel * mBackgroundLb;
@property (weak, nonatomic) IBOutlet UIButton * mDarkBtn;
@property (weak, nonatomic) IBOutlet UIButton * mLightBtn;
@property (weak, nonatomic) IBOutlet UIButton * mAutoBtn;
@property (strong, nonatomic) NSNumber * backgroundMode;


@end

NS_ASSUME_NONNULL_END
