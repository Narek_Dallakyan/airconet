//
//  PowerMeterSettingsView.h
//  Airconet
//
//  Created by Karine Karapetyan on 07-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSDropdown.h"

NS_ASSUME_NONNULL_BEGIN
@protocol PowerMeterSettingsDelegate<NSObject>

-(void)handlerCountry:(NSNumber *)countryId;
-(void)handlerVoltage:(NSNumber *)volt;
-(void)handlerDays:(NSString *)day electricBillId:(NSNumber *)electricBillId;
-(void)handlerPrices:(NSString *)price electricBillId:(NSNumber *)electricBillId;
-(void)handlerSizeOfLocation:(NSString *)sqm;
-(void)handlerMaxPower:(NSString *)kw;
-(void)handlerRefresh;

-(void)errorTextFiled:(UITextField *)txtFl;
@end
@interface PowerMeterSettingsView : UITableViewCell {
   // VSDropdown *_dropdown;

}
@property (weak, nonatomic) id<PowerMeterSettingsDelegate> delegate;
@property (strong, nonatomic) VSDropdown * dropdown;
@property (weak, nonatomic) IBOutlet UILabel * mPowerMeterLb;
@property (weak, nonatomic) IBOutlet UILabel * mCountryLb;
@property (weak, nonatomic) IBOutlet UIButton * mSelectCountryBtn;
@property (weak, nonatomic) IBOutlet UIButton * mSelectCountryTriangle;
@property (strong, nonatomic) IBOutlet UILabel *mBaseElectricBillLB;

@property (weak, nonatomic) IBOutlet UILabel * mSizeOfLocationLb;
@property (weak, nonatomic) IBOutlet UITextField * mSizeOfLocationTxtFl;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *mDayDropDownCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *mMonthCollection;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *mDayCollectionBtn;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *mDropDownCollectionBtn;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *mPriceCollectionTxtFl;
@property (strong, nonatomic) IBOutlet UIView *mBaseElectricBackView;
@property (strong, nonatomic) IBOutlet UILabel *mMaxPmLb;
@property (strong, nonatomic) IBOutlet UITextField *mMaxPmTxtFl;
@property (strong, nonatomic) IBOutlet UIView *mVoltageBackV;
@property (strong, nonatomic) IBOutlet UILabel *mVoltageLb;
@property (strong, nonatomic) IBOutlet UIButton *mVoltageDropDownBtn;
@property (strong, nonatomic) IBOutlet UIButton *mVoltageTriangleBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mBaseElectricTopLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mPowerMeterBackgVHightLayout;


- (IBAction)dayDropDown:(UIButton *)sender;

@property (assign, nonatomic) BOOL * isDefault;
@property (weak, nonatomic) NSArray * countryList;
@property (weak, nonatomic) NSArray *baseElectricBillArr;
@property (strong, nonatomic) UIColor * dropDownColor;
@property (assign, nonatomic) BOOL isMasterPm;
@property (assign, nonatomic) BOOL isMasterPmOffline;

@end

NS_ASSUME_NONNULL_END
