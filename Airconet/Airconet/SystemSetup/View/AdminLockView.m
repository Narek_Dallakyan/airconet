//
//  AdminLockView.m
//  Airconet
//
//  Created by Karine Karapetyan on 07-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "AdminLockView.h"

@implementation AdminLockView

-(void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setupView];
    // Configure the view for the selected state
}
-(void)setupView {
    if (_isAdminLock) {
        [_mAdminLockRadioBtn setImage:[UIImage imageNamed:@"light_check"] forState:UIControlStateNormal];
    } else {
        [_mAdminLockRadioBtn setImage:[UIImage imageNamed:@"light_uncheck"] forState:UIControlStateNormal];
    }
}
- (IBAction)adminLock:(UIButton *)sender {
    if (_isAdminLock) {
        [_mAdminLockRadioBtn setImage:[UIImage imageNamed:@"light_uncheck"] forState:UIControlStateNormal];
    } else {
        [_mAdminLockRadioBtn setImage:[UIImage imageNamed:@"light_check"] forState:UIControlStateNormal];
    }
    _isAdminLock = !_isAdminLock;
    [self.delegate handlerAdminLock:_isAdminLock];
}

@end
