//
//  AdminLockView.h
//  Airconet
//
//  Created by Karine Karapetyan on 07-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AdminLockViewDelegate <NSObject>

-(void)handlerAdminLock:(BOOL)isAdminLock;
@end

@interface AdminLockView : UITableViewCell
@property(weak, nonatomic) id<AdminLockViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel * mAdminLockLb;
@property (weak, nonatomic) IBOutlet UILabel * mInfoLb;
@property (weak, nonatomic) IBOutlet UIButton * mAdminLockRadioBtn;
@property (assign, nonatomic) BOOL isAdminLock;
@end

NS_ASSUME_NONNULL_END
