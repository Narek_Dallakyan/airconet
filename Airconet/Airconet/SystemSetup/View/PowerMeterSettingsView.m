//
//  PowerMeterSettingsView.m
//  Airconet
//
//  Created by Karine Karapetyan on 07-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "PowerMeterSettingsView.h"
#import "ACommonTools.h"
#import "APublicDefine.h"
#import "UITextField+Content.h"


typedef enum DropDownType {
    country = 0,
    month,
    voltage
}DropDownType;

@interface PowerMeterSettingsView () <VSDropdownDelegate > {
    UIButton * currTriangleBtn;
    NSInteger currDropDownTag;
    NSMutableArray * countryNameArr;
    NSMutableArray * countryIdArr;
    BOOL isDigit;
}
@end

enum DropDownType dropDownType;
@implementation PowerMeterSettingsView
-(void)awakeFromNib {
    [super awakeFromNib];
    if (SCREEN_Width < 370) {
           [_mCountryLb setFont:[UIFont fontWithName:@"Calibri" size:13]];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if ([ACommonTools getBoolValueForKey:@"CloseDropDown"]) {
        [_dropdown remove];
        [self closeAllDropDowns];
        [_dropdown.tableView removeFromSuperview];
    } else{
        [_dropdown remove];
              [self closeAllDropDowns];
              [_dropdown.tableView removeFromSuperview];
        [self dropDownSetup];
    }
    [self setupView];    // Configure the view for the selected state
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    //NSLog(@"touchesBegan");
    [self closeDropDown];
}
-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    //NSLog(@"touchesMoved");
    [self closeDropDown];
}
-(void)closeDropDown {
    [self closeAllDropDowns];
    [_dropdown remove];
}
-(void)setupView {
    for (UIView * day in self.mDayDropDownCollection) {
        day.layer.masksToBounds = YES;
        day.layer.borderWidth = 1.f;
        day.layer.borderColor=[BaseColor CGColor];
    }
    for (UITextField * txtFl in self.mPriceCollectionTxtFl) {
        [txtFl setBlueBorderStyle:1.f];
    }
    for (UIButton * btn in self.mDropDownCollectionBtn) {
        [btn setImage:Image(@"syst_drop_down_img") forState:UIControlStateNormal];
    }
    [self.mSizeOfLocationTxtFl setBlueBorderStyle];
    [self.mMaxPmTxtFl setBlueBorderStyle];
       if (self.isMasterPm) {
         [self.mBaseElectricBackView setHidden:YES];
         [self.mVoltageBackV setHidden:YES];
         self.mBaseElectricTopLayout.constant = 80;
         self.mPowerMeterBackgVHightLayout.constant = 500;
        [self addButtonToRightViewTextField];

     } else {
         [self.mBaseElectricBackView setHidden:NO];
         [self.mVoltageBackV setHidden:NO];
         self.mBaseElectricTopLayout.constant = 150;
         self.mPowerMeterBackgVHightLayout.constant = 570;
       if (SCREEN_Width < 370) {
           self.mPowerMeterBackgVHightLayout.constant = 800;
        }
     }
    
    [self sortCountryList];
    [self setValue];
}
-(void)addButtonToRightViewTextField {
    UIButton * refreshBtn = [[UIButton alloc] init];
       [refreshBtn addTarget:self action:@selector(refresh) forControlEvents:UIControlEventTouchUpInside];
    [refreshBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 5)];//(top, left, bottom, right)
    if (_isMasterPmOffline) {
        [self.mMaxPmTxtFl setButtonInRightView:refreshBtn buttonImg:[UIImage imageNamed: @"warning_small"]];
    } else {
       [self.mMaxPmTxtFl setButtonInRightView:refreshBtn buttonImg:[UIImage imageNamed: @"refersh_double"]];
    }
}

-(void)sortCountryList {
    countryNameArr = [NSMutableArray array];
    countryIdArr = [NSMutableArray array];
    for (NSDictionary * dic in _countryList) {
        [countryIdArr addObject:[dic valueForKey:@"id"]];
        [countryNameArr addObject:[dic valueForKey:@"name"]];
    }
}

-(void)setValue {
    for (int i = 0; i < _baseElectricBillArr.count; i++) {
        NSDictionary * dic = [_baseElectricBillArr objectAtIndex:i];
        
        UILabel * lb = [_mMonthCollection objectAtIndex:i];
        lb.tag = [[dic valueForKey:@"month"] intValue];
        lb.text = [MonthNameList objectAtIndex:lb.tag-1];
       
        
        UIButton * trianglBtn = [_mDropDownCollectionBtn objectAtIndex:i];
        trianglBtn.tag = [[dic valueForKey:@"month"] intValue];

        UIButton * btn =  [_mDayCollectionBtn objectAtIndex:i];
        btn.tag = [[dic valueForKey:@"month"] intValue];
        if ([[dic valueForKey:@"day"] stringValue].length == 1) {
            [btn setTitle:[NSString stringWithFormat:@"0%@", [[dic valueForKey:@"day"] stringValue]] forState:UIControlStateNormal];
        } else {
            [btn setTitle:[[dic valueForKey:@"day"] stringValue] forState:UIControlStateNormal];
        }
        
        UITextField * txtFl = [_mPriceCollectionTxtFl objectAtIndex:i];
        txtFl.tag = [[dic valueForKey:@"month"] intValue];
        txtFl.text = [[dic valueForKey:@"price"] stringValue];
       // NSLog(@"Dic = %@\n", dic);
        //NSLog(@"txtFl.text = %@\n", txtFl.text);
    }
}
-(void)setLableBorder:(UILabel *)lb {
    lb.layer.borderWidth = 1.f;
    lb.layer.borderColor=[[UIColor blackColor] CGColor];
}
-(void) dropDownSetup {
    _dropdown = [[VSDropdown alloc]initWithDelegate:self];
    [_dropdown setAdoptParentTheme:YES];
    [_dropdown setShouldSortItems:YES];
    _dropdown.allowMultipleSelection = NO;
    _dropdown.textColor = BaseColor;
    _dropdown.separatorColor = BaseColor;
    _dropdown.tableView.backgroundColor = _dropDownColor;
    [self closeAllDropDowns];
    [ACommonTools setObjectValue:@0 forKey:@"DropDownX"];
}
-(void)closeAllDropDowns {
    [self.mSelectCountryTriangle setImage:[UIImage imageNamed:@"down_triangle"] forState:UIControlStateNormal];
    [self.mVoltageTriangleBtn setImage:[UIImage imageNamed:@"down_triangle"] forState:UIControlStateNormal];
    for (UIButton * btn in self.mDropDownCollectionBtn) {
        [btn setImage:Image(@"syst_drop_down_img") forState:UIControlStateNormal];
    }
}

-(void)dropDownHandler:(UIButton *)triangle  dropDown:(UIButton *)dropDown adContents:(NSArray *)contents {
    currTriangleBtn = triangle;
    [_dropdown remove];
    [self closeAllDropDowns];
    for (UIButton *trBtn in _mDropDownCollectionBtn) {
        if (trBtn.tag == dropDown.tag) {
            triangle = trBtn;
            currTriangleBtn = triangle;
        }
    }
    
    UIImage * imgDown;
    UIImage * imgUp;
    if (dropDownType == country || dropDownType == voltage) {
        imgDown = Image(@"down_triangle");
        imgUp = Image(@"up_triangle");
    } else {
        imgDown = Image(@"syst_drop_down_img");
        imgUp = Image(@"syst_drop_up_img");
    }
    if ([triangle.imageView.image isEqual:imgDown]) {
        [self showDropDownForButton:dropDown  adContents:contents];
        [triangle setImage:imgUp forState:UIControlStateNormal];
    } else {
        [triangle setImage:imgDown forState:UIControlStateNormal];
        [_dropdown remove];
    }
}
-(void)showDropDownForButton:(UIButton *)sender adContents:(NSArray *)contents {
    [_dropdown setDrodownAnimation:rand()%2];
    [_dropdown setupDropdownForView:sender];
    [_dropdown setSeparatorColor:sender.titleLabel.textColor];
    [_dropdown reloadDropdownWithContents:contents andSelectedItems:@[[sender titleForState:UIControlStateNormal]]];
}

-(void)refresh {
    if (!_isMasterPmOffline) {
        [self.delegate handlerRefresh];
    }
}

//Select country
- (IBAction)selecteCountryDropDown:(UIButton *)sender {
    dropDownType = country;
    [ACommonTools setObjectValue:@100 forKey:@"DropDownWidth"];
    [self dropDownHandler:self.mSelectCountryTriangle dropDown:sender adContents:countryNameArr];
}
- (IBAction)selectCountryTriangle:(UIButton *)sender {
    dropDownType = country;
    [ACommonTools setObjectValue:@100 forKey:@"DropDownWidth"];
    [self dropDownHandler:sender dropDown:self.mSelectCountryBtn adContents:countryNameArr];
}
- (IBAction)selectVoltageDropDown:(UIButton *)sender {
    dropDownType = voltage;
    [ACommonTools setObjectValue:@80 forKey:@"DropDownWidth"];
    [self dropDownHandler:self.mVoltageTriangleBtn dropDown:sender adContents:VoltageList];
}
- (IBAction)selectVoltageTriangle:(UIButton *)sender {
    dropDownType = voltage;
    [ACommonTools setObjectValue:@80 forKey:@"DropDownWidth"];
    [self dropDownHandler:sender dropDown:self.mVoltageDropDownBtn adContents:VoltageList];
}


#pragma mark: UItextFiled Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSMutableString * newStr = [NSMutableString stringWithString:textField.text];
    if(string.length > 0) {
        [newStr insertString:string atIndex:range.location];
    } else {
       newStr = [[newStr stringByReplacingCharactersInRange:range withString:@""] mutableCopy];
    }
    if (newStr.length == 0) {
        newStr = [NSMutableString stringWithString:@"0"];
    }
//         NSScanner *scanner = [NSScanner scannerWithString:newStr];
//         BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
         if ([self isNumeric:newStr]) {
             if ([textField isEqual:self.mSizeOfLocationTxtFl]) {//sqm
                 [self.delegate handlerSizeOfLocation:newStr];
             } else if ([textField isEqual:self.mMaxPmTxtFl]) { //kw
                 [self.delegate handlerMaxPower:newStr];
             } else {//price
                 [self.delegate handlerPrices:newStr electricBillId:[self getId:textField.tag]];
             }
         } else {
            [self.delegate errorTextFiled:textField];
         }
    return YES;
}
-(BOOL)isNumeric:(NSString *)str {
     NSScanner *scanner = [NSScanner scannerWithString:str];
     BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
     if (isNumeric) {
         return YES;
     } else {
         NSInteger point = [[str componentsSeparatedByString:@"."] count];
         if (point == 2) {
             str = [str stringByReplacingOccurrencesOfString:@"." withString:@""];
             NSScanner *scanner1 = [NSScanner scannerWithString:str];
             BOOL isNumeric1 = [scanner1 scanInteger:NULL] && [scanner1 isAtEnd];
             if (isNumeric1) {
                return YES;
             }
         }
     }
    return NO;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - VSDropdown Delegate methods.
- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected {
    NSString *allSelectedItems = nil;
    if (selected) {
        UIButton *btn = (UIButton *)dropDown.dropDownView;
        allSelectedItems = [dropDown.selectedItems firstObject];
        [btn setTitle:allSelectedItems forState:UIControlStateNormal];
    }
    if (dropDownType == country) {
        long index = [countryNameArr indexOfObject:allSelectedItems];
        
        [currTriangleBtn setImage:Image(@"down_triangle") forState:UIControlStateNormal];
        if (index >= 0 && index <= countryNameArr.count-1 ) {
            [self.delegate handlerCountry:[countryIdArr objectAtIndex:index]];
        }
    } else if (dropDownType == voltage) {
        NSString * volt =  [allSelectedItems substringToIndex:[str length]-1];
           [currTriangleBtn setImage:Image(@"down_triangle") forState:UIControlStateNormal];
           [self.delegate handlerVoltage:[NSNumber numberWithInteger:[volt integerValue]]];
    } else {
        [currTriangleBtn setImage:Image(@"syst_drop_down_img") forState:UIControlStateNormal];
        [self.delegate handlerDays:allSelectedItems electricBillId:[self getId:currDropDownTag]];
    }
}
-(void) callDropDownDelegate:(NSNumber *)value {
    if (dropDownType == country) {
        [self.delegate handlerCountry:value];
    }
}
-(NSNumber * )getId:(NSInteger)tag {
    for (NSDictionary * dic in _baseElectricBillArr) {
        if ([[dic valueForKey:@"month"] integerValue] == tag) {
            return [dic valueForKey:@"id"];
        }
    }
    return nil;
}
- (IBAction)dayDropDown:(UIButton *)sender {
    dropDownType = month;
    currDropDownTag = sender.tag;
       [ACommonTools setObjectValue:@60 forKey:@"DropDownWidth"];
       [self dropDownHandler:(UIButton*)[_mDropDownCollectionBtn objectAtIndex:sender.tag-1] dropDown:sender adContents:[self getDaysList:sender.tag]];
}

-(NSMutableArray *)getDaysList:(NSInteger)tag {
    for (int i = 0; i < self.baseElectricBillArr.count; i++) {
        if (tag == [[[self.baseElectricBillArr objectAtIndex:i] valueForKey:@"month"] intValue]) {
            NSInteger daysLenght = [[[self.baseElectricBillArr objectAtIndex:i] valueForKey:@"daysLength"] intValue];
            NSMutableArray * daysList = [NSMutableArray array];
            for (int j = 0; j < daysLenght; j++) {
                [daysList addObject:[DaysList objectAtIndex:j]];
            }
            return daysList;
        }
    }
    return nil;
}
@end
