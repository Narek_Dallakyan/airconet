//
//  HideDevicesView.m
//  Airconet
//
//  Created by Karine Karapetyan on 07-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.

#import "HideDevicesView.h"

@implementation HideDevicesView

-(void)awakeFromNib {
    [super awakeFromNib];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (IBAction)hideDevice:(UIButton *)sender {
    [self.delegate handlerHideDevice];
}

@end
