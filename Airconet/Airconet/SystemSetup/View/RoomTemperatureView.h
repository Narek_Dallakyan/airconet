//
//  RoomTemperatureView.h
//  Airconet
//
//  Created by Karine Karapetyan on 07-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTRangeSlider.h"


NS_ASSUME_NONNULL_BEGIN

@interface RoomTemperatureView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel * mRoomTembLb;
@property (weak, nonatomic) IBOutlet UILabel * mInfoLb;
@property (weak, nonatomic) IBOutlet TTRangeSlider * mSlideBgkV;
@end

NS_ASSUME_NONNULL_END
