//
//  RoomTemperatureView.m
//  Airconet
//
//  Created by Karine Karapetyan on 07-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "RoomTemperatureView.h"
#import "APublicDefine.h"

@implementation RoomTemperatureView

-(void)awakeFromNib {
    [super awakeFromNib];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setupView];
    // Configure the view for the selected state
}

-(void)setupView {
    if (SCREEN_Width < 370) {
        [_mRoomTembLb setFont:[UIFont fontWithName:@"Calibri" size:13]];
    } 
    //currency range slider
    self.mSlideBgkV.minValue = 10;
    self.mSlideBgkV.maxValue = 40;
    self.mSlideBgkV.handleColor = [UIColor greenColor];
    self.mSlideBgkV.handleDiameter = 30;
    
    
    self.mSlideBgkV.minLabelColour = [UIColor whiteColor];
    self.mSlideBgkV.maxLabelColour = [UIColor whiteColor];
    self.mSlideBgkV.maxHandleColor = RGB(193, 0, 4);
    self.mSlideBgkV.minHandleColor = RGB(40, 88, 149);
    self.mSlideBgkV.tintColorBetweenHandles = [UIColor blackColor];
    self.mSlideBgkV.lineHeight = 7;
    self.mSlideBgkV.lineBorderWidth = 2;
    self.mSlideBgkV.lineBorderColor = [UIColor darkGrayColor];
    self.mSlideBgkV.tintColor = [UIColor blackColor];
    self.mSlideBgkV.maxLabelFont = [UIFont systemFontOfSize:20.f];
    self.mSlideBgkV.minLabelFont = [UIFont systemFontOfSize:20.f];
}


@end
