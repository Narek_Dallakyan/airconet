//
//  GeneralSetupViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum BackgroundMode {
    Dark,
    Light,
    Auto
    
}BackgroundMode;
@interface GeneralSetupViewController : BaseViewController

@end

NS_ASSUME_NONNULL_END
