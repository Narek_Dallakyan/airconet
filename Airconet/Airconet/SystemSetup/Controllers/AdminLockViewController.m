//
//  AdminLockViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "AdminLockViewController.h"
#import "CustomUITextFiled.h"
#import "UITextField+Content.h"
#import "CustomAlertView.h"
#import "APublicDefine.h"
#import "ACommonTools.h"


@interface AdminLockViewController ()<CustomAlertDelegate>
@property (weak, nonatomic) IBOutlet UILabel *mAdminLockLb;
@property (weak, nonatomic) IBOutlet UILabel *mRememberMeLb;
@property (weak, nonatomic) IBOutlet CustomUITextFiled *mPasswordtxtFl;
@property (weak, nonatomic) IBOutlet UIButton *mLoginBtn;
@property (weak, nonatomic) IBOutlet UIButton *mRememberCheckBoxBtn;
@property (weak, nonatomic) IBOutlet UIButton *mForgotPassBtn;
@property (weak, nonatomic) IBOutlet UIButton *mBackBtn;

@end

@implementation AdminLockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [self setBackground:self];

}
-(void)viewWillDisappear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = NO;
}


-(void)setupView {
    self.navigationController.navigationBar.hidden = YES;
    if (![ACommonTools getObjectValueForKey:KeyRememberAdminCheck] || [[ACommonTools getObjectValueForKey:KeyRememberAdminCheck] isEqual:@"yes"]) {
        [self.mRememberCheckBoxBtn setImage:Image(@"circle_check") forState:UIControlStateNormal];
    } else if ([[ACommonTools getObjectValueForKey:KeyRememberAdminCheck] isEqual:@"no"]) {
        [self.mRememberCheckBoxBtn setImage:Image(@"uncheck") forState:UIControlStateNormal];
    }
    
    [self.mPasswordtxtFl setBlueBorderStyle];
     [self.mPasswordtxtFl setPlaceholderColor:PlaceholderColor text:self.mPasswordtxtFl.placeholder];
    [self.mPasswordtxtFl setImageInLeftView:[UIImage imageNamed:@"password"]];
    [self addButtonToRightViewTextField];
}
-(void)addButtonToRightViewTextField {
    UIButton * secureBtn = [[UIButton alloc] init];
       [secureBtn addTarget:self action:@selector(secureBtnAction:) forControlEvents:UIControlEventTouchUpInside];
       [self.mPasswordtxtFl setButtonInRightView:secureBtn buttonImg:[UIImage imageNamed: @"not_visible"]];
}

-(void)secureBtnAction:(UIButton *)btn {
    if ([btn.imageView.image isEqual:[UIImage imageNamed:@"not_visible"]]) {
        [btn setImage:[UIImage imageNamed:@"visible"] forState:UIControlStateNormal];
        [self.mPasswordtxtFl setSecureTextEntry:NO];
    } else {
        [btn setImage:[UIImage imageNamed:@"not_visible"] forState:UIControlStateNormal];
        [self.mPasswordtxtFl setSecureTextEntry:YES];
    }
}
#pragma mark:Actions

- (IBAction)back:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
- (IBAction)login:(UIButton *)sender {
    if (_mPasswordtxtFl.text.length <= 0) {
        [self showAlert:NSLocalizedString(@"fill_pass", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
    } else {
        if ([[ACommonTools getObjectValueForKey: AKeyCurrentUserPwd] isEqual:_mPasswordtxtFl.text]) {
            if ([_mRememberCheckBoxBtn.currentImage isEqual:Image(@"circle_check")]) {
                [ACommonTools setBoolValue:YES forKey:KeyRememberAdmin];
            } else {
                [ACommonTools setBoolValue:NO forKey:KeyRememberAdmin];
            }
            [ACommonTools setBoolValue:YES forKey:KeyIsUnLockMenu];
            [ACommonTools setObjectValue:@"yes" forKey:@"loginFirstTime"];

            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self showAlert:NSLocalizedString(@"invalid_pass", nil) ok:NSLocalizedString(@"ok", nil) cencel: @"" delegate:self];
        }
    }
}
- (IBAction)rememberMe:(UIButton *)sender {
    if ([sender.currentImage isEqual:Image(@"circle_check")]) {
        [sender setImage:Image(@"uncheck") forState:UIControlStateNormal];
        [ACommonTools setObjectValue:@"no" forKey:KeyRememberAdminCheck];
    } else {
        [sender setImage:Image(@"circle_check") forState:UIControlStateNormal];
        [ACommonTools setObjectValue:@"yes" forKey:KeyRememberAdminCheck];

    }

}
- (IBAction)forgetPassword:(UIButton *)sender {
    [self presentViewControllerWithIdentifier:@"ResetPassword" controller:self];
}

#pragma mark: UITextFiled Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.returnKeyType = UIReturnKeyDone;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}


@end
