//
//  HideDeviceViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 07-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "HideDeviceViewController.h"
#import "AddTimerDevicesTableViewCell.h"
#import "Devices.h"
#import "ACommonTools.h"

@interface HideDeviceViewController ()<CustomAlertDelegate> {
    NSMutableArray * devicesArr;
}
@property (weak, nonatomic) IBOutlet UILabel *mSelectDevLb;
@property (weak, nonatomic) IBOutlet UITableView *mDeviceTbv;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityV;

@end

@implementation HideDeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}
-(void)viewWillAppear:(BOOL)animated {
    [_mActivityV startAnimating];
    [self setBackground:self];
    [self getAllDevices];

}

-(void)setupView {
    self.mDeviceTbv.separatorColor = [UIColor clearColor];
    [self setNavigationStyle];
    [self setBarButton];

}
-(void)setNavigationStyle {
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self setNavigationStyle:BaseColor tintColor:[UIColor whiteColor] titleText:NSLocalizedString(@"hide_dev", nil) titleColor:NavTitleColor];
      for (UIView *view in self.navigationController.navigationBar.subviews) {
            if ([view isKindOfClass:[DevicesNavigationBar class]]) {
                [view removeFromSuperview];
            }
        }
}
-(void)setBarButton {
    UIButton * rightbtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
    [rightbtn setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    [rightbtn addTarget:self action:@selector(handlerCancel)
         forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithCustomView:rightbtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    UIButton * leftbtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
       [leftbtn setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
       [leftbtn addTarget:self action:@selector(handlerSave)
            forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftbtn];
       self.navigationItem.leftBarButtonItem = leftButton;
}

-(void)setAllDevises:(NSArray *)result {
devicesArr = [NSMutableArray array];
for (NSDictionary * device in result) {
    if (![[device valueForKey:@"deviceType"] isEqual:@"PM"]) {
        [devicesArr addObject:[[[Devices alloc]init] getDeviceObj:device]];
    }
}
    [self setHiddeDevicesInfo];
    [self.mDeviceTbv reloadData];
    [_mActivityV stopAnimating];
}

-(void)setHiddeDevicesInfo {
    if ([[ACommonTools getObjectValueForKey:@"HiddenDevices"] count] > 0) {
        hiddenDevicesArr = [ACommonTools getObjectValueForKey:@"HiddenDevices"];
        for (int i = 0; i < devicesArr.count; i++) {
            [[devicesArr objectAtIndex:i] setIsCheckDevice:NO];
            [hiddenDevicesArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([[[devicesArr objectAtIndex:i] mac] isEqualToString:obj]) {
                    [[devicesArr objectAtIndex:i] setIsCheckDevice:YES];
                }
            }];
        }
    }
}
#pragma mark: GET
- (void)getAllDevices {
    [[RequestManager sheredMenage] getJsonDataWithUrl:AGetdevdevices andParameters:@{}
                                         isNeedCookie:NO success:^(id response) {
        
        NSNumber *code = [response objectForKey:AResultCode];
        if (code.intValue == 0) {
            NSArray *array = [response objectForKey:@"values"];
            [self setAllDevises:array];
        }
    } failure:^(NSError *error) {
        [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
    }];
}



-(void)updateHiddenDeviceMacs {
    hiddenDevicesArr = [NSMutableArray array];
    for (Devices * dev in devicesArr) {
        if (dev.isCheckDevice) {
            [hiddenDevicesArr addObject: dev.mac];
        }
    }
    [ACommonTools setObjectValue:hiddenDevicesArr forKey:@"HiddenDevices"];
}

-(void)handlerCancel {
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)handlerSave {
    [self updateHiddenDeviceMacs];
    [self.navigationController popViewControllerAnimated:YES];


    
}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return devicesArr.count;
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    hiddenDevicesArr = [NSMutableArray array];
    AddTimerDevicesTableViewCell * hideDevCell = [tableView dequeueReusableCellWithIdentifier:@"AddTimerDevicesCell"];
    Devices * dev = [devicesArr objectAtIndex:indexPath.row];
    [hideDevCell.mTitleLb setText:dev.name];
     hideDevCell.isCheck = dev.isCheckDevice;
    if ([dev.deviceType isEqualToString:@"SW"]) {
        [hideDevCell.mModeImgV setImage:[UIImage imageNamed:@"ic_grey_lamp"]];
    } else if ([dev.model caseInsensitiveCompare:@"Sensor"] == NSOrderedSame) {
        [hideDevCell.mModeImgV setImage:[UIImage imageNamed:@"sys_sensor"]];
    } else {
        [hideDevCell.mModeImgV setImage:[UIImage imageNamed:@"light_cold"]];
    }
  
               
        
    return hideDevCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    Devices * dev = [devicesArr objectAtIndex:indexPath.row];
    if (dev.isCheckDevice) {
        [ [devicesArr objectAtIndex:indexPath.row] setIsCheckDevice:NO];
    } else {
        [ [devicesArr objectAtIndex:indexPath.row] setIsCheckDevice:YES];
    }
    [tableView reloadData];
    
}


#pragma mark: CustomAlertDelegate
-(void)handelOk {
    
}
@end
