//
//  HideDeviceViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 07-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HideDeviceViewController : BaseViewController

@end

NS_ASSUME_NONNULL_END
