//
//  GeneralSetupViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "GeneralSetupViewController.h"
#import "PowerMeterSettingsView.h"
#import "BackgroundView.h"
#import "HideDevicesView.h"
#import "RoomTemperatureView.h"
#import "AdminLockView.h"
#import "TTRangeSlider.h"
#import "ACommonTools.h"
#import "GeneralSetup.h"
#import "PowerMeterSettings.h"


@interface GeneralSetupViewController () <BackgroundViewDelegate, AdminLockViewDelegate,
HideDevicesViewDelegate, TTRangeSliderDelegate,
CustomAlertDelegate, PowerMeterSettingsDelegate,
UIScrollViewDelegate> {
    
    GeneralSetup * generalSetup;
    NSMutableDictionary * hazardRoomTempDic;
    PowerMeterSettings * powerMeterSettings;
    NSMutableArray * newDaysElectricBillArr;
    NSMutableArray * newPricesElectricBillArr;
    UIColor * selfColor;
    BOOL isUpdateHazardRoom;
    BOOL isUpdateCountry;
    BOOL isUpdateSqm;
    BOOL isUpdateKw;
    BOOL isUpdateVolt;
}
@property (weak, nonatomic) IBOutlet UITableView *mGeneralSetupTbV;


@end
enum BackgroundMode backgroundMode;

@implementation GeneralSetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [self setBackground:self];
    [self setNavigationStyle];
    [self setupView];
    [ACommonTools setBoolValue:NO forKey:@"CloseDropDown"];
    powerMeterSettings = [[PowerMeterSettings alloc] init];
}
-(void)viewDidAppear:(BOOL)animated  {
    [super viewDidAppear:YES];
    self.navigationController.navigationBar.topItem.title = NSLocalizedString(@"system_setup", nil);
    [[NSNotificationCenter defaultCenter]
        addObserver:self selector:@selector(updateMaxPowerFeedback:) name:NotificationPowerMeter object:nil];
    //[self.mGeneralSetupTbV reloadData];
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NotificationBackgroundMode object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [ACommonTools setBoolValue:YES forKey:@"CloseDropDown"];
    [self.mGeneralSetupTbV reloadData];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setupView {

    newDaysElectricBillArr = [NSMutableArray array];
    newPricesElectricBillArr = [NSMutableArray array];
    generalSetup = [[GeneralSetup alloc] init];
    selfColor = self.view.backgroundColor;
    [self setBarButton];
    self.mGeneralSetupTbV.separatorColor = [UIColor clearColor];
    [self setAdminLock];
    [self getHazartRoomTemperature];
    [self getBackgroundMode];
    [self getPMInfo];

}



-(void)setNavigationStyle {
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self setNavigationStyle:BaseColor tintColor:[UIColor whiteColor] titleText:NSLocalizedString(@"system_setup", nil) titleColor:NavTitleColor];
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if ([view isKindOfClass:[DevicesNavigationBar class]]) {
            [view removeFromSuperview];
        }
    }
    self.navigationItem.titleView = [self setNavigationTitle:@"system_setup"];
}

-(void)setBarButton {
    UIButton * rightbtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
    [rightbtn setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    [rightbtn addTarget:self action:@selector(handlerCancel)
       forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithCustomView:rightbtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    UIButton * leftbtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
    [leftbtn setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
    [leftbtn addTarget:self action:@selector(handlerSave)
      forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftbtn];
    self.navigationItem.leftBarButtonItem = leftButton;
}

-(void)setAdminLock {
    if ([ACommonTools getBoolValueForKey:KeyIsAdminLock]) {
        generalSetup.isAdminLock = YES;
    } else {
        generalSetup.isAdminLock = NO;
    }
}

-(void)getBackgroundMode {
    if ([[ACommonTools getObjectValueForKey:@"BackgroundMode"] isEqual:@"dark"]) {
        backgroundMode = Dark;
    } else if ([[ACommonTools getObjectValueForKey:@"BackgroundMode"] isEqual:@"light"]) {
        backgroundMode = Light;
    } else {
        backgroundMode = Auto;
    }
}

#pragma mark: Get Requests
-(void)getHazartRoomTemperature {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] getDataWithUrl:AGetHazardRoomTemp success:^(id  _Nonnull response) {
           // NSLog(@"AGetHazardRoomTemp response  = %@\n", response);
            self->hazardRoomTempDic = [NSMutableDictionary dictionary];
            self->hazardRoomTempDic = [response mutableCopy];
            self->generalSetup.t1h = [response valueForKey:@"t1h"];
            self->generalSetup.t1l = [response valueForKey:@"t1l"];
            self->powerMeterSettings.countryId = [response valueForKeyPath:@"user.deviceCountry.id"];
            self->powerMeterSettings.countryName = [response valueForKeyPath:@"user.deviceCountry.name"];
            [self getWorkingHour];
            //[self.mGeneralSetupTbV reloadData];
        } failure:^(NSError * _Nonnull error) {
            //NSLog(@"AGetHazardRoomTemp response error  = %@\n", error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil)ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
    });
}
-(void)getWorkingHour {
    //AGetHazardRoomTemp
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] getDataWithUrl:AGetSystemWorkingHour success:^(id  _Nonnull response) {
            //NSLog(@"AGetSystemWorkingHour response  = %@\n", response);
            self->powerMeterSettings.workingHours = [response mutableCopy];
            self->powerMeterSettings.sqm = [NSNumber numberWithInteger: [[response valueForKey:@"sqm"] integerValue]];
            self->powerMeterSettings.whId = [response valueForKey:@"id"];
            [self getMaxPower];

        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"AGetSystemWorkingHour response error  = %@\n", error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
    });
}

-(void)getPMInfo {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
               [[RequestManager sheredMenage] getDataWithUrl:AGetPmInfo success:^(id  _Nonnull response) {
                  // NSLog(@"AGetPmInfo response  = %@\n", response);
                   self->powerMeterSettings.masterPm = [[response valueForKey:@"hasUserMasterPM"] boolValue];
                  // self->navBar.isLinkedPm = [response valueForKey:@"hasUserLinkedPM"];
                   [self getPmCountry];
               } failure:^(NSError * _Nonnull error) {
                  // NSLog(@"AGetPmInfo response error  = %@\n", error.localizedDescription);
               }];
       });
}

-(void)getPmCountry {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] getDataWithUrl:AGetCountry success:^(id  _Nonnull response) {
           // NSLog(@"AGetCountry response  = %@\n", response);
            self->powerMeterSettings.countryArr = response;
            [self getBaseBill];
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"AGetCountry response error  = %@\n", error.localizedDescription);
            // [self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
        }];
    });
}

-(void)getMaxPower {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           [[RequestManager sheredMenage] getDataWithUrl:AGetMaxPower success:^(id  _Nonnull response) {
               //NSLog(@"AGetMaxPower response  = %@\n", response);
               self->powerMeterSettings.maxPower = [NSNumber numberWithFloat:[response floatValue]];
               if (!self->powerMeterSettings.masterPm) {
                   [self getVoltage];
               } else {
                   [self.mGeneralSetupTbV reloadData];
               }
           } failure:^(NSError * _Nonnull error) {
               //NSLog(@"AGetMaxPower response error  = %@\n", error.localizedDescription);
           }];
       });
}

-(void)getVoltage {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           [[RequestManager sheredMenage] getDataWithUrl:AGetVoltage success:^(id  _Nonnull response) {
              // NSLog(@"AGetVoltage response  = %@\n", response);
               NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
               f.numberStyle = NSNumberFormatterDecimalStyle;
               self->powerMeterSettings.volt = [f numberFromString:response];
               [self.mGeneralSetupTbV reloadData];
           } failure:^(NSError * _Nonnull error) {
              // NSLog(@"AGetVoltage response error  = %@\n", error.localizedDescription);
           }];
       });
}

-(void)getBaseBill {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[RequestManager sheredMenage] getDataWithUrl:AGetBaseBill success:^(id  _Nonnull response) {
                //NSLog(@"AGetBaseBill response  = %@\n", response);
                self->powerMeterSettings.baseElectricBillArr = [NSMutableArray array];
                for (NSDictionary * dic in response) {
                    NSMutableDictionary *  dict = [NSMutableDictionary dictionary];
                    [dict setValue:[dic valueForKeyPath:@"baseBill.day"] forKey:@"day"];
                    [dict setValue:[dic valueForKeyPath:@"baseBill.month"] forKey:@"month"];
                    [dict setValue:[dic valueForKeyPath:@"baseBill.id"] forKey:@"id"];
                    [dict setValue:[dic valueForKeyPath:@"baseBill.price"] forKey:@"price"];
                    [dict setValue:[dic valueForKey:@"daysLength"] forKey:@"daysLength"];                    
                    [self->powerMeterSettings.baseElectricBillArr addObject:dict];
                }
                    [self.mGeneralSetupTbV reloadData];

            } failure:^(NSError * _Nonnull error) {
                //NSLog(@"AGetBaseBill response error  = %@\n", error.localizedDescription);
               // [self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
            }];
    });
}
#pragma mark: Update Requests
-(void)updateHazartRoomTemp {
    [hazardRoomTempDic setValue:generalSetup.t1l forKey:@"t1l"];
    [hazardRoomTempDic setValue:generalSetup.t1h forKey:@"t1h"];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] postJsonWithContentType:APostHazardRoomTemp andParameters:self->hazardRoomTempDic success:^(id _Nonnull response) {
           // NSLog(@"APostHazardRoomTemp response  = %@\n", response);
            
        } failure:^(NSError * _Nonnull error) {
            //NSLog(@"APostHazardRoomTemp response  = %@\n", error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
            
            
        }];
    });
}
-(void)updatePmCountry {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostUpdateCountry andParameters:@{@"countryId" : self->powerMeterSettings.countryId} success:^(id _Nonnull response) {
            //NSLog(@"APostUpdateCountry response  = %@\n", response);
        } failure:^(NSError * _Nonnull error) {
           //NSLog(@"APostUpdateCountry response  = %@\n", error.localizedDescription);
            // [self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
        }];
    });
}
-(void)updateWorkingHour {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
       // NSDictionary * param = [PowerMeterSettings setWorkingHourDic:self->powerMeterSettings];
        [self->powerMeterSettings.workingHours setValue:self->powerMeterSettings.sqm forKey:@"sqm"];
        [[RequestManager sheredMenage] postJsonWithContentType:APostSystemWorkingHour andParameters:self->powerMeterSettings.workingHours success:^(id _Nonnull response) {
            //NSLog(@"APostSystemWorkingHour response  = %@\n", response);
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"APostSystemWorkingHour response  = %@\n", error.localizedDescription);
            // [self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
        }];
    });
}

-(void)updateDayElectricBill {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (NSDictionary * param in self->newDaysElectricBillArr) {
            [[RequestManager sheredMenage] postJsonDataWithUrl:APostDayBaseBill andParameters:param success:^(id _Nonnull response) {
                     // NSLog(@"APostDayBaseBill response  = %@\n", response);
                  } failure:^(NSError * _Nonnull error) {
                      //NSLog(@"APostDayBaseBill response  = %@\n", error.localizedDescription);
                      // [self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
                  }];
        }
      
    });
}
-(void)updatePriceElectricBill {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (NSDictionary * param in self->newPricesElectricBillArr) {
            
            [[RequestManager sheredMenage] postJsonDataWithUrl:APostPriceBaseBill andParameters:param success:^(id _Nonnull response) {
                //NSLog(@"APostPriceBaseBill response  = %@\n", response);
            } failure:^(NSError * _Nonnull error) {
               // NSLog(@"APostPriceBaseBill response  = %@\n", error.localizedDescription);
                // [self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
            }];
        }
    });
}

-(void)updateMaxPower {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostMaxPower andParameters:@{@"peakKW": self->powerMeterSettings.maxPower} success:^(id _Nonnull response) {
                //NSLog(@"APostMaxPower response  = %@\n", response);
            } failure:^(NSError * _Nonnull error) {
                //NSLog(@"APostMaxPower response  = %@\n", error.localizedDescription);
                // [self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
            }];
    });
}

-(void)updateVoltage {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         [[RequestManager sheredMenage] postJsonDataWithUrl:APostVoltage andParameters:@{@"volt": self->powerMeterSettings.volt} success:^(id _Nonnull response) {
                 //NSLog(@"APostVoltage response  = %@\n", response);
             } failure:^(NSError * _Nonnull error) {
                 //NSLog(@"APostVoltage response  = %@\n", error.localizedDescription);
                 // [self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
             }];
     });
}
-(void)refreshMaxPower {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           [[RequestManager sheredMenage] postJsonDataWithUrl:APostRefreshMaxPower andParameters:@{@"peakKW": self->powerMeterSettings.maxPower} success:^(id _Nonnull response) {
                   //NSLog(@"APostRefreshMaxPower response  = %@\n", response);
               if ([response isEqual:@"-1"]) {
                   self->powerMeterSettings.masterPmOffline = YES;
                   [self.mGeneralSetupTbV reloadData];
                  // [self showAlert:@"Master power meter is offline" ok:@"OK" cencel:@"" delegate:self];
               } else if ([response isEqual:@"1"]) {
                   //ok
               }
               } failure:^(NSError * _Nonnull error) {
                   //NSLog(@"APostRefreshMaxPower response  = %@\n", error.localizedDescription);
                   NSString * notFound = @"not found (404)";
                   if ([error.localizedDescription containsString:notFound]) {
                       [self showAlert:NSLocalizedString(@"missing_master_pm", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
                   }
                   // [self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
               }];
       });
}

-(void)handlerCancel {
    [self deleteDropDown];
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)handlerSave {
    // [self deleteDropDown];
    switch (backgroundMode) {
        case 0:
            [ACommonTools setObjectValue:@"dark" forKey:@"BackgroundMode"];
            break;
        case 1:
            [ACommonTools setObjectValue:@"light" forKey:@"BackgroundMode"];
            break;
        default:
            [ACommonTools setObjectValue:@"auto" forKey:@"BackgroundMode"];
            break;
    }
    
    NSNumber * first = [[NSNumber alloc] initWithBool:[ACommonTools getBoolValueForKey:KeyIsAdminLock]];
    NSNumber * second = [[NSNumber alloc] initWithBool:generalSetup.isAdminLock];
    if (![first isEqual: second] ) {
        if (generalSetup.isAdminLock) {
            [ACommonTools setBoolValue:NO forKey:KeyRememberAdmin];
            [ACommonTools setBoolValue:YES forKey:KeyIsAdminLock];
            [ACommonTools setBoolValue:NO forKey: KeyIsUnLockMenu];
            
        } else {
            [ACommonTools setBoolValue:NO forKey:KeyIsAdminLock];
            [ACommonTools setBoolValue:YES forKey:KeyIsUnLockMenu];
            [ACommonTools removeValueForKey:@"loginFirstTime"];
        }
    }
    
   if (isUpdateHazardRoom) {
          [self updateHazartRoomTemp];
      }
    if (isUpdateSqm) {
        [self updateWorkingHour];
    }
    if (isUpdateKw) {
        [self performSelectorInBackground:@selector(updateMaxPower) withObject:nil];
    }
    if (isUpdateVolt) {
        [self performSelectorInBackground:@selector(updateVoltage) withObject:nil];        
    }
    if (isUpdateCountry) {
        [self updatePmCountry];
       }
    if (powerMeterSettings.masterPm) {
        [self savePmSettings];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationBackgroundMode object:nil userInfo:@{@"BackgroundModeCheck": @(YES)}];
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)savePmSettings {
    [self performSelectorInBackground:@selector(updateDayElectricBill) withObject:nil];
    [self performSelectorInBackground:@selector(updatePriceElectricBill) withObject:nil];
}

-(void)deleteDropDown {
    [ACommonTools setBoolValue:YES forKey:@"CloseDropDown"];
    [self.mGeneralSetupTbV reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;//allDevices.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0: {
            BackgroundView * backgroundCell = [tableView dequeueReusableCellWithIdentifier:@"BackgroundCell"];
            backgroundCell.delegate = self;
            backgroundCell.backgroundMode = @(backgroundMode);
            return backgroundCell;
        }
        case 1: {
            AdminLockView * adminLockCell = [tableView dequeueReusableCellWithIdentifier:@"AdminLockCell"];
            adminLockCell.delegate = self;
            adminLockCell.isAdminLock = generalSetup.isAdminLock;
            return adminLockCell;
        }
        case 2: {
            HideDevicesView * hideDeviceCell = [tableView dequeueReusableCellWithIdentifier:@"HideDeviceCell"];
            hideDeviceCell.delegate = self;
            return hideDeviceCell;
        }
        case 3: {
            RoomTemperatureView * roomTemperatureCell = [tableView dequeueReusableCellWithIdentifier:@"RoomTemperatureCell"];
            roomTemperatureCell.mSlideBgkV.delegate = self;
            roomTemperatureCell.mSlideBgkV.selectedMinimum = [generalSetup.t1l floatValue];
            roomTemperatureCell.mSlideBgkV.selectedMaximum = [generalSetup.t1h floatValue];
            return roomTemperatureCell;
        }
        default: {
            PowerMeterSettingsView * powerMeterSettCell = [tableView dequeueReusableCellWithIdentifier:@"PowerMeterSettCell"];
            powerMeterSettCell.delegate = self;
            [self setPowerMeterSettingsCell:powerMeterSettCell];
            return powerMeterSettCell;
        }
    }
    
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 4) {
        if (powerMeterSettings.masterPm) {
            if (SCREEN_Width < 370) {
                return 650.f;
            }
            return 570.f;
        }
        return 300;
    }
   
        return tableView.rowHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 2) {
        [self pushViewControllerWithIdentifier:@"HideDeviceViewController" controller:self];
    }
}

-(void)setPowerMeterSettingsCell:(PowerMeterSettingsView *) powerMeterSettCell {
    powerMeterSettCell.isMasterPm = powerMeterSettings.masterPm;
    powerMeterSettCell.isMasterPmOffline = powerMeterSettings.masterPmOffline;
    if (!powerMeterSettings.masterPm) {
         [powerMeterSettCell.mVoltageDropDownBtn setTitle:[NSString stringWithFormat:@"%@V", [powerMeterSettings.volt stringValue]] forState:UIControlStateNormal];
    }
    if (![powerMeterSettings.countryName isKindOfClass:[NSNull class]]) {
        [powerMeterSettCell.mSelectCountryBtn setTitle:powerMeterSettings.countryName forState:UIControlStateNormal];
    } else {
        [powerMeterSettCell.mSelectCountryBtn setTitle:NSLocalizedString(@"select_country", nil) forState:UIControlStateNormal];
    }
    powerMeterSettCell.countryList = powerMeterSettings.countryArr;
    powerMeterSettCell.mSizeOfLocationTxtFl.text = [powerMeterSettings.sqm stringValue];
    powerMeterSettCell.mMaxPmTxtFl.text = [NSString stringWithFormat:@"%.1f",[powerMeterSettings.maxPower floatValue]];
    powerMeterSettCell.baseElectricBillArr = powerMeterSettings.baseElectricBillArr;
    powerMeterSettCell.dropDownColor = [BaseViewController setDropDownColor:self];
}

#pragma mark -- Keyboard Fubctions
- (void)keyboardWillShow:(NSNotification *)sender {
    CGFloat height = [[sender.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(self.mGeneralSetupTbV.contentInset.top, 0, height+10, 0);
    _mGeneralSetupTbV.contentInset = edgeInsets;
    _mGeneralSetupTbV.scrollIndicatorInsets = edgeInsets;
}

- (void)keyboardWillHide:(NSNotification *)sender {
    UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
    _mGeneralSetupTbV.contentInset = edgeInsets;
    _mGeneralSetupTbV.scrollIndicatorInsets = edgeInsets;
}

#pragma mark: CustomAlert Delegate
-(void)handelOk {
}
#pragma mark : BackgroundView Delegate
-(void)backgroundMode:(NSNumber *)mode {
    switch (mode.integerValue) {
        case 0:
            if (@available(iOS 13.0, *)) {
                self.overrideUserInterfaceStyle = UIUserInterfaceStyleDark;
                backgroundMode = Dark;
            }
            break;
        case 1:
            if (@available(iOS 13.0, *)) {
                self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
                backgroundMode = Light;
            }
            break;
        default:
            backgroundMode = Auto;
            [self setBackgroundAuto:self];
            break;
    }
    [self.mGeneralSetupTbV reloadData];
}
#pragma mark: AdminLockV Delegate
-(void)handlerAdminLock:(BOOL)isAdminLock {
    //   [self pushViewControllerWithIdentifier:@"AdminLockViewController" controller:self];
    generalSetup.isAdminLock = isAdminLock;
}
#pragma mark: HideDeviceV Delegate
-(void)handlerHideDevice {
}

#pragma mark: PowerMeter Delegate
-(void)errorTextFiled:(UITextField *)txtFl {
    [self showAlert:NSLocalizedString(@"enter_num", nil)ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
}
-(void)handlerCountry:(NSNumber *)countryId {
    if ([powerMeterSettings.countryId isKindOfClass:[NSNull class]] || ![countryId isEqualToNumber:powerMeterSettings.countryId]) {
        isUpdateCountry = YES;
    }
    powerMeterSettings.countryId = countryId;
}
-(void)handlerVoltage:(NSNumber *)volt {
    if (![volt isEqualToNumber:powerMeterSettings.volt]) {
        isUpdateVolt = YES;
    }
    powerMeterSettings.volt = volt;
}
-(void)handlerMaxPower:(NSString *)kw {
    if (![kw isEqual:[powerMeterSettings.maxPower stringValue]]) {
        isUpdateKw = YES;
    }
    powerMeterSettings.maxPower = [NSNumber numberWithInteger:[kw floatValue]];
}

-(void)handlerSizeOfLocation:(NSString *)sqm {
     if (![sqm isEqual:[powerMeterSettings.sqm stringValue]]) {
        isUpdateSqm = YES;
    }
    powerMeterSettings.sqm = [NSNumber numberWithInteger: [sqm integerValue]];
}
-(void)handlerRefresh {
    [self refreshMaxPower];
}
-(void)handlerDays:(NSString *)day electricBillId:(nonnull NSNumber *)electricBillId {
    BOOL isupdate = NO;
     for (int i = 0; i < newDaysElectricBillArr.count; i++) {
         if ([[[newDaysElectricBillArr objectAtIndex:i] valueForKey:@"baseBillId"] isEqual:electricBillId]) {
               NSDictionary * param = @{@"baseBillId": electricBillId, @"day":[NSNumber numberWithInteger: [day integerValue]] };
             [newDaysElectricBillArr replaceObjectAtIndex:i withObject:param];
             isupdate = YES;
             break;
         }
     }
    if (!isupdate) {
        NSDictionary * param = @{@"baseBillId": electricBillId, @"day":[NSNumber numberWithInteger: [day integerValue]] };
        [newDaysElectricBillArr addObject:param];
    }
}

-(void)handlerPrices:(NSString *)price electricBillId:(NSNumber *)electricBillId {
    BOOL isupdate = NO;
    for (int i = 0; i < newPricesElectricBillArr.count; i++) {
        if ([[[newPricesElectricBillArr objectAtIndex:i] valueForKey:@"baseBillId"] isEqual:electricBillId]) {
             NSDictionary * param = @{@"baseBillId": electricBillId, @"price":[NSNumber numberWithInteger: [price integerValue]]};
            [newPricesElectricBillArr replaceObjectAtIndex:i withObject:param];
            isupdate = YES;
            break;
        }
    }
    if (!isupdate) {
        NSDictionary * param = @{@"baseBillId": electricBillId, @"price":[NSNumber numberWithInteger: [price integerValue]]};
        [newPricesElectricBillArr addObject:param];
    }
}


#pragma mark TTRangeSliderViewDelegate
-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum{
   // NSLog(@"Currency slider updated. Min Value: %.0f Max Value: %.0f", selectedMinimum, selectedMaximum);
    generalSetup.t1l = @(roundf(selectedMinimum));
    generalSetup.t1h = @(roundf(selectedMaximum));
    isUpdateHazardRoom = YES;
}

#pragma mark: Scroll Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

#pragma mark - Notification Master PowerMeter
-(void) updateMaxPowerFeedback:(NSNotification *) notification {
    NSDictionary *dict = notification.userInfo;
    if (dict != nil) {
         powerMeterSettings.maxPower = [dict valueForKey:@"kwHour"];
        [self.mGeneralSetupTbV reloadData];
    }
}

@end
