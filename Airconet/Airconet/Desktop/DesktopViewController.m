//
//  DesktopViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 20-04-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "DesktopViewController.h"
#import "APublicDefine.h"

@interface DesktopViewController ()<WKNavigationDelegate>
//@property(strong,nonatomic) WKWebView *webView;

@end

@implementation DesktopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationStyle];
    [self loadWebV];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.mActivityV startAnimating];
    [self setBackground:self];
}
-(void)viewDidAppear:(BOOL)animated {
    [self setBackground:self];

}

-(void)setNavigationStyle {
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self setNavigationStyle:BaseColor tintColor:[UIColor whiteColor] titleText:NSLocalizedString(@"airCon", nil) titleColor:NavTitleColor];
      for (UIView *view in self.navigationController.navigationBar.subviews) {
            if ([view isKindOfClass:[DevicesNavigationBar class]]) {
                [view removeFromSuperview];
            }
        }
}

-(void)loadWebV {
    NSURL *url = [NSURL URLWithString:WebInfoUrl];//@"https://www.airconet.info"
    NSURLRequest *urlReq = [NSURLRequest requestWithURL:url];
    _mWebV.navigationDelegate = self;
    [_mWebV loadRequest:urlReq];
    _mWebV.backgroundColor = self.view.backgroundColor;
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
    [self.mActivityV stopAnimating];
    [self.mActivityV setHidden:YES];
}


@end
