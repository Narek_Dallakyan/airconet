//
//  DesktopViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 20-04-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DesktopViewController : BaseViewController
@property (weak, nonatomic) IBOutlet WKWebView *mWebV;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityV;


@end

NS_ASSUME_NONNULL_END
