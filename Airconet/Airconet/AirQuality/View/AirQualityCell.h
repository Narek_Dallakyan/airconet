//
//  AirQualityCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AirQualityCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mHeartImg;
@property (weak, nonatomic) IBOutlet UILabel *mConditionerNameLb;
@property (weak, nonatomic) IBOutlet UIView *mBottomLineV;
@property (weak, nonatomic) IBOutlet UILabel *mOneLb;
@property (weak, nonatomic) IBOutlet UILabel *mtwoLb;
@property (weak, nonatomic) IBOutlet UILabel *mThreeLb;
@property (weak, nonatomic) IBOutlet UILabel *mFourLb;
@property (weak, nonatomic) IBOutlet UILabel *mFiveLb;
@property (weak, nonatomic) IBOutlet UIButton *mGas1Btn;
@property (weak, nonatomic) IBOutlet UIButton *mGas2Btn;
@property (weak, nonatomic) IBOutlet UIButton *mGas3Btn;
@property (weak, nonatomic) IBOutlet UIButton *mGas4Btn;
@property (weak, nonatomic) IBOutlet UIButton *mGas5Btn;
@property (strong, nonatomic) IBOutlet UIImageView *mWarningImgV;
@property (strong, nonatomic) IBOutlet UIStackView *mAirQualityStackV;

- (IBAction)gas1:(UIButton *)sender;
- (IBAction)gas2:(UIButton *)sender;
- (IBAction)gas3:(UIButton *)sender;
- (IBAction)gas4:(UIButton *)sender;
- (IBAction)gas5:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
