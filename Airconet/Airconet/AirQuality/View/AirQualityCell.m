//
//  AirQualityCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "AirQualityCell.h"
#import "APublicDefine.h"

@implementation AirQualityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setupView];
    // Configure the view for the selected state
}

-(void)setupView {
    
    _mGas1Btn.layer.cornerRadius = 8.f;
    _mGas1Btn.layer.borderColor = ColdColor.CGColor;
    _mGas1Btn.layer.borderWidth = 3.f;
    _mGas2Btn.layer.cornerRadius = 8.f;
    _mGas2Btn.layer.borderColor = ColdColor.CGColor;
    _mGas2Btn.layer.borderWidth = 3.f;
    _mGas3Btn.layer.cornerRadius = 8.f;
    _mGas3Btn.layer.borderColor = ColdColor.CGColor;
    _mGas3Btn.layer.borderWidth = 3.f;
    _mGas4Btn.layer.cornerRadius = 8.f;
    _mGas4Btn.layer.borderColor = ColdColor.CGColor;
    _mGas4Btn.layer.borderWidth = 3.f;
    _mGas5Btn.layer.cornerRadius = 8.f;
    _mGas5Btn.layer.borderColor = ColdColor.CGColor;
    _mGas5Btn.layer.borderWidth = 3.f;
}

- (IBAction)gas1:(UIButton *)sender {
}

- (IBAction)gas2:(UIButton *)sender {
}

- (IBAction)gas3:(UIButton *)sender {
}

- (IBAction)gas4:(UIButton *)sender {
}

- (IBAction)gas5:(UIButton *)sender {
}

@end
