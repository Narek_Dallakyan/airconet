//
//  AirQualityViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 05-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "AirQualityViewController.h"
#import "AirQualityCell.h"
#import "AirQuality.h"

@interface AirQualityViewController ()<CustomAlertDelegate> {
    NSMutableArray * airQualityArr;
    int airQualityResponseCount;
}
@property (weak, nonatomic) IBOutlet UITableView *mAirQualityTbv;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityV;

@end

@implementation AirQualityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.mActivityV startAnimating];
    self.mAirQualityTbv.separatorColor = [UIColor clearColor];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [self setBackground:self];
    dispatch_async(dispatch_get_main_queue(), ^{
    });
    [self setupView];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidAppear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark: SET
-(void)setupView {
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(updateByFeedback:) name:NotificationAirQuality object:nil];
    airQualityArr = [NSMutableArray array];
    airQualityResponseCount = 0;
    [self setNavigationStyle];
    [self.mActivityV startAnimating];
    [self getAllDevices];
}

-(void)setNavigationStyle {
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self setNavigationStyle:BaseColor tintColor:[UIColor whiteColor] titleText:NSLocalizedString(@"air_quality", nil) titleColor:NavTitleColor];
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if ([view isKindOfClass:[DevicesNavigationBar class]]) {
            [view removeFromSuperview];
        }
    }
}

-(void)getAirQuality:(NSArray *)allDevises {
    NSInteger airQualCount = 0;
    for (NSDictionary * deviceDic in allDevises) {
        if ([[deviceDic valueForKey:@"model"] caseInsensitiveCompare:@"sensor"] == NSOrderedSame) {
            airQualCount++;
           // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
              //  dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
               // NSLog(@"mac of sensor = %@\n", [deviceDic valueForKey:@"mac"]);
            dispatch_queue_t q = dispatch_queue_create("com.my2.queue", NULL);
            dispatch_sync(q, ^{
                [[RequestManager sheredMenage] getJsonDataWithUrl:APostAirQuality
                                                    andParameters:@{@"mac":[deviceDic valueForKey:@"mac"]}
                                                     isNeedCookie:NO
                                                          success:^(id  _Nonnull response) {
                    //NSLog(@"AirQuality response  = %@\n", response);
                   // dispatch_semaphore_signal(semaphore);
                    self->airQualityResponseCount++;
                    AirQuality * airQuality = [[AirQuality alloc] init];
                    airQuality.mac = [deviceDic valueForKey:@"mac"];
                    airQuality.name = [deviceDic valueForKey:@"name"];
                    airQuality.isConnect = [[response valueForKey:@"connect"] boolValue];
                    if ([[response valueForKey:@"type"] isEqual:@"gas"]) {
                        airQuality.typeGas = [response valueForKey:@"type"];
                        airQuality.numberGas = [[response valueForKey:@"number"] integerValue];
                    }
                    [self->airQualityArr addObject:airQuality];
                    //NSLog(@"self->airQualityResponseCount = %i\n", self->airQualityResponseCount)
                   // if (airQualCount == self->airQualityResponseCount) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.mAirQualityTbv reloadData];
                            [self.mActivityV stopAnimating];
                        });
                   // }
                } failure:^(NSError * _Nonnull error) {
                   // NSLog(@"AirQuality response  error = %@\n", error.localizedDescription);
                     self->airQualityResponseCount++;
                  //  if (airQualCount == self->airQualityResponseCount) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.mAirQualityTbv reloadData];
                            [self.mActivityV stopAnimating];
                        });
                   // }
                    // [self showAlert:@"" ok:@"OK" cencel:@"" delegate:self];
                }];
            });
               // dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
           // });
        }
    }
}

-(void)getAllDevices{
   // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

    [[RequestManager sheredMenage] getJsonDataWithUrl:AGetdevdevices andParameters:@{}
                                         isNeedCookie:NO success:^(id response) {
        
        NSNumber *code = [response objectForKey:AResultCode];
        if (code.intValue == 0) {
            NSArray *array = [response objectForKey:@"values"];
            //NSLog(@" getAllDevices = %@\n", array);
            [self getAirQuality: array];
        }
    } failure:^(NSError *error) {
        [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
    }];
   // });
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return airQualityArr.count;//allDevices.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AirQualityCell * airQualityCell = [tableView dequeueReusableCellWithIdentifier:@"AirQualityCell"];
    AirQuality * airQuality = [airQualityArr objectAtIndex:indexPath.row];
    [self setAirQualityCell:airQualityCell currAirQuality:airQuality];
    [airQualityCell.mConditionerNameLb setText:airQuality.name];
    return airQualityCell;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 130;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // [self goToAddDevicePage:NO];
}

-(void)setAirQualityCell:(AirQualityCell *)airQualityCell currAirQuality:(AirQuality *)currAirQuality {
    dispatch_async(dispatch_get_main_queue(), ^{
         [airQualityCell.mGas1Btn setHidden:YES];
           [airQualityCell.mGas2Btn setHidden:YES];
           [airQualityCell.mGas3Btn setHidden:YES];
           [airQualityCell.mGas4Btn setHidden:YES];
           [airQualityCell.mGas5Btn setHidden:YES];
           [airQualityCell.mWarningImgV setHidden:currAirQuality.isConnect];
           [airQualityCell.mAirQualityStackV setHidden:!currAirQuality.isConnect];
           if (currAirQuality.isConnect) {
               if (currAirQuality.typeGas.length > 0) {
                   switch (currAirQuality.numberGas) {
                       case 1:
                           [airQualityCell.mGas1Btn setHidden:NO];
                           break;
                       case 2:
                           [airQualityCell.mGas2Btn setHidden:NO];
                           break;
                       case 3:
                           [airQualityCell.mGas3Btn setHidden:NO];
                           break;
                       case 4:
                           [airQualityCell.mGas4Btn setHidden:NO];
                           break;
                       default:
                           [airQualityCell.mGas5Btn setHidden:NO];
                           break;
                   }
               }
           }
    });
   
}

#pragma mark - Notification
-(void) updateByFeedback:(NSNotification *) notification {
    NSDictionary *dict = notification.userInfo;
    //NSLog(@"updateByFeedback = %@", dict);
    if (dict != nil) {
        for (int i = 0; i < airQualityArr.count; i++) {
            if ([[dict valueForKey:@"mac"] isEqual:[[airQualityArr objectAtIndex:i] mac]]) {
                if ([[dict valueForKey:@"type"] isEqual:@"gas"]) {
                    
                    [[airQualityArr objectAtIndex:i] setTypeGas:[dict valueForKey:@"type"]];
                    [[airQualityArr objectAtIndex:i] setNumberGas:[[dict valueForKey:@"number"]integerValue]];
                    [[airQualityArr objectAtIndex:i] setIsConnect:YES];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.mAirQualityTbv reloadData];
                });
            } else if ([[dict valueForKey:@"model"] caseInsensitiveCompare:@"sensor"] == NSOrderedSame){
                airQualityArr = [NSMutableArray array];
                [self getAllDevices];
            }
        }
    }
}
#pragma mark: CustomAlertDelegate
-(void)handelOk {
    
}

@end
