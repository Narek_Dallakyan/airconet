//
//  PowerMeterViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 08-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "PowerMeterViewController.h"
#import "GaugeTableViewCell.h"
#import "TotalEnergyTableViewCell.h"
#import "EnergyPerDeviceTableViewCell.h"
#import "EnergyRateTableViewCell.h"
#import "ElectricBillTableViewCell.h"
#import "DeviceProportionTableViewCell.h"
#import "SavingTableViewCell.h"
#import "PowerMeter.h"
#import "EfficiencyRate.h"
#import "ACommonTools.h"

@interface PowerMeterViewController ()< EfficiencyRateDelegate>{
    MainPowerMeter * powerMeter;
    BOOL isUpdateEfficencyRateV;
}
@property (weak, nonatomic) IBOutlet UITableView *mPowerMeterTbV;
@property (strong, nonatomic) NSTimer * electricOfflineTimer;


@end

@implementation PowerMeterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated {
    [self setupView];
}
-(void)viewDidAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter]
    addObserver:self selector:@selector(updateElectricByFeedback:) name:NotificationPowerMeter object:nil];
}
-(void)viewWillDisappear:(BOOL)animated {
    [_electricOfflineTimer invalidate];
    _electricOfflineTimer = nil;
}
- (NSTimer *) electricOfflineTimer {
    if (!_electricOfflineTimer) {
        _electricOfflineTimer = [NSTimer timerWithTimeInterval:20.f target:self selector:@selector(isOfflineElectric) userInfo:nil repeats:YES];
    }
    return _electricOfflineTimer;
}

-(void)setupView {
    powerMeter = [[MainPowerMeter alloc] init];
    powerMeter.navBar = [[NavigationBar alloc] init];
    powerMeter.navBar.dateModeForRequest = @"DAY";
    powerMeter.navBar.dateMode = [BaseViewController getDateMode:self->powerMeter.navBar.dateModeForRequest];
    powerMeter.navBar.pmModeTypeForRequest = @"HOURS";
    powerMeter.navBar.pmModeType = [BaseViewController getPmMode:self->powerMeter.navBar.pmModeTypeForRequest];

    [self setNavigationStyle];
    [self setBackground:self];
    [self getPmMaster];
}

-(void)setNavigationStyle {
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self setNavigationStyle:BaseColor tintColor:[UIColor whiteColor] titleText:NSLocalizedString(@"power_meter", nil) titleColor:NavTitleColor];
      for (UIView *view in self.navigationController.navigationBar.subviews) {
            if ([view isKindOfClass:[DevicesNavigationBar class]]) {
                [view removeFromSuperview];
            }
        }
}


-(void)getPmMaster {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[RequestManager sheredMenage] getDataWithUrl:AGetPmMaster success:^(id  _Nonnull response) {
               // NSLog(@"AGetPmMaster response  = %@\n", response);
                self->powerMeter.navBar.masterPmMac = [response valueForKey:@"mac"];
                self->powerMeter.navBar.isMasterPm = [response valueForKey:@"masterEnabled"];
                [self getPmElectric];
            } failure:^(NSError * _Nonnull error) {
               // NSLog(@"AGetPmMaster response error  = %@\n", error.localizedDescription);
                NSString * notFound = @"not found (404)";
                  if ([error.localizedDescription containsString:notFound]) {
                      self->powerMeter.navBar.isMasterPm = NO;
                      self->isUpdateEfficencyRateV = YES;
                      [self performSelectorInBackground:@selector(getEnergyUsagePerDevice) withObject:nil];
                    
                  }
                //[self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
            }];
    });
}
-(void)getPmElectric {
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         NSString * urlFormat = [NSString stringWithFormat:@"%@?pmMac=%@", AGetPmElectric, self->powerMeter.navBar.masterPmMac];
            [[RequestManager sheredMenage] getDataWithUrl:urlFormat success:^(id  _Nonnull response) {
                //NSLog(@"AGetPmElectric response  = %@\n", response);
                self->powerMeter.electric = [[Electric alloc] init];
                self->powerMeter.electric.acMac = [response valueForKey:@"acMac"];
                self->powerMeter.electric.graphValue = [[response valueForKey:@"graphValue"] floatValue];
                self->powerMeter.electric.kwHour = [[response valueForKey:@"kwHour"] floatValue];
                self->powerMeter.electric.priceHr = [[response valueForKey:@"priceHr"] floatValue];
                self->powerMeter.electric.pf = [[response valueForKey:@"pf"] floatValue];
                self->powerMeter.electric.pfColor = [Electric getPfColor:self->powerMeter.electric.pf];
                self->powerMeter.electric.isOnline = YES;
                self->isUpdateEfficencyRateV = YES;
                [self.mPowerMeterTbV reloadData];
                [[NSRunLoop mainRunLoop] addTimer:self.electricOfflineTimer forMode:NSRunLoopCommonModes];
                [self performSelectorInBackground:@selector(getEnergyUsagePerDevice) withObject:nil];
            } failure:^(NSError * _Nonnull error) {
                self->isUpdateEfficencyRateV = YES;
                //NSLog(@"AGetNavigationDeviceMode response error  = %@\n", error.localizedDescription);
            }];
        });
}

-(void)isOfflineElectric{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSString * url = [NSString stringWithFormat:@"%@%@", AGetIsOnline, self->powerMeter.navBar.masterPmMac];
            [[RequestManager sheredMenage] getJsonDataWithUrl:url andParameters:@{}
                                                 isNeedCookie:NO success:^(id  _Nonnull response) {
                //  NSLog(@"checkDeviceOffline respons@ = %@\n", response);
                self->powerMeter.electric.isOnline = YES;
            } failure:^(NSError * _Nonnull error) {
                NSString * notFound = @"not found (404)";
                if ( [error.localizedDescription containsString:notFound]) {
                    self->powerMeter.electric.graphValue = 0;
                    self->powerMeter.electric.pf = 0;
                    self->powerMeter.electric.kwHour = 0;
                    self->powerMeter.electric.priceHr = 0;
                    self-> powerMeter.electric.pfColor = [UIColor clearColor];
                    self->powerMeter.electric.isOnline = NO;
                }
            }];
    });
}

-(void)getEnergyUsagePerDevice {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           NSString * urlFormar = [NSString stringWithFormat:@"%@?timeRange=%@&powerMeterMode=%@", AGetEnergyUsagePerDevice, self->powerMeter.navBar.dateModeForRequest, self->powerMeter.navBar.pmModeTypeForRequest];
           // NSLog(@"AGetEnergyUsagePerDevice url = %@\n", urlFormar);
           [[RequestManager sheredMenage] getDataWithUrl:urlFormar success:^(id  _Nonnull response) {
               //NSLog(@"AGetEnergyUsagePerDevice response  = %@\n", response);
               self->powerMeter.energyUsagePerDev = [[EnergyUsagePerDevice alloc] init];
               self->powerMeter.energyUsagePerDev.nameList = [NSMutableArray array];
               self->powerMeter.energyUsagePerDev.valueList = [NSMutableArray array];
               for (NSDictionary * dic in response) {
                   [self->powerMeter.energyUsagePerDev.nameList addObject:[dic valueForKeyPath:@"device.name"]];
                   [self->powerMeter.energyUsagePerDev.valueList addObject:[dic valueForKey:@"value"]];
               }
               [self getDevicesProportion];
           } failure:^(NSError * _Nonnull error) {
               //NSLog(@"AGetEnergyUsagePerDevice response error  = %@\n", error.localizedDescription);
           }];
       });
}

-(void)getDevicesProportion{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           NSString * urlFormar = [NSString stringWithFormat:@"%@?timeRange=%@&powerMeterMode=%@", AGetProportionEnergy, self->powerMeter.navBar.dateModeForRequest, self->powerMeter.navBar.pmModeTypeForRequest];
            //NSLog(@"AGetProportionEnergy url = %@\n", urlFormar);
           [[RequestManager sheredMenage] getDataWithUrl:urlFormar success:^(id  _Nonnull response) {
              // NSLog(@"AGetProportionEnergy response  = %@\n", response);
               self->powerMeter.devicesProportion = [[DevicesProportion alloc] init];
               self->powerMeter.devicesProportion.pmModeDivideHour = [response valueForKeyPath:@"proportionEnergyDto.pmModeDivideHour"];
               self->powerMeter.devicesProportion.pmModeDivideHourAndSqm = [response valueForKeyPath:@"proportionEnergyDto.pmModeDivideHourAndSqm"];
               self->powerMeter.devicesProportion.pmModeTotal = [response valueForKeyPath:@"proportionEnergyDto.pmModeTotal"];
               
               EfficiencyEnergyRate * efficiencyEnergyRate = [[EfficiencyEnergyRate alloc] init];
               efficiencyEnergyRate.allErrorNumber = [response valueForKeyPath:@"efficiencyEnergyRateAppDto.allErrorNumber"];
               efficiencyEnergyRate.acErrorNumber = [response valueForKeyPath:@"efficiencyEnergyRateAppDto.acErrorNumber"];
               self->powerMeter.efficiencyEnergyRate = efficiencyEnergyRate;
               self->powerMeter.devicesProportion.proportionEnergyGraphArr = [NSMutableArray array];
               
               for (NSDictionary * dic in [response valueForKeyPath:@"proportionEnergyDto.proportionEnergyGraphDataDtoList"]) {
                   ProportionEnergyGraph * proportionEnergyGraph = [[ProportionEnergyGraph alloc] init];
                   proportionEnergyGraph.deviceName = [dic valueForKey:@"deviceName"];
                   proportionEnergyGraph.percent = [dic valueForKey:@"percent"];
                   if ([proportionEnergyGraph.percent intValue] > 0) {
                       [self->powerMeter.devicesProportion.proportionEnergyGraphArr addObject:proportionEnergyGraph];
                   }
               }
               
                [self getTotalEnergyUsage];
           } failure:^(NSError * _Nonnull error) {
               //NSLog(@"AGetEnergyUsagePerDevice response error  = %@\n", error.localizedDescription);
           }];
       });
}

-(void)getTotalEnergyUsage {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           NSString * urlFormar = [NSString stringWithFormat:@"%@?timeRange=%@&powerMeterMode=%@", AGetTotalEnergy, self->powerMeter.navBar.dateModeForRequest, self->powerMeter.navBar.pmModeTypeForRequest];
            //NSLog(@"AGetTotalEnergy url = %@\n", urlFormar);
           [[RequestManager sheredMenage] getDataWithUrl:urlFormar success:^(id  _Nonnull response) {
              // NSLog(@"AGetTotalEnergy response  = %@\n", response);
               self->powerMeter.totalEnergyUsage = [[TotalEnergyUsage alloc] init];
               self->powerMeter.totalEnergyUsage.acValueList = [NSMutableArray array];
               self->powerMeter.totalEnergyUsage.swValueList = [NSMutableArray array];
               self->powerMeter.totalEnergyUsage.othersValueList = [NSMutableArray array];

               self->powerMeter.totalEnergyUsage.acValueList = [response valueForKey:@"acValueList"];
               self->powerMeter.totalEnergyUsage.swValueList = [response valueForKey:@"swValueList"];
               self->powerMeter.totalEnergyUsage.othersValueList = [response valueForKey:@"othersValueList"];
               if (self->powerMeter.navBar.isMasterPm) {
                   [self performSelectorInBackground:@selector(getElectricBill) withObject:nil];
               }else {
                   [self.mPowerMeterTbV reloadData];
               }
           } failure:^(NSError * _Nonnull error) {
               //NSLog(@"AGetTotalEnergy response error  = %@\n", error.localizedDescription);
           }];
       });
}

-(void)getElectricBill {
dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
          [[RequestManager sheredMenage] getDataWithUrl:AGetBaseBillGraph success:^(id  _Nonnull response) {
              //NSLog(@"AGetBaseBillGraph response  = %@\n", response);
              self->powerMeter.electricBillArr = [NSMutableArray array];
              self->powerMeter.electricBillArr = [response mutableCopy];
              self->powerMeter.isThereElectricBill = YES;
              [self.mPowerMeterTbV reloadData];

          } failure:^(NSError * _Nonnull error) {
              //NSLog(@"AGetBaseBillGraph response error  = %@\n", error.localizedDescription);
              self->powerMeter.isThereElectricBill = NO;
              [self.mPowerMeterTbV reloadData];

          }];
      });
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (powerMeter.navBar.isMasterPm) {
        return 6;
    }
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.separatorColor = [UIColor clearColor];
    switch (indexPath.row) {
        case 0: {
            GaugeTableViewCell * gaugeCell = [tableView dequeueReusableCellWithIdentifier:@"GaugeCell"];
            gaugeCell.isUpdate = isUpdateEfficencyRateV;
            gaugeCell.mEfficiencyRateV.delegate = self;
            gaugeCell.mEfficiencyRateV.isPowerMeter = YES;
            gaugeCell.mEfficiencyRateV.navBarObj = powerMeter.navBar;
            [self setElectricCell:gaugeCell];
            return gaugeCell;
        }
        case 1: {
            TotalEnergyTableViewCell * totalEnergyCell = [tableView dequeueReusableCellWithIdentifier:@"TotalEnergyCell"];
            totalEnergyCell.acValueList = powerMeter.totalEnergyUsage.acValueList;
            totalEnergyCell.swValueList = powerMeter.totalEnergyUsage.swValueList;
            totalEnergyCell.othersValueList = powerMeter.totalEnergyUsage.othersValueList;
            totalEnergyCell.timeRange = powerMeter.navBar.dateMode;
            return totalEnergyCell;
        }
        case 2: {
            EnergyPerDeviceTableViewCell * energyPerDeviceCell = [tableView dequeueReusableCellWithIdentifier:@"EnergyPerDeviceCell"];
            energyPerDeviceCell.graphNameArr = powerMeter.energyUsagePerDev.nameList;
            energyPerDeviceCell.graphValueArr = powerMeter.energyUsagePerDev.valueList;
            energyPerDeviceCell.dateMode = powerMeter.navBar.dateMode;
            energyPerDeviceCell.pmModeType = powerMeter.navBar.pmModeType;
            return energyPerDeviceCell;
        }
        case 3: {
            DeviceProportionTableViewCell * deviceProportionTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"DeviceProportionCell"];
            [self setDevicesProportion:deviceProportionTableViewCell];
            return deviceProportionTableViewCell;
        }
        case 4: {
            EnergyRateTableViewCell * energyRateTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"EnergyRateCell"];
            energyRateTableViewCell.acErrorNumber = powerMeter.efficiencyEnergyRate.acErrorNumber;
             energyRateTableViewCell.allErrorNumber = powerMeter.efficiencyEnergyRate.allErrorNumber;
            return energyRateTableViewCell;
        }
       default: {
            ElectricBillTableViewCell * electricBillTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"ElectricBillCell"];
            electricBillTableViewCell.dateMode = powerMeter.navBar.dateMode;
           electricBillTableViewCell.electricBillArr = powerMeter.electricBillArr;
            return electricBillTableViewCell;
        }
    }
    return nil;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        if (indexPath.row == 2) {
            return 280;
    }
    return tableView.rowHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   // [self goToAddDevicePage:NO];
}

-(void)setElectricCell:(GaugeTableViewCell *)gaugeCell {
    gaugeCell.mWarningBtn.hidden = powerMeter.electric.isOnline;
    gaugeCell.isMasterPM = powerMeter.navBar.isMasterPm;
    gaugeCell.mPfLb.text = [NSString stringWithFormat:@"%@%.1f", NSLocalizedString(@"pf", nil), powerMeter.electric.pf];
    gaugeCell.mPfLb.textColor = powerMeter.electric.pfColor;
    gaugeCell.mWkHLb.text = [NSString stringWithFormat:@"%.1f%@",powerMeter.electric.kwHour, NSLocalizedString(@"kw_h", nil)];
    gaugeCell.mPriceHLb.text = [NSString stringWithFormat:@"%@%.1f%@",NSLocalizedString(@"$", nil), powerMeter.electric.priceHr, NSLocalizedString(@"hr", nil)];
    gaugeCell.pfValue = powerMeter.electric.graphValue;
}

-(void)setDevicesProportion:(DeviceProportionTableViewCell *) deviceProportionTableViewCell {
    deviceProportionTableViewCell.totalValue =  powerMeter.devicesProportion.pmModeTotal;
               deviceProportionTableViewCell.pmMode = powerMeter.navBar.pmModeType;
               deviceProportionTableViewCell.mHrLb.text = [NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"$", nil), [powerMeter.devicesProportion.pmModeDivideHour stringValue], NSLocalizedString(@"hr", nil)];
               deviceProportionTableViewCell.mHrSqmLb.text = [NSString stringWithFormat:@"%@%.2f%@",NSLocalizedString(@"$", nil), [powerMeter.devicesProportion.pmModeDivideHourAndSqm floatValue], NSLocalizedString(@"hr_sqm", nil)];
               deviceProportionTableViewCell.graphArr = powerMeter.devicesProportion.proportionEnergyGraphArr;
}

#pragma mark: GaugeTableViewCellDelegate
-(void)updateNavigationObj:(NavigationBar *)navBarObj {
    
    powerMeter.navBar = navBarObj;
    [self performSelectorInBackground:@selector(getEnergyUsagePerDevice) withObject:nil];
    [self performSelectorInBackground:@selector(getDevicesProportion) withObject:nil];
    [self performSelectorInBackground:@selector(getTotalEnergyUsage) withObject:nil];
    if (navBarObj.isMasterPm) {
        [self performSelectorInBackground:@selector(getElectricBill) withObject:nil];
    }
}
  
//FUNCTION NAME _ _ -[MainPageViewController webSocket:didReceiveMessage:]:- LINE NUMBER _ _  1096  webSocketreceiveMessage = {"pf":0.0,"priceHr":68.46,"graphValue":0.0,"kwHour":0.978}


#pragma mark - Notification PowerMeter
-(void) updateElectricByFeedback:(NSNotification *) notification {
    NSDictionary *dict = notification.userInfo;
    if (dict != nil) {
        powerMeter.electric.graphValue = [[dict valueForKey:@"graphValue"] floatValue];
        powerMeter.electric.pf = [[dict valueForKey:@"pf"] floatValue];
        powerMeter.electric.kwHour = [[dict valueForKey:@"kwHour"] floatValue];
        powerMeter.electric.priceHr = [[dict valueForKey:@"priceHr"] floatValue];
        powerMeter.electric.pfColor = [Electric getPfColor:powerMeter.electric.pf];
        powerMeter.electric.isOnline = YES;
        [self.mPowerMeterTbV reloadData];
    }
}


@end
