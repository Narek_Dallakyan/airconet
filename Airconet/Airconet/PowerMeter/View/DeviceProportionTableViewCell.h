//
//  DeviceProportionTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 09-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Airconet-Swift.h"


NS_ASSUME_NONNULL_BEGIN

@interface DeviceProportionTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet PieChartView *chartView;
@property (strong, nonatomic) IBOutlet UILabel *mHrLb;
@property (strong, nonatomic) IBOutlet UILabel *mHrSqmLb;
@property (strong, nonatomic) NSString * pmMode;
@property (strong, nonatomic) NSArray * graphArr;
@property (strong, nonatomic) NSNumber * totalValue;
@end


NS_ASSUME_NONNULL_END
