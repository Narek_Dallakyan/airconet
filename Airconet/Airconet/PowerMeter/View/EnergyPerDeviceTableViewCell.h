//
//  EnergyPerDeviceTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 08-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PowerMeter.h"
#import "BarChartCustomView.h"


NS_ASSUME_NONNULL_BEGIN

@interface EnergyPerDeviceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet BarChartCustomView *mGraphContentV;
@property (strong, nonatomic) NSArray * graphNameArr;
@property (strong, nonatomic) NSArray * graphValueArr;
@property (strong, nonatomic) NSString * dateMode;
@property (strong, nonatomic) NSString * pmModeType;

@end

NS_ASSUME_NONNULL_END
