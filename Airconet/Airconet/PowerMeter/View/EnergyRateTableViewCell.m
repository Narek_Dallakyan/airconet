//
//  EnergyRateTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 11-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "EnergyRateTableViewCell.h"
#import "APublicDefine.h"

@implementation EnergyRateTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setupView];
    // Configure the view for the selected state
}

-(void)setupView {
    _mAll1Btn.layer.cornerRadius = 8.f;
    _mAll1Btn.layer.borderColor = ColdColor.CGColor;
    _mAll1Btn.layer.borderWidth = 3.f;
    _mAll2Btn.layer.cornerRadius = 8.f;
    _mAll2Btn.layer.borderColor = ColdColor.CGColor;
    _mAll2Btn.layer.borderWidth = 3.f;
    _mAll3Btn.layer.cornerRadius = 8.f;
    _mAll3Btn.layer.borderColor = ColdColor.CGColor;
    _mAll3Btn.layer.borderWidth = 3.f;
    _mAll4Btn.layer.cornerRadius = 8.f;
    _mAll4Btn.layer.borderColor = ColdColor.CGColor;
    _mAll4Btn.layer.borderWidth = 3.f;
    _mAll5Btn.layer.cornerRadius = 8.f;
    _mAll5Btn.layer.borderColor = ColdColor.CGColor;
    _mAll5Btn.layer.borderWidth = 3.f;
    _mAll6Btn.layer.cornerRadius = 8.f;
    _mAll6Btn.layer.borderColor = ColdColor.CGColor;
    _mAll6Btn.layer.borderWidth = 3.f;
    _mAll7Btn.layer.cornerRadius = 8.f;
    _mAll7Btn.layer.borderColor = ColdColor.CGColor;
    _mAll7Btn.layer.borderWidth = 3.f;
    
    _mAc1Btn.layer.borderColor = ColdColor.CGColor;
    _mAc1Btn.layer.cornerRadius = 8.f;
    _mAc1Btn.layer.borderWidth = 3.f;
    _mAC2Btn.layer.borderColor = ColdColor.CGColor;
    _mAC2Btn.layer.cornerRadius = 8.f;
    _mAC2Btn.layer.borderWidth = 3.f;
    _mAC3Btn.layer.borderColor = ColdColor.CGColor;
    _mAC3Btn.layer.cornerRadius = 8.f;
    _mAC3Btn.layer.borderWidth = 3.f;
    _mAC4Btn.layer.borderColor = ColdColor.CGColor;
    _mAC4Btn.layer.cornerRadius = 8.f;
    _mAC4Btn.layer.borderWidth = 3.f;
    _mAC5Btn.layer.borderColor = ColdColor.CGColor;
    _mAC5Btn.layer.cornerRadius = 8.f;
    _mAC5Btn.layer.borderWidth = 3.f;
    _mAC6Btn.layer.borderColor = ColdColor.CGColor;
    _mAC6Btn.layer.cornerRadius = 8.f;
    _mAC6Btn.layer.borderWidth = 3.f;
    _mAc7Btn.layer.borderColor = ColdColor.CGColor;
    _mAc7Btn.layer.cornerRadius = 8.f;
    _mAc7Btn.layer.borderWidth = 3.f;
    
    _mAll1Btn.hidden = YES;
    _mAll2Btn.hidden = YES;
    _mAll3Btn.hidden = YES;
    _mAll4Btn.hidden = YES;
    _mAll5Btn.hidden = YES;
    _mAll6Btn.hidden = YES;
    _mAll7Btn.hidden = YES;
    _mAc1Btn.hidden = YES;
    _mAC2Btn.hidden = YES;
    _mAC3Btn.hidden = YES;
    _mAC4Btn.hidden = YES;
    _mAC5Btn.hidden = YES;
    _mAC6Btn.hidden = YES;
    _mAc7Btn.hidden = YES;
    [self setAc];
    [self setAll];
}

-(void)setAc{
    switch ([self.acErrorNumber integerValue]) {
        case 1:
            [self.mAc1Btn setHidden:NO];
            break;
        case 2:
            [self.mAC2Btn setHidden:NO];
            break;
        case 3:
            [self.mAC3Btn setHidden:NO];
            break;
        case 4:
            [self.mAC4Btn setHidden:NO];
            break;
        case 5:
            [self.mAC5Btn setHidden:NO];
            break;
        case 6:
            [self.mAC6Btn setHidden:NO];
            break;
        case 7:
            [self.mAc7Btn setHidden:NO];
            break;
        default:
            break;
    }
}
-(void)setAll{
    switch ([self.allErrorNumber integerValue]) {
        case 1:
            [self.mAll1Btn setHidden:NO];
            break;
        case 2:
            [self.mAll2Btn setHidden:NO];
            break;
        case 3:
            [self.mAll3Btn setHidden:NO];
            break;
        case 4:
            [self.mAll4Btn setHidden:NO];
            break;
        case 5:
            [self.mAll5Btn setHidden:NO];
            break;
        case 6:
            [self.mAll6Btn setHidden:NO];
            break;
        case 7:
            [self.mAll7Btn setHidden:NO];
            break;
        default:
            break;
    }
}
- (IBAction)all:(UIButton *)sender {
}

- (IBAction)ac:(UIButton *)sender {
}

@end
