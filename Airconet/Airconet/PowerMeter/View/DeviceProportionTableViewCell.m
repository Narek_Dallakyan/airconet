//
//  DeviceProportionTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 09-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "DeviceProportionTableViewCell.h"
#import "APublicDefine.h"
#import "PowerMeter.h"

@interface DeviceProportionTableViewCell () <ChartViewDelegate> {
    NSArray *parties;

}
@end

@implementation DeviceProportionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setupView];
    // Configure the view for the selected state
}
 

-(void)setupView {
    [self setTotal];
    [self setupPieChartView:_chartView];
    
    _chartView.legend.enabled = NO;
    _chartView.delegate = self;
    _chartView.backgroundColor = RGB(64, 64, 64);
    [_chartView setExtraOffsetsWithLeft:20.f top:0.f right:20.f bottom:0.f];
    [_chartView animateWithYAxisDuration:0 easingOption:ChartEasingOptionEaseOutBack];
    
    [self setDataCount:(int)self.graphArr.count range:100];//6
}

- (void)setDataCount:(int)count range:(double)range {
    NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < count; i++) {
        ProportionEnergyGraph * proportionEnergy =  (ProportionEnergyGraph *)[self.graphArr objectAtIndex:i];
        [entries addObject:[[PieChartDataEntry alloc] initWithValue:[proportionEnergy.percent doubleValue] label:[NSString stringWithFormat:@"%@\n %li%@", proportionEnergy.deviceName, (long)[proportionEnergy.percent integerValue], @"%"]]];

    }
    PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithEntries:entries label:@"Election Results"];
    dataSet.sliceSpace = 0.0;
    // add a lot of colors
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    [colors addObject:[UIColor colorWithRed:132/255.f green:171/255.f blue:79/255.f alpha:1.f]];
    [colors addObject:[UIColor colorWithRed:85/255.f green:114/255.f blue:193/255.f alpha:1.f]];
    [colors addObject:[UIColor colorWithRed:211/255.f green:124/255.f blue:58/255.f alpha:1.f]];
    [colors addObject:[UIColor colorWithRed:164/255.f green:164/255.f blue:164/255.f alpha:1.f]];
    [colors addObject:[UIColor colorWithRed:237/255.f green:191/255.f blue:44/255.f alpha:1.f]];
    [colors addObject:[UIColor colorWithRed:113/255.f green:153/255.f blue:210/255.f alpha:1.f]];
    if (self.graphArr.count != 0) {
         while  (self.graphArr.count > colors.count) {
               [colors addObject:[UIColor colorWithRed:132/255.f green:171/255.f blue:79/255.f alpha:1.f]];
               [colors addObject:[UIColor colorWithRed:85/255.f green:114/255.f blue:193/255.f alpha:1.f]];
               [colors addObject:[UIColor colorWithRed:211/255.f green:124/255.f blue:58/255.f alpha:1.f]];
               [colors addObject:[UIColor colorWithRed:164/255.f green:164/255.f blue:164/255.f alpha:1.f]];
               [colors addObject:[UIColor colorWithRed:237/255.f green:191/255.f blue:44/255.f alpha:1.f]];
               [colors addObject:[UIColor colorWithRed:113/255.f green:153/255.f blue:210/255.f alpha:1.f]];
           }
    }
   
    dataSet.colors = colors;
    dataSet.yValuePosition = PieChartValuePositionOutsideSlice;
    
    PieChartData *data = [[PieChartData alloc] initWithDataSet:dataSet];
    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:pFormatter]];
    [data setValueFont:[UIFont fontWithName:@"Calibri-Bold" size:16.f]];
    [data setValueTextColor:UIColor.whiteColor];

    _chartView.data = data;
    [_chartView highlightValues:nil];
    //super is base Demo controller
    [self handleOption:@"toggleValues" forChartView:_chartView];

}
#pragma mark - Common option actions

- (void)handleOption:(NSString *)key forChartView:(ChartViewBase *)chartView {
    if ([key isEqualToString:@"toggleValues"]) {
        for (id<IChartDataSet> set in chartView.data.dataSets) {
            set.drawValuesEnabled = !set.isDrawValuesEnabled;
        }
        [chartView setNeedsDisplay];
    }
}

- (void)setupPieChartView:(PieChartView *)chartView
{
    chartView.usePercentValuesEnabled = YES;
    chartView.drawSlicesUnderHoleEnabled = NO;
    chartView.holeRadiusPercent = 0.53;
    chartView.transparentCircleRadiusPercent = 0.53;
    chartView.chartDescription.enabled = NO;
    [chartView setExtraOffsetsWithLeft:5.f top:10.f right:5.f bottom:5.f];
    
    chartView.drawCenterTextEnabled = YES;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSMutableAttributedString *centerText = [[NSMutableAttributedString alloc] initWithString:[self setTotal]];
    [centerText addAttributes:@{
                                NSFontAttributeName: [UIFont fontWithName:@"Calibri-Bold" size:22.f],
                                NSForegroundColorAttributeName: [UIColor whiteColor]
                                } range:NSMakeRange(0, centerText.length)];
    chartView.centerAttributedText = centerText;
    
    chartView.drawHoleEnabled = YES;
    chartView.rotationAngle = -90.0;
    chartView.rotationEnabled = NO; //disable rotation
    chartView.highlightPerTapEnabled = NO; //disable touch and biger pie
    chartView.holeColor = RGB(64, 64, 64);
    
    ChartLegend *l = chartView.legend;
    l.horizontalAlignment = ChartLegendHorizontalAlignmentRight;
    l.verticalAlignment = ChartLegendVerticalAlignmentTop;
    l.orientation = ChartLegendOrientationVertical;
    l.textColor = [UIColor clearColor];
    l.drawInside = NO;
    l.xEntrySpace = 7.0;
    l.yEntrySpace = 0.0;
    l.yOffset = 0.0;
}


-(NSString *)setTotal {
    if ([self.pmMode isEqual:NSLocalizedString(@"hour", nil)]) {
        return [NSString stringWithFormat:@"%@ %@%@", NSLocalizedString(@"total", nil), self.totalValue, NSLocalizedString(@"h", nil)];
    } else if ([self.pmMode isEqual:NSLocalizedString(@"$", nil)]) {
       // if ([[self.totalValue stringValue] containsString:@"."]) {
            return [NSString stringWithFormat:@"%@ %@%.1f",NSLocalizedString(@"total", nil) ,NSLocalizedString(@"$", nil), [self.totalValue floatValue] ];
//        }
//        return [NSString stringWithFormat:@"%@ %@%f",NSLocalizedString(@"total", nil), NSLocalizedString(@"$", nil), [self.totalValue floatValue] ];
    } else {
       // if ([[self.totalValue stringValue] containsString:@"."]) {
         return [NSString stringWithFormat:@"%@ %.1f%@", NSLocalizedString(@"total", nil),  [self.totalValue floatValue], NSLocalizedString(@"kw_h", nil)];
//        }
//        return [NSString stringWithFormat:@"%@ %f%@",NSLocalizedString(@"total", nil),  [self.totalValue floatValue], NSLocalizedString(@"kw_h", nil)];
    }
}
@end
