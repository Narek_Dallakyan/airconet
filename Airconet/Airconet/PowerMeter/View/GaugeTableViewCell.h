//
//  GaugeTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 08-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMGaugeView.h"
#import "EfficiencyRate.h"

NS_ASSUME_NONNULL_BEGIN
@protocol GaugeTableViewCellDelegate <NSObject>

-(void)handlerTime:(NSInteger)timer;
-(void)handlerPowermeterMode:(NSInteger)mode;


@end

@interface GaugeTableViewCell : UITableViewCell
@property (strong, nonatomic) id<GaugeTableViewCellDelegate> delegate;

@property (assign, nonatomic) CGFloat pfValue;
@property (assign, nonatomic) BOOL isMasterPM;
@property (assign, nonatomic) BOOL isUpdate;
@property (strong, nonatomic) IBOutlet EfficiencyRate *mEfficiencyRateV;
@property (weak, nonatomic) IBOutlet WMGaugeView *mGaugeV;
@property (weak, nonatomic) IBOutlet UILabel *mPriceHLb;
@property (weak, nonatomic) IBOutlet UILabel *mWkHLb;
@property (strong, nonatomic) IBOutlet UIButton *mWarningBtn;
@property (strong, nonatomic) IBOutlet UILabel *mPfLb;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mPickerBackgVTopLayout;
- (IBAction)warning:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
