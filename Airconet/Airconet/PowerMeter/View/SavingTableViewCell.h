//
//  SavingTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 12-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SavingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mSavingLb;
@property (weak, nonatomic) IBOutlet UILabel *mWToWLb;
@property (weak, nonatomic) IBOutlet UILabel *mMToMLb;
@property (weak, nonatomic) IBOutlet UILabel *mYtoYLb;
@property (weak, nonatomic) IBOutlet UITextField *mWtoWTxtFl;
@property (weak, nonatomic) IBOutlet UITextField *mMtoMTxtFl;
@property (weak, nonatomic) IBOutlet UITextField *mYtoYTxtFl;

@end

NS_ASSUME_NONNULL_END
