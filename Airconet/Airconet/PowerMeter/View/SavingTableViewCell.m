//
//  SavingTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 12-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "SavingTableViewCell.h"

@implementation SavingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self sertupView];
}
-(void)sertupView {
    _mMToMLb.layer.borderColor = [UIColor blackColor].CGColor;
      _mMToMLb.layer.borderWidth = 1.f;
    _mWToWLb.layer.borderColor = [UIColor blackColor].CGColor;
    _mWToWLb.layer.borderWidth = 1.f;
    _mYtoYLb.layer.borderColor = [UIColor blackColor].CGColor;
    _mYtoYLb.layer.borderWidth = 1.f;
    _mWtoWTxtFl.layer.borderColor = [UIColor blackColor].CGColor;
    _mWtoWTxtFl.layer.borderWidth = 1.f;
    _mMtoMTxtFl.layer.borderColor = [UIColor blackColor].CGColor;
    _mMtoMTxtFl.layer.borderWidth = 1.f;
    _mYtoYTxtFl.layer.borderColor = [UIColor blackColor].CGColor;
    _mYtoYTxtFl.layer.borderWidth = 1.f;
}

@end
