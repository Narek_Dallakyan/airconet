//
//  EnergyPerDeviceTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 08-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "EnergyPerDeviceTableViewCell.h"
#import "BaseViewController.h"
#import "ACommonTools.h"
#import "APublicDefine.h"
#import "BarGraphView.h"

@interface EnergyPerDeviceTableViewCell (){
    Graph * graph;

}
@end
@implementation EnergyPerDeviceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.mGraphContentV.graphDataArr = _graphValueArr;
    self.mGraphContentV.graphDateArr = _graphNameArr;
    self.mGraphContentV.dateMode = _dateMode;
    self.mGraphContentV.pmModeType = _pmModeType;
    self.mGraphContentV.viewType = @"PM";
    [BaseViewController setGradient:self.mGraphContentV];
    [self.mGraphContentV awakeFromNib];

}


@end
