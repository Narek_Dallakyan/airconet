//
//  EnergyRateTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 11-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EnergyRateTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mEnergyRateLb;
@property (weak, nonatomic) IBOutlet UIImageView *mSadImg;
@property (weak, nonatomic) IBOutlet UIImageView *mSmileImg;
@property (weak, nonatomic) IBOutlet UILabel *m1Lb;
@property (weak, nonatomic) IBOutlet UILabel *m2Lb;
@property (weak, nonatomic) IBOutlet UILabel *m3Lb;
@property (weak, nonatomic) IBOutlet UILabel *m4Lb;
@property (weak, nonatomic) IBOutlet UILabel *m5Lb;
@property (weak, nonatomic) IBOutlet UILabel *m6Lb;
@property (weak, nonatomic) IBOutlet UILabel *m7Lb;
@property (weak, nonatomic) IBOutlet UIButton *mAll1Btn;
@property (weak, nonatomic) IBOutlet UIButton *mAll2Btn;
@property (weak, nonatomic) IBOutlet UIButton *mAll3Btn;
@property (weak, nonatomic) IBOutlet UIButton *mAll4Btn;
@property (weak, nonatomic) IBOutlet UIButton *mAll5Btn;
@property (weak, nonatomic) IBOutlet UIButton *mAll6Btn;
@property (weak, nonatomic) IBOutlet UIButton *mAll7Btn;
@property (weak, nonatomic) IBOutlet UIButton *mAc1Btn;
@property (weak, nonatomic) IBOutlet UIButton *mAC2Btn;
@property (weak, nonatomic) IBOutlet UIButton *mAC3Btn;
@property (weak, nonatomic) IBOutlet UIButton *mAC4Btn;
@property (weak, nonatomic) IBOutlet UIButton *mAC5Btn;
@property (weak, nonatomic) IBOutlet UIButton *mAC6Btn;
@property (weak, nonatomic) IBOutlet UIButton *mAc7Btn;

@property (strong, nonatomic) NSNumber * acErrorNumber;
@property (strong, nonatomic) NSNumber * allErrorNumber;


@end

NS_ASSUME_NONNULL_END
