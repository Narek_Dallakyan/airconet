//
//  ElectricBillTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 12-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Airconet-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface ElectricBillTableViewCell : UITableViewCell <ChartViewDelegate, IChartAxisValueFormatter>

@property (strong, nonatomic) NSString * dateMode;
@property (strong, nonatomic) NSArray * electricBillArr;

@property (strong, nonatomic) IBOutlet UILabel *melectricBillLb;
@property (strong, nonatomic) IBOutlet BarChartView *chartView;


@end

NS_ASSUME_NONNULL_END
