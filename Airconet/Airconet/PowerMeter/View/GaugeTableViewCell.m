//
//  GaugeTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 08-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "GaugeTableViewCell.h"
#import "APublicDefine.h"
#import "DevicesNavigationBar.h"
#import "ACommonTools.h"

@interface GaugeTableViewCell () {
    DevicesNavigationBar * navView;
}

@end
@implementation GaugeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if (!self.isMasterPM) {
        [self.mGaugeV setHidden:YES];
        if (self.frame.size.width < 370) {
            self.mPickerBackgVTopLayout.constant = -158;//-184;
        } else {
            self.mPickerBackgVTopLayout.constant = -184;
        }
    } else {
        self.mPickerBackgVTopLayout.constant = -5;
        if (self.pfValue) {
            [self addGuage];
        }
    }
    if (_isUpdate) {
        [self.mEfficiencyRateV awakeFromNib];
    }
}


-(void)addGuage {
    [self.mGaugeV setHidden:NO];

    [ACommonTools setBoolValue:YES forKey:@"isPm"];

    self.mGaugeV.style = [WMGaugeViewStyle3D new];
     self.mGaugeV.maxValue = 10.0;//max gauge value
     self.mGaugeV.showRangeLabels = YES;
     self.mGaugeV.rangeValues = @[ @3, @7,@10];//graph's color range
     self.mGaugeV.rangeColors = @[ RGB(86, 129, 59),    RGB(255, 252, 57),  RGB(193, 0, 4),  [UIColor blueColor] /*RGB(231, 32, 43)*/    ];//grap's colors
    //_gaugeView.rangeLabels = @[ @"VERY LOW",          @"LOW",             @"OK",              @"OVER FILL"        ];
     self.mGaugeV.unitOfMeasurement = @"PF 0.8";
     self.mGaugeV.showUnitOfMeasurement = NO;
     self.mGaugeV.scaleDivisionsWidth = 0.008;
     self.mGaugeV.scaleSubdivisionsWidth = 0.006;
     self.mGaugeV.rangeLabelsFontColor = [UIColor blackColor];
     self.mGaugeV.rangeLabelsWidth = 0.055;//gauge's width
     self.mGaugeV.rangeLabelsFont = [UIFont fontWithName:@"Calibri" size:0.04];
    self.mPfLb.hidden = NO;
    if (!self.mWarningBtn.isHidden) {
        self.mPfLb.hidden = YES;
    }
    
    [NSTimer scheduledTimerWithTimeInterval:2.0
                                     target:self
                                   selector:@selector(gaugeUpdateTimer:)
                                   userInfo:nil
                                    repeats:NO];
}
-(void)gaugeUpdateTimer:(NSTimer *)timer
{
    self.mGaugeV.value = self.pfValue;//rand()%(int)self.mGaugeV.maxValue;
//    [_gaugeView2 setValue:rand()%(int)_gaugeView2.maxValue animated:YES duration:1.6 completion:^(BOOL finished) {
//        NSLog(@"gaugeView2 animation complete");
//    }];
}

- (IBAction)warning:(UIButton *)sender {
}
@end
