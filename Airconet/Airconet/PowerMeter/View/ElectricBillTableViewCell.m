//
//  ElectricBillTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 12-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "ElectricBillTableViewCell.h"
#import "BaseViewController.h"
#import "APublicDefine.h"

@interface  ElectricBillTableViewCell () {
   NSMutableArray *dataList;
}

@end
@implementation ElectricBillTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self sertupView];
    // Configure the view for the selected state
}

-(void) sertupView {
    dataList = [NSMutableArray array];

    for (int i = 0; i < self.electricBillArr.count; i++) {
        NSDictionary * dic = [self.electricBillArr objectAtIndex:i];
        NSMutableDictionary * dict = [NSMutableDictionary dictionary];
        [dict setValue:@(i) forKey:@"xValue"];
        [dict setValue:[dic valueForKey:@"value"] forKey:@"yValue"];
        [dict setValue:[MonthNameList objectAtIndex:[[dic valueForKey:@"month"] intValue]-1] forKey:@"xLabel"];
        [dataList addObject:dict];
    }
    [self sertupGraph];
    [BaseViewController setGradient:self];
}

-(void)sertupGraph {
    [self setupBarLineChartView:_chartView];
    
    _chartView.delegate = self;
    _chartView.extraTopOffset = -30.f;
    _chartView.extraBottomOffset = 10.f;
    _chartView.extraLeftOffset = 5.f;
    _chartView.extraRightOffset = 5.f;
    _chartView.drawBarShadowEnabled = NO;
    _chartView.drawValueAboveBarEnabled = YES;
    _chartView.chartDescription.enabled = NO;
    // scaling can now only be done on x- and y-axis separately
    _chartView.pinchZoomEnabled = NO;
    _chartView.drawGridBackgroundEnabled = NO;
    
    ChartXAxis *xAxis = _chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont systemFontOfSize:13.f];
    xAxis.drawGridLinesEnabled = NO;
    xAxis.drawAxisLineEnabled = NO;
    xAxis.labelTextColor = [UIColor grayColor];
    xAxis.labelCount = 12;
    xAxis.centerAxisLabelsEnabled = NO;
    xAxis.granularity = 1.0;
    xAxis.valueFormatter = self;
    
    ChartYAxis *leftAxis = _chartView.leftAxis;
    leftAxis.drawLabelsEnabled = YES;
    leftAxis.spaceTop = 0.25;
    leftAxis.spaceBottom = 0.25;
    leftAxis.drawAxisLineEnabled = NO;
    leftAxis.drawGridLinesEnabled = YES;
    leftAxis.drawZeroLineEnabled = YES;
    leftAxis.zeroLineColor = UIColor.grayColor;
    leftAxis.zeroLineWidth = 0.7f;
    leftAxis.labelTextColor = [UIColor grayColor];
    
    _chartView.rightAxis.enabled = NO;
    _chartView.legend.enabled = NO;
    
    [self setChartData];
}

- (void)setChartData {
    NSMutableArray<BarChartDataEntry *> *values = [[NSMutableArray alloc] init];
    NSMutableArray<UIColor *> *colors = [[NSMutableArray alloc] init];
    
    UIColor *green = GreenColor;
    UIColor *red = RedColor;
    
    for (int i = 0; i < dataList.count; i++)
    {
        NSDictionary *d = dataList[i];
        BarChartDataEntry *entry = [[BarChartDataEntry alloc] initWithX:[d[@"xValue"] doubleValue] y:[d[@"yValue"] doubleValue]];
        [values addObject:entry];
        
        // specific colors
        if ([d[@"yValue"] doubleValue] >= 0.f) {
            [colors addObject:green];
        } else {
            [colors addObject:red];
        }
    }
    
    BarChartDataSet *set = set = [[BarChartDataSet alloc] initWithEntries:values label:@"Values"];
    set.colors = colors;
    set.valueColors = colors;
    BarChartData *data = [[BarChartData alloc] initWithDataSet:set];
    [data setValueFont:[UIFont systemFontOfSize:10.f]];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.maximumFractionDigits = 1;
    [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:formatter]];
    data.barWidth = 0.7;
    _chartView.data = data;
    _chartView.noDataText = NSLocalizedString(@"no_chart_data", nil);
       _chartView.noDataTextColor = [UIColor grayColor];
       _chartView.noDataFont = [UIFont fontWithName:@"calibri" size:13];
    [BaseViewController setGradient:self];

}


- (void)setupBarLineChartView:(BarLineChartViewBase *)chartView {
    
    chartView.chartDescription.enabled = NO;
    chartView.drawGridBackgroundEnabled = NO;
    chartView.dragEnabled = YES;
    [chartView setScaleEnabled:YES];
    chartView.pinchZoomEnabled = NO;
    // ChartYAxis *leftAxis = chartView.leftAxis;
    ChartXAxis *xAxis = chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    chartView.rightAxis.enabled = NO;
}

#pragma mark - ChartViewDelegate

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry highlight:(ChartHighlight * __nonnull)highlight {
   // NSLog(@"chartValueSelected");
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView {
   // NSLog(@"chartValueNothingSelected");
}

#pragma mark - IAxisValueFormatter

- (NSString *)stringForValue:(double)value
                        axis:(ChartAxisBase *)axis {
    return dataList[MIN(MAX((int) value, 0), dataList.count - 1)][@"xLabel"];
}
@end
