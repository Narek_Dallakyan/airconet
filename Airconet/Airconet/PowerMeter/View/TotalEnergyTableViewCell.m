//
//  TotalEnergyTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 08-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "TotalEnergyTableViewCell.h"
#import "BaseViewController.h"
#import "APublicDefine.h"


@implementation TotalEnergyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setupView];
    // Configure the view for the selected state
}

-(void)setupView {
    [BaseViewController setGradient:self];
    [BaseViewController setGradient:self.deviceTypeBackV];
    [BaseViewController setGradient:self.daysBackV];
    
    _chartView.delegate = self;
    
    _chartView.chartDescription.enabled = NO;
    
    _chartView.maxVisibleCount = 40;
    _chartView.pinchZoomEnabled = NO;
    _chartView.drawGridBackgroundEnabled = NO;
    _chartView.drawBarShadowEnabled = NO;
    _chartView.drawValueAboveBarEnabled = NO;
    _chartView.highlightFullBarEnabled = NO;
    _chartView.userInteractionEnabled = NO;
    _chartView.backgroundColor = [UIColor clearColor];
    
    NSNumberFormatter *leftAxisFormatter = [[NSNumberFormatter alloc] init];
    leftAxisFormatter.maximumFractionDigits = 1;
     leftAxisFormatter.negativeSuffix = @" ";
     leftAxisFormatter.positiveSuffix = @" ";
    
    ChartYAxis *leftAxis = _chartView.leftAxis;
    leftAxis.valueFormatter = [[ChartDefaultAxisValueFormatter alloc] initWithFormatter:leftAxisFormatter];
    leftAxis.axisMinimum = 0.15; // this replaces startAtZero = YES
    leftAxis.axisLineColor = [UIColor grayColor];
    leftAxis.labelTextColor = [UIColor grayColor];
    _chartView.rightAxis.enabled = NO;
    
    ChartXAxis *xAxis = _chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionTop;
    xAxis.granularity = 100.f; //graph linse
    xAxis.labelTextColor = [UIColor clearColor];
    
    ChartLegend *l = _chartView.legend;
    l.orientation = ChartLegendOrientationHorizontal;
    l.drawInside = NO;
    l.form = ChartLegendFormLine;
    l.formSize = 0.0;
    l.formToTextSpace = 40.0;
    l.xEntrySpace = 6.0;
    l.textColor = [UIColor clearColor];
    _chartView.noDataText = NSLocalizedString(@"no_chart_data", nil);
    _chartView.noDataTextColor = [UIColor grayColor];
    _chartView.noDataFont = [UIFont fontWithName:@"calibri" size:13];
    if (self.acValueList.count > 0) {
        int xValue = [self getTimerRangeOfGraph];
        [self setDataCount:xValue + 1 range:10.0];
    }
    [self handleOption:@"toggleValues" forChartView:_chartView];
    [_chartView bringSubviewToFront:self.daysBackV];
}


-(void)setDataCount:(int)count range:(double)range {
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < self.acValueList.count; i++) {
        [yVals addObject:[[BarChartDataEntry alloc] initWithX:i yValues:@[[self.othersValueList objectAtIndex:i], [self.swValueList objectAtIndex:i], [self.acValueList objectAtIndex:i]] icon: [UIImage imageNamed:@"icon"]]];
    }
    
    BarChartDataSet *set1 = nil;
    if (_chartView.data.dataSetCount > 0) {
        set1 = (BarChartDataSet *)_chartView.data.dataSets[0];
        [set1 replaceEntries: yVals];
        [_chartView.data notifyDataChanged];
        [_chartView notifyDataSetChanged];
        
    } else {
        set1 = [[BarChartDataSet alloc] initWithEntries:yVals label:@""];
        
        set1.drawIconsEnabled = NO;
        set1.colors = @[RGB(165, 165, 165), RGB(211, 124, 58),RGB(63, 86, 148) ];
        set1.stackLabels = [self getMonthList];
        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        [dataSets addObject:set1];
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.maximumFractionDigits = 1;
        formatter.negativeSuffix = NSLocalizedString(@"$", nil);
        formatter.positiveSuffix =NSLocalizedString(@"$", nil);
        
        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:7.f]];
        [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:formatter]];
        [data setValueTextColor:UIColor.grayColor];
        
        _chartView.fitBars = YES;
        _chartView.data = data;
    }
    [self sendSubviewToBack:self.daysBackV];
    
}

-(NSMutableArray *)getMonthList {
    NSDate *today = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSMutableArray *datesThisMonth = [NSMutableArray array];
    NSRange rangeOfDaysThisMonth = [cal rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:today];
    for (NSInteger i = rangeOfDaysThisMonth.location; i < NSMaxRange(rangeOfDaysThisMonth); ++i) {
        [datesThisMonth addObject:[NSString stringWithFormat:@"%li", (long)i]];
    }
    return datesThisMonth;
}

#pragma mark - Common option actions
- (void)handleOption:(NSString *)key forChartView:(ChartViewBase *)chartView {
    if ([key isEqualToString:@"toggleValues"]) {
        for (id<IChartDataSet> set in chartView.data.dataSets) {
            set.drawValuesEnabled = !set.isDrawValuesEnabled;
        }
        [chartView setNeedsDisplay];
    }
}

-(int)getTimerRangeOfGraph {
    if ([self.timeRange isEqualToString:NSLocalizedString(@"day", nil)]) {
        [self showGraphNumbers:24];
        self.stackVLeadingLayout.constant = 40;
        return 24;
    } else if ([self.timeRange isEqualToString:NSLocalizedString(@"week", nil)]) {
        self.stackVLeadingLayout.constant = 50;
        self.stackVTrailingLayout.constant = 40;
        [self showGraphNumbers:7];
        return 7;
    } else if ([self.timeRange isEqualToString:NSLocalizedString(@"month", nil)]) {
        [self showGraphNumbers:30];
        self.stackVLeadingLayout.constant = 30;
        self.stackVTrailingLayout.constant = 0;
        return 30;//month;
    } else if ([self.timeRange isEqualToString:NSLocalizedString(@"year", nil)]) {
        self.stackVLeadingLayout.constant = 40;
        self.stackVTrailingLayout.constant = 10;
        [self showGraphNumbers:12];
        return 12;
    }
    return 0;
}

-(void)showGraphNumbers:(int)count {
    for (int i = 0; i < 31; i++) {
        [[self.daysCollection objectAtIndex:i] setHidden:YES];
    }
    for (int i = 0; i < count; i++) {
        for (UILabel * lb in self.daysCollection) {
            if ([lb tag] == i) {
                [lb setHidden:NO];
               // NSLog(@"[[self getHorizontalElementList]objectAtIndex:i] = %@\n", [[self getHorizontalElementList]objectAtIndex:i]);
                if ([self.timeRange isEqual:NSLocalizedString(@"day", nil)]) {
                    if (i%3 == 0 ) {
                        [lb setText:[[self getHorizontalElementList]objectAtIndex:i]];
                    } else {
                        [lb setText:@""];
                    }
                } else  if ([self.timeRange isEqual:NSLocalizedString(@"month", nil)]) {
                    if (i%2 == 0 ) {
                        [lb setText:[[self getHorizontalElementList]objectAtIndex:i]];
                    } else {
                        [lb setText:@""];
                    }
                } else {
                    [lb setText:[[self getHorizontalElementList]objectAtIndex:i]];
                }
                
            }
        }
    }
}

-(NSMutableArray * )getHorizontalElementList {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[[NSDate alloc] init]];
    [components setHour:-[components hour]];
    [components setMinute:-[components minute]];
    [components setSecond:-[components second]];
    NSDate *today = [cal dateByAddingComponents:components toDate:[[NSDate alloc] init] options:0];
    
    if ([self.timeRange isEqual:NSLocalizedString(@"day", nil)]) {
        NSMutableArray * dayArr = [NSMutableArray array];
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"HH:mm"];
        
        for (int i = 23; i >= 0 ; i--) {
            NSDate *minusOneHr = [[NSDate date] dateByAddingTimeInterval:-60*60 * i];
            NSString *dayString = [[outputFormatter stringFromDate:minusOneHr] capitalizedString];
            [dayArr addObject:dayString];
        }
        NSString *todayString = [[outputFormatter stringFromDate:[NSDate date]] capitalizedString];
        [dayArr addObject:todayString];
        
        return dayArr;
        
    } else if ([self.timeRange isEqual:NSLocalizedString(@"week", nil)]) {
        NSMutableArray * weekArr = [NSMutableArray array];
        for (int i = 6; i >= 0 ; i--) {
            [components setHour:(i*-24)];
            [components setMinute:0];
            [components setSecond:0];
            NSDate *yesterday = [cal dateByAddingComponents:components toDate: today options:0];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat=@"EEE";
            NSString *dayString = [[dateFormatter stringFromDate:yesterday] capitalizedString];
            [weekArr addObject:dayString];
        }
        return  weekArr;
        
    } else if ([self.timeRange isEqual:NSLocalizedString(@"month", nil)]) {
        NSMutableArray * monthArr = [NSMutableArray array];
        for (int i = 29; i >= 0 ; i--) {
            [components setHour:(i*-24)];
            [components setMinute:0];
            [components setSecond:0];
            NSDate *yesterday = [cal dateByAddingComponents:components toDate: today options:0];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat=@"dd";
            NSString *dayString = [[dateFormatter stringFromDate:yesterday] capitalizedString];
            [monthArr addObject:dayString];
        }
        return monthArr;
        
    } else if ([self.timeRange isEqual:NSLocalizedString(@"year", nil)]) { //year+
        NSDateComponents *components1 = [cal components:( NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour ) fromDate:[[NSDate alloc] init]];
        
        [components1 setMonth:1];
        NSDate *today = [cal dateByAddingComponents:components1 toDate:[[NSDate alloc] init] options:0];
        NSMutableArray * yearArr = [NSMutableArray array];
        for (int i = 0; i < 12; i++) {
            [components1 setMonth:i];
            NSDate *yesterday = [cal dateByAddingComponents:components1 toDate: today options:0];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat=@"MMM";
            NSString *dayString = [[dateFormatter stringFromDate:yesterday] capitalizedString];
            NSString *month = [dayString substringToIndex:3];

            [yearArr addObject:month];
        }
        return yearArr;
    }
    return nil;
}
@end
