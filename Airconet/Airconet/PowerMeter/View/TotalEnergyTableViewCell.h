//
//  TotalEnergyTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 08-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Airconet-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface TotalEnergyTableViewCell : UITableViewCell <ChartViewDelegate> 
@property (strong, nonatomic) IBOutlet BarChartView *chartView;
@property (strong, nonatomic) IBOutlet UILabel *acLb;
@property (strong, nonatomic) IBOutlet UILabel *swLb;
@property (strong, nonatomic) IBOutlet UILabel *othersLb;
@property (strong, nonatomic) IBOutlet UIView *deviceTypeBackV;
@property (strong, nonatomic) IBOutlet UIView *daysBackV;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *daysCollection;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *stackVLeadingLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *stackVTrailingLayout;

@property (nonatomic, strong) NSString *timeRange;
@property (nonatomic, strong) NSArray * acValueList;
@property (nonatomic, strong) NSArray * swValueList;
@property (nonatomic, strong) NSArray * othersValueList;

@end

NS_ASSUME_NONNULL_END
