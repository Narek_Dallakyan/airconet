//
//  OpenMessageViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/1/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "OpenMessageViewController.h"
#import "LivingRoomViewController.h"
#import "MessagesViewController.h"

@interface OpenMessageViewController () {
    NSMutableArray * allMessages;
    NSMutableArray * allH250;

}
@property (weak, nonatomic) IBOutlet UIView *mNetworkErrorV;
@property (weak, nonatomic) IBOutlet UILabel *mNetworkErrorLb;
@property (weak, nonatomic) IBOutlet UILabel *mDateLb;
@property (weak, nonatomic) IBOutlet UILabel *mHeadingMessageLb;
@property (weak, nonatomic) IBOutlet UILabel *mErrorNumberLb;
@property (weak, nonatomic) IBOutlet UILabel *mMessageLB;
@property (weak, nonatomic) IBOutlet UIButton *mOkBtn;
@property (weak, nonatomic) IBOutlet UIButton *mTechSupportBtn;
@property (strong, nonatomic) IBOutlet UILabel *mSystemMessageLb;
@property (strong, nonatomic) IBOutlet UILabel *mPpmLb;

@end

@implementation OpenMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mSystemMessageLb.text = NSLocalizedString(@"system_message", nil);
   // [self setupView];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.navigationController.navigationBar setHidden:YES];
    [self setBackground:self];
    [self setupView];
}


-(void)setupView {
    allMessages = [NSMutableArray array];
    allH250 = [NSMutableArray array];
    if (_deviceMac) { //came from Main page
        [self getError];
    } else { //came from message page
        [self setErrorInfo];
    }
}
-(void)setErrorInfo {
    [self setErrorView];
    [_mDateLb setText:_currErrMessage.date];
    [_mHeadingMessageLb setText:_currErrMessage.name];
    [_mHeadingMessageLb setTextColor:_currErrMessage.color];
    [_mMessageLB setText:_currErrMessage.errorCode];
    [_mErrorNumberLb setText:[NSString stringWithFormat:@"%@ #%@", NSLocalizedString(@"err_num", nil), _currErrMessage.errorNumber]];
    if (_currErrMessage.ppm.length > 0) {
        [_mPpmLb setHidden:NO];
        [_mPpmLb setText:_currErrMessage.ppm];
    }
}

-(void)setErrorView {
    if (_currErrMessage.mac.length > 0) { //error
       [self.mSystemMessageLb setHidden:YES];
        [self.mNetworkErrorV setHidden:NO];
        [_mTechSupportBtn setHidden:NO];
        [_mErrorNumberLb setHidden:NO];
        [_mNetworkErrorLb setText:NSLocalizedString(@"dev_err", nil)];
        
    } else {//notification
        [self.mSystemMessageLb setHidden:NO];
        [self.mNetworkErrorV setHidden:YES];
        [_mTechSupportBtn setHidden:YES];
        [_mErrorNumberLb setHidden:YES];
        [_mNetworkErrorLb setText:NSLocalizedString(@"system_message", nil)];
        
    }
}
- (void)getError {
    [[RequestManager sheredMenage] getJsonDataWithUrl:AGetnotificationquery andParameters:@{} isNeedCookie:NO success:^(id  _Nonnull response) {
        NSNumber *result = [response objectForKey:AResultCode];
               if (result.intValue == 0) {
                   NSDictionary *result = [response objectForKey:AAttachment];
                   [self setErrorMessages:result];
               }
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

-(void)setErrorMessages:(NSDictionary *)result {
    for (NSDictionary * dic in [result objectForKey:@"errors"]) {
        [allMessages addObject:[[[ErrorMessage alloc] init] getErrorMessageObj:dic]];
    }
    for (NSDictionary * dic in [result objectForKey:@"h250"]) {
           [allH250 addObject:[[[H250 alloc] init] getH250Obj:dic]];
       }
    _currErrMessage = [self getDeviceError];
    [self setErrorInfo];
}

-(ErrorMessage *)getDeviceError {
    for (ErrorMessage * errMess in allMessages) {
        if ([errMess.mac isEqualToString:_deviceMac]) {
            return  errMess;
        }
    }
    return nil;
}

- (IBAction)techSupport:(UIButton *)sender {
    [self pushViewControllerWithIdentifier:@"TechSupportViewController" controller:self];

}
- (IBAction)ok:(UIButton *)sender {
    [self.navigationController.navigationBar setHidden:NO];
    [self.navigationController popViewControllerAnimated:YES];

}



@end
