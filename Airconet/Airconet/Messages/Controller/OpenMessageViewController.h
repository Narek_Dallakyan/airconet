//
//  OpenMessageViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/1/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"
#import "ErrorMessage.h"
#import "AllDevises.h"

NS_ASSUME_NONNULL_BEGIN

@interface OpenMessageViewController : BaseViewController
@property (strong, nonatomic) ErrorMessage * currErrMessage;
@property (strong, nonatomic) AllDevises * currAllDevice;
@property (strong, nonatomic) NSString * deviceMac;
@property (assign, nonatomic) BOOL isMainPage;


@end

NS_ASSUME_NONNULL_END
