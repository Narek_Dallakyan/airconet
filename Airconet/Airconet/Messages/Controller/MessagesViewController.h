//
//  MessagesViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/31/20.
//  Copyright © 2020Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MessagesViewController : BaseViewController
@end

NS_ASSUME_NONNULL_END
