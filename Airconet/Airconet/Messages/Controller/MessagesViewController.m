//
//  MessagesViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/31/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "MessagesViewController.h"
#import "MessagesTableViewCell.h"
#import "LoadMessageTableViewCell.h"
#import "OpenMessageViewController.h"
#import "ErrorMessage.h"
#import "APublicDefine.h"
#import "PopupView.h"
#import "AppDelegate.h"

@interface MessagesViewController ()<PopupViewDelegate> {
    NSMutableArray * allMessages;
    UIButton * navDeletBtn;
    UIButton * navBackBtn;
    NSInteger pageNumber;
    BOOL isLoadCell;
    NSNumber * badge;
    
}
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityV;
@property (weak, nonatomic) IBOutlet UITableView *mMessagesTbV;
@end

@implementation MessagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [_mActivityV startAnimating];
    [self setBackground:self];
    [self setupView];
}

-(void)setupView {
    NSString * token =  [(AppDelegate*)[[UIApplication sharedApplication] delegate] firebaseToken];
    if (token.length > 0) {
        UIApplication.sharedApplication.applicationIconBadgeNumber = 0;
        badge = @(UIApplication.sharedApplication.applicationIconBadgeNumber);
        [self performSelectorInBackground:@selector(restartBadge:) withObject:token];
    }
    
    allMessages = [NSMutableArray array];
    pageNumber = 1;
    [self setNavigationStyle];
    [self setNavigationBarButton];
    [self getErrorCode];
}
-(void)setNavigationBarButton {
    UIImage* deleteImage = [UIImage imageNamed:@"delete_nav"];
    navDeletBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, deleteImage.size.width, deleteImage.size.height)];
    [navDeletBtn setBackgroundImage:deleteImage forState:UIControlStateNormal];
    [navDeletBtn addTarget:self action:@selector(deleteAllMessages)
         forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithCustomView:navDeletBtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    UIImage* backImg = [UIImage imageNamed:@"ic_left"];
    if ([BaseViewController isDeviceLanguageRTL]) {
        backImg = [UIImage imageNamed:@"right_direction"];
    }
    navBackBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, backImg.size.width, backImg.size.height)];
    [navBackBtn setBackgroundImage:backImg forState:UIControlStateNormal];
    [navBackBtn addTarget:self action:@selector(back)
          forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:navBackBtn];
    self.navigationItem.leftBarButtonItem = leftButton;
}

-(void)setNavigationStyle {
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self setNavigationStyle:BaseColor tintColor:[UIColor whiteColor] titleText:NSLocalizedString(@"messages", nil) titleColor:NavTitleColor];
      for (UIView *view in self.navigationController.navigationBar.subviews) {
            if ([view isKindOfClass:[DevicesNavigationBar class]]) {
                [view removeFromSuperview];
            }
        }
}
-(void)hiddenNavigationItems:(BOOL)hide {
    [navDeletBtn setHidden: hide];
    [navBackBtn setHidden:hide];
}

-(void)hideDeleteAll {
     if (self->allMessages.count == 0) {
        [self->navDeletBtn setHidden:YES];
     } else {
         [self->navDeletBtn setHidden:NO];

     }
}
#pragma mark: GET
- (void)getErrorCode {
    [[RequestManager sheredMenage] getJsonDataWithUrl:AGetAlertAll andParameters:@{@"pageNumber":@(pageNumber), @"pageSize":@(20)} isNeedCookie:NO success:^(id  _Nonnull response) {
       // NSLog(@"getErrorCode response = %@\n", response);
        self->pageNumber++;
        [self setErrorMessages:response];
        
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"getErrorCode Error = %@\n", error.localizedDescription);
    }];
}

-(void)setErrorMessages:(NSArray *)response {
    for (NSDictionary * dic in response) {
        [allMessages addObject:[[[ErrorMessage alloc] init] getErrorMessageObj:dic]];
    }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideDeleteAll];
            [self.mMessagesTbV reloadData];
            [self.mActivityV stopAnimating];
        });

}

-(void)deleteErrMessage:(ErrorMessage *)currMessage success:(void (^)(id))success {
    NSString * formatUrl = [NSString stringWithFormat:@"%@?id=%@",AGetnotificationerrorCodedelete,
                            currMessage.errId];
    [[RequestManager sheredMenage] postJsonDataWithUrl:formatUrl andParameters:@{} success:^(id response) {
        success(response);
    } failure:^(NSError *error) {
    }];
    
}

-(void)restartBadge:(NSString *)token {
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostResetBadge andParameters:@{@"token" : token, @"currentBadgeNumber" : self->badge} success:^(id response) {
        //NSLog(@"APostResetBadge response = %@\n",response);
       } failure:^(NSError *error) {
           //NSLog(@"APostResetBadge Error = %@\n", error.localizedDescription);
       }];
}
 
-(void)back {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) deleteAllMessages {
    [self popupAlert:NSLocalizedString(@"delete_alert", nil)
                  frame:CGRectMake(0, -44, 230, 170) ok:NSLocalizedString(@"yes", nil)
                 cencel:NSLocalizedString(@"no", nil) delegate:self];
}

-(void)openMessage:(ErrorMessage *)currMessage {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    OpenMessageViewController * vc = [storyboard   instantiateViewControllerWithIdentifier:@"OpenMessageViewController"];
    vc.currErrMessage = currMessage;
     [self.navigationController pushViewController:vc animated:YES];

}
- (IBAction)deleteMessage:(UIButton *)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.mMessagesTbV];
    NSIndexPath *indexPath = [self.mMessagesTbV indexPathForRowAtPoint:buttonPosition];
    if (self->allMessages.count > 0 && indexPath.row >= 0 && indexPath.row <= self->allMessages.count-1) {
        dispatch_queue_t q = dispatch_queue_create("com.my1.queue", NULL);
        dispatch_sync(q, ^{
            [self deleteErrMessage:[self->allMessages objectAtIndex:indexPath.row] success:^(id response) {
                if ([[response objectForKey:AResultCode] intValue] == 0) {
                    if (self->allMessages.count > 0 && indexPath.row >= 0 && indexPath.row <= self->allMessages.count-1) {
                        [self->allMessages removeObjectAtIndex:indexPath.row];
                        [self.mMessagesTbV deselectRowAtIndexPath:indexPath animated:YES];
                        [self.mMessagesTbV reloadData];
                        [self hideDeleteAll];
                    }
                }
                
            }];
        });
    }

}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (allMessages.count > 0) {
        return allMessages.count +1;
    }
    return allMessages.count;//message count
}

#pragma mark - UITableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (pageNumber > 1 && indexPath.row == allMessages.count) {
        LoadMessageTableViewCell * loadMessageCell = [tableView dequeueReusableCellWithIdentifier:@"LoadMessageCell"];
        if (isLoadCell) {
             [loadMessageCell.mLoadActivityV startAnimating];
        } else {
            [loadMessageCell.mLoadActivityV stopAnimating];
        }
        return loadMessageCell;
    } else {
        MessagesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"messageCell"];
        ErrorMessage * errMessage = [allMessages objectAtIndex:indexPath.row];
        [cell.mHeaderLb setTextColor:errMessage.color];
        [cell.mHeaderLb setText:errMessage.name];
        [cell.mMessageLb setText:errMessage.errorCode];
        [cell.mDateLb setText:errMessage.date];
        if ([errMessage.color isEqual:RedColor]) {
            [cell.mErrorImg setHidden:NO];
        } else {
            [cell.mErrorImg setHidden:YES];
        }
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self openMessage:[allMessages objectAtIndex:indexPath.row]];
}

#pragma mark --- UIScrollView Delegate Method ---

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float offset = (scrollView.contentOffset.y - (scrollView.contentSize.height - scrollView.frame.size.height));
    if (offset >= 0 && offset <= 105)
    {
        NSIndexPath *indexPath  = [NSIndexPath indexPathForRow:allMessages.count inSection:0];
        isLoadCell = YES;
        
                [self.mMessagesTbV beginUpdates];
                [self.mMessagesTbV reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
                [self.mMessagesTbV endUpdates];
        isLoadCell = NO;
        [self getErrorCode];
    }

   // NSLog(@"Scroll : %f",offset);
}

#pragma mark: Popup  Delegate
-(void)handlerYes {
   // NSLog(@"Popup  Delegate Popup  Delegate\n");
    [self.mActivityV startAnimating];
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostDeletAllError andParameters:@{} success:^(id response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self->allMessages removeAllObjects];
            [self hideDeleteAll];
            [self.mMessagesTbV reloadData];
            [self.mActivityV stopAnimating];

        });

       } failure:^(NSError *error) {
           [self.mActivityV stopAnimating];
          // [self.view makeToast:NSLocalizedString(@"Operation Failure!", @"")];
       }];

}
-(void)handlerCancel {
}
@end
