//
//  MessagesTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/31/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessagesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *mTopLineV;
@property (weak, nonatomic) IBOutlet UIView *mBottomV;
@property (weak, nonatomic) IBOutlet UILabel *mDateLb;
@property (weak, nonatomic) IBOutlet UILabel *mHeaderLb;
@property (weak, nonatomic) IBOutlet UILabel *mMessageLb;
@property (weak, nonatomic) IBOutlet UIImageView *mErrorImg;
@property (weak, nonatomic) IBOutlet UIButton *mDeleteBtn;
- (IBAction)deleteMessage:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
