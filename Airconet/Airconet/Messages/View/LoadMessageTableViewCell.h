//
//  LoadMessageTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 30-05-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoadMessageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * mLoadActivityV;
@end

NS_ASSUME_NONNULL_END
