//
//  DeviceSuccessfulyAddedTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 18-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "DeviceSuccessfulyAddedTableViewCell.h"
#import "UITextField+Content.h"
#import "APublicDefine.h"


@implementation DeviceSuccessfulyAddedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setupView];
}

-(void)setupView {
    [self.mNameTextFl setBlueBorderStyle];
    [self.mNameTextFl setPlaceholderColor:PlaceholderColor text:self.mNameTextFl.placeholder];
    self.mSaveBtn.layer.cornerRadius = 4.f;
}
- (IBAction)save:(UIButton *)sender {
    [self.delegate handlerSave:sender name:self.mNameTextFl.text];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

     if (textField.tag == _deviceCount -1) {
           [textField resignFirstResponder];
     } else {
         [self.delegate returenName:textField];
     }
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
   // NSLog(@"textFieldShouldEndEditing tag = %li\n  text = %@\n", (long)textField.tag, textField.text );
    [self.delegate getDeviceName:textField.text index:textField.tag];

    return YES;
}
@end
