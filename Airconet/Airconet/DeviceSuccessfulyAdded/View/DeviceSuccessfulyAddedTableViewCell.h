//
//  DeviceSuccessfulyAddedTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 18-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol DeviceSuccessfulyAddedDelegate <NSObject>

@optional
-(void)getDeviceName:(NSString*)name index:(NSInteger)index;
-(void)handlerSave:(UIButton *)btn name:(NSString *)name;
-(void)returenName:(UITextField *)textFild;

@end
@interface DeviceSuccessfulyAddedTableViewCell : UITableViewCell
@property (weak, nonatomic) id<DeviceSuccessfulyAddedDelegate> delegate;
@property (assign, nonatomic) NSInteger  deviceCount;
@property (weak, nonatomic) IBOutlet UITextField *mNameTextFl;
@property (weak, nonatomic) IBOutlet UIButton *mSaveBtn;

@end

NS_ASSUME_NONNULL_END
