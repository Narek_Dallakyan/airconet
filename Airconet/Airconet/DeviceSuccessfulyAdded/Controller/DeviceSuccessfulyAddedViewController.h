//
//  DeviceSuccessfulyAddedViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 18-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"
#import "Devices.h"

NS_ASSUME_NONNULL_BEGIN

@interface DeviceSuccessfulyAddedViewController : BaseViewController
@property (strong, nonatomic) NSMutableArray * mNewDevicesArr;

@end

NS_ASSUME_NONNULL_END
