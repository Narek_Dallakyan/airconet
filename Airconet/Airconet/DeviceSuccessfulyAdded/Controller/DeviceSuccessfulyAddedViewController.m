//
//  DeviceSuccessfulyAddedViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 18-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "DeviceSuccessfulyAddedViewController.h"
#import "DeviceSuccessfulyAddedTableViewCell.h"
#import "APublicDefine.h"
#import "VSDropdown.h"
#import "ACommonTools.h"
#import "GCDAsyncUdpSocket.h"
#import "ASetDeviceSocket.h"
#import "Reachability.h"
#import "DeviceErrorView.h"



@interface DeviceSuccessfulyAddedViewController ()< VSDropdownDelegate, CustomAlertDelegate, DeviceSuccessfulyAddedDelegate, GCDAsyncUdpSocketDelegate, PopupRemotControllDelegate>  {
    VSDropdown *_dropdown;
    NSString * deviceType;
    NSIndexPath *clickedIndexPath;
    __block int saveDeviceCounter;
    __block int saveDeviceIndex;
 
}
@property (weak, nonatomic) IBOutlet UIImageView *mCheckImg;
@property (weak, nonatomic) IBOutlet UILabel *mSuccessLb;
@property (weak, nonatomic) IBOutlet UIView *mSelectAirConBackgV;
@property (weak, nonatomic) IBOutlet UIButton *mSelectAirConDropDownBtn;
@property (weak, nonatomic) IBOutlet UIButton *mTriangleBtn;
@property (weak, nonatomic) IBOutlet UITableView *mNameTbV;
@property (weak, nonatomic) IBOutlet UIButton *mSaveBtn;
@property (nonatomic, strong) ASetDeviceSocket *setSocket;
@property (nonatomic, strong) NSMutableArray * devicesArr;




@end

@implementation DeviceSuccessfulyAddedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   // NSLog(@"DeviceSuccessfulyAddedViewController\n");
    [self setupView];
    
    // Do any additional setup after loading the view.
}
-(void) viewWillAppear:(BOOL)animated {
    [self setBackground:self];
    [self dropDownSetup];

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeDropDown];
}

-(void)setNavigationStyle {
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self setNavigationStyle:BaseColor tintColor:[UIColor whiteColor] titleText:NSLocalizedString(@"add_device", nil) titleColor:NavTitleColor];
      for (UIView *view in self.navigationController.navigationBar.subviews) {
            if ([view isKindOfClass:[DevicesNavigationBar class]]) {
                [view removeFromSuperview];
            }
        }
}
-(void)setupView {

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self setNavigationStyle];
    self.mSelectAirConBackgV.layer.cornerRadius = 5.f;
    self.mSelectAirConBackgV.layer.borderColor = BaseColor.CGColor;
    self.mSelectAirConBackgV.layer.borderWidth = 3;
    self.mNameTbV.separatorColor = [UIColor clearColor];
    [self.mSelectAirConBackgV setHidden:NO];
    [self.mNameTbV reloadData];
}

-(void) showErrorController:(BOOL)isNetworkError  {
    NSLog(@"showErrorController");
    UIWindow * keyWindow = [BaseViewController keyWindow];
    DeviceErrorView * deviceErrorV = [[DeviceErrorView alloc] initWithFrame:[keyWindow bounds] delegate:self];
    deviceErrorV.isNetworkErr = isNetworkError;
    [keyWindow addSubview:deviceErrorV];
        deviceErrorV.mScrollV.contentOffset = CGPointMake(0, 0);
    deviceErrorV.mScrollV.scrollEnabled = YES;
   // deviceErrorV.mScrollContenierHeight.constant = self.mScrollV.frame.size.height;
    deviceErrorV.userInteractionEnabled = YES;
    UIButton * buttons = [deviceErrorV.mTechSupport objectAtIndex:0];
     deviceErrorV.mScrollV.contentSize = CGSizeMake( [[UIScreen mainScreen] bounds].size.width, buttons.frame.origin.y -88);
}

-(BOOL)isValidFields:(NSInteger)index {
    
    if ([self.mSelectAirConDropDownBtn.titleLabel.text isEqualToString:NSLocalizedString(@"select_dev_type", nil)]) {
        [self showAlert:NSLocalizedString(@"select_dev_type_err", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        return NO;
    }
    
    Devices * dev = [_mNewDevicesArr objectAtIndex:index];
           // NSLog(@"_mNewDevicesArr.coutn = %i\n dev.name = %@\n ", _mNewDevicesArr.count, dev.name);
            if (dev.name.length == 0) {
                [self showAlert:NSLocalizedString(@"fill_name", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
                return NO;
            }
    return YES;
}
#pragma mark: Actions
- (IBAction)save:(UIButton *)sender {
}

#pragma mark: DROP DOWN
-(void) dropDownSetup {
    _dropdown = [[VSDropdown alloc]initWithDelegate:self];
    [_dropdown setAdoptParentTheme:YES];
    [_dropdown setShouldSortItems:YES];
    _dropdown.allowMultipleSelection = NO;
    _dropdown.maxDropdownHeight = 200.f;
    _dropdown.textColor = PlaceholderColor;
    _dropdown.separatorColor = PlaceholderColor;
    _dropdown.tableView.backgroundColor = [BaseViewController setDropDownColor:self];
    [self.mTriangleBtn setImage:[UIImage imageNamed:@"gray_down_triangle"] forState:UIControlStateNormal];
    [ACommonTools setObjectValue:@0 forKey:@"DropDownX"];
    [ACommonTools setObjectValue:@(SCREEN_Width -40) forKey:@"DropDownWidth"];
}


-(void)removeDropDown {
    [self.mTriangleBtn setImage:[UIImage imageNamed:@"gray_down_triangle"] forState:UIControlStateNormal];
    [_dropdown remove];
}
-(void)showDropDownForButton:(UIButton *)sender adContents:(NSArray *)contents {
    [_dropdown setDrodownAnimation:rand()%2];
    [_dropdown setupDropdownForView:sender];
    [_dropdown setSeparatorColor:sender.titleLabel.textColor];
    [_dropdown reloadDropdownWithContents:contents andSelectedItems:@[[sender titleForState:UIControlStateNormal]]];
}
- (IBAction)selectAirConDropDown:(UIButton *)sender {
    
    if ([self.mTriangleBtn.imageView.image isEqual:[UIImage imageNamed:@"gray_down_triangle"]]) {
           [self showDropDownForButton:sender adContents:DeviceTypeList];
           [self.mTriangleBtn setImage:[UIImage imageNamed:@"gray_up_triangle"] forState:UIControlStateNormal];
       } else {
           [self.mTriangleBtn setImage:[UIImage imageNamed:@"gray_down_triangle"] forState:UIControlStateNormal];
           [_dropdown remove];
       }
}
- (IBAction)selectAirConTriangle:(UIButton *)sender {
    if ([sender.imageView.image isEqual:[UIImage imageNamed:@"gray_down_triangle"]]) {
              [self showDropDownForButton:_mSelectAirConDropDownBtn adContents:DeviceTypeList];
              [self.mTriangleBtn setImage:[UIImage imageNamed:@"gray_up_triangle"] forState:UIControlStateNormal];
          } else {
              [self.mTriangleBtn setImage:[UIImage imageNamed:@"gray_down_triangle"] forState:UIControlStateNormal];
              [_dropdown remove];
          }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _mNewDevicesArr.count;
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DeviceSuccessfulyAddedTableViewCell * deviceSuccessfulyCell = [tableView dequeueReusableCellWithIdentifier:@"DeviceSuccessfulyAddedCell"];
    deviceSuccessfulyCell.delegate = self;
    [deviceSuccessfulyCell.mNameTextFl setTag: indexPath.row];
    deviceSuccessfulyCell.deviceCount = _mNewDevicesArr.count;
    if (indexPath.row == _mNewDevicesArr.count -1) {
        deviceSuccessfulyCell.mNameTextFl.returnKeyType = UIReturnKeyDone;
    } else {
        deviceSuccessfulyCell.mNameTextFl.returnKeyType = UIReturnKeyNext;
    }
    return deviceSuccessfulyCell;
}

#pragma mark - VSDropdown Delegate methods.
- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected {
    
    if (selected) {
        UIButton *btn = (UIButton *)dropDown.dropDownView;
        NSString *allSelectedItems = nil;
        allSelectedItems = [dropDown.selectedItems firstObject];
        [btn setTitle:allSelectedItems forState:UIControlStateNormal];
        deviceType = allSelectedItems;
    }
    [self.mTriangleBtn setImage:[UIImage imageNamed:@"gray_down_triangle"] forState:UIControlStateNormal];
}

#pragma mark:  Error Delegate
-(void)handlerRetryOk {
    [self hideErrorView];
}
-(void)handlerTechSupport {
    [self hideErrorView];
    [self pushViewControllerWithIdentifier:@"TechSupportViewController" controller:self];
}
-(void)handlerBack {
    [self hideErrorView];
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)hideErrorView {
    UIWindow * keyWindow = [BaseViewController keyWindow];
    for(UIView *subView in keyWindow.subviews) {
        if([subView isKindOfClass:[DeviceErrorView class]]){
            [subView removeFromSuperview];
        }
    }
}
#pragma mark: PopupRemotControllDelegate
-(void)handlerRemotControllOk {
    if (saveDeviceIndex == self.mNewDevicesArr.count) {
        [self goToMainPage];
    }
}
#pragma mark: DeviceSuccessfulyAddedDelegate
-(void)getDeviceName:(NSString *)name index:(NSInteger)index {
    [[_mNewDevicesArr objectAtIndex:index] setName:name ];
    NSLog(@"textfiled name = %@\n     textfiled index = %li\n ", name, index);
}
-(void)returenName:(UITextField *)textFild {
    CGPoint buttonPosition = [textFild convertPoint:CGPointZero toView:self.mNameTbV];
    clickedIndexPath = [self.mNameTbV indexPathForRowAtPoint:buttonPosition];
    if (clickedIndexPath.row == _mNewDevicesArr.count -1) {
    } else {
        DeviceSuccessfulyAddedTableViewCell *cell = (DeviceSuccessfulyAddedTableViewCell *)[self.mNameTbV cellForRowAtIndexPath: [NSIndexPath indexPathForRow:clickedIndexPath.row +1 inSection:0]];
        [cell.mNameTextFl becomeFirstResponder];
    }
}

-(void)handlerSave:(UIButton *)btn name:(nonnull NSString *)name {
    CGPoint buttonPosition = [btn convertPoint:CGPointZero toView:self.mNameTbV];
       clickedIndexPath = [self.mNameTbV indexPathForRowAtPoint:buttonPosition];
     [[_mNewDevicesArr objectAtIndex:clickedIndexPath.row] setName:name ];
    NSLog(@"handlerSave index = %i\n", clickedIndexPath.row);
    if ([self isValidFields:clickedIndexPath.row]) {
        [self saveDevice:clickedIndexPath.row];
    }
}

#pragma mark -- Keyboard Fubctions
- (void)keyboardWillShow:(NSNotification *)sender {
    CGFloat height = [[sender.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    NSLog(@"SCREEN_Height = %f\n     keyboard  height = %f\n ", SCREEN_Height, height);
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, (SCREEN_Height - height)/2, 0);
    _mNameTbV.contentInset = edgeInsets;
    _mNameTbV.scrollIndicatorInsets = edgeInsets;
}

- (void)keyboardWillHide:(NSNotification *)sender {
    UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
    _mNameTbV.contentInset = edgeInsets;
    _mNameTbV.scrollIndicatorInsets = edgeInsets;
}
#pragma mark: Device Status
- (void)getDeviceStatu:(NSString *)deviceMac {
    NSString *formatUrl = [NSString stringWithFormat:@"%@/%@",AGetdevqueryHvac,deviceMac];
    [[RequestManager sheredMenage] getJsonDataWithUrl:formatUrl
                                        andParameters:@{}
                                         isNeedCookie:NO success:^(id response) {
        NSLog(@"request = %@\n  respons = %@\n", formatUrl, response);
        
        self->saveDeviceCounter++;
                   if (self->saveDeviceCounter == self->_mNewDevicesArr.count) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           if ([self->deviceType isEqualToString:NSLocalizedString(@"ac", nil)] || [self->deviceType isEqualToString:NSLocalizedString(@"ac_sensor", nil)]) {
                               [self popupRemoteControllAlert:CGRectMake(0, -44, 280, 230) ok:NSLocalizedString(@"ok", nil) delegate:self];
                           } else {
                               [self goToMainPage];
                               
                           }
                       });
                   }

    } failure:^(NSError *error) {
        [self showErrorController:YES];
        NSLog(@"error = %@\n", error.localizedDescription);
    }];
}

#pragma mark: SAVE DEVICE
- (void)saveDevice:(NSInteger)index {
    Devices * device = [_mNewDevicesArr objectAtIndex:index];
    NSString *configData = [NSString stringWithFormat:@"{\"client_set\":[{\"ip\":\"%s\",\"port\":%@,\"protocol\":\"UDP\",\"local_port\":80},{\"protocol\":\"\",\"ip\":\"\"},{\"protocol\":\"\",\"ip\":\"\"},{\"protocol\":\"\",\"ip\":\"\"}]}",ServerIP,AServerUDPPort];
    ASetDeviceSocket *socket = [[ASetDeviceSocket alloc] init];
    socket.ip =  device.privateIp;// [param objectForKey:@"privateIp"];
    socket.command = @"/config?command=client";
    socket.configData = configData;
    self.setSocket = socket;
    [socket send];
    NSLog(@"\nDeviceSuccessfulyAddedViewController\n  device.privateIp = %@\n",  device.privateIp);
    [self sendSave:device index:index];
}

-(void)sendSave:(Devices *)device index:(NSInteger)index {
   // @[@"Air Conditioner", @"AC + Sensor", @"Thermostat",@"Switch", @"Power Meter"]
//    NSString * model = self.mSelectAirConDropDownBtn.titleLabel.text;
//    if ([self.mSelectAirConDropDownBtn.titleLabel.text isEqualToString:@"AC + Sensor"]) {
//        model = @"sensor";
//    }
    device.manufacturer = @"";
    device.model = [Devices getDeviceModel:self.mSelectAirConDropDownBtn.titleLabel.text];;
    device.seriesNumber = @"testIos_seriesNumber";
    device.dId = @(index);
    device.name = device.name;
    device.index = [NSNumber numberWithInteger:index];
    device.deviceType = [Devices getDeviceType:deviceType];
    NSMutableDictionary * param = [[Devices getDeviceJson:device] mutableCopy];
    NSLog(@"Device dict for saveing  = %@\n", param);
    
    NSString *formatURL = nil;
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reach currentReachabilityStatus];
    if (status == ReachableViaWiFi) {
        NSString *ssid = [ACommonTools getWifiName];
        NSString * urlSsid = [ssid stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        formatURL = [NSString stringWithFormat:@"%@?ssid=%@",APostdevsaveDevice,urlSsid];
    } else {
        formatURL = APostdevsaveDevice;
    }
    NSLog(@"formatURL = %@\n",formatURL);
    NSLog(@"param = %@\n",param);
    
    [[RequestManager sheredMenage] postJsonDataWithUrl:formatURL andParameters:param success:^(id _Nonnull response) {
           NSLog(@"response = %@\n", response);
            NSNumber *code = [response objectForKey:@"returnCode"];
           if (code.intValue != 0) {
               //Device error
               [self showErrorController:NO];
           }
           else {
               if ([self->deviceType isEqualToString:NSLocalizedString(@"ac", nil)] || [self->deviceType isEqualToString:NSLocalizedString(@"ac_sensor", nil)]) {
                    NSLog(@"enter in  if ([self->deviceType isEqualToString:NSLocalizedString(ac, nil)] || [self->deviceType isEqualToString:NSLocalizedString(ac_sensor, nil)]) case\n");
                   [self popupRemoteControllAlert:CGRectMake(0, -44, 280, 230) ok:NSLocalizedString(@"ok", nil) delegate:self];
                   self->saveDeviceIndex ++;
               } else if (index == self.mNewDevicesArr.count-1) {
                   [self goToMainPage];
                   self->saveDeviceIndex ++;
                   NSLog(@"enter in  if (index == self.mNewDevicesArr.count-1) case\n");
               }
           }
       } failure:^(NSError * _Nonnull error) {
           NSLog(@"error = %@\n", error.localizedDescription);
           [self showErrorController:YES];

       }];
}



@end
