//
//  LoginViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/23/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//


#import "LoginViewController.h"
#import "MainViewController.h"
#import "RequestManager.h"
#import "ACommonTools.h"
#import "CustomAlertView.h"
#import "Validator.h"
#import "AppDelegate.h"
#import "Firebase.h"
//#import <FirebaseCrashlytics/FirebaseCrashlytics.h>

@interface LoginViewController ()<CustomAlertDelegate>
@property (weak, nonatomic) IBOutlet UIView *mLunchScreenV;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityInd;
@end

RequestManager * requestMeneger;

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    //assert(false);
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self setBackground:self];
}

-(void)viewDidAppear:(BOOL)animated {
   if (![ACommonTools getObjectValueForKey:AKeyCurrentUserName]  || ![ACommonTools getObjectValueForKey:AKeyCurrentUserPwd]) {
          [self.mLunchScreenV setHidden:YES];
      } else {
          dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.5 * NSEC_PER_SEC));
          dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
              [self.mLunchScreenV setHidden:YES];
          });
      }

}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void) setupView {
    [self preferredStatusBarStyle];
}


-(BOOL) isValidFields {
    if (self.mLoginView.mEmailTxtFl.text.length == 0 || self.mLoginView.mPasswordTxtFl.text.length == 0) {
           [self showAlert:NSLocalizedString(@"fill_info_note", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
           return NO;
       } else if (![Validator isValidEmail:self.mLoginView.mEmailTxtFl.text]) {
           [self showAlert:NSLocalizedString(@"correct_email_note", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
           return NO;
       }
    return YES;
    }

- (IBAction)login:(UIButton *)sender {
    if ([self isValidFields]) {
        [self.mActivityInd startAnimating];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:self.mLoginView.mEmailTxtFl.text forKey:@"username"];
        [dict setValue:self.mLoginView.mPasswordTxtFl.text forKey:@"password"];
        [dict setValue:@"true" forKey:@"remember-me"];
        [dict setValue:@"fasle" forKey:@"isServer"];
        [dict setValue:@"" forKey:@"token"];
        [dict setValue:@"tRue" forKey:@"isDebug"];
        [dict setValue:@"IOS" forKey:@"os_type"];
        
        if ([(AppDelegate*)[[UIApplication sharedApplication] delegate] firebaseToken].length > 0) {
                [dict setValue:[(AppDelegate*)[[UIApplication sharedApplication] delegate] firebaseToken] forKey:@"firebase-token"];

               }
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostjspringsecuritycheck
                                             andParameters:dict
                                                   success:^(id response) {
            [RequestManager setCookie];
            [ACommonTools setObjectValue:self.mLoginView.mEmailTxtFl.text forKey:AKeyCurrentUserName];
            [ACommonTools setObjectValue:self.mLoginView.mPasswordTxtFl.text forKey:AKeyCurrentUserPwd];
            [ACommonTools setObjectValue:@"yes" forKey:KeyRememberAdminCheck];
            [ACommonTools setBoolValue:YES forKey:AKeyLastLoginSuccess];
            
            [self.mActivityInd startAnimating];
            [self goToMainPage];
        } failure:^(NSError *error) {
            [self.mActivityInd stopAnimating];
            [self showAlert:NSLocalizedString(@"invalid_pass_note", nil) ok:NSLocalizedString(@"ok", nil) cencel: @"" delegate:self];
        }];
    }
}

- (IBAction)forgotPassword:(UIButton *)sender {
    [self presentViewControllerWithIdentifier:@"ResetPassword" controller:self];
}

- (IBAction)signUp:(UIButton *)sender {
    [self presentViewControllerWithIdentifier:@"Signup" controller:self];
}
-(void)handelOk {
     [self.view setAlpha:1.0];
    [self.view setUserInteractionEnabled:YES];
    //NSLog(@"handler Ok from Alert\n");
}

#pragma mark -- Keyboard Fubctions
- (void)keyboardWillShow:(NSNotification *)sender {
    CGFloat height = [[sender.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(self.mScrollView.contentInset.top, 0, height, 0);
    _mScrollView.contentInset = edgeInsets;
    _mScrollView.scrollIndicatorInsets = edgeInsets;
}

- (void)keyboardWillHide:(NSNotification *)sender {
    UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
    _mScrollView.contentInset = edgeInsets;
    _mScrollView.scrollIndicatorInsets = edgeInsets;
}

-(void)goToMainPage {

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"MainPageViewController"]]];
    MainViewController *mainViewController =  [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    mainViewController.rootViewController = navigationController;
    NSUInteger val = 0;
       [mainViewController setupWithType:val];
    UIWindow *window = [BaseViewController keyWindow];
       window.rootViewController = mainViewController;
       [UIView transitionWithView:window
                         duration:0.3
                          options:UIViewAnimationOptionTransitionCrossDissolve
                       animations:nil
                       completion:nil];
}



@end
