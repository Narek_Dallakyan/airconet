//
//  LoginViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/23/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"
#import "LoginView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIScrollView *mScrollView;
@property (strong, nonatomic) IBOutlet LoginView *mLoginView;
-(void)goToMainPage;
@end

NS_ASSUME_NONNULL_END
