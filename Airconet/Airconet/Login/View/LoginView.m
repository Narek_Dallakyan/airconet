//
//  LoginView.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/23/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "LoginView.h"
#import "APublicDefine.h"
#import "UITextField+Content.h"

@implementation LoginView

-(void)awakeFromNib {
  //  [self setLocalizedLang];
    [self.mEmailTxtFl setBlueBorderStyle];
    [self.mPasswordTxtFl setBlueBorderStyle];
    [self.mEmailTxtFl setPlaceholderColor:PlaceholderColor text:self.mEmailTxtFl.placeholder];
     [self.mPasswordTxtFl setPlaceholderColor:PlaceholderColor text:self.mPasswordTxtFl.placeholder];
    [self.mEmailTxtFl setImageInLeftView:[UIImage imageNamed:@"email"]];
    [self.mPasswordTxtFl setImageInLeftView:[UIImage imageNamed:@"password"]];
    [self.mEmailTxtFl setRightPadding];
    [self addButtonToRightViewTextField];
    [super awakeFromNib];
}

//-(void)setLocalizedLang {
//    [self.mLoginLb setCenter:self.center];
//    self.mLoginLb.text = NSLocalizedString(@"login", nil);
//    self.mEmailTxtFl.placeholder = NSLocalizedString(@"e_mail", nil);
//    self.mPasswordTxtFl.placeholder = NSLocalizedString(@"pass", nil);
//    [self.mLoginBtn setTitle:NSLocalizedString(@"login", nil) forState:UIControlStateNormal];
//    [self.mForgotPassBtn setTitle:NSLocalizedString(@"forgot_pass", nil) forState:UIControlStateNormal];
//    [self.mSingupBtn setTitle:NSLocalizedString(@"signup", nil) forState:UIControlStateNormal];
//}
-(void)addButtonToRightViewTextField {
    UIButton * secureBtn = [[UIButton alloc] init];
       [secureBtn addTarget:self action:@selector(secureBtnAction:) forControlEvents:UIControlEventTouchUpInside];
       
       [self.mPasswordTxtFl setButtonInRightView:secureBtn buttonImg:[UIImage imageNamed: @"not_visible"]];
}

-(void)secureBtnAction:(UIButton *)btn {
    if ([btn.imageView.image isEqual:[UIImage imageNamed:@"not_visible"]]) {
        [btn setImage:[UIImage imageNamed:@"visible"] forState:UIControlStateNormal];
        [self.mPasswordTxtFl setSecureTextEntry:NO];
    } else {
        [btn setImage:[UIImage imageNamed:@"not_visible"] forState:UIControlStateNormal];
        [self.mPasswordTxtFl setSecureTextEntry:YES];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.mEmailTxtFl) {
        textField.returnKeyType = UIReturnKeyNext;
    } else if (textField == self.mPasswordTxtFl) {
        textField.returnKeyType = UIReturnKeyDone;
    }
}
-(void)textFieldDidChangeSelection:(UITextField *)textField {
    if ([textField isEqual:self.mEmailTxtFl]) {
        if (textField.text.length == 0) {
            [textField setRightPadding];
        } else
            [textField deletRightPadding];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.mEmailTxtFl) {
        [self.mPasswordTxtFl becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
