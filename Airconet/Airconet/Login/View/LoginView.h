//
//  LoginView.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/23/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomUITextFiled.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginView : UIView <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *mUserImg;
@property (weak, nonatomic) IBOutlet UITextField *mEmailTxtFl;
@property (weak, nonatomic) IBOutlet CustomUITextFiled *mPasswordTxtFl;
@property (weak, nonatomic) IBOutlet UIButton *mLoginBtn;
@property (weak, nonatomic) IBOutlet UIButton *mForgotPassBtn;
@property (weak, nonatomic) IBOutlet UIButton *mSingupBtn;
@property (strong, nonatomic) IBOutlet UILabel *mLoginLb;

@end

NS_ASSUME_NONNULL_END
