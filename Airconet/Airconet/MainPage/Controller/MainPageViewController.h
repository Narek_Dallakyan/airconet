//
//  MainPageViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/25/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum DeviceType {
    devices,
    group
    
}DeviceType;

@interface MainPageViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *mMenuBarBtn;

@end

NS_ASSUME_NONNULL_END
