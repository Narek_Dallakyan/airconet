//
//  MainPageViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/25/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "MainPageViewController.h"
#import "LivingRoomSettingsTableViewCell.h"
#import "LivingRoomViewController.h"
#import "GroupSettingsTableViewCell.h"
#import "OpenMessageViewController.h"
#import "AddDeviceView.h"
#import "RequestManager.h"
#import "AllDevises.h"
#import "Group.h"
#import "SRWebSocket.h"
#import "AVersionConfig.h"
#import "NSDictionary+Object.h"
#import "AFNetworking.h"
#import "AFURLRequestSerialization.h"
#import "ACommonTools.h"
#import "NavigationBar.h"
#import "AppDelegate.h"

@interface MainPageViewController () <DeviceNavigationBarDelegate,
LivingRoomSettingsTableViewCelldelegate,
GroupSettingsCellDelegate,
SRWebSocketDelegate, CustomAlertDelegate> {
    DevicesNavigationBar * navView;
    NavigationBar * navBar;
    NSMutableArray * allDevises;
    NSMutableArray * allGroups;
    
    NSIndexPath * clickedIndexPath;
    UIButton * onOffBtn;
    int allOffCounter;
    BOOL isAllOff;
    BOOL isFeedbackInProces;
    NSDictionary * requestDic;
    NSMutableArray * feedbackMacsArr;
    NSString * currName;
    NSString * currEfficiencyRate;
    NSString * currDevOnMac;
    BOOL currHasDevicePM;;
    BOOL currError;
    BOOL currTimer;
    BOOL isInLivingRoom;
    BOOL isInSelfControll;
    BOOL isCallEfficiencyRate;
    NSNumber * badge;
    NSMutableArray * currTemperatureList;
    NSInteger warningRow;
    
}
@property (strong, nonatomic) NSTimer *offlineTimer;
@property (weak, nonatomic) IBOutlet UILabel *addDevNotesLb;
@property (weak, nonatomic) IBOutlet UIButton *mGroupBtn;
@property (weak, nonatomic) IBOutlet UIButton *mDevicesBtn;
@property (weak, nonatomic) IBOutlet UITableView *mDevicesTbV;
@property (weak, nonatomic) IBOutlet UITableView *mGroupTbv;
@property (weak, nonatomic) IBOutlet UIButton *mStartBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityV;
@property (nonatomic, strong) SRWebSocket *webSocket;




@end
enum DeviceType deviceType;

@implementation MainPageViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self preferredStatusBarStyle];
    [self.mActivityV startAnimating];
    deviceType = devices;
     if (SCREEN_Width < 370) {
         [_mGroupBtn.titleLabel setFont:[UIFont fontWithName:@"Calibri" size:20]];
         [_mDevicesBtn.titleLabel setFont:[UIFont fontWithName:@"Calibri" size:20]];
     }
    // SOCKET ***********************
    if (!ATESTFLAG) {
        [self openWebSocket];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    if (![(AppDelegate*)[[UIApplication sharedApplication] delegate] notificationDic]) {
        
        isInSelfControll = YES;
        isCallEfficiencyRate = NO;
        [self setBackground:self];
        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(openAdminLock:) name:NotificationAdminLock object:nil];
        
        [self.mActivityV startAnimating];
        [self setHiddenDevices];
        
        [self setupView];
        isInLivingRoom = NO;
        if (deviceType == devices) {
            [self getDeviceStatuses];
            //SOCKET
            [self openWebSocket];
        } else {
            [self getGroup];
            deviceType = group;
            [self.mGroupBtn setTitleColor:BaseColor forState:UIControlStateNormal];
            [_mDevicesBtn setTitleColor:GrayDevColor forState:UIControlStateNormal];
            [self hideDevices:YES];
        }
    }
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    if ([(AppDelegate*)[[UIApplication sharedApplication] delegate] notificationDic]) {
        UIApplication.sharedApplication.applicationIconBadgeNumber = 0;
        [self openNotofication];
        [(AppDelegate*)[[UIApplication sharedApplication] delegate] setNotificationDic:nil];
    }
}
-(void)viewDidDisappear:(BOOL)animated {
    
}
-(void)viewWillDisappear:(BOOL)animated {
    
    isInSelfControll = NO;
    [self.offlineTimer invalidate];
    self.offlineTimer = nil;
}

#pragma mark: Open Notification
-(void)openNotofication {
    if ([(AppDelegate*)[[UIApplication sharedApplication] delegate] notificationDic]) {
        NSDictionary * notif = [(AppDelegate*)[[UIApplication sharedApplication] delegate] notificationDic];
        
        ErrorMessage * errMessage = [[ErrorMessage alloc] init];
        errMessage.badge = [notif valueForKeyPath:@"aps.badge"];
        errMessage.name = [notif valueForKeyPath:@"aps.alert.title"];
        errMessage.errorCode = [notif valueForKeyPath:@"aps.alert.body"];
        errMessage.date = [notif valueForKey:@"createdDate"];
        errMessage.errorNumber = [notif valueForKey:@"errorNumber"];
        errMessage.mac = [notif valueForKey:@"mac"];
        if ([notif valueForKey:@"ppm"] &&
            ![[notif valueForKey:@"ppm"] isKindOfClass:[NSNull class]] &&
            [[notif valueForKey:@"ppm"] length] > 0) {
            errMessage.ppm = [notif valueForKey:@"ppm"];
        }
        if (errMessage.mac.length > 0) {
            errMessage.color  = RedColor;
        } else {
            errMessage.color  = GreenAlertColor;
        }
        UIApplication.sharedApplication.applicationIconBadgeNumber = [errMessage.badge integerValue] - 1;
        NSString * token =  [(AppDelegate*)[[UIApplication sharedApplication] delegate] firebaseToken];
        badge = @(UIApplication.sharedApplication.applicationIconBadgeNumber);
        [self performSelectorInBackground:@selector(restartBadge:) withObject:token];
        [self openError:errMessage];
    }
}
-(void)restartBadge:(NSString *)token {
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostResetBadge andParameters:@{@"token" : token, @"currentBadgeNumber" : badge} success:^(id response) {
       // NSLog(@"APostResetBadge response = %@\n",response);
       } failure:^(NSError *error) {
         //  NSLog(@"APostResetBadge Error = %@\n", error.localizedDescription);
       }];
}

#pragma mark: SET DEVICES

-(void)setupView {
    feedbackMacsArr = [NSMutableArray array];
    deviceMacs = [NSMutableArray array];
    [self setNavigationStyle];
    [self addSubviewToNavigationBar];
    //for navigat
    self.navigationController.navigationBar.prefersLargeTitles = YES;
}

-(void)setNavigationStyle {
    if ([self isDarkMode]) {
        [self setNavigationStyle:GrayBarColor tintColor:BaseColor titleText:@"" titleColor:[UIColor clearColor]];
    } else {
        [self setNavigationStyle:[UIColor whiteColor] tintColor:BaseColor titleText:@"" titleColor:[UIColor clearColor]];
    }
    //check if Admin don't login
    if (![ACommonTools getBoolValueForKey:KeyIsUnLockMenu]) {
       // NSLog(@"KeyIsUnLockMenu = NO")
        [self.navigationItem.leftBarButtonItem setTintColor:[UIColor grayColor]];
    } else {
       // NSLog(@"KeyIsUnLockMenu = yes")
        if (![ACommonTools getObjectValueForKey:@"loginFirstTime"] || [[ACommonTools getObjectValueForKey:@"loginFirstTime"] isEqual:@"yes"]) {
            [self.navigationItem.leftBarButtonItem setTintColor:BaseColor];
        }else {
            [self.navigationItem.leftBarButtonItem setTintColor:[UIColor grayColor]];
        }
    }
}
-(void)setRightBarButton {
    UIImage* onOff = [UIImage imageNamed:@"ic_log_out"];
    onOffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    onOffBtn.frame = CGRectMake(50, 50, 70, 150);
    [onOffBtn setTitle:NSLocalizedString(@"all_off", nil)  forState:UIControlStateNormal];
    [onOffBtn setImage:onOff forState:UIControlStateNormal];
    [onOffBtn setTitleColor:BaseColor forState:UIControlStateNormal];
    [[onOffBtn titleLabel] setFont:[UIFont fontWithName:@"Calibri" size:16.f]];
    [onOffBtn.titleLabel setTextAlignment:NSTextAlignmentRight];
    [onOffBtn setContentMode:UIViewContentModeRight];
    [onOffBtn setImageEdgeInsets:UIEdgeInsetsMake(10, 0, -10, 0)];
    [onOffBtn setTitleEdgeInsets:UIEdgeInsetsMake(9, 0, -9, 0)];
    onOffBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    onOffBtn.titleLabel.numberOfLines = 2;
    
    [onOffBtn addTarget:self action:@selector(allOnOff) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:onOffBtn];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = 0;
    self.navigationItem.rightBarButtonItems = @[negativeSpacer, customBarItem];
    [self performSelectorInBackground:@selector(getAllOffOn) withObject:nil];
}

-(void)setHiddenDevices {
    if ([[ACommonTools getObjectValueForKey:@"HiddenDevices"] count] > 0) {
        hiddenDevicesArr = [NSMutableArray array];
        hiddenDevicesArr = [ACommonTools getObjectValueForKey:@"HiddenDevices"];
    }
}

-(void)setAllDevises:(NSArray *)result {
    allDevises = [NSMutableArray array];
    
    for (NSDictionary * device in result) {
        requestDic = device;
        //if exist hidden devices it doesnt add in the devices array list
        if (hiddenDevicesArr.count > 0) {
            if (![self isHiddenDevice:[device valueForKeyPath:@"deviceStatus.mac"]]) {
                [allDevises addObject:[[[AllDevises alloc]init] getDeviceObj:[device valueForKeyPath:@"deviceStatus"]]];
                [[allDevises lastObject] setName:[device valueForKeyPath:@"deviceName"]];
                [self getSetupPoints:(int)allDevises.count -1];
                isCallEfficiencyRate = YES;
            }
        } else {
            [allDevises addObject:[[[AllDevises alloc]init] getDeviceObj:[device valueForKeyPath:@"deviceStatus"]]];
            [[allDevises lastObject] setName:[device valueForKeyPath:@"deviceName"]];
            [self getSetupPoints:(int)allDevises.count -1];
            isCallEfficiencyRate = YES;
        }
    }
    [self.mDevicesTbV reloadData];
    [_mActivityV stopAnimating];
    [self performSelectorInBackground:@selector(checkDeviceOffline) withObject:nil];
    
    if (allDevises.count > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController.navigationBar setBarTintColor:[UIColor colorNamed:@"system_bg_color"] ];
            [self setRightBarButton];
            [self.mDevicesTbV setHidden:NO];
            [self.mGroupBtn setHidden:NO];
            [self.mDevicesBtn setHidden:NO];
            [self setDeviceMacs];
            [self performSelectorInBackground:@selector(setError) withObject:nil];
            [[NSRunLoop mainRunLoop] addTimer:self.offlineTimer forMode:NSRunLoopCommonModes];
            [self performSelectorInBackground:@selector(getEfficiencyRateData) withObject:nil];
        });
    } else {
        [self.mStartBtn setHidden:NO];
        [self.addDevNotesLb setHidden:NO];
        [self.mDevicesTbV setHidden:YES];
        [self.mGroupBtn setHidden:YES];
        [self.mDevicesBtn setHidden:YES];
        [navView setHidden:YES];
    }
    isAllOff = [self isAllOff];
    [self setAllOnOffInfo];
    mainDevicesArr = [NSMutableArray array];
    mainDevicesArr = allDevises;
    
}


-(void)setDeviceMacs {
    for (AllDevises * device in allDevises) {
        [deviceMacs addObject:device.mac];
    }
}
-(void)getAllOffOn {
    NSLog(@"getAllOffOn\n");

    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * urlFormar = [NSString stringWithFormat:@"%@?isOn=%@", AGetAllOnOff,@"false"];

        [[RequestManager sheredMenage] getDataWithUrl:urlFormar  success:^(id _Nonnull response) {
           // NSLog(@"AGetAllOnOff response = %@\n", response);
            if ([response isEqual:@"true"]) {
                self->isAllOff = YES;
            } else {
                self->isAllOff = NO;
            }
            [self setAllOnOffInfo];
        } failure:^(NSError * _Nonnull error) {
            //NSLog(@"AGetAllOnOff response = %@\n", error.localizedDescription);
        }];
    //});
}

-(void)setAllOnOffInfo {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self->isAllOff ) {
            [self->onOffBtn setTitle:NSLocalizedString(@"all_off", nil) forState:UIControlStateNormal];
            [self->onOffBtn setImage:[UIImage imageNamed:@"ic_log_out"] forState:UIControlStateNormal];
        } else {
            [self->onOffBtn setTitle:NSLocalizedString(@"all_on", nil) forState:UIControlStateNormal];
            [self->onOffBtn setImage:[UIImage imageNamed:@"ic_log_off"] forState:UIControlStateNormal];
        }
    });
}

-(void)setClock {
    NSLog(@"setClock\n");

   // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] getJsonDataWithUrl:AGetAllTimer andParameters:[self concatMacs] isNeedCookie:NO success:^(id _Nonnull response) {
           // NSLog(@"Device Timer response = %@\n", response);
            for (int i = 0; i < self->allDevises.count; i++) {
                [response enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([[obj valueForKey:@"mac"] isEqual:[[self->allDevises objectAtIndex:i] mac]]) {
                        [[[self->allDevises objectAtIndex:i] deviceVisual] setIsTimer:[[obj valueForKey:@"exist"] boolValue]];
                    }
                }];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.mDevicesTbV reloadData];
            });
        } failure:^(NSError * _Nonnull error) {
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
   // });
}

-(void)setError  {
    NSLog(@"setError\n");
    
    // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    for (int i = 0; i < self->allDevises.count; i++) {
        dispatch_queue_t q = dispatch_queue_create("com.my1.queue", NULL);
        dispatch_sync(q, ^{
            AllDevises * dev = [self->allDevises objectAtIndex:i];
            [[RequestManager sheredMenage] getJsonDataWithUrl:AGetLastAlert andParameters:@{@"mac":dev.mac} isNeedCookie:NO success:^(id  _Nonnull response) {
                // NSLog(@"getErrorCode response = %@\n", response);
                if (response) {
                    [[self->allDevises objectAtIndex:i] setErrorMessage:[[ErrorMessage alloc] getErrorMessageObj:response]];
                    [[[self->allDevises objectAtIndex:i] deviceVisual] setIsError: YES];
                }
                if (i == self->allDevises.count -1) {
                    [self setClock];
                }
            } failure:^(NSError * _Nonnull error) {
                // NSLog(@"getErrorCode Error = %@\n", error.localizedDescription);
                if (i == self->allDevises.count -1) {
                    [self setClock];
                }
            }];
        });
    }
    //});
}

-(void)setAllOffOn:(BOOL)isOff {
    for (int i = 0; i < allDevises.count; i++) {
        if (isOff) {
            [[allDevises objectAtIndex:i] setOnoff:86];
        } else {
            [[allDevises objectAtIndex:i] setOnoff:85];
        }
    }
    [self.mDevicesTbV reloadData];
}

#pragma mark: SET GROUPS
-(void)setGroups:(NSArray *)result {
    allGroups = [NSMutableArray array];
    for (NSDictionary * group in result) {
        if (![self isHiddenGroupsDevice:[group valueForKey:@"macList"]]) {
            [allGroups addObject:[[[Group alloc] init] getGroupObj:[group valueForKey:@"group"]]];
        }
    }
    if (allGroups.count > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mGroupTbv reloadData];
        });
    } else {
        [_mActivityV stopAnimating];
    }
}

#pragma mark : Get
-(void)getGroup {
    NSLog(@"getGroup\n");

   //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[RequestManager sheredMenage] getJsonDataWithUrl:AGetgroup
                                            andParameters:@{}
                                             isNeedCookie:NO
                                                  success:^(id  _Nonnull response) {
            //  NSLog(@"getGroup respons@ = %@\n", response);
            if (response) {
                [self setGroups:[response valueForKey:@"attachment"]];
            }
        } failure:^(NSError * _Nonnull error) {
            [self.mActivityV stopAnimating];
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
   // });
}
-(void)getDeviceStatuses {
    NSLog(@"getDeviceStatuses\n");

    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] getJsonDataWithUrl:AGetdevdeviceStatuses
                                            andParameters:@{}
                                             isNeedCookie:NO
                                                  success:^(id  _Nonnull response) {
           // NSLog(@"getDeviceStatuses respons@ = %@\n", response);
            if (response) {
                [self setAllDevises:response];
            }
        } failure:^(NSError * _Nonnull error) {
            [self.mActivityV stopAnimating];
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
    //});
}

-(void)getSetupPoints:(int) index{
    NSLog(@"getSetupPoints\n");
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * urlFormar = [NSString stringWithFormat:@"%@?mac=%@", AGetSetupPoint,[[self->allDevises objectAtIndex:index] mac]];
        [[RequestManager sheredMenage] getDataWithUrl:urlFormar success:^(id  _Nonnull response) {
           // NSLog(@"AGetSetupPoint response  = %@\n", response);
            if (([[response valueForKey:@"minPoint"] integerValue] != 16 || [[response valueForKey:@"maxPoint"] integerValue] != 32) && [[response valueForKey:@"keepSetPointInRange"] boolValue] ) {
                [[self->allDevises objectAtIndex:index] setTemperatureList:[self getTemperatureList:[response valueForKey:@"minPoint"] maxPoint:[response valueForKey:@"maxPoint"]]];
            }
        } failure:^(NSError * _Nonnull error) {
            //NSLog(@"AGetSetupPoint response error  = %@\n", error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
    //});
}
-(NSMutableArray *)getTemperatureList:(NSNumber *)minPoint maxPoint:(NSNumber *)maxPoint {
    NSMutableArray * tempArr  = [NSMutableArray array];
    tempArr = [TemperatureList mutableCopy];
    NSString * minStr = [NSString stringWithFormat: @"%@°", minPoint];
    NSString * maxStr = [NSString stringWithFormat: @"%@°", maxPoint];
    NSInteger minIndex = [tempArr indexOfObject:minStr];
    if (minIndex > 0 && minIndex <  tempArr.count -1) {
        [tempArr removeObjectsInRange:NSMakeRange(0, minIndex)];
    }
    NSInteger maxIndex = [tempArr indexOfObject:maxStr];
    if (maxIndex < tempArr.count -1 && maxIndex > 0 ) {
        [tempArr removeObjectsInRange:NSMakeRange( maxIndex+1, tempArr.count - maxIndex-1)];
    }
    return tempArr;
}

-(void)getEfficiencyRateData {
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
      __block int j = 0;

        for (int i = 0; i < self->allDevises.count; i++) {
            NSLog(@"in the FOR i = %i\n", i);
             NSLog(@"getEfficiencyRateData\n");
            dispatch_queue_t q = dispatch_queue_create("com.my2.queue", NULL);
            dispatch_sync(q, ^{
            NSString * urlFormat = [NSString stringWithFormat:@"%@?deviceMac=%@&timeRange=%@&powerMeterMode=%@", AGetNavigationDeviceMode, [[self->allDevises objectAtIndex:i] mac], self->navBar.dateModeForRequest, self->navBar.pmModeTypeForRequest];
            [[RequestManager sheredMenage] getDataWithUrl:urlFormat success:^(id  _Nonnull response) {
                NSLog(@"AGetNavigationDeviceMode response  = %@\n", response);
               // NSLog(@"\nself->navBar.pmModeType = %@\n", self->navBar.pmModeType);
                
                NSLog(@"in the REQUEST i = %i\n", i);

                [[self->allDevises objectAtIndex:i] setHasDevicePowerMeter:[[response valueForKey:@"hasDevicePowerMeter"] boolValue]];
                NSString * pmMode = @"";
                if ([[[response valueForKey:@"pmMode"] stringValue] length] > 3) {
                    pmMode = [NSString stringWithFormat:@"%.1f", [[response valueForKey:@"pmMode"] floatValue]];
                } else {
                    pmMode = [[response valueForKey:@"pmMode"] stringValue];
                }
                if ([self->navBar.pmModeType isEqual:NSLocalizedString(@"hour", nil)]) {
                    [[self->allDevises objectAtIndex:i] setEfficiencyRate:[NSString stringWithFormat:@"%@%@",pmMode, NSLocalizedString(@"h", nil)]];
                } else if ([self->navBar.pmModeType isEqual:NSLocalizedString(@"$", nil)]) {
                    [[self->allDevises objectAtIndex:i] setEfficiencyRate:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"$", nil), pmMode]];
                } else {
                    [[self->allDevises objectAtIndex:i] setEfficiencyRate:[NSString stringWithFormat:@"%@%@",pmMode, NSLocalizedString(@"kw_h", nil)]];
                }
                j++;
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (j == self->allDevises.count ) {
                        [self.mDevicesTbV reloadData];
                    }
                });
            } failure:^(NSError * _Nonnull error) {
               // NSLog(@"AGetNavigationDeviceMode response error  = %@\n", error.localizedDescription);
            }];
            });
        }
    //});
}


-(void)addSubviewToNavigationBar {
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if ([view isKindOfClass:[DevicesNavigationBar class]]) {
            [view removeFromSuperview];
            break;
        }
    }
    CGFloat x = self.view.frame.size.width/2 - 130;
    navView = [[DevicesNavigationBar alloc] initWithFrame:CGRectMake(x,  -10, 210, 90)];

    CGFloat width = 210;
    if ([BaseViewController isDeviceLanguageRTL]) { //Arabic, Hebrew
        if (SCREEN_Width < 370) {
            x += 30;
            width = 190;
        } else {
            x += 45;
        }
    } else {
         if (SCREEN_Width < 370) {
             x += 15;
             width = 190;
         }
    }
    if (SCREEN_Width < 370) {
        navView.frame = CGRectMake(x, -10,190, 90);
    } else {
        navView.frame = CGRectMake(x, -10, 210, 90);
    }
    [self.navigationController.navigationBar addSubview:navView];
    [self setNavigationStyle];
    navView.delegate = self;
}


-(void)checkDeviceOffline{
    dispatch_queue_t q = dispatch_queue_create("com.my.queue", NULL);
    dispatch_sync(q, ^{
        // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            for (int i = 0; i < self->allDevises.count; i++) {
                NSLog(@"checkDeviceOffline\n");
                AllDevises * dev = [self->allDevises objectAtIndex:i];
               // NSLog(@"device name = %@\n", dev.name);
                NSString * url = [NSString stringWithFormat:@"%@%@", AGetIsOnline, dev.mac];
                [[RequestManager sheredMenage] getJsonDataWithUrl:url andParameters:@{}
                                                     isNeedCookie:NO success:^(id  _Nonnull response) {
                    //  NSLog(@"checkDeviceOffline respons@ = %@\n", response);
                } failure:^(NSError * _Nonnull error) {
                    //NSString *string = @"Status Code: 404,";
                    NSString * notFound = @"not found (404)";
                    //  NSLog(@"Device name  = %@\n device ofline = YES\n", dev.name);
                    if ( [error.localizedDescription containsString:notFound]) {
                        [[[self->allDevises objectAtIndex:i]deviceVisual]  setIsWarning:YES];
                       // NSLog(@"device name = %@\n is offline", [[self->allDevises objectAtIndex:i] name]);

                    }
                    if (i == self->allDevises.count-1) {
                        [self.mDevicesTbV reloadData];
                    }
                }];
            }
        //});
    });
   
}

- (NSTimer *) offlineTimer {
    if (!_offlineTimer) {
        _offlineTimer = [NSTimer timerWithTimeInterval:20.f target:self selector:@selector(checkDeviceOffline) userInfo:nil repeats:YES];
    }
    return _offlineTimer;
}

-(NSDictionary *)concatMacs {
    __block NSString * macs = @"";
    [allDevises enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        AllDevises * dev = (AllDevises *)obj;
        macs = [macs stringByAppendingString:[NSString stringWithFormat:@"%@|",dev.mac]];
    }];
    return [NSDictionary dictionaryWithObjectsAndKeys:macs,@"macs", nil];
}
-(void) allOnOff {
    isAllOff = !isAllOff;
    NSString * boolVal = isAllOff ? @"false" : @"true";
    [self setAllOffOn:isAllOff];
    [self setAllOnOffInfo];
    [[RequestManager sheredMenage] getDataWithUrl:[NSString stringWithFormat:@"%@?on=%@",AGetdevswitchHvacs, boolVal] success:^(id  _Nonnull response) {
    } failure:^(NSError * _Nonnull error) {
        //show error
    }];
}
-(BOOL)isAllOff {
    for (AllDevises * device in allDevises) {
        if (device.onoff == 85 || device.onoff == 55) {
            return NO;
        }
    }
    return YES;
}

#pragma mark: DEVICE UPDATE
-(void)updateDeviceObj:(AllDevises *)device  {
    NSDictionary * newDic = [AllDevises dictionaryWithObject:device];
    AllDevises * newDevice = [[[AllDevises alloc] init] getDeviceObj:newDic];
    newDevice.temperatureList = device.temperatureList;
    newDevice.deviceVisual = device.deviceVisual;
    newDevice.name = device.name;
    [allDevises replaceObjectAtIndex:clickedIndexPath.row withObject:newDevice];
    dispatch_async(dispatch_get_main_queue(), ^{
        //    [self.mDevicesTbV reloadData];
        //        [self.mDevicesTbV beginUpdates];
        //        [self.mDevicesTbV reloadRowsAtIndexPaths:[NSArray arrayWithObjects:self->clickedIndexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
        //        [self.mDevicesTbV endUpdates];
        [self->_mDevicesTbV reloadRowsAtIndexPaths:[NSArray arrayWithObject:self->clickedIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    });
    
}
-(void)updateGeneralInfo:(AllDevises *)currDevice  {
    NSLog(@"updateGeneralInfo\n");

    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableDictionary *dict = [[AllDevises  dictionaryForDeviceUpdate:currDevice] mutableCopy];
        // NSLog(@"Dic = %@\n", dict);
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostdevsetHvac andParameters:dict success:^(id _Nonnull response) {
            // NSLog(@"response = %@\n", response);
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"error = %@\n", error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
            
            //error
        }];
    //});
}
-(void)updateOnOff:(AllDevises *)currDevice on:(NSString * )on  {
    NSLog(@"updateOnOff\n");

    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if ([on isEqualToString:@"true"]) {
            self->currDevOnMac = currDevice.mac;
        }
        [[RequestManager sheredMenage] getDataWithUrl:[NSString stringWithFormat:@"%@/%@?on=%@", AGetdevswitchHvac, currDevice.mac, on] success:^(id  _Nonnull response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.mDevicesTbV reloadData];
                self->isAllOff = [self isAllOff];
                [self setAllOnOffInfo];
            });
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"error = %@\n", error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
    //});
}

#pragma mark: GROUP UPDATE
-(void)updateGroupObj:(Group *)group  {
    NSDictionary * newDic = [Group dictionaryWithObject:group];
    Group * newGroup = [[[Group alloc] init] getGroupObj:newDic];
    [allGroups replaceObjectAtIndex:clickedIndexPath.row withObject:newGroup];
}
-(void)updateGroupOnOff:(Group *)currGroup on:(NSString * )on  {
    NSLog(@"updateGroupOnOff\n");

    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary * param = @{@"on": on, @"groupId": @(currGroup.GrId)};
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostgroupswitchGroup andParameters:param success:^(id _Nonnull response) {
           // NSLog(@"response = %@\n", response);
            [self updateGroupOnOffBtn:on img:Image(@"little_check")];
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"error = %@\n", error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil)  ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
    //});
}

-(void)updateGroupTemp:(Group *)currGroup temp:(NSInteger)temp  {
    NSLog(@"updateGroupTemp\n");

  //  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary * param = @{@"tgtTemp": @(temp), @"groupId": @(currGroup.GrId)};
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostgroupsetGroupTgtTemp andParameters:param success:^(id _Nonnull response) {
           // NSLog(@"response = %@\n", response);
            [self updateGroupOnOffBtn:@"true" img:Image(@"little_check")];
            
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"error = %@\n", error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        }];
   // });
}

-(void)updateGroupFan:(Group *)currGroup fan:(NSInteger)fan  {
    NSLog(@"updateGroupFan\n");
   // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary * param = @{@"fan": @(fan), @"groupId": @(currGroup.GrId)};
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostgroupsetGroupFan andParameters:param success:^(id _Nonnull response) {
            //NSLog(@"response = %@\n", response);
            [self updateGroupOnOffBtn:@"true" img:Image(@"little_check")];
            
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"error = %@\n", error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
            
        }];
   // });
}
-(void)updateGroupMode:(Group *)currGroup mode:(NSInteger)mode  {
    NSLog(@"updateGroupMode\n");

   // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary * param = @{@"mode": @(mode), @"groupId": @(currGroup.GrId)};
        [[RequestManager sheredMenage] postJsonDataWithUrl:APostgroupsetGroupMode andParameters:param success:^(id _Nonnull response) {
            //NSLog(@"response = %@\n", response);
            [self updateGroupOnOffBtn:@"true" img:Image(@"little_check")];
        } failure:^(NSError * _Nonnull error) {
            //NSLog(@"error = %@\n", error.localizedDescription);
            [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
            
        }];
   // });
}

-(void)updateGroupOnOffBtn:(NSString *)on img:(UIImage *)img {
    GroupSettingsTableViewCell * groupCell = (GroupSettingsTableViewCell * )[_mGroupTbv cellForRowAtIndexPath:clickedIndexPath];
    UIButton * onOffBtn = [[UIButton alloc] init];
    NSString * title;
    if ([on isEqualToString:@"true"]) {
        onOffBtn = groupCell.mOnBtn;
        title = NSLocalizedString(@"on", nil);
    } else {
        onOffBtn = groupCell.mOffBtn;
        title = NSLocalizedString(@"off", nil);
        
    }
    [onOffBtn setImage:img forState:UIControlStateNormal];
    [onOffBtn  setTitle:@"" forState:UIControlStateNormal];
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [onOffBtn setImage:nil forState:UIControlStateNormal];
        [onOffBtn setTitle:title forState:UIControlStateNormal];
    });
}

#pragma mark: ACTIONS
- (IBAction)start:(UIButton *)sender {
    [self pushViewControllerWithIdentifier:@"AddDevice" controller:self];
}
- (IBAction)devices:(UIButton *)sender {
    deviceType = devices;
    [self getDeviceStatuses];
    [sender setTitleColor:BaseColor forState:UIControlStateNormal];
    [_mGroupBtn setTitleColor:GrayDevColor forState:UIControlStateNormal];
    [self hideDevices:NO];
}
- (IBAction)group:(UIButton *)sender {
    [_webSocket close];
    _webSocket = nil;
    [self getGroup];
    deviceType = group;
    [sender setTitleColor:BaseColor forState:UIControlStateNormal];
    [_mDevicesBtn setTitleColor:GrayDevColor forState:UIControlStateNormal];
    [self hideDevices:YES];
}

-(void)hideDevices:(BOOL)hide {
    [self.mActivityV startAnimating];
    [self.mGroupTbv setHidden:!hide];
    [self.mDevicesTbV setHidden:hide];
}



#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.mGroupTbv) {
        return allGroups.count;//message count
    }
    return allDevises.count;//message count
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.mDevicesTbV) {
        AllDevises * device = [allDevises objectAtIndex:indexPath.row];
        LivingRoomSettingsTableViewCell * livingRoomSettCell = [self.mDevicesTbV dequeueReusableCellWithIdentifier:@"livingRoomSettingsCell"];
        livingRoomSettCell.delegate = self;
        [livingRoomSettCell.mGradientV setHidden:YES];
        [livingRoomSettCell.mMainLb setText:device.name];
        if (device.hasDevicePowerMeter) {
            [livingRoomSettCell.mPriceLb setText:device.efficiencyRate];
        } else {
            if ([navBar.pmModeType isEqualToString:NSLocalizedString(@"hour", nil)]) {
                [livingRoomSettCell.mPriceLb setText:device.efficiencyRate];
            } else {
                [livingRoomSettCell.mPriceLb setText:@"--"];
            }
        }
        [self setOnOff:livingRoomSettCell currDevice:device index:(int)indexPath.row];
        [self showError:livingRoomSettCell currDevice:device];
        [self showClock:livingRoomSettCell currDevice:device];
        
        return livingRoomSettCell;
    } else {
        Group * group = [allGroups objectAtIndex:indexPath.row];
        GroupSettingsTableViewCell * groupSettCell = [self.mGroupTbv dequeueReusableCellWithIdentifier:@"groupSettingsCell"];
        groupSettCell.delegate = self;
        [groupSettCell.mGroupLb setText:group.name];
        [self setGroupOnOff:groupSettCell currGroup:group index:(int)indexPath.row];
        [self setGroupSettings:groupSettCell currGroup:group index:(int)indexPath.row];
        if (indexPath.row == allGroups.count -1) {
            [_mActivityV stopAnimating];
        }
        return groupSettCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}

-(void)setOnOff:(LivingRoomSettingsTableViewCell *)livingCell
     currDevice:(AllDevises *)currDevice index:(int)index {
    
    
    livingCell.mTurnOnOffBtn.tag = currDevice.onoff;
    livingCell.mOffBtn.tag = currDevice.onoff;
    if ([[[allDevises objectAtIndex:index] deviceVisual] isWarning]) {
        allOffCounter++;
        warningRow = index;
        [self showWarning:livingCell];
    } else {
        if (currDevice.onoff == 85 || currDevice.onoff == 55) { //is on
            
            if ([currDevice.deviceType isEqual:@"SW"]) {
                [self setSWLivingCell:livingCell currDevice:currDevice index:index];
            }  else {
                [self setACLivingCell:livingCell currDevice:currDevice index:index];
            }
            
        }  else if ( currDevice.onoff == 86 || currDevice.onoff == 170){ //is off
            allOffCounter++;
            [livingCell.mTemperatureLb setHidden:NO];
            [livingCell.mTemperatureLb setText:[NSString stringWithFormat:@"%li°",(long)currDevice.envTemp] ];
            [livingCell.mGradientV setHidden:YES];
            [livingCell.mBackgroundV setBackgroundColor:GrayDevColor];
            [livingCell.mofflineBackgV setHidden:NO];
            
            [livingCell.mWarningImg setHidden:YES];
              [livingCell.mOffBtn setHidden:NO];
          if ([currDevice.deviceType isEqual:@"SW"]) {
                [livingCell.mTemperatureV setHidden:YES];
                [livingCell.mOffBtn setBackgroundImage:[UIImage imageNamed:@"ic_grey_lamp"] forState:UIControlStateNormal];
        } else {
                 [livingCell.mOffBtn setBackgroundImage:[UIImage imageNamed:@"off"] forState:UIControlStateNormal];
            }
        } else { //warning
            [[[allDevises objectAtIndex:index] deviceVisual] setIsWarning:YES];
            warningRow = index;
            [self showWarning:livingCell];
        }
    }
   // isAllOff = [self isAllOff];
}


-(void)setSWLivingCell:(LivingRoomSettingsTableViewCell *)livingCell
            currDevice:(AllDevises *)currDevice index:(int)index {
    
    
    [livingCell.mTemperatureV setHidden:YES];
    [livingCell.mofflineBackgV setHidden:NO];
    [livingCell.mBackgroundV setBackgroundColor:YellowLightDevColor];
    [livingCell.mOffBtn setBackgroundImage:[UIImage imageNamed:@"yellow_lamp"] forState:UIControlStateNormal];
    
}

-(void)setACLivingCell:(LivingRoomSettingsTableViewCell *)livingCell
            currDevice:(AllDevises *)currDevice index:(int)index {
    
    [livingCell.mTemperatureV setHidden:NO];
    [livingCell.mofflineBackgV setHidden:YES];
    NSString* currTemp = [NSString stringWithFormat:@"%li°", currDevice.tgtTemp];
    NSString* currFan;
    if (currDevice.fan == 0) {
        currFan = @"A";
    } else
        currFan = [NSString stringWithFormat:@"%li", (long)currDevice.fan];
    switch (currDevice.mode) {
        case 33:
        case 51:// auto mode icon
            [self setGradient:livingCell];
            break;
    }
    [livingCell.mTemperatureLb setText:[NSString stringWithFormat:@"%li°",(long)currDevice.envTemp] ];
    [livingCell.mTurnOnOffBtn setBackgroundImage:currDevice.deviceVisual.turnOnOffImg forState:UIControlStateNormal];
    [livingCell.mDevicesModeBtn setImage:currDevice.deviceVisual.modeImage  forState:UIControlStateNormal];
    [livingCell.mTemperatureBtn setTitle:currTemp forState:UIControlStateNormal];
    [livingCell.mtemperaturePickV setTintColor:currDevice.deviceVisual.modeColor];
    [livingCell.fanBtn setTitle:currFan forState:UIControlStateNormal];
    if ([FanModesList indexOfObject:currFan] >= 0 && [FanModesList indexOfObject:currFan] <= FanModesList.count-1 ) {
        [livingCell.mFanPickerV selectRow:[FanModesList indexOfObject:currFan] inComponent:0 animated:YES];
    }
    [livingCell.fanBtn setImage:currDevice.deviceVisual.fanImage forState:UIControlStateNormal];
    livingCell.temperatureList = currDevice.temperatureList;
    if (![currDevice.deviceVisual.modeColor isEqual:GrayDevColor]) {
        [livingCell.mTemperatureBtn setTitleColor:currDevice.deviceVisual.modeColor forState:UIControlStateNormal];
        [livingCell.fanBtn setTitleColor:currDevice.deviceVisual.modeColor forState:UIControlStateNormal];
    }
    
    NSInteger modeIndex = [BaseViewController indexOfMode: currDevice.deviceVisual.modeImage];
    [livingCell.mDevicesModePickerV selectRow:modeIndex inComponent:0 animated:YES];
    if ([currDevice.temperatureList indexOfObject:currTemp] >= 0 && [currDevice.temperatureList indexOfObject:currTemp] <= currDevice.temperatureList.count-1 ) {
        [livingCell.mtemperaturePickV selectRow:[currDevice.temperatureList indexOfObject:currTemp] inComponent:0 animated:YES];
    }
    
    if (![currDevice.deviceVisual.modeColor isEqual:AutoColor]) {
        [livingCell.mBackgroundV setBackgroundColor:currDevice.deviceVisual.modeColor];
    }
    [allDevises replaceObjectAtIndex:index withObject:currDevice] ;
}

-(void)setGradient:(LivingRoomSettingsTableViewCell *)livingCell {
    [livingCell.mGradientV setHidden:NO];
    UIColor *leftColor = [UIColor colorWithRed:40.0/255.0 green:88.0/255.0 blue:149.0/255.0 alpha:1.0];
    UIColor *rightColor = [UIColor colorWithRed:177.0/255.0 green:2.0/255.0 blue:17.0/255.0 alpha:1.0];
    // Create the gradient
    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
    theViewGradient.colors = [NSArray arrayWithObjects: (id)leftColor.CGColor, (id)rightColor.CGColor, nil];
    theViewGradient.frame = CGRectMake(0, 0, self.view.frame.size.width, 30);
    theViewGradient.startPoint = CGPointMake(0.0, 0.5);
    theViewGradient.endPoint = CGPointMake(1.0, 0.5);
    //Add gradient to view
    [livingCell.mGradientV.layer insertSublayer:theViewGradient atIndex:0];
}

-(void)showWarning:(LivingRoomSettingsTableViewCell *)livingCell  {
    [livingCell.mofflineBackgV setHidden:NO];
    [livingCell.mWarningImg setHidden:NO];
    [livingCell.mOffBtn setHidden:YES];
    [livingCell.mGradientV setHidden:YES];
    [livingCell.mBackgroundV setBackgroundColor:GrayDevColor];
    [livingCell.mTemperatureLb setText:@"--"];
    [livingCell.mPriceLb setText:@"--"];
    
    
}
-(void)showError:(LivingRoomSettingsTableViewCell *)livingCell currDevice:(AllDevises *)currDevice  {
    if (currDevice.deviceVisual.isError) {
        [livingCell.mErrorBtn setHidden:NO];
    } else {
        [livingCell.mErrorBtn setHidden:YES];
    }
}
-(void)showClock:(LivingRoomSettingsTableViewCell *)livingCell currDevice:(AllDevises *)currDevice {
    if (currDevice.deviceVisual.isTimer) {
        [livingCell.mTimerImg setHidden:NO];
    } else {
        [livingCell.mTimerImg setHidden:YES];
    }
}


#pragma mark: GROUP  SET CELL
-(void)setGroupOnOff:(GroupSettingsTableViewCell *)groupCell
           currGroup:(Group *)currGroup index:(int)index {
    if (currGroup.onoff == 85 || currGroup.onoff == 55) { //is on
        
        groupCell.isOn = YES;
        // [self setGroupSettings:groupCell currGroup:currGroup index:index];
    }  else if ( currGroup.onoff == 86 || currGroup.onoff == 170){ //is off
        groupCell.isOn = NO;
        [groupCell.mOnOffBackgroundV setHidden:NO];
    } else { //warning
        [[[allGroups objectAtIndex:index] groupVisual] setIsWarning:YES];
        // [self showWarning:groupCell];
    }
}

-(void)setGroupSettings:(GroupSettingsTableViewCell *)groupCell currGroup:(Group *)currGroup index:(int)index {
    
    NSString * currFan;
    if (currGroup.fan == 0) {
        currFan = @"A";
    } else
        currFan = [NSString stringWithFormat:@"%li", (long)currGroup.fan];
    [groupCell.mTemperatureBtn setTitle:[NSString stringWithFormat:@"%li°",(long)currGroup.tgtTemp]  forState:UIControlStateNormal ];
    [groupCell.mModeBtn setImage:currGroup.groupVisual.modeImage forState:UIControlStateNormal];
    [groupCell.mTemperatureBtn setTitleColor:currGroup.groupVisual.modeColor forState:UIControlStateNormal];
    if ([TemperatureList indexOfObject:[NSString stringWithFormat:@"%li°",(long)currGroup.tgtTemp]] >= 0 && [TemperatureList indexOfObject:[NSString stringWithFormat:@"%li°",(long)currGroup.tgtTemp]] <= TemperatureList.count-1 ) {
        [groupCell.mTemperaturePickV selectRow:[TemperatureList indexOfObject:[NSString stringWithFormat:@"%li°",(long)currGroup.tgtTemp]] inComponent:0 animated:YES];
    }
    [groupCell.mTemperaturePickV setTintColor:currGroup.groupVisual.modeColor];
    
    [groupCell.mFanBtn setTitleColor:currGroup.groupVisual.modeColor forState:UIControlStateNormal];
    [groupCell.mFanBtn setTitle:currFan forState:UIControlStateNormal];
    if ([FanModesList indexOfObject:currFan] >= 0 && [FanModesList indexOfObject:currFan] <= FanModesList.count-1 ) {
        [groupCell.mFanPickV selectRow:[FanModesList indexOfObject:currFan] inComponent:0 animated:YES];
    }
    [groupCell.mFanBtn setImage:currGroup.groupVisual.fanImage forState:UIControlStateNormal];
    [groupCell.mModePick selectRow:[BaseViewController indexOfMode: currGroup.groupVisual.modeImage]  inComponent:0 animated:YES];
    if (currGroup.onoff == 85 ||currGroup.onoff == 55) {
        // [groupCell.mOnBtn setBackgroundColor:currGroup.groupVisual.modeColor];
    } else  if (currGroup.onoff == 86 || currGroup.onoff == 170)  {
        //[groupCell.mOffBtn setBackgroundColor:currGroup.groupVisual.modeColor];
    } else{
        // [groupCell.mWarningImg setHidden:NO];
        currGroup.groupVisual.isWarning = YES;
        [self updateGroupOnOffBtn:@"true" img:Image(@"white_triangle_error")];
        //if after 5 second doesnt come feadback show main worning;
    }
    [allGroups replaceObjectAtIndex:index withObject:currGroup] ;
}

#pragma mark: LiuvingRoomTable Cell Delegate
-(void)selectedRow:(UIButton *)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.mDevicesTbV];
    clickedIndexPath = [self.mDevicesTbV indexPathForRowAtPoint:buttonPosition];
    [self didSelectRowAtIndexPath:clickedIndexPath isDevice:YES];
}

-(void)handlerTurnOnOff:(UIButton *)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.mDevicesTbV];
    clickedIndexPath = [self.mDevicesTbV indexPathForRowAtPoint:buttonPosition];
    [[allDevises objectAtIndex:clickedIndexPath.row] setOnoff:sender.tag];
    if (sender.tag == 86 || sender.tag == 170) {
        [self updateOnOff:[allDevises objectAtIndex:clickedIndexPath.row]on:@"false"];
    } else {
        [self updateOnOff:[allDevises objectAtIndex:clickedIndexPath.row]on:@"true"];
    }
}

-(void) handlError:(UIButton *)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.mDevicesTbV];
    clickedIndexPath = [self.mDevicesTbV indexPathForRowAtPoint:buttonPosition];
    [self openError:[[allDevises objectAtIndex:clickedIndexPath.row] errorMessage]];
}
-(void)openError:(ErrorMessage *)errMessage  {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    OpenMessageViewController * vc = [storyboard   instantiateViewControllerWithIdentifier:@"OpenMessageViewController"];
    if (errMessage) {
        vc.currErrMessage = errMessage;
        vc.isMainPage = YES;
    } else {
        vc.deviceMac = [[allDevises objectAtIndex:clickedIndexPath.row] mac];
        vc.currAllDevice = [allDevises objectAtIndex:clickedIndexPath.row];
        vc.isMainPage = YES;
    }
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)temperatureChanges:(UIPickerView *)picker value:(NSInteger)value {
    CGPoint buttonPosition = [picker convertPoint:CGPointZero toView:self.mDevicesTbV];
    clickedIndexPath = [self.mDevicesTbV indexPathForRowAtPoint:buttonPosition];
    if ([[allDevises objectAtIndex:clickedIndexPath.row] tgtTemp] != value) {
        [[allDevises objectAtIndex:clickedIndexPath.row] setTgtTemp:value];
        [self updateGeneralInfo:[allDevises objectAtIndex:clickedIndexPath.row]];
    }
}
-(void)fanChanges:(UIPickerView *)picker value:(NSInteger)value {
    CGPoint buttonPosition = [picker convertPoint:CGPointZero toView:self.mDevicesTbV];
    clickedIndexPath = [self.mDevicesTbV indexPathForRowAtPoint:buttonPosition];
    if ([[allDevises objectAtIndex:clickedIndexPath.row] fan] != value) {
        [[allDevises objectAtIndex:clickedIndexPath.row] setFan:value];
        [self updateGeneralInfo:[allDevises objectAtIndex:clickedIndexPath.row]];
    }
    
}
-(void)modeChanges:(UIPickerView *)picker value:(NSInteger)value
           modeImg:(nonnull UIImage *)modeImg modeColro:(nonnull UIColor *)modeColor{
    
    CGPoint buttonPosition = [picker convertPoint:CGPointZero toView:self.mDevicesTbV];
    clickedIndexPath = [self.mDevicesTbV indexPathForRowAtPoint:buttonPosition];
    AllDevises * device = [allDevises objectAtIndex:clickedIndexPath.row];
    if (device.mode != value) {
        [device setMode:value];
        [self updateGeneralInfo:device];
        
    }
}

-(void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath isDevice:(BOOL)isDevice {
       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LivingRoomViewController * vc = [storyboard   instantiateViewControllerWithIdentifier:@"LivingRoomViewController"];
        [ACommonTools setBoolValue:NO forKey:@"isPm"];
        vc.allDevises = allDevises;
        isInLivingRoom = YES;
        if (isDevice && !([[[allDevises objectAtIndex:indexPath.row] deviceType] isKindOfClass:[NSNull class]] || [[[allDevises objectAtIndex:indexPath.row] deviceType] length] <= 0)) {
            vc.isDevice = YES;
            vc.currDevice = [allDevises objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:vc animated:YES];

        } else if (!isDevice) {
            vc.isDevice = NO;
            vc.currGroup = [allGroups objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:vc animated:YES];

        }
 
}

#pragma mark: Group Delegate
-(void)groupSelectedRow:(UIButton *)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.mGroupTbv];
    clickedIndexPath = [self.mGroupTbv indexPathForRowAtPoint:buttonPosition];
    [self didSelectRowAtIndexPath:clickedIndexPath isDevice:NO];
    
}
-(void)groupHandlerTurnOn:(UIButton *)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.mGroupTbv];
    clickedIndexPath = [self.mGroupTbv indexPathForRowAtPoint:buttonPosition];
    [self updateGroupOnOff:[allGroups objectAtIndex:clickedIndexPath.row] on:@"true"];
    
}
-(void)groupTandlerTurnOff:(UIButton *)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.mGroupTbv];
    clickedIndexPath = [self.mGroupTbv indexPathForRowAtPoint:buttonPosition];
    [self updateGroupOnOff:[allGroups objectAtIndex:clickedIndexPath.row] on:@"false"];
    
}
-(void)groupTemperatureChanges:(UIPickerView *)picker value:(NSInteger)value {
    CGPoint buttonPosition = [picker convertPoint:CGPointZero toView:self.mGroupTbv];
    clickedIndexPath = [self.mGroupTbv indexPathForRowAtPoint:buttonPosition];
    if ([[allGroups objectAtIndex:clickedIndexPath.row] tgtTemp] != value) {
        [[allGroups objectAtIndex:clickedIndexPath.row] setTgtTemp:value];
        [self.mGroupTbv reloadData];
        [self updateGroupTemp:[allGroups objectAtIndex:clickedIndexPath.row] temp:value];
        
    }
}

-(void)groupFanChanges:(UIPickerView *)picker value:(NSInteger)value {
    CGPoint buttonPosition = [picker convertPoint:CGPointZero toView:self.mGroupTbv];
    clickedIndexPath = [self.mGroupTbv indexPathForRowAtPoint:buttonPosition];
    if ([[allGroups objectAtIndex:clickedIndexPath.row] fan] != value) {
        [[allGroups objectAtIndex:clickedIndexPath.row] setFan:value];
        [self.mGroupTbv reloadData];
        [self updateGroupFan:[allGroups objectAtIndex:clickedIndexPath.row] fan:value];
    }
    
}
-(void)groupModeChanges:(UIPickerView *)picker value:(NSInteger)value {
    CGPoint buttonPosition = [picker convertPoint:CGPointZero toView:self.mGroupTbv];
    clickedIndexPath = [self.mGroupTbv indexPathForRowAtPoint:buttonPosition];
    Group * group = [allGroups objectAtIndex:clickedIndexPath.row];
    if (group.mode != value) {
        [[allGroups objectAtIndex:clickedIndexPath.row] setMode:value];
        [self updateGroupObj:[allGroups objectAtIndex:clickedIndexPath.row]];
        [self.mGroupTbv reloadData];
        [self updateGroupMode:[allGroups objectAtIndex:clickedIndexPath.row] mode:value];
    }
}


-(void)updateDeviceStatus:(NSDictionary *)dict {
    AllDevises * newDev = [[AllDevises alloc] init];
    NSDictionary * currDic = [dict objectForKey:DeviceStatus];
    if ([dict objectForKey:@"priceHr"]) {
        if ([self->navBar.pmModeType isEqual:NSLocalizedString(@"$", nil)]) {
             [[self->allDevises objectAtIndex:clickedIndexPath.row] setEfficiencyRate:[NSString stringWithFormat:@"%@%.1f",NSLocalizedString(@"$", nil), [[dict objectForKey:@"priceHr"] floatValue]]];
        } else if ([self->navBar.pmModeType isEqual:NSLocalizedString(@"kWh", nil)]) {
             [[self->allDevises objectAtIndex:clickedIndexPath.row] setEfficiencyRate:[NSString stringWithFormat:@"%@%.1f",NSLocalizedString(@"$", nil), [[dict objectForKey:@"kwHour"] floatValue]]];
        }
        newDev = [allDevises objectAtIndex:clickedIndexPath.row];

    } else if ([[currDic valueForKey:@"onoff"] isEqual:@86] || [[currDic valueForKey:@"onoff"] isEqual:@170]) {
        
        [[allDevises objectAtIndex:clickedIndexPath.row] setOnoff:86];
        [[allDevises objectAtIndex:clickedIndexPath.row] setEnvTemp:[[currDic valueForKey:@"envTemp"] integerValue]];
        [[[allDevises objectAtIndex:clickedIndexPath.row] deviceVisual] setIsWarning:NO];
        newDev = [allDevises objectAtIndex:clickedIndexPath.row];
        
        //Error
    }else {
        AllDevises * device = [[[AllDevises alloc]init] getDeviceObj:currDic];
        device.deviceVisual.isError = currError;
        device.deviceVisual.isTimer = currTimer;
        device.temperatureList = currTemperatureList;
        device.name = currName;
        device.efficiencyRate = currEfficiencyRate;
        device.hasDevicePowerMeter = currHasDevicePM;
        if ([[currDic valueForKey:@"onoff"] isEqual:@85] || [[currDic valueForKey:@"onoff"] isEqual:@55]) {
            isAllOff = NO;
            [self setAllOnOffInfo];
        }
        
        if ( [currDic objectForKey:@"errorCode"]) {
            device.deviceVisual.isError = YES;
            device.errorMessage = [[ErrorMessage alloc] getErrorMessageObj:currDic];
           }
        if ([[[dict objectForKey:RequestBytes] lastObject]  isEqual: @-72]) {
            [device.deviceVisual setIsWarning:YES];
        }
        newDev = device;

    }
    
   
    if (newDev) {
        if(newDev.onoff == 170) {
            newDev.onoff = 86;
        }
        [allDevises replaceObjectAtIndex:clickedIndexPath.row withObject: newDev];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mDevicesTbV reloadData];
        });
    }
}
- (BOOL)findDeviceFromLocalData:(NSString *)mac {
    for (int i = 0; i < allDevises.count; i++) {
        AllDevises * dev  = [allDevises objectAtIndex:i];
        if ([mac caseInsensitiveCompare:dev.mac] == NSOrderedSame) {
            clickedIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
            currName = dev.name;
            currError = dev.deviceVisual.isError;
            currTimer = dev.deviceVisual.isTimer;
            currTemperatureList = [NSMutableArray array];
            currTemperatureList = dev.temperatureList;
            currEfficiencyRate = dev.efficiencyRate;
            currHasDevicePM = dev.hasDevicePowerMeter;
            return  YES;
        }
    }
    return NO;
}
#pragma mark: DeviceNavigationBarDelegate
-(void)choosePriceType:(NSString *)type {
}
-(void)handlerNavBarbuttons:(int)tag {
}
-(void) handlerPowerMeter {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController * vc = [storyboard   instantiateViewControllerWithIdentifier:@"PowerMeterViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)updateNavigationObj:(NavigationBar *)navBarObj {
    if (deviceType == devices) {
        navBar = [[NavigationBar alloc] init];
        navBar = navBarObj;
        if (isCallEfficiencyRate) {
            [self performSelectorInBackground:@selector(getEfficiencyRateData) withObject:nil];
        }
    }
}

#pragma mark SRWebSocket delegate
- (void)openWebSocket {
    if ([_webSocket readyState] == SR_OPEN) {
        [_webSocket close];
    }
    _webSocket = nil;
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    NSLog(@"cookies = %@\n", cookies);
    NSURL *wsUrl = [NSURL URLWithString:ABaseWSURLString];
    _webSocket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:wsUrl]];
    _webSocket.delegate = self;
    _webSocket.requestCookies = cookies;
    [_webSocket open];
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    NSLog(@"webSocket Did Open Success");
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    if (error) {
        NSLog(@"webSocket open error = %@",error.localizedDescription);
        [self openWebSocket];
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    if (deviceType == devices) {
       // NSLog(@"webSocketreceiveMessage = %@",message);
        
        NSData *jsonData = [message dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:nil];
        //check if the feedback device is in the list of hidden devices or not
        if (![self isHiddenDevice:[[dict objectForKey:DeviceStatus] objectForKey:@"mac"]]) {
            if (isInSelfControll) {
                //device
                if ([dict objectForKey:DeviceStatus]) {
                    NSDictionary * currDic = [dict objectForKey:DeviceStatus];
                    [feedbackMacsArr addObject:[currDic objectForKey:@"mac"]];
                    if ([self findDeviceFromLocalData:[currDic valueForKey:@"mac"]]) {
                        [self updateDeviceStatus:dict];
                    }
                    //Power meter
                } else if ([dict objectForKey:@"priceHr"] ) {
                    if ([navBar.pmModeType isEqualToString:NSLocalizedString(@"$", nil)]) {
                        if ([dict objectForKey:@"acMac"] && [[dict objectForKey:@"acMac"] length] > 0) {
                            if ([self findDeviceFromLocalData:[dict objectForKey:@"acMac"]]) {
                                [self updateDeviceStatus:dict];
                            }
                        } else {
                            navBar.pmMode = [dict objectForKey:@"priceHr"];
                            [navView.mPriceBtn setTitle:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"$", nil), navBar.pmMode] forState:UIControlStateNormal];
                            [navView.mPricePickV selectRow:1 inComponent:0 animated:NO];
                        }
                        
                    } else if ([navBar.pmModeType isEqualToString:@"kWh"]) {
                        if ([dict objectForKey:@"acMac"] && [[dict objectForKey:@"acMac"] length] > 0) {
                            if ([self findDeviceFromLocalData:[dict objectForKey:@"acMac"]]) {
                                [self updateDeviceStatus:dict];
                            }
                            
                        } else {
                            navBar.pmMode = [dict objectForKey:@"kwHour"];
                            [navView.mPriceBtn setTitle:[NSString stringWithFormat:@"%@kWh", navBar.pmMode] forState:UIControlStateNormal];
                            [navView.mPricePickV selectRow:0 inComponent:0 animated:NO];
                        }
                    }
                } else if ([dict valueForKey:@"allOn"]) {
                    isAllOff = ![[dict valueForKey:@"allOn"] boolValue];
                    [self setAllOnOffInfo];
                }
                
            }
            //send feedback to Living
            else if (isInLivingRoom) {
                [[NSNotificationCenter defaultCenter] postNotificationName:NotificationLivingRoom object:nil userInfo:dict];
            }
            //send feedback to AirQuality
            else if ([[dict valueForKey:@"type"] isEqual:@"gas"] ) {
                [[NSNotificationCenter defaultCenter] postNotificationName:NotificationAirQuality object:nil userInfo:dict];
            }
            //send feedback to DeviceSetup
            else if ([dict valueForKey:@"powerMeter"] != nil) {
                [[NSNotificationCenter defaultCenter] postNotificationName:NotificationDeviceSetup object:nil userInfo:dict];
            }
            else if ([dict objectForKey:@"priceHr"]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:NotificationPowerMeter object:nil userInfo:dict];
            }
        }
        
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean
{
   // NSLog(@"webSocket close code = %ld, reason = %@, wasClean = %d",(long)code,reason,wasClean);
    [self openWebSocket];
}

#pragma mark: CustomAlert Delegate
-(void)handelOk {
    
}

#pragma mark: Menu Notifivation
-(void) openAdminLock:(NSNotification *) notification {
    NSDictionary *dict = notification.userInfo;
    if (dict != nil) {
        if ([[dict valueForKey:@"openMenu"] isEqual:@"false"]) {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:NotificationAdminLock object:nil];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController * vc = [storyboard   instantiateViewControllerWithIdentifier:@"AdminLockViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}


//FUNCTION NAME _ _ -[MainPageViewController webSocket:didReceiveMessage:]:- LINE NUMBER _ _  1130  webSocketreceiveMessage = {"id":5423,"userId":67,"accountName":"Mr Ekon","name":"sensor 2t","phone":"0989962862","mac":"500291DA9075","model":"sensor","severity":2000,"color":"red","description":"Unknown","descriptionHebrew":"לא ידוע","received":false,"errorCode":"Low air quality level (2 out of 5)","appCode":"Low air quality level (2 out of 5)","startTime":1590340463000,"startTimeTimezone":1590364800000,"cleared":false,"deleted":false,"phaseNumber":0,"deviceType":"AC","alertType":"OPEN","errorNumber":"gas","localRegisterDate":"2020-05-24T17:14:23.882654Z","errorValue":2.0,"errorType":"AIR_QUALITY_ERROR"}


@end
