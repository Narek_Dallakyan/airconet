//
//  GroupSettingsTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/3/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol  GroupSettingsCellDelegate <NSObject>
-(void)groupSelectedRow:(UIButton *)sender;
-(void)groupHandlerTurnOn:(UIButton *)sender;
-(void)groupTandlerTurnOff:(UIButton *)sender;
-(void)groupTemperatureChanges:(UIPickerView *)picker value:(NSInteger)value;
-(void)groupFanChanges:(UIPickerView *)picker value:(NSInteger)value;
-(void)groupModeChanges:(UIPickerView *)picker value:(NSInteger)value;
@end

@interface GroupSettingsTableViewCell : UITableViewCell
@property(weak, nonatomic) id<GroupSettingsCellDelegate> delegate;

@property (assign, nonatomic) BOOL isOn;

@property (weak, nonatomic) IBOutlet UILabel *mGroupLb;
@property (weak, nonatomic) IBOutlet UIView *mPriceBackgroundV;
@property (weak, nonatomic) IBOutlet UILabel *mPriceLb;
@property (weak, nonatomic) IBOutlet UIImageView *mWarningImg;
@property (weak, nonatomic) IBOutlet UIView *mGroupBckgrouyndV;
@property (weak, nonatomic) IBOutlet UIImageView *mRightDirectionImg;

@property (weak, nonatomic) IBOutlet UIView *mWarningBackgroundV;
@property (weak, nonatomic) IBOutlet UIImageView *mBigWarningImg;
@property (weak, nonatomic) IBOutlet UIImageView *mLittleWarningImg;
@property (weak, nonatomic) IBOutlet UIPickerView *mTemperaturePickV;
@property (weak, nonatomic) IBOutlet UIButton *mTemperatureBtn;
@property (weak, nonatomic) IBOutlet UIPickerView *mFanPickV;
@property (weak, nonatomic) IBOutlet UIButton *mFanBtn;
@property (weak, nonatomic) IBOutlet UIPickerView *mModePick;
@property (weak, nonatomic) IBOutlet UIButton *mModeBtn;
@property (weak, nonatomic) IBOutlet UIView *mOnOffBackgroundV;
@property (weak, nonatomic) IBOutlet UIButton *mOnBtn;
@property (weak, nonatomic) IBOutlet UIButton *mOffBtn;
- (IBAction)Temperature:(UIButton *)sender;
- (IBAction)fan:(UIButton *)sender;
- (IBAction)mode:(UIButton *)sender;
- (IBAction)on:(UIButton *)sender;
- (IBAction)off:(UIButton *)sender;
- (IBAction)next:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
