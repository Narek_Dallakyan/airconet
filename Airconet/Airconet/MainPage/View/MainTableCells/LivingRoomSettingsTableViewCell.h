//
//  LivingRoomSettingsTableViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/2/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol  LivingRoomSettingsTableViewCelldelegate <NSObject>
-(void)selectedRow:(UIButton *)sender;
-(void)handlerTurnOnOff:(UIButton *)sender;
-(void)handlError:(UIButton *)sender;
-(void)temperatureChanges:(UIPickerView *)picker value:(NSInteger)value;
-(void)fanChanges:(UIPickerView *)picker value:(NSInteger)value;
-(void)modeChanges:(UIPickerView *)picker value:(NSInteger)value modeImg:(UIImage *)modeImg modeColro:(UIColor *)modeColor;


@end

@interface LivingRoomSettingsTableViewCell : UITableViewCell

@property(weak, nonatomic) id<LivingRoomSettingsTableViewCelldelegate> delegate;
//@property (strong, nonatomic) NSNumber * minPoint;
@property (assign, nonatomic) NSInteger modeImageIndex;
@property (strong, nonatomic) NSMutableArray * temperatureList;



@property (weak, nonatomic) IBOutlet UIView *mBackgroundV;
@property (weak, nonatomic) IBOutlet UILabel *mMainLb;
@property (weak, nonatomic) IBOutlet UIView *mTemperatureV;
@property (weak, nonatomic) IBOutlet UIImageView *mHomeImg;
@property (weak, nonatomic) IBOutlet UILabel *mTemperatureLb;
@property (weak, nonatomic) IBOutlet UIView *mPriceV;
@property (weak, nonatomic) IBOutlet UILabel *mPriceLb;
@property (weak, nonatomic) IBOutlet UIImageView *mTimerImg;
@property (weak, nonatomic) IBOutlet UIImageView *mRigntImg;
@property (weak, nonatomic) IBOutlet UIImageView *mTriangleErrorImg;


@property (weak, nonatomic) IBOutlet UIButton *mErrorBtn;
@property (weak, nonatomic) IBOutlet UIButton *mTurnOnOffBtn;
@property (weak, nonatomic) IBOutlet UIPickerView *mtemperaturePickV;
@property (weak, nonatomic) IBOutlet UIButton *mTemperatureBtn;
@property (weak, nonatomic) IBOutlet UIButton *fanBtn;
@property (weak, nonatomic) IBOutlet UIPickerView *mFanPickerV;
@property (weak, nonatomic) IBOutlet UIButton *mDevicesModeBtn;
@property (weak, nonatomic) IBOutlet UIPickerView *mDevicesModePickerV;
@property (weak, nonatomic) IBOutlet UIButton *mOffBtn;
@property (weak, nonatomic) IBOutlet UIView *mofflineBackgV;
@property (weak, nonatomic) IBOutlet UIView *mGradientV;
@property (weak, nonatomic) IBOutlet UIImageView *mWarningImg;



- (IBAction)off:(UIButton *)sender;
- (IBAction)next:(UIButton *)sender;
- (IBAction)temperature:(UIButton *)sender;
- (IBAction)fan:(UIButton *)sender;
- (IBAction)devicesMode:(UIButton *)sender;
- (IBAction)turnOnOff:(UIButton *)sender;
- (IBAction)error:(UIButton *)sender;


@end

NS_ASSUME_NONNULL_END
