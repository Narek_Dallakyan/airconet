//
//  GroupSettingsTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/3/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "GroupSettingsTableViewCell.h"
#import "APublicDefine.h"
#import "BaseViewController.h"

@implementation GroupSettingsTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    self.mOnOffBackgroundV.layer.cornerRadius = 8.f;
    self.mOnOffBackgroundV.layer.borderWidth = 1.f;
    self.mOnOffBackgroundV.layer.borderColor = GrayDevColor.CGColor;
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

 
    // Configure the view for the selected state
}

-(void)setIsOn:(BOOL)isOn {
    if(isOn) {
    } else {
    
    }
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    [pickerView.subviews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
        subview.hidden = (CGRectGetHeight(subview.frame) < 1.0);
    }];
    return 1;
    
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if ([pickerView isEqual: self.mTemperaturePickV]) {
        return [TemperatureList count];
    } else if ([pickerView isEqual: self.mFanPickV]) {
        return [FanModesList count];
    }
    return [DevisesModeImgList count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if ([pickerView isEqual: self.mTemperaturePickV]) {
        return [TemperatureList objectAtIndex:row];
    } else if ([pickerView isEqual: self.mFanPickV]) {
        return [FanModesList objectAtIndex:row];
    }
    return @"";
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    NSInteger currentRow = [pickerView selectedRowInComponent:0];
    //NSLog(@"\n currentRow = %i\n", currentRow);

       if ([pickerView isEqual: self.mTemperaturePickV]) {
           UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 35)];
           label.font = [UIFont fontWithName:@"Calibri" size:34.f];
           label.textColor = GrayTitleColor;
           label.textAlignment = NSTextAlignmentCenter;
           label.text = [TemperatureList objectAtIndex:row];
           if (row == currentRow) {
               label.textColor = self.mTemperatureBtn.titleLabel.textColor;
           }
           return label;
       } else if ([pickerView isEqual: self.mFanPickV]) {
           UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 35.f)];
           [btn setTitle:FanModesList[row] forState:UIControlStateNormal];
           [btn setImage:[UIImage imageNamed:@"gray_fan"] forState:UIControlStateNormal];
           btn.titleLabel.font = [UIFont fontWithName:@"Calibri" size:34.f];
           [btn setTitleColor:GrayDevColor forState:UIControlStateNormal];
           [pickerView addSubview:btn];
           if (row == currentRow) {
               [btn setTitleColor:self.mFanBtn.titleLabel.textColor forState:UIControlStateNormal];
               [btn setImage:self.mFanBtn.imageView.image forState:UIControlStateNormal];
           }
           return btn;
       } else {
           UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 35.f)];
           [btn setImage:[DevisesModeImgList objectAtIndex:row] forState:UIControlStateNormal];
         //  NSLog(@"\n before btn image  = %@\n", btn.currentImage);
           [pickerView addSubview:btn];
           if (row == currentRow) {
              // NSLog(@"\nrow  = %i\n", row);
              // NSLog(@"\ncurrentRow  = %i\n", currentRow);
               if (row == 0) {
                    [btn setImage:[UIImage imageNamed:@"auto_A"] forState:UIControlStateNormal];
               } else if (row == 2) {
                   [btn setImage:[UIImage imageNamed:@"dry"] forState:UIControlStateNormal];
               }else {
                   [btn setImage:self.mModeBtn.imageView.image forState:UIControlStateNormal];
               }
              // NSLog(@"\nmiddle  btn image  = %@\n", btn.currentImage);

           }
           //NSLog(@"\nafter  btn image  = %@\n", btn.currentImage);

           return btn;
       }
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return  35;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
   // NSLog(@"CALL didSelectRow!!!!!\n");
    if ([pickerView isEqual: self.mTemperaturePickV]) {
        [self.mTemperatureBtn setHidden:NO];
        [self.mTemperaturePickV setHidden:YES];
        [self.mTemperatureBtn setTitle:[TemperatureList objectAtIndex:row] forState:UIControlStateNormal];
        [self.delegate groupTemperatureChanges:pickerView value:[[TemperatureList objectAtIndex:row] integerValue]];
        
    } else if ([pickerView isEqual: self.mFanPickV]) {
                [self.mFanBtn setHidden:NO];
                [self.mFanPickV setHidden:YES];
                NSString * fan = [FanModesList objectAtIndex:row];
                [self.mFanBtn setTitle:fan forState:UIControlStateNormal];
                if ([fan isEqualToString:@"A"]) {
                    [self.delegate groupFanChanges:pickerView value:0];
                } else
                    [self.delegate groupFanChanges:pickerView value:[[FanModesList objectAtIndex:row] integerValue]];
    } else {
        [self.mModeBtn setHidden:NO];
        [self.mModePick setHidden:YES];
        [self.delegate groupModeChanges:pickerView value:[BaseViewController getModeValue:(int)row]];
    }
}


- (IBAction)Temperature:(UIButton *)sender {
    [self.mTemperatureBtn setHidden:YES];
       [self.mTemperaturePickV setHidden:NO];
}

- (IBAction)fan:(UIButton *)sender {
    [self.mFanBtn setHidden:YES];
      [self.mFanPickV setHidden:NO];
}

- (IBAction)mode:(UIButton *)sender {
    UIImage *img = [sender imageForState:UIControlStateNormal];
    [self.mModePick selectRow:[BaseViewController indexOfMode:img] inComponent:0 animated:NO];
    [self.mModeBtn setHidden:YES];
    [self.mModePick setHidden:NO];
}

- (IBAction)on:(UIButton *)sender {
    self.isOn = YES;
    sender.tag = 85;
    [self.delegate groupHandlerTurnOn:sender];
}

- (IBAction)off:(UIButton *)sender {
    self.isOn = NO;
    sender.tag = 86;
    [self.delegate groupTandlerTurnOff:sender];
}

- (IBAction)next:(UIButton *)sender {
    [self.delegate groupSelectedRow:sender];
}
@end
