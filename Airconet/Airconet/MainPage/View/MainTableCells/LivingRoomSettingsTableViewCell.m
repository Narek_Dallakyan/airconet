//
//  LivingRoomSettingsTableViewCell.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/2/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "LivingRoomSettingsTableViewCell.h"
#import "BaseViewController.h"
#import "APublicDefine.h"

@interface  LivingRoomSettingsTableViewCell () {
    UIColor * modeColor;
}
@end


@implementation LivingRoomSettingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
     if ([BaseViewController isDeviceLanguageRTL]) {
         [_mRigntImg setImage:[UIImage imageNamed:@"ic_left"]];
     }
        
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self.mTimerImg setImage:[UIImage imageNamed:@"timer"]];
    //[self.fanBtn setImageEdgeInsets:UIEdgeInsetsMake(-5, 0, 5, 0)];//(top, left, bottom, right)
   // [self.mDevicesModeBtn setImageEdgeInsets:UIEdgeInsetsMake(-1, -1, 1, 1)];
   // [self.mErrorBtn setImageEdgeInsets:UIEdgeInsetsMake(-10, -10, 10, 10)];

    
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    [pickerView.subviews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
        subview.hidden = (CGRectGetHeight(subview.frame) < 1.0);
    }];
    return 1;
    
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if ([pickerView isEqual: self.mtemperaturePickV]) {
        return [_temperatureList count];
    } else if ([pickerView isEqual: self.mFanPickerV]) {
        return [FanModesList count];
    }
    return [DevisesModeImgList count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if ([pickerView isEqual: self.mtemperaturePickV]) {
        return [_temperatureList objectAtIndex:row];
    } else if ([pickerView isEqual: self.mFanPickerV]) {
        return [FanModesList objectAtIndex:row];
    }
    return @"";
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    NSInteger currentRow = [pickerView selectedRowInComponent:0];
    if ([pickerView isEqual: self.mtemperaturePickV]) {
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 35)];
        label.font = [UIFont fontWithName:@"Calibri" size:34.f];
        label.textColor = GrayTitleColor;
        label.textAlignment = NSTextAlignmentCenter;
        if (row >= _temperatureList.count) {
            label.text = [_temperatureList lastObject];
        } else {
            label.text = [_temperatureList objectAtIndex:row];
        }
        if (row == currentRow) {
            label.textColor = self.mTemperatureBtn.titleLabel.textColor;
        }
        return label;
    } else if ([pickerView isEqual: self.mFanPickerV]) {
        UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 35.f)];
        [btn setTitle:FanModesList[row] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"gray_fan"] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:@"Calibri" size:34.f];
        [btn setTitleColor:GrayDevColor forState:UIControlStateNormal];
        [pickerView addSubview:btn];
        if (row == currentRow) {
            [btn setTitleColor:self.fanBtn.titleLabel.textColor forState:UIControlStateNormal];
            [btn setImage:self.fanBtn.imageView.image forState:UIControlStateNormal];
        }
        return btn;
    } else {
        UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 35.f)];
        [btn setImage:[DevisesModeImgList objectAtIndex:row] forState:UIControlStateNormal];
       // NSLog(@"DevisesModeImgList row = %i\n", row);
        [pickerView addSubview:btn];
        if (row == currentRow) {
            if (row == 0) {
                 [btn setImage:[UIImage imageNamed:@"auto_A"] forState:UIControlStateNormal];
            } else if (row == 2) {
                [btn setImage:[UIImage imageNamed:@"dry"] forState:UIControlStateNormal];
            }else {
                [btn setImage:self.mDevicesModeBtn.imageView.image forState:UIControlStateNormal];
            }
        }
        return btn;
    }
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return  35;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
   // NSLog(@"CALL didSelectRow!!!!!\n");

    if ([pickerView isEqual: self.mtemperaturePickV]) {
        [self.mTemperatureBtn setHidden:NO];
        [self.mtemperaturePickV setHidden:YES];
        [self.mTemperatureBtn setTitle:[_temperatureList objectAtIndex:row] forState:UIControlStateNormal];
        [self.delegate temperatureChanges:pickerView value:[[_temperatureList objectAtIndex:row] integerValue]];
        
    } else if ([pickerView isEqual: self.mFanPickerV]) {
                [self.fanBtn setHidden:NO];
                [self.mFanPickerV setHidden:YES];
                NSString * fan = [FanModesList objectAtIndex:row];
                [self.fanBtn setTitle:fan forState:UIControlStateNormal];
                if ([fan isEqualToString:@"A"]) {
                    [self.delegate fanChanges:pickerView value:0];
                } else
                    [self.delegate fanChanges:pickerView value:[[FanModesList objectAtIndex:row] integerValue]];
    } else {
        
        
        [self.mtemperaturePickV setHidden:YES];
       // NSLog(@"[DevisesModeImgList objectAtIndex:row] = %@\n", [DevisesModeImgList objectAtIndex:row]);
       // NSLog(@"self.mDevicesModeBtn image = %@\n", self.mDevicesModeBtn.currentImage);

        [self.mDevicesModeBtn setImage:[DevisesModeImgList objectAtIndex:row] forState:UIControlStateNormal];
        //  NSLog(@"self.mDevicesModeBtn image = %@\n", self.mDevicesModeBtn.currentImage);
        NSInteger value  = [BaseViewController getModeValue:(int)row];
        [self updateCell:value];
        [self.delegate modeChanges:pickerView value:value
                           modeImg:[DevisesModeImgList objectAtIndex:row]
                         modeColro:modeColor ];
        [self.mDevicesModeBtn setHidden:NO];
        [self.mDevicesModePickerV setHidden:YES];
    }
}

-(void)updateCell:(NSInteger)mode {
        switch (mode) {
               case 11:
               case 17:
                   // cold mode icon
                modeColor = ColdColor;
                [self.mTurnOnOffBtn setBackgroundImage:Image(@"cold_mode") forState:UIControlStateNormal];
                [self.fanBtn setImage:Image(@"blue_fan") forState:UIControlStateNormal];
                [self.mDevicesModeBtn setImage:Image(@"cold") forState:UIControlStateNormal];
                [self.mTemperatureBtn setTitleColor:ColdColor forState:UIControlStateNormal];
                [self.fanBtn setTitleColor:ColdColor forState: UIControlStateNormal];
                [self.mBackgroundV setBackgroundColor:ColdColor];
                [self.mGradientV setHidden:YES];
                [self.mBackgroundV setHidden:NO];

                   break;
               case 22:
               case 34:
                // hot mode icon
                modeColor = HotColor;
                [self.mTurnOnOffBtn setBackgroundImage:Image(@"heat_mode") forState:UIControlStateNormal];
                [self.fanBtn setImage:Image(@"red_fan_up") forState:UIControlStateNormal];
                [self.mDevicesModeBtn setImage:Image(@"hot") forState:UIControlStateNormal];
                [self.mTemperatureBtn setTitleColor:HotColor forState:UIControlStateNormal];
                [self.fanBtn setTitleColor:HotColor forState: UIControlStateNormal];
                [self.mBackgroundV setBackgroundColor:HotColor];
                [self.mGradientV setHidden:YES];

                   break;
               case 33:
               case 51:
                   // auto mode icon
                modeColor = AutoColor;
                [self.mTurnOnOffBtn setBackgroundImage:Image(@"auto_mode") forState:UIControlStateNormal];
                [self.fanBtn setImage:Image(@"purple_fan_up") forState:UIControlStateNormal];
                [self.mDevicesModeBtn setImage:Image(@"auto_A") forState:UIControlStateNormal];
                [self.mTemperatureBtn setTitleColor:AutoColor forState:UIControlStateNormal];
                [self.fanBtn setTitleColor:AutoColor forState: UIControlStateNormal];
                [self setGradient];
                //[self.mBackgroundV setBackgroundColor:AutoColor];
                   break;
               case 44:
               case 68:
                   // fan mode icon
                modeColor = FanColor;
                [self.mTurnOnOffBtn setBackgroundImage:Image(@"fan_mode") forState:UIControlStateNormal];
                [self.fanBtn setImage:Image(@"green_fan_up") forState:UIControlStateNormal];
                [self.mDevicesModeBtn setImage:Image(@"green_fan_up") forState:UIControlStateNormal];
                [self.mTemperatureBtn setTitleColor:FanColor forState:UIControlStateNormal];
                [self.fanBtn setTitleColor:FanColor forState: UIControlStateNormal];
                [self.mBackgroundV setBackgroundColor:FanColor];
                [self.mGradientV setHidden:YES];

                break;
            case 55:
               case 85:
                   // dry mode icon
                modeColor = DryColor;
                [self.mTurnOnOffBtn setBackgroundImage:Image(@"dry_mode") forState:UIControlStateNormal];
                [self.fanBtn setImage:Image(@"yellow_fan_up") forState:UIControlStateNormal];
                [self.mDevicesModeBtn setImage:Image(@"dry") forState:UIControlStateNormal];
                [self.mTemperatureBtn setTitleColor:DryColor forState:UIControlStateNormal];
                [self.fanBtn setTitleColor:DryColor forState: UIControlStateNormal];
                [self.mBackgroundV setBackgroundColor:DryColor];
                [self.mGradientV setHidden:YES];

                   break;
           }
}

-(void)setGradient{
    [self.mGradientV setHidden:NO];
    UIColor *leftColor = [UIColor colorWithRed:40.0/255.0 green:88.0/255.0 blue:149.0/255.0 alpha:1.0];
    UIColor *rightColor = [UIColor colorWithRed:177.0/255.0 green:2.0/255.0 blue:17.0/255.0 alpha:1.0];
    // Create the gradient
    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
    theViewGradient.colors = [NSArray arrayWithObjects: (id)leftColor.CGColor, (id)rightColor.CGColor, nil];
    theViewGradient.frame = CGRectMake(0, 0, self.frame.size.width, 25);
    theViewGradient.startPoint = CGPointMake(0.0, 0.5);
    theViewGradient.endPoint = CGPointMake(1.0, 0.5);
    //Add gradient to view
    [self.mGradientV.layer insertSublayer:theViewGradient atIndex:0];
}


- (IBAction)temperature:(UIButton *)sender {
    [self.mTemperatureBtn setHidden:YES];
    [self.mtemperaturePickV setHidden:NO];
    [self.mtemperaturePickV  reloadAllComponents];
    
    [self.fanBtn setHidden:NO];
     [self.mFanPickerV setHidden:YES];
     [self.mDevicesModeBtn setHidden:NO];
      [self.mDevicesModePickerV setHidden:YES];
}

- (IBAction)next:(UIButton *)sender {
    [self.delegate selectedRow:sender];
}

- (IBAction)off:(UIButton *)sender {
    [self.fanBtn setHidden:NO];
    [self.mFanPickerV setHidden:YES];
    [self.mDevicesModeBtn setHidden:NO];
     [self.mDevicesModePickerV setHidden:YES];
    [self.mTemperatureBtn setHidden:NO];
    [self.mtemperaturePickV setHidden:YES];

    if (sender.tag == 85) { //it's on
        sender.tag = 86;
        self.mTurnOnOffBtn.tag = 86;
    } else {//off
        sender.tag = 85;
        self.mTurnOnOffBtn.tag = 85;
    }
    [self.delegate handlerTurnOnOff:sender];
}

- (IBAction)fan:(UIButton *)sender {
    [self.fanBtn setHidden:YES];
    [self.mFanPickerV setHidden:NO];
    

     [self.mDevicesModeBtn setHidden:NO];
      [self.mDevicesModePickerV setHidden:YES];
     [self.mTemperatureBtn setHidden:NO];
     [self.mtemperaturePickV setHidden:YES];
    
}

- (IBAction)error:(UIButton *)sender {
    [self.delegate handlError:sender];
}

- (IBAction)turnOnOff:(UIButton *)sender {
    if (sender.tag == 85) { //it's on
        sender.tag = 86;
        self.mOffBtn.tag = 86;
    } else {//off
        sender.tag = 85;
        self.mOffBtn.tag = 85;

    }
    [self.delegate handlerTurnOnOff:sender];

}

- (IBAction)devicesMode:(UIButton *)sender {
//    UIImage *img = [sender imageForState:UIControlStateNormal];
//    [self.mDevicesModePickerV selectRow:[BaseViewController indexOfMode:img] inComponent:0 animated:NO];
    [self.mDevicesModeBtn setHidden:YES];
    [self.mDevicesModePickerV setHidden:NO];
    
    [self.fanBtn setHidden:NO];
     [self.mFanPickerV setHidden:YES];
     [self.mTemperatureBtn setHidden:NO];
     [self.mtemperaturePickV setHidden:YES];
}


@end
