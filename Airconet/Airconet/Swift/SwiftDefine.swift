//
//  SwiftDefine.swift
//  Airconet
//
//  Created by Karine Karapetyan on 24-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

let baseColor = UIColor( red: CGFloat(51/255.0), green:CGFloat(153/255.0), blue: CGFloat(255/255.0), alpha: CGFloat(1.0) )
let navTitleColor = UIColor( red: CGFloat(27/255.0), green:CGFloat(58/255.0), blue: CGFloat(99/255.0), alpha: CGFloat(1.0) )
let darkBackgColor = UIColor( red: CGFloat(38/255.0), green:CGFloat(38/255.0), blue: CGFloat(38/255.0), alpha: CGFloat(1.0) )


//#define GrayBarColor [UIColor colorWithRed:38.0f/255.0f green:38.0f/255.0f blue:38.0f/255.0f alpha:1.0]
//#define NavTitleColor [UIColor colorWithRed:27.0f/255.0f green:58.0f/255.0f blue:99.0f/255.0f alpha:1.0]
//#define PlaceholderColor [UIColor colorWithRed:127.0f/255.0f green: 127.0f/255.0f blue:127.0f/255.0f alpha:1.0]
//#define RedColor [UIColor colorWithRed:192.0f/255.0f green: 0/255.0f blue:0.0f/255.0f alpha:1.0]
//#define GreenColor [UIColor colorWithRed:48.0f/255.0f green: 204/255.0f blue:114.0f/255.0f alpha:1.0]
//#define GrayTitleColor [UIColor colorWithRed:117.0f/255.0f green:117.0f/255.0f blue:117.0f/255.0f alpha:1.0]
