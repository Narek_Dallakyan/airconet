//
//  ViewController.swift
//  JTAppleCalendar iOS Example
//
//  Created by JayT on 2016-08-10.
//
//

import UIKit
import JTAppleCalendar



class ViewController: UIViewController {
    var selectedDateArr = NSMutableArray()

    @IBOutlet weak var calendarView: JTACMonthView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var weekViewStack: UIStackView!
    @IBOutlet var numbers: [UIButton]!
    @IBOutlet var outDates: [UIButton]!
    @IBOutlet var inDates: [UIButton]!
    
    @IBOutlet weak var mMonLb: UILabel!
    @IBOutlet weak var mTusLb: UILabel!
    @IBOutlet weak var mFriLb: UILabel!
    
    @IBOutlet weak var mWenLb: UILabel!
    @IBOutlet weak var mThuLb: UILabel!
    @IBOutlet weak var mSutLb: UILabel!
    @IBOutlet weak var mSunLb: UILabel!
    
    var test: RequestManager!
    
    var numberOfRows = 6
    let formatter = DateFormatter()
    var testCalendar = Calendar.current
    var generateInDates: InDateCellGeneration = .forAllMonths
    var generateOutDates: OutDateCellGeneration = .off
    var hasStrictBoundaries = true
    let disabledColor = UIColor.lightGray
    let enabledColor = UIColor.blue
    var monthSize: MonthSize? = MonthSize(defaultSize: 50, months: [50: [.feb, .apr]])
    var prepostHiddenValue = false
    var outsideHeaderVisibilityIsOn = true
    var insideHeaderVisibilityIsOn = false
    var showVisibleDate = false
    
    var currentScrollModeIndex = 2
    let allScrollModes: [ScrollingMode] = [
        .none,
        .nonStopTo(customInterval: 374, withResistance: 0.5),
        .nonStopToCell(withResistance: 0.5),
        .nonStopToSection(withResistance: 0.5),
        .stopAtEach(customInterval: 374),
        .stopAtEachCalendarFrame,
        .stopAtEachSection
    ]
    
        override func viewDidLoad() {
            super.viewDidLoad()
            self.getHoliday()

            calendarView.register(UINib(nibName: "PinkSectionHeaderView", bundle: Bundle.main),
                                  forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                  withReuseIdentifier: "PinkSectionHeaderView")
            
            calendarView.allowsMultipleSelection = true
    //        calendarView.allowsMultipleSelection = true
            
            self.calendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
                self.setupViewsOfCalendar(from: visibleDates)
            }

             self.calendarView.scrollToDate(Date(),animateScroll: false)
             //self.calendarView.selectDates([ Date() ])
            setupScrollMode()
        }
    
    override func viewWillAppear(_ animated: Bool) {
        //set black boarder to week days
        self.setNavigationStyle()
        let baseVc = BaseViewController()
        if baseVc.isDarkMode() {
            self.view.backgroundColor = darkBackgColor
        } else {
            self.view.backgroundColor = .white
        }
        self.barButton()
        self.mMonLb.layer.borderColor = UIColor.black.cgColor
        self.mMonLb.layer.borderWidth = 2 as CGFloat
        self.mTusLb.layer.borderColor = UIColor.black.cgColor
        self.mTusLb.layer.borderWidth = 2 as CGFloat
        self.mWenLb.layer.borderColor = UIColor.black.cgColor
        self.mWenLb.layer.borderWidth = 2 as CGFloat
        self.mThuLb.layer.borderColor = UIColor.black.cgColor
        self.mThuLb.layer.borderWidth = 2 as CGFloat
        self.mFriLb.layer.borderColor = UIColor.black.cgColor
        self.mFriLb.layer.borderWidth = 2 as CGFloat
        self.mSunLb.layer.borderColor = UIColor.black.cgColor
        self.mSunLb.layer.borderWidth = 2 as CGFloat
        self.mSutLb.layer.borderColor = UIColor.black.cgColor
        self.mSutLb.layer.borderWidth = 2 as CGFloat
        self.mMonLb.text = NSLocalizedString("mon", comment: "")
        self.mTusLb.text = NSLocalizedString("tue", comment: "")
        self.mWenLb.text = NSLocalizedString("wed", comment: "")
        self.mThuLb.text = NSLocalizedString("thu", comment: "")
        self.mFriLb.text = NSLocalizedString("fri", comment: "")
        self.mSutLb.text = NSLocalizedString("sat", comment: "")
        self.mSunLb.text = NSLocalizedString("sun", comment: "")

        calendarView.cellSize = 45
        
    }
    
    func setNavigationStyle(){
         self.navigationItem.title = NSLocalizedString("holiday", comment: "");
        self.navigationController!.navigationBar.prefersLargeTitles = false;

        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = baseColor
        self.navigationController!.navigationBar.isTranslucent = false
         self.navigationController!.navigationBar.titleTextAttributes =  [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): navTitleColor,
            NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): UIFont(name: "Calibri", size: 22)! ]
        for  view in self.navigationController!.navigationBar.subviews {
            if view is DevicesNavigationBar {
                view.removeFromSuperview()
            }
        }
    }
    
    func isSelectedDate(_ date: Date) -> Bool {
        for item in selectedDateArr {
            if (date as AnyObject).isEqual(item) {
                return true
            }
        }
        return false
    }
    
    func getHoliday() {
        RequestManager.sheredMenage().getDataWithUrl(AGetHoliday, success: { (response) in
            print("AGetHoliday = \(response)")
            let responseDate = self.getDateArray((response as AnyObject).mutableCopy() as! NSMutableArray)
         DispatchQueue.main.async {
            self.calendarView.selectDates(responseDate as! [Date])
            print("selectedDateArr= \(self.selectedDateArr)")
            self.selectedDateArr.removeAllObjects()
            }
        })
    { (error) in
            print("AGetHoliday error= \(error.localizedDescription)")
        }
    }
    
    func setHoliday() {
        let dateArr = NSMutableArray()
        for item in self.selectedDateArr {
            dateArr.add(BaseViewController.convertString(from: item as! Date))
        }
    print("dateArr = \(dateArr)")

        RequestManager.sheredMenage().postJson(withContentType: APostHolidayAddRemove, andParameters: ["selectedDate" : dateArr], success: { (response) in
            print("APostHolidayAddRemove error= \(response)")
            self.navigationController?.popViewController(animated: true)

        }) { (error) in
            print("APostHolidayAddRemove error= \(error.localizedDescription)")

        }
    }
    
    func getDateArray(_ arr: NSMutableArray) -> NSMutableArray {
        let dateArr = NSMutableArray()
        for dateString in arr{
            let date = BaseViewController.convertDate(from: dateString as! String)
            dateArr.add(date)
        }
        return dateArr
    }
    
    func barButton(){
        let rightBtn = UIBarButtonItem(title: NSLocalizedString("cancel", comment: ""), style: .plain, target: self, action: #selector(cancel))
        self.navigationItem.rightBarButtonItem  = rightBtn
        
        let leftBtn = UIBarButtonItem(title: NSLocalizedString("save", comment: ""), style: .plain, target: self, action: #selector(save))
               self.navigationItem.leftBarButtonItem  = leftBtn
    }
    
    @objc func cancel () {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func save () {
        self.setHoliday()
    }
    
    var rangeSelectedDates: [Date] = []
    func didStartRangeSelecting(gesture: UILongPressGestureRecognizer) {
        let point = gesture.location(in: gesture.view!)
        rangeSelectedDates = calendarView.selectedDates
        if let cellState = calendarView.cellStatus(at: point) {
            let date = cellState.date
            if !calendarView.selectedDates.contains(date) {
                let dateRange = calendarView.generateDateRange(from: calendarView.selectedDates.first ?? date, to: date)
                for aDate in dateRange {
                    if !rangeSelectedDates.contains(aDate) {
                        rangeSelectedDates.append(aDate)
                    }
                }
                calendarView.selectDates(from: rangeSelectedDates.first!, to: date, keepSelectionIfMultiSelectionAllowed: true)
            } else {
                let indexOfNewlySelectedDate = rangeSelectedDates.firstIndex(of: date)! + 1
                let lastIndex = rangeSelectedDates.endIndex
                let followingDay = testCalendar.date(byAdding: .day, value: 1, to: date)!
                calendarView.selectDates(from: followingDay, to: rangeSelectedDates.last!, keepSelectionIfMultiSelectionAllowed: false)
                rangeSelectedDates.removeSubrange(indexOfNewlySelectedDate..<lastIndex)
            }
        }
        
        if gesture.state == .ended {
            rangeSelectedDates.removeAll()
        }
    }

    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = testCalendar.component(.year, from: startDate)
        let monthNum = Calendar.current.component(.month, from: startDate)
        
                if !showVisibleDate {
                    showVisibleDate = true
                    let currDate = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "M"
                    let formatter1 = DateFormatter()
                    formatter1.dateFormat = "LLLL"
                    let currMonthNum = formatter.string(from: currDate)
                    let currMonthName = formatter1.string(from: currDate)
                    monthLabel.text = currMonthName + " (" +  String(currMonthNum) + ") "  + String(year)
                } else {
                    monthLabel.text = monthName + " (" +  String(monthNum) + ") "  + String(year)
                }
    }
    
    func handleCellConfiguration(cell: JTACDayCell?, cellState: CellState) {
        handleCellSelection(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        if cellState.dateBelongsTo == .thisMonth {
            cell?.isHidden = false
        } else {
            cell?.isHidden = true
        }
    }
    
    // Function to handle the text color of the calendar
    func handleCellTextColor(view: JTACDayCell?, cellState: CellState) {
        guard let myCustomCell = view as? CellView  else {
            return
        }

        if cellState.isSelected {
            //selection and deselection of the  day
            if myCustomCell.dayLabel.textColor != .green {
                if myCustomCell.dayLabel.textColor == baseColor {
                    myCustomCell.dayLabel.textColor = .white
                } else {
                    myCustomCell.dayLabel.textColor = baseColor
                }
            }
        } else {
            if cellState.dateBelongsTo == .thisMonth {
                myCustomCell.dayLabel.textColor = .white
            }
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        let visibleDates = calendarView.visibleDates()
        calendarView.viewWillTransition(to: .zero, with: coordinator, anchorDate: visibleDates.monthDates.first?.date)
    }

    // Function to handle the calendar selection
    func handleCellSelection(view: JTACDayCell?, cellState: CellState) {
       guard let myCustomCell = view as? CellView else {return }

        if cellState.isSelected {
            //myCustomCell.selectedView.layer.cornerRadius =  13
        myCustomCell.selectedView.isHidden = false
        } else {
            myCustomCell.selectedView.isHidden = true
        }
    }
    
    func setupScrollMode() {
        currentScrollModeIndex = 2
        calendarView.scrollingMode = allScrollModes[currentScrollModeIndex]
    }
}

// MARK : JTAppleCalendarDelegate
extension ViewController: JTACMonthViewDelegate, JTACMonthViewDataSource {
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = testCalendar.timeZone
        formatter.locale = testCalendar.locale
        
        //get current year
        let date = Date()
        let calendar = Calendar.current
        let year =  calendar.component(.year, from: date)
        let startDate = formatter.date(from: "\(year) 01 01")!
        let endDate = formatter.date(from: "2030 12 01")!
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: numberOfRows,
                                                 calendar: testCalendar,
                                                 generateInDates: generateInDates,
                                                 generateOutDates: generateOutDates,
                                                 firstDayOfWeek: .monday,
                                                 hasStrictBoundaries: hasStrictBoundaries)
        return parameters
    }
    
    func configureVisibleCell(myCustomCell: CellView, cellState: CellState, date: Date, indexPath: IndexPath) {
        myCustomCell.dayLabel.text = cellState.text
        if testCalendar.isDateInToday(date) {
            myCustomCell.backgroundColor = .clear
        } else {
            myCustomCell.backgroundColor = .black
        }
        handleCellConfiguration(cell: myCustomCell, cellState: cellState)
        
        
        if cellState.text == "1" {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM"
            let month = formatter.string(from: date)
            myCustomCell.monthLabel.text = "\(month) \(cellState.text)"
        } else {
            myCustomCell.monthLabel.text = ""
        }
    }
    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        // This function should have the same code as the cellForItemAt function
        let myCustomCell = cell as! CellView
        configureVisibleCell(myCustomCell: myCustomCell, cellState: cellState, date: date, indexPath: indexPath)
    }
    
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "CellView", for: indexPath) as! CellView
        configureVisibleCell(myCustomCell: myCustomCell, cellState: cellState, date: date, indexPath: indexPath)
       
        return myCustomCell
    }

    func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        handleCellConfiguration(cell: cell, cellState: cellState)
        let currSelectedDate = cellState.date
        selectedDateArr.add(currSelectedDate)
    }

    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
       // cell.dayLabel.textColor = UIColor.blue
        handleCellConfiguration(cell: cell, cellState: cellState)
        
        let currSelectedDate = cellState.date
        selectedDateArr.add(currSelectedDate)
        print("selectedDateArr after add = \(selectedDateArr)")
    }
    
    func calendar(_ calendar: JTACMonthView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
//        print("After: \(calendar.contentOffset.y)")

    }
    
    func calendar(_ calendar: JTACMonthView, willScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(from: visibleDates)
    }
    
    func calendar(_ calendar: JTACMonthView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTACMonthReusableView {
        let date = range.start
        let month = testCalendar.component(.month, from: date)
        formatter.dateFormat = "MM"
        //*********
        
              let month1 = testCalendar.dateComponents([.month], from: date).month!
              let monthName = DateFormatter().monthSymbols[(month1-1) % 12]
              // 0 indexed array
              let year = testCalendar.component(.year, from: date)
              let monthNum = Calendar.current.component(.month, from: date)
              let dateText = monthName + " (" +  String(monthNum) + ") "  + String(year)
        //***********
        
        
        
        
        let header: JTACMonthReusableView
        if month % 2 > 0 {
            header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "WhiteSectionHeaderView", for: indexPath)
            (header as! WhiteSectionHeaderView).title.text = dateText//formatter.string(from: date)
        } else {
           header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "PinkSectionHeaderView", for: indexPath)
            (header as! PinkSectionHeaderView).title.text = dateText//formatter.string(from: date)
        }
        return header
    }
    
    func sizeOfDecorationView(indexPath: IndexPath) -> CGRect {
        let stride = calendarView.frame.width * CGFloat(indexPath.section)
        return CGRect(x: stride + 5, y: 5, width: calendarView.frame.width - 10, height: calendarView.frame.height - 10)
    }
    
    func calendarSizeForMonths(_ calendar: JTACMonthView?) -> MonthSize? {
        return monthSize
    }
}


extension Array where Element: Equatable {
    func removing(_ obj: Element) -> [Element] {
        return filter { $0 != obj }
    }
}
