//
//  ResetPasswordViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/27/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "ResetPasswordView.h"
#import "RequestManager.h"
#import "Validator.h"
#import "ACommonTools.h"
@interface ResetPasswordViewController ()<CustomAlertDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *mScrollV;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityInd;
@property (strong, nonatomic) IBOutlet ResetPasswordView *mResetPasswordV;
@property (weak, nonatomic) IBOutlet UILabel *mStatusLb;

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self preferredStatusBarStyle];
}

-(void)viewWillAppear:(BOOL)animated {
    [self setBackground:self];

}

- (IBAction)resetPassword:(UIButton *)sender {
    if ([self isValidFields]) {
        [self.mActivityInd startAnimating];
        NSDictionary *dict = @{@"email": self.mResetPasswordV.mEmailTxtFl.text};
            [[RequestManager sheredMenage] postJsonDataWithUrl:AGetuserrequestRecoverPassword andParameters:dict success:^(id response) {
                dispatch_async(dispatch_get_main_queue(),^{
                    [self.mActivityInd stopAnimating];
                });
                if ([[response valueForKey:@"attachment"] isEqual:@"failed"] || ![[response valueForKey:@"result"] isEqual:[NSNumber numberWithInt:0]]) {
                    [self showAlert:NSLocalizedString(@"correct_email_note", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
                    
                } else if ([[response valueForKey:@"result"] isEqual:[NSNumber numberWithInt:0]]) {
                    dispatch_async(dispatch_get_main_queue(),^{
                        [ACommonTools setObjectValue: self.mResetPasswordV.mEmailTxtFl.text forKey:AKeyCurrentUserName];

                        [self showAlert:NSLocalizedString(@"check_email", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];

                    });
                }
            } failure:^(NSError *error) {
                [self showAlert:NSLocalizedString(@"correct_email_note", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
            }];
        }
}

-(BOOL) isValidFields {
    if ([self.mResetPasswordV.mEmailTxtFl.text isEqualToString:NSLocalizedString(@"email", nil)] ||  self.mResetPasswordV.mEmailTxtFl.text.length == 0 ) {
        [self showAlert:NSLocalizedString(@"fill_email", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        return NO;
    } else if (![Validator isValidEmail:self.mResetPasswordV.mEmailTxtFl.text]) {
        [self showAlert:NSLocalizedString(@"correct_email_note", nil)ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
        return NO;
    }
    return YES;
}

- (IBAction)cancel:(UIButton *)sender {
   // [self presentViewControllerWithIdentifier:@"Login" controller:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark : Custom Alert View delegate
-(void)handelOk {
    
}
-(void)handelOkWithMessage:(NSString *)message {
    [self presentViewControllerWithIdentifier:@"ChangePassword" controller:self];
}
@end
