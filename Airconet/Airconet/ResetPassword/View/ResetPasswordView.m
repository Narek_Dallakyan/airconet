//
//  ResetPasswordView.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/27/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "ResetPasswordView.h"
#import "APublicDefine.h"
#import "UITextField+Content.h"

@implementation ResetPasswordView

-(void)awakeFromNib {
    [self.mEmailTxtFl setBlueBorderStyle];
    [self.mEmailTxtFl setImageInLeftView:[UIImage imageNamed:@"email"]];
    [self.mEnterEmailLb setText:NSLocalizedString(@"enter_email", nil)];
    [self.mEmailTxtFl setRightPadding];
    
    [self.mEmailTxtFl setPlaceholderColor:PlaceholderColor text:self.mEmailTxtFl.placeholder];
    [super awakeFromNib];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.returnKeyType = UIReturnKeyDone;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidChangeSelection:(UITextField *)textField {
    if (textField.text.length == 0) {
        [textField setRightPadding];
    } else
        [textField deletRightPadding];
}

@end
