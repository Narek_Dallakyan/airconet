//
//  ResetPasswordView.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/27/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ResetPasswordView : UIView
@property (weak, nonatomic) IBOutlet UITextField *mEmailTxtFl;
@property (weak, nonatomic) IBOutlet UILabel *mEnterEmailLb;
@property (strong, nonatomic) IBOutlet UIButton *mResetPassBtn;
@property (strong, nonatomic) IBOutlet UIButton *mCancelBtn;

@end

NS_ASSUME_NONNULL_END
