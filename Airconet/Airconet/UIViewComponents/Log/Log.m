//
//  Log.m
//  DCC
//
//  Created by Armen Shahvaladyan on 1/11/16.
//  Copyright © 2016 Armen. All rights reserved.
//

#import "Log.h"
#import "ACommonTools.h"


@implementation Log


void _Log(const char *file, int lineNumber, const char *funcName, NSString *format,...) {
    va_list ap;
    va_start (ap, format);
    format = [format stringByAppendingString:@"\n"];
    NSString *msg = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@",format] arguments:ap];
    va_end (ap);
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    fprintf(stderr,"FUNCTION NAME _ _ %s:- LINE NUMBER _ _  %3d  %s", funcName, lineNumber, [msg UTF8String]);

    NSString * logStr = [NSString stringWithFormat:@"FUNCTION NAME _ _ %s:- LINE NUMBER _ _  %3d  %s",/*time,*/ funcName, lineNumber, [msg UTF8String]];

    dateFormatter = nil;

   if (![ACommonTools getBoolValueForKey:@"isSentLogfile"] ) {

        append(logStr);
    }
}


void append(NSString *msg){
    // get path to Documents/somefile.txt
   // [ACommonTools setBoolValue:YES forKey:@"isSentLogfile"];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"logfile.txt"];
    // create if needed
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]){
        fprintf(stderr,"Creating file at %s",[path UTF8String]);
        [[NSData data] writeToFile:path atomically:YES];
    }
    // append
    NSFileHandle *handle = [NSFileHandle fileHandleForWritingAtPath:path];
    [handle truncateFileAtOffset:[handle seekToEndOfFile]];
    [handle writeData:[msg dataUsingEncoding:NSUTF8StringEncoding]];
    [handle closeFile];
}

@end
