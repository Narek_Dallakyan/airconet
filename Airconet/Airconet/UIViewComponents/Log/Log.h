//
//  Log.h
//  DCC
//
//  Created by Armen Shahvaladyan on 1/11/16.
//  Copyright © 2016 Armen. All rights reserved.
//

#import <Foundation/Foundation.h>

#define NSLog(args...) _Log(__FILE__,__LINE__,__PRETTY_FUNCTION__,args);

@interface Log : NSObject

void _Log(const char *file, int lineNumber, const char *funcName, NSString *format,...);

@end
