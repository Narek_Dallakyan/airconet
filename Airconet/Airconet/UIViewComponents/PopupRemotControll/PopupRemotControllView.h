//
//  PopupRemotControllView.h
//  Airconet
//
//  Created by Karine Karapetyan on 18-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol PopupRemotControllDelegate <NSObject>

@optional
-(void) handlerRemotControllOk;
@end
@interface PopupRemotControllView : UIView
@property (strong, nonatomic) id <PopupRemotControllDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *mRemoteImg;
@property (weak, nonatomic) IBOutlet UILabel *mInfoFirstLb;
@property (weak, nonatomic) IBOutlet UIButton *mOkBtn;
@property (weak, nonatomic) IBOutlet UILabel *mInfoSecondLb;
- (IBAction)ok:(UIButton *)sender;
- (id)initWithFrame:(CGRect)frame
           delegate:(nonnull id<PopupRemotControllDelegate>)delegate;
@end

NS_ASSUME_NONNULL_END
