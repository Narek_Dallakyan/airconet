//
//  PopupRemotControllView.m
//  Airconet
//
//  Created by Karine Karapetyan on 18-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "PopupRemotControllView.h"
#import "APublicDefine.h"
#import "BaseViewController.h"



@interface PopupRemotControllView ()

@end

@implementation PopupRemotControllView

- (id)initWithFrame:(CGRect)frame 
           delegate:(nonnull id<PopupRemotControllDelegate>)delegate {
    self = [super initWithFrame:frame];
    PopupRemotControllView * view = nil;
    NSArray *views = [[NSBundle mainBundle] loadNibNamed: @"PopupRemotControllView" owner: self options: nil];
       if (views) {
           for (UIView *aView in views) {
               if ([aView isKindOfClass: [PopupRemotControllView class]])
                   view = (PopupRemotControllView *)aView;
               view.delegate = delegate;
                view.layer.cornerRadius = 9.f;
               view.layer.borderWidth = 2.5f;
               view.layer.borderColor = [BaseColor CGColor];
               if ([[[BaseViewController alloc] init] isDarkMode]) {
                   [self setBackgroundColor:GrayBarColor];
               } else {
                   [self setBackgroundColor:[UIColor whiteColor]];
               }
              // [self setupView];
           }
       }
       return view;
    
}

-(void)setupView {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mInfoFirstLb setText:NSLocalizedString(@"press_any_button", nil)];
        [self.mInfoFirstLb setText:NSLocalizedString(@"mount_device", nil)];
        [self.mOkBtn setTitle:NSLocalizedString(@"ok", nil) forState:UIControlStateNormal];
    });
}

- (IBAction)ok:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(handlerRemotControllOk)]) {
               [self.delegate handlerRemotControllOk];
           }
    for(UIView *subView in self.superview.subviews) {
        [subView setUserInteractionEnabled:YES];
        [subView setAlpha:1.0];
    }
    [self removeFromSuperview];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
