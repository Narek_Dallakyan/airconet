//
//  CustomUITextFiled.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/1/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomUITextFiled : UITextField

@end

NS_ASSUME_NONNULL_END
