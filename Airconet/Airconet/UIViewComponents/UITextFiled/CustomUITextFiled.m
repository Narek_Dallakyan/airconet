//
//  CustomUITextFiled.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/1/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "CustomUITextFiled.h"

@implementation CustomUITextFiled

- (CGRect) rightViewRectForBounds:(CGRect)bounds {

    CGRect textRect = [super rightViewRectForBounds:bounds];
    textRect.origin.x -= 10;
    return textRect;
}

@end
