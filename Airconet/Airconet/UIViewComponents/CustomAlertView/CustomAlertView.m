//
//  CustomAlertView.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/27/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "CustomAlertView.h"
#import "APublicDefine.h"
#import "BaseViewController.h"


@interface CustomAlertView ()
@end

@implementation CustomAlertView


-(id)initWithFrame:(CGRect)frame message:(NSString *)message delegate:(nonnull id<CustomAlertDelegate>)delegate {
    self = [super initWithFrame:frame];
    CustomAlertView * view = nil;
    NSArray *views = [[NSBundle mainBundle] loadNibNamed: @"CustomAlertView" owner: self options: nil];
       if (views) {
           for (UIView *aView in views) {
               if ([aView isKindOfClass: [CustomAlertView class]])
                   view = (CustomAlertView *)aView;
               view.delegate = delegate;
                view.layer.cornerRadius = 9.f;
               view.layer.borderWidth = 2.5f;
               view.layer.borderColor = [BaseColor CGColor];
               [view.mMessageLb setText:message];
               if ([[[BaseViewController alloc] init] isDarkMode]) {
                   [self setBackgroundColor:GrayBarColor];
               } else {
                   [self setBackgroundColor:[UIColor whiteColor]];
               }
               [self setupView];
           }
       }
       return view;
}

-(void)setupView {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mOkBtn setTitle:NSLocalizedString(@"ok", nil) forState:UIControlStateNormal];
        [self.mCancelBtn setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    });
}

-(BOOL)isOkWithAction {
    if ([self.mMessageLb.text isEqualToString:NSLocalizedString(@"comfirm_email", nil)] ||
        [self.mMessageLb.text isEqualToString:NSLocalizedString(@"check_email", nil)] ||
        [self.mMessageLb.text isEqualToString:NSLocalizedString(@"fill_email_again", nil)]) {
        return YES;
    }
    return NO;
}

- (IBAction)ok:(UIButton *)sender {
    if ([self isOkWithAction]) {
        [self.delegate handelOkWithMessage:self.mMessageLb.text];
    } else {
        if ([self.delegate respondsToSelector:@selector(handelOk)]) {
            [self.delegate handelOk];
        }
        
    }
    for(UIView *subView in self.superview.subviews) {
        [subView setUserInteractionEnabled:YES];
        [subView setAlpha:1.0];
    }
    [self removeFromSuperview];
}

- (IBAction)cancel:(UIButton *)sender {
}


@end
