//
//  CustomAlertView.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/27/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol CustomAlertDelegate <NSObject>

@optional
-(void)handelOk;
-(void)handelCancel;
-(void)handelOkWithMessage:(NSString *)message;


@end

@interface CustomAlertView : UIView
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mOkBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mCancelBtnWidth;
@property (weak, nonatomic) IBOutlet UILabel *mMessageLb;
@property (weak, nonatomic) IBOutlet UIButton *mOkBtn;

@property (weak, nonatomic) IBOutlet UIButton *mCancelBtn;
@property(weak, nonatomic) id<CustomAlertDelegate> delegate;
-(id)initWithFrame:(CGRect)frame
           message:(NSString *)message
          delegate:(id<CustomAlertDelegate>)delegate;
- (IBAction)ok:(UIButton *)sender;
- (IBAction)cancel:(UIButton *)sender;
@end

NS_ASSUME_NONNULL_END
