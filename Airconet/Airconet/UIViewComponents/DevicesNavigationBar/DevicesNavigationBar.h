//
//  DevicesNavigationBar.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/3/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBar.h"

NS_ASSUME_NONNULL_BEGIN
@protocol DeviceNavigationBarDelegate <NSObject>

-(void)choosePriceType:(NSString*)type;
-(void)handlerNavBarbuttons:(int)tag;
-(void)handlerPowerMeter;
-(void)updateNavigationObj:(NavigationBar *)navBarObj;
@end
@interface DevicesNavigationBar : UIView

@property (strong, nonatomic) id<DeviceNavigationBarDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIPickerView *mTimerPickV;
@property (weak, nonatomic) IBOutlet UIButton *mTimerBtn;
@property (weak, nonatomic) IBOutlet UIPickerView *mPricePickV;
@property (weak, nonatomic) IBOutlet UIButton *mPriceBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mWidthLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mCenterXLayout;
@property (weak, nonatomic) IBOutlet UIButton *mPowerMeterBtn;
- (IBAction)timer:(UIButton *)sender;
- (IBAction)price:(UIButton *)sender;
- (IBAction)powerMeter:(UIButton *)sender;

-(instancetype)initWithFrame:(CGRect)frame;
@end

NS_ASSUME_NONNULL_END
