//
//  DevicesNavigationBar.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/3/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "DevicesNavigationBar.h"
#import "APublicDefine.h"
#import "AKeysDefine.h"
#import "RequestManager.h"
#import "BaseViewController.h"

@interface DevicesNavigationBar () {
    NavigationBar * navBar;
    NSMutableArray * timerList;
    NSMutableArray * priceList;
    NSMutableArray * priceListForRequest;
    NSMutableArray * timerListForRequest;
}
@end

@implementation DevicesNavigationBar

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed: @"DevicesNavigationBar" owner: self options: nil];
        self.contentView = [nibViews objectAtIndex:0];
        self.contentView.frame = self.bounds;
        self.contentView.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
                                             UIViewAutoresizingFlexibleHeight);
        [self addSubview:self.contentView];
        [self setupView];
    }
    return self;
}


-(void)setupView {
    navBar = [[NavigationBar alloc] init];
    timerList = [NSMutableArray array];
    priceList = [NSMutableArray array];
    timerListForRequest = [NSMutableArray array];
    priceListForRequest = [NSMutableArray array];
    if (SCREEN_Width < 370) {
        [_mTimerBtn.titleLabel setFont:[UIFont fontWithName:@"Calibri" size:14]];
        [_mPriceBtn.titleLabel setFont:[UIFont fontWithName:@"Calibri" size:14]];
        [_mPowerMeterBtn.titleLabel setFont:[UIFont fontWithName:@"Calibri" size:14]];
    }
    [self getPMInfo];
}
#pragma mark: CUSTOM NAVIGATION BAR

-(void)getPMInfo {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] getDataWithUrl:AGetPmInfo success:^(id  _Nonnull response) {
           // NSLog(@"AGetPmInfo response  = %@\n", response);
            self->navBar.isMasterPm = [[response valueForKey:@"hasUserMasterPM"] boolValue];
            self->navBar.isLinkedPm = [[response valueForKey:@"hasUserLinkedPM"] boolValue];
            self->navBar.isLinkedSensorPm =[[response valueForKey:@"hasUserSensorLinkedPM"] boolValue];
            [self updateNavifationBarObj];
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"AGetPmInfo response error  = %@\n", error.localizedDescription);
        }];
    });
}

-(void)getNavigationInfo {
    if([self->navBar.dateModeForRequest isEqualToString:@"REALTIME"] && [self->navBar.pmModeTypeForRequest isEqualToString:@"HOURS"]) {
        self->navBar.pmModeTypeForRequest = @"COST";
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         NSString * urlFormar = [NSString stringWithFormat:@"%@?timeRange=%@&powerMeterMode=%@", AGetNavigationDeviceMode, self->navBar.dateModeForRequest, self->navBar.pmModeTypeForRequest];
        //NSLog(@"getNavigationInfo url = %@\n", urlFormar);
        [[RequestManager sheredMenage] getDataWithUrl:urlFormar success:^(id  _Nonnull response) {
            self->navBar.errorNumber = [response valueForKey:@"errorNumber"];
            self->navBar.pmMode = [response valueForKey:@"pmMode"];
            [self updateNavifationBarInfo];
           // NSLog(@"AGetNavigationDeviceMode response  = %@\n", response);
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"AGetNavigationDeviceMode response error  = %@\n", error.localizedDescription);
        }];
    });
}
-(void)updateNavifationBarObj{
    if (!navBar.isMasterPm && !navBar.isLinkedSensorPm) {
         self.frame = CGRectMake(self.frame.origin.x+30, self.frame.origin.y, self.frame.size.width-30, self.frame.size.height);
    }
    navBar.dateModeForRequest = @"DAY";
    navBar.pmModeTypeForRequest = @"HOURS";
    navBar.pmModeType = NSLocalizedString(@"hour", nil);// @"Hour";
    [self getNavigationInfo];
    [self.delegate updateNavigationObj:navBar];
    
}

-(void)updateNavifationBarInfo{
    navBar.dateMode = [BaseViewController getDateMode:navBar.dateModeForRequest];
    navBar.pmModeType = [BaseViewController getPmMode:navBar.pmModeTypeForRequest];
    if (!navBar.isMasterPm && !navBar.isLinkedSensorPm) {
        if (SCREEN_Width < 370 ) {
            self.mCenterXLayout.constant = 0;
            self.mWidthLayout.constant = self.frame.size.width-30;
        } else {
           
            self.mCenterXLayout.constant = 20;
            self.mWidthLayout.constant = self.frame.size.width-50;
        }
        
        timerList = [TimerWithoutOnlineList mutableCopy];
        priceList = [PriceHourList mutableCopy];
        timerListForRequest = [TimerForRequestWithoutOnlineList mutableCopy];
        priceListForRequest = [PriceHRequestList mutableCopy];
        [timerList removeLastObject];
        [timerListForRequest removeLastObject];
        [self.mTimerPickV reloadAllComponents];
        [self.mPricePickV reloadAllComponents];
    } else {
        self.mCenterXLayout.constant = 10;
        self.mWidthLayout.constant = 230;
        if (![self->navBar.dateModeForRequest isEqualToString:@"REALTIME"]) {
            priceListForRequest = [PriceForRequestList mutableCopy];
            priceList = [PriceList mutableCopy];
        } else {
            priceList = [PriceWithoutHList mutableCopy];//[PriceWithoutHList mutableCopy];
            priceListForRequest = [PriceWithoutHRequestList mutableCopy];
        }
        timerList = [TimerList mutableCopy];
        timerListForRequest = [TimerForRequestList mutableCopy];
        [self.mTimerPickV reloadAllComponents];
        [self.mPricePickV reloadAllComponents];
        navBar.leafColor = [NavigationBar leafColor: navBar.errorNumber];
        navBar.leafImg = [NavigationBar leafImg: navBar.errorNumber];
        [self.mPowerMeterBtn setHidden:NO];
        [self.mPowerMeterBtn setImage:navBar.leafImg forState:UIControlStateNormal];
        [self.mPowerMeterBtn setTitle:[navBar.errorNumber stringValue] forState:UIControlStateNormal];
        [self.mPowerMeterBtn setTitleColor:navBar.leafColor forState:UIControlStateNormal];
    }
    
    
    [self.mTimerBtn setTitle:navBar.dateMode forState:UIControlStateNormal];
     if ([timerList indexOfObject:navBar.dateMode] >= 0 && [timerList indexOfObject:navBar.dateMode] <= timerList.count-1 ) {
         [self.mTimerPickV selectRow:[timerList indexOfObject:navBar.dateMode] inComponent:0 animated:YES];
     }
    if ([priceList indexOfObject:navBar.pmModeType] >= 0 && [priceList indexOfObject:navBar.pmModeType]<= priceList.count-1 ) {
        [self.mPricePickV selectRow:[priceList indexOfObject:navBar.pmModeType] inComponent:0 animated:YES];
    }
    
    if ([navBar.pmModeType isEqualToString:NSLocalizedString(@"hour", nil)]) {
        if ([[ navBar.pmMode stringValue] length] > 3) {
            [self.mPriceBtn setTitle:[NSString stringWithFormat: @"%.1f%@", [ navBar.pmMode floatValue], NSLocalizedString(@"h", nil)] forState:UIControlStateNormal];
        } else {
            [self.mPriceBtn setTitle:[NSString stringWithFormat: @"%@%@", navBar.pmMode, NSLocalizedString(@"h", nil)] forState:UIControlStateNormal];
        }
    } else if ([navBar.pmModeType isEqualToString:NSLocalizedString(@"kWh", nil)]) {
        if ([[ navBar.pmMode stringValue] length] > 3) {
            [self.mPriceBtn setTitle:[NSString stringWithFormat: @"%.1f%@", [ navBar.pmMode floatValue], NSLocalizedString(@"kw_h", nil)] forState:UIControlStateNormal];
        } else {
            [self.mPriceBtn setTitle:[NSString stringWithFormat: @"%@%@", navBar.pmMode, NSLocalizedString(@"kw_h", nil)] forState:UIControlStateNormal];
        }
    } else {
        if ([[navBar.pmMode stringValue] length] > 3) {
            [self.mPriceBtn setTitle:[NSString stringWithFormat: @"%@%.1f",NSLocalizedString(@"$", nil), [ navBar.pmMode floatValue]] forState:UIControlStateNormal];
        } else {
            [self.mPriceBtn setTitle:[NSString stringWithFormat: @"%@%@",NSLocalizedString(@"$", nil), navBar.pmMode] forState:UIControlStateNormal];
        }
    }
    
    if ([navBar.errorNumber integerValue] == 0 ) {
        [self.mPowerMeterBtn setHidden:YES];
    }
    [self setNeedsDisplay];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    [pickerView.subviews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
        subview.hidden = (CGRectGetHeight(subview.frame) < 1.0);
    }];
    return 1;
    
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView.tag == 1) {
        return [timerList count];
    }
    return [priceList count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if ([pickerView isEqual: self.mTimerPickV]) {
        return [timerList objectAtIndex:row];
    } else if ([pickerView isEqual: self.mPricePickV]) {
        return [priceList objectAtIndex:row];
    }
    return @"";
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    NSInteger currentRow = [pickerView selectedRowInComponent:0];
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0,70, 60)];
    label.font = [UIFont fontWithName:@"Calibri" size:19.f];
    label.textColor = GrayTitleColor;
    label.textAlignment = NSTextAlignmentCenter;
    if (pickerView.tag == 1) {
        label.text = [timerList objectAtIndex:row];
    } else {
        label.text = [priceList objectAtIndex:row];
    }
    if (row == currentRow) {
        label.textColor = ColdColor;
    }
    return label;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return  20;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    UILabel *labelSelected = (UILabel*)[pickerView viewForRow:row forComponent:component];
    [labelSelected setTextColor:ColdColor];
    if (pickerView.tag == 1) {
        [self.mTimerBtn setHidden:NO];
        [self.mTimerPickV setHidden:YES];
        [self.mTimerBtn setTitle:[timerList objectAtIndex:row] forState:UIControlStateNormal];
        //NSLog(@"mTimerBtn title = %@\n", [timerList objectAtIndex:row]);
        navBar.dateMode = [timerList objectAtIndex:row];
        navBar.dateModeForRequest = [timerListForRequest objectAtIndex:row];
        [self.delegate updateNavigationObj:navBar];
        [self getNavigationInfo];
    } else  {
        [self.mPriceBtn setHidden:NO];
        [self.mPricePickV setHidden:YES];
        [self.mPriceBtn setTitle:[priceList objectAtIndex:row] forState:UIControlStateNormal];
       // NSLog(@"mPriceBtn title = %@\n", [priceList objectAtIndex:row]);
        
        navBar.pmModeType = [priceList objectAtIndex:row];
        navBar.pmModeTypeForRequest = [priceListForRequest objectAtIndex:row];
        [self.delegate updateNavigationObj:navBar];
        [self getNavigationInfo];
        
        [self.delegate choosePriceType:[priceList objectAtIndex:row]];
    }
}

- (void)selectRow:(NSInteger)row inComponent:(NSInteger)component animated:(BOOL)animated {
   // NSLog(@"CALL selectRow!!!!!\n");
}

- (IBAction)timer:(UIButton *)sender {
    [self.mTimerBtn setHidden:YES];
    [self.mTimerPickV setHidden:NO];
}

- (IBAction)price:(UIButton *)sender {
    if (navBar.isMasterPm || navBar.isLinkedSensorPm) {
        [self.mPriceBtn setHidden:YES];
        [self.mPricePickV setHidden:NO];
    }
}

- (IBAction)powerMeter:(UIButton *)sender {
    [self.delegate handlerPowerMeter];
}
@end
