//
//  PopupView.h
//  Airconet
//
//  Created by Karine Karapetyan on 27-02-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol PopupViewDelegate <NSObject>

@optional
-(void) handlerYes;
-(void) handlerCancel;
@end
@interface PopupView : UIView
@property (strong, nonatomic) id <PopupViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *mErrorImg;
@property (weak, nonatomic) IBOutlet UILabel *mAlertLb;
@property (weak, nonatomic) IBOutlet UILabel *mMessageLb;
@property (weak, nonatomic) IBOutlet UIButton *mCancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *mYesBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mOkbtncenterLayout;

- (IBAction)cancel:(UIButton *)sender;
- (IBAction)yes:(UIButton *)sender;

- (id)initWithFrame:(CGRect)frame message:(NSString *)message delegate:(nonnull id<PopupViewDelegate>)delegate;
@end

NS_ASSUME_NONNULL_END
