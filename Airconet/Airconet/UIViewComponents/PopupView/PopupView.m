//
//  PopupView.m
//  Airconet
//
//  Created by Karine Karapetyan on 27-02-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "PopupView.h"
#import "APublicDefine.h"
#import "BaseViewController.h"


@implementation PopupView



- (id)initWithFrame:(CGRect)frame message:(NSString *)message delegate:(nonnull id<PopupViewDelegate>)delegate
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed: @"PopupView" owner: self options: nil];
        self.contentView = [nibViews objectAtIndex:0];
        self.contentView.frame = self.bounds;
        self.contentView.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
           UIViewAutoresizingFlexibleHeight);
        self.delegate = delegate;
        _contentView.layer.cornerRadius = 9.f;
        _contentView.layer.borderWidth = 2.5f;
        _contentView.layer.borderColor = [BaseColor CGColor];
        [self addSubview:self.contentView];
        [self.mMessageLb setText:message];
        [self moveOkInCenter:message];
        if ([[[BaseViewController alloc] init] isDarkMode]) {
            [self.contentView setBackgroundColor:GrayBarColor];
        } else {
            [self.contentView setBackgroundColor:[UIColor whiteColor]];
        }
        [self setupView];
    }
    return self;
}

-(void)setupView {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mAlertLb setText:NSLocalizedString(@"alert", nil)];
        [self.mCancelBtn setTitle:NSLocalizedString(@"no", nil) forState:UIControlStateNormal];
        [self.mYesBtn setTitle:NSLocalizedString(@"yes", nil) forState:UIControlStateNormal];
        if ([self.mMessageLb.text isEqualToString:NSLocalizedString(@"dev_are_conn", nil)]) {
               [self.mCancelBtn setTitle:NSLocalizedString(@"yes", nil) forState:UIControlStateNormal];
           }
    });
}

-(void)moveOkInCenter:(NSString *) message {
    if ([self.mMessageLb.text isEqualToString:NSLocalizedString(@"dev_are_conn", nil)]) {
        _mOkbtncenterLayout.constant = 35;
        _mYesBtn.hidden = YES;
    } else {
       _mOkbtncenterLayout.constant = -50;
        _mYesBtn.hidden = NO;
    }
}

-(void)deletAlert {
    for(UIView *subView in self.superview.subviews) {
          [subView setUserInteractionEnabled:YES];
          [subView setAlpha:1.0];
          if([subView isKindOfClass:[PopupView class]]){
              [subView removeFromSuperview];
          }
      }
}
- (IBAction)yes:(UIButton *)sender {
    [self deletAlert];
    [self.delegate handlerYes];
}

- (IBAction)cancel:(UIButton *)sender {
    [self deletAlert];
}
@end
