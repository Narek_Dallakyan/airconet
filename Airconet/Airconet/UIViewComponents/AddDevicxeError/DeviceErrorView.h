//
//  DeviceErrorView.h
//  Airconet
//
//  Created by Karine Karapetyan on 27-02-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol DeviceErrorViewDelegate <NSObject>

@optional
-(void)handlerRetryOk;
-(void)handlerTechSupport;
-(void)handlerBack;



@end

@interface DeviceErrorView : UIView
@property(weak, nonatomic) id<DeviceErrorViewDelegate> delegate;


@property(assign, nonatomic)BOOL isNetworkErr;

@property (weak, nonatomic) IBOutlet UIScrollView *mScrollV;
@property (weak, nonatomic) IBOutlet UILabel *mErrorStatusLb;
@property (weak, nonatomic) IBOutlet UILabel *mPlTryStepsLb;
@property (weak, nonatomic) IBOutlet UILabel *mFirstLb;
@property (weak, nonatomic) IBOutlet UILabel *mSecondLb;
@property (weak, nonatomic) IBOutlet UILabel *mThirdLb;
@property (weak, nonatomic) IBOutlet UILabel *mFourthLb;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *mTechSupport;
@property (weak, nonatomic) IBOutlet UILabel *mDevErrorLb;
@property (weak, nonatomic) IBOutlet UILabel *mIfDoesntWorkLb;
@property (weak, nonatomic) IBOutlet UIButton *mBackBtn;

@property (weak, nonatomic) IBOutlet UIButton *mRetryBt;
@property (weak, nonatomic) IBOutlet UIButton *mOkBtn;
@property (weak, nonatomic) IBOutlet UIView *mDeviceErrorBackgroundV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mScrollContenierHeight;
@property (weak, nonatomic) IBOutlet UIView *mScrollContentV;
@property (weak, nonatomic) IBOutlet UIView *mNavigationV;
- (IBAction)back:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mNavigationBottomLayout;


- (id)initWithFrame:(CGRect)frame delegate:(nonnull id<DeviceErrorViewDelegate>)delegate;
@end

NS_ASSUME_NONNULL_END
