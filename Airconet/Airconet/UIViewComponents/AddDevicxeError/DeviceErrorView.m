//
//  DeviceErrorView.m
//  Airconet
//
//  Created by Karine Karapetyan on 27-02-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "DeviceErrorView.h"
#import "BaseViewController.h"

@implementation DeviceErrorView


//
//-(void)viewDidAppear:(BOOL)animated  {
//
//       self.mScrollV.contentOffset = CGPointMake(0, 0);
//    self.mScrollV.scrollEnabled = YES;
//    self.userInteractionEnabled = YES;
//    self.mScrollContenierHeight.constant = self.mScrollV.frame.size.height;
//
//}
//


-(void)didMoveToSuperview {
//    UIButton * buttons = [self.mTechSupport objectAtIndex:0];
    self.mScrollV.contentSize = CGSizeMake( [[UIScreen mainScreen] bounds].size.width, 500);
}


- (id)initWithFrame:(CGRect)frame delegate:(nonnull id<DeviceErrorViewDelegate>)delegate
{
    self = [super initWithFrame:frame];
//        self.mScrollV.contentOffset = CGPointMake(0, 0);
//        self.mScrollV.scrollEnabled = YES;
//        self.userInteractionEnabled = YES;
//        self.mScrollContenierHeight.constant = self.mScrollV.frame.size.height;

    if (self) {
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed: @"DeviceErrorView" owner: self options: nil];
        self.contentView = [nibViews objectAtIndex:0];
        self.contentView.frame = self.bounds;
        self.contentView.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
           UIViewAutoresizingFlexibleHeight);
        self.delegate = delegate;
        [self addSubview:self.contentView];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setupView];
        });
    }
    return self;
}


-(void)setupView {
    
    if (_isNetworkErr) {
        self.mNavigationV.hidden = NO;
        self.mDeviceErrorBackgroundV.hidden = YES;
        [self.mErrorStatusLb setText: NSLocalizedString(@"net_err", nil)];
        [self.mPlTryStepsLb setText: NSLocalizedString(@"try_steps", nil)];
        [self.mFirstLb setText:NSLocalizedString(@"reset_router", nil)];
        [self.mSecondLb setText: NSLocalizedString(@"press_reset_btn", nil)];
        [self.mThirdLb setText: NSLocalizedString(@"router_2.4", nil)];
        [self.mFourthLb setText: NSLocalizedString(@"try_again", nil)];
        [self.mIfDoesntWorkLb setText: NSLocalizedString(@"contact_us", nil)];
        [self.mRetryBt setTitle: NSLocalizedString(@"retry", nil) forState:UIControlStateNormal];
        for (UIButton *btn in self.mTechSupport) {
            [btn setTitle: NSLocalizedString(@"tech_supp", nil) forState:UIControlStateNormal];

        }

        if ([[[BaseViewController alloc] init] isDarkMode]) {
            [self.mBackBtn setImage:Image(@"back") forState:UIControlStateNormal];
        } else
            [self.mBackBtn setImage:Image(@"ic_left_dark") forState:UIControlStateNormal];
        self.mNavigationBottomLayout.constant = 0.f;

    } else {
        self.mNavigationV.hidden = YES;
        self.mDeviceErrorBackgroundV.hidden = NO;
        self.mErrorStatusLb.text = NSLocalizedString(@"dev_err!", nil);
        self.mDevErrorLb.text = NSLocalizedString(@"dev_exist_in_sys", nil);
        self.mNavigationBottomLayout.constant = 88.f;
    }
}
- (IBAction)retryOrOK:(UIButton *)sender {
    [self.delegate handlerRetryOk];
}

- (IBAction)techSupport:(UIButton *)sender {
    [self.delegate handlerTechSupport];
}

- (IBAction)back:(UIButton *)sender {
    [self.delegate handlerBack];
}
@end
