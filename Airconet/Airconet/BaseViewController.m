//
//  BaseViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/23/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"
#import "MainViewController.h"
#import "ACommonTools.h"

NSMutableArray * deviceMacs;
NSMutableArray * hiddenDevicesArr;
NSMutableArray * mainDevicesArr;
NSString* ipAddrDataExt;
@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}
-(void)setNavigationStyle:(UIColor *)barTintColor
                tintColor:(UIColor *)tintColor
                titleText:(NSString *)titleText
               titleColor:(UIColor *)titleColor {
    [self.navigationController.navigationBar setBarTintColor:barTintColor];
     [self.navigationController.navigationBar setTintColor:tintColor];
    [self.navigationController.navigationBar setTranslucent:NO];
    if (titleText.length > 0) {
            [self.navigationController.navigationBar setTitleTextAttributes:@{
            NSForegroundColorAttributeName: titleColor,
            NSFontAttributeName: [UIFont fontWithName:@"Calibri" size:21]
        }];
        self.navigationController.topViewController.title = titleText;
    }
}

-(UILabel *)setNavigationTitle:(NSString *)txt {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 480, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 2;
    if (SCREEN_Width < 370 && ([txt isEqualToString:@"switch_setup"] || [txt isEqualToString:@"aircon_setup"] ||
                               [txt isEqualToString:@"sensor_setup"] || [txt isEqualToString:@"thermostat_setup"] ||
                               [txt isEqualToString:@"pm_setup"])) {
        label.font = [UIFont fontWithName:@"Calibri" size: 18.0f];
        if ([txt isEqualToString:@"aircon_setup"]) {
            label.font = [UIFont fontWithName:@"Calibri" size: 14.0f];
        }
    } else {
        label.font = [UIFont fontWithName:@"Calibri" size: 21.0f];
    }
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = NavTitleColor;
    label.text =  NSLocalizedString(txt, nil);
    return label;
}

-(void)presentViewControllerWithIdentifier:(NSString *)storyboardIdentifier controller:(UIViewController *)viewCont {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController * vc = [storyboard   instantiateViewControllerWithIdentifier:storyboardIdentifier] ;
    [viewCont presentViewController:vc animated:YES completion:nil];
}

-(void)pushViewControllerWithIdentifier:(NSString *)storyboardIdentifier controller:(UIViewController *)viewCont {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController * vc = [storyboard   instantiateViewControllerWithIdentifier:storyboardIdentifier];
    [viewCont.navigationController pushViewController:vc animated:YES];
}
-(void)goToMainPage {

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"MainPageViewController"]]];
    MainViewController *mainViewController =  [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];;
    mainViewController.rootViewController = navigationController;
    NSUInteger val = 0;
       [mainViewController setupWithType:val];
    UIWindow *window = [BaseViewController keyWindow];
       window.rootViewController = mainViewController;

       [UIView transitionWithView:window
                         duration:0.3
                          options:UIViewAnimationOptionTransitionCrossDissolve
                       animations:nil
                       completion:nil];
}

+(UIWindow*)keyWindow {
    UIWindow        *foundWindow = nil;
    NSArray         *windows = [[UIApplication sharedApplication]windows];
    for (UIWindow   *window in windows) {
        if (window.isKeyWindow) {
            foundWindow = window;
            break;
        }
    }
    return foundWindow;
}


-(void)showAlert:(NSString *)message
              ok:(NSString*)title
          cencel:(NSString *)cencel
        delegate:(id<CustomAlertDelegate>)delegate  {

    CustomAlertView * customAlert = [[CustomAlertView alloc] initWithFrame:CGRectMake(0, 0, 200, 150) message:message delegate:delegate];
    if (title.length > 0) {
        customAlert.mOkBtn.titleLabel.text = title;
        if (cencel.length == 0) {
            customAlert.mCancelBtnWidth.constant = - (2 *customAlert.mCancelBtn.frame.size.width);
          }
    } else {
        customAlert.mOkBtnHeight.constant = 0.f;
        customAlert.mMessageLb.textColor = BaseColor;
        customAlert.mOkBtn.hidden = YES;
        customAlert.mCancelBtn.hidden = YES;
    }
    customAlert.frame = CGRectMake(SCREEN_Width/2 - customAlert.frame.size.width/2, SCREEN_Height/2 - customAlert.frame.size.height/2 - 64, customAlert.frame.size.width, customAlert.frame.size.height);
    customAlert.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [self.view addSubview:customAlert];
    for (UIView *view in self.view.subviews) {
        view.userInteractionEnabled=NO;
        if (![view isKindOfClass:[CustomAlertView class]]) {
            [view setAlpha:0.5];
        }
    }
    customAlert.userInteractionEnabled=YES;
    [self animationPopup:customAlert];
}

-(void)popupAlert:(NSString *)message
            frame:(CGRect)frame
              ok:(NSString*)title
          cencel:(NSString *)cencel
        delegate:(id<PopupViewDelegate>)delegate  {

    PopupView * customAlert = [[PopupView alloc] initWithFrame:frame message:message delegate:delegate];
    customAlert.mYesBtn.titleLabel.text = title;
    customAlert.frame = CGRectMake(SCREEN_Width/2 - customAlert.frame.size.width/2, SCREEN_Height/2 - customAlert.frame.size.height/2 - 64, customAlert.frame.size.width, [self requiredHeight:message lbWidth:200]+125);
    customAlert.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [self.view addSubview:customAlert];
    for (UIView *view in self.view.subviews) {
        view.userInteractionEnabled=NO;
        if (![view isKindOfClass:[PopupView class]]) {
            [view setAlpha:0.5];
        }
    }
    customAlert.userInteractionEnabled=YES;
    [self animationPopup:customAlert];
}

-(void)popupRemoteControllAlert:(CGRect)frame
                             ok:(NSString*)title
                       delegate:(id<PopupRemotControllDelegate>)delegate  {
    
    PopupRemotControllView * customAlert = [[PopupRemotControllView alloc] initWithFrame:frame delegate:delegate];
    customAlert.mOkBtn.titleLabel.text = title;
    [customAlert.mInfoFirstLb setText:NSLocalizedString(@"press_any_button", nil)];
    [customAlert.mInfoSecondLb setText:NSLocalizedString(@"mount_device", nil)];
   customAlert.frame = CGRectMake(SCREEN_Width/2 - customAlert.frame.size.width/2, SCREEN_Height/2 - customAlert.frame.size.height/2 - 64, 280, 230 +[self getHeight]);
    customAlert.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [self.view addSubview:customAlert];
    for (UIView *view in self.view.subviews) {
        view.userInteractionEnabled=NO;
        if (![view isKindOfClass:[PopupRemotControllView class]]) {
            [view setAlpha:0.5];
        }
    }
    customAlert.userInteractionEnabled=YES;
    [self animationPopup:customAlert];
}

-(CGFloat)getHeight {
    NSString * language = [[NSLocale preferredLanguages] firstObject];
    if ([language containsString:@"ru"]) {
        return 60;
    } else if ([language containsString:@"es"]) {
         return 20;
    } else  if ([language containsString:@"pl"]) {
        return 0;
    }
    return -15;
}
-(void)animationPopup:(UIView *)view {
    [UIView animateWithDuration:1.0 delay:0.2 usingSpringWithDamping:0.6f
             initialSpringVelocity:1.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
           view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
       } completion:^(BOOL finished) {
           view.transform = CGAffineTransformIdentity;
       }];
}

- (CGFloat)requiredHeight:(NSString*)labelText lbWidth:(CGFloat)lbWidth {

    UIFont *font = [UIFont fontWithName:@"Calibri" size:18.0];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, lbWidth, CGFLOAT_MAX)];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.font = font;
    label.text = labelText;
    [label sizeToFit];
    return label.frame.size.height ;

}

-(BOOL)isDarkMode {
    if ([[ACommonTools getObjectValueForKey:@"BackgroundMode"] isEqual:@"light"]) {
        return NO;
    } else if ([[ACommonTools getObjectValueForKey:@"BackgroundMode"] isEqual:@"dark"]) {
        return YES;
    } else {
        NSString * mode = [self getCurrentBackgroundMode];
        if([mode isEqualToString:@"light"]) {
            return NO;
        } else if ([mode isEqualToString:@"dark"]) {
            return YES;
        }
    }
    return NO;
}

+(NSInteger )getModeValue:(int)index  {
    switch (index) {
        case 0://auto
            return 51;
        case 1://cold
            return 17;
        case 2://dry
            return 85;
        case 3://hot
            return 34;
        case 4://fan
            return 68;
        default:
            break;
    }
    return 0;
}


+(NSString *)getModeName:(NSNumber *)value {
    switch ([value intValue]) {
        case 11:
        case 17:
            return NSLocalizedString(@"cold", nil);
        case 22:
        case 34:
            return NSLocalizedString(@"hot", nil);
        case 33:
        case 51:
            return NSLocalizedString(@"auto", nil);
        case 44:
        case 68:
            return NSLocalizedString(@"fan", nil);
        case 55:
        case 85:
            return NSLocalizedString(@"dry", nil);
    }
    return @"";
}

+(NSMutableAttributedString *) getAttributStringPossition:(NSString *)attribut text:(NSString *)text color:(UIColor *)color {
    if (text.length > 0) {
        NSRange range = [text rangeOfString:attribut];
           if(range.location != NSNotFound) {
               
               NSMutableAttributedString * attText = [[NSMutableAttributedString alloc] initWithString:text];
               [attText addAttribute: NSForegroundColorAttributeName value: color range: NSMakeRange(range.location, text.length - range.location)];
               return attText;
           }
    }
    return nil;
}

+(void)setGradient:(UIView *)view  {
    UIColor *borderColor = [UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1.0];
    UIColor *centerColor = [UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0];
    // Create the gradient
    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
    theViewGradient.colors = [NSArray arrayWithObjects: (id)borderColor.CGColor, (id)centerColor.CGColor,(id)borderColor.CGColor, nil];
    theViewGradient.frame = CGRectMake(0, 0, SCREEN_Width, view.frame.size.height);
    theViewGradient.startPoint = CGPointMake(0.0, 0.5);
    theViewGradient.endPoint = CGPointMake(1.0, 0.5);
    //Add gradient to view
    [view.layer insertSublayer:theViewGradient atIndex:0];
}

-(NSString *)getCurrentBackgroundMode {
    NSDate *now = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    timeFormatter.dateFormat = @"HH:mm";
    [timeFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSDateFormatter *timeFormatter2 = [[NSDateFormatter alloc] init];
    timeFormatter2.dateFormat = @"yyyy-MM-dd";
    [timeFormatter2 setTimeZone:[NSTimeZone systemTimeZone]];
    NSString * currDate = [timeFormatter2 stringFromDate:now];
    //NSLog(@"The Current Time is %@",[timeFormatter2 stringFromDate:now]);
    
    NSDateFormatter *timeFormatter3 = [[NSDateFormatter alloc] init];
    [timeFormatter3 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *timeStart = [timeFormatter3 dateFromString:[NSString stringWithFormat:@"%@ 06:00:00",currDate]];
    NSLog(@"timeStart = %@\n", timeStart);
    
    NSDate *timeEnd = [timeFormatter3 dateFromString:[NSString stringWithFormat:@"%@ 18:00:00",currDate]];
    NSLog(@"timeEnd = %@\n", timeEnd);
    NSComparisonResult result1 = [now compare:timeStart];
    NSComparisonResult result2 = [now compare:timeEnd];

      if((result1 == NSOrderedDescending && result2 == NSOrderedAscending) || result1 == NSOrderedSame) {
         // NSLog(@"6:00 < now < 18:00 ");
          return @"light";
      } else {
          //NSLog(@"18:00 < now < 6:00 ");
          return @"dark";
      }
    return @"";
}

-(void)setBackground:(UIViewController *)vc {
    if ([[ACommonTools getObjectValueForKey:@"BackgroundMode"] isEqual:@"light"]) {
        if (@available(iOS 13.0, *)) {
            vc.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
        }
    } else if ([[ACommonTools getObjectValueForKey:@"BackgroundMode"] isEqual:@"dark"]) {
        if (@available(iOS 13.0, *)) {
            vc.overrideUserInterfaceStyle = UIUserInterfaceStyleDark;
        }
    } else {
        NSString * mode = [self getCurrentBackgroundMode];
        if([mode isEqualToString:@"light"]) {
            if (@available(iOS 13.0, *)) {
                vc.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
            }
        } else if ([mode isEqualToString:@"dark"]) {
            if (@available(iOS 13.0, *)) {
                vc.overrideUserInterfaceStyle = UIUserInterfaceStyleDark;
            }
        }
    }
}

-(void)setBackgroundAuto:(UIViewController *)vc {
    NSString * mode = [self getCurrentBackgroundMode];
    if([mode isEqualToString:@"light"]) {
        if (@available(iOS 13.0, *)) {
            vc.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
        }
    } else if ([mode isEqualToString:@"dark"]) {
        if (@available(iOS 13.0, *)) {
            vc.overrideUserInterfaceStyle = UIUserInterfaceStyleDark;
        }
    }
}

-(BOOL)isHiddenDevice:(NSString *)mac {
    for (NSString * hiddenMac in hiddenDevicesArr) {
        if ([hiddenMac isEqualToString:mac]) {
            return YES;
        }
    }
    return NO;
}

-(BOOL)isHiddenGroupsDevice:(NSArray *)devicemacs {
    for (NSString * hiddenMac in hiddenDevicesArr) {
        
        for (NSString * groupMac in devicemacs) {
            if ([hiddenMac isEqualToString:groupMac]) {
                return YES;
            }
        }
    }
    
    return NO;
}

+(NSInteger)indexOfMode:(UIImage *)img {
    for (int i= 0; i < DevisesModeImgList.count; i ++) {
        if ([UIImagePNGRepresentation([DevisesModeImgList objectAtIndex:i]) isEqualToData:UIImagePNGRepresentation(img)]) {
            return i;
        }
    }
    return 0;
}

+(NSDate *)convertDateFromString:(NSString *)str {
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [timeFormatter dateFromString:str];
    return  date;
}
+(NSString *)convertStringFromDate:(NSDate *)date {
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString * str = [timeFormatter stringFromDate:date];
    return  str;
}

#pragma mark: NAVIGATION BAR
+(NSString *)getDateMode:(NSString *)dateModeForRequest {
    if ([dateModeForRequest isEqualToString:@"DAY"]) {
       return NSLocalizedString(@"day", nil);
    } else if ([dateModeForRequest isEqualToString:@"WEEK"]) {
       return NSLocalizedString(@"week", nil);
    } else if ([dateModeForRequest isEqualToString:@"MONTH"]) {
        return NSLocalizedString(@"month", nil);
    } else if ([dateModeForRequest isEqualToString:@"YEAR"]) {
        return NSLocalizedString(@"year", nil);
    } else if ([dateModeForRequest isEqualToString:@"REALTIME"]) {
        return NSLocalizedString(@"online", nil);
    }
   return @"";
}
+(NSString *)getPmMode:(NSString *)pmModeTypeForRequest {
    if ([pmModeTypeForRequest isEqualToString:@"COST"]) {
        return NSLocalizedString(@"$", nil);
     } else if ([pmModeTypeForRequest isEqualToString:@"KWH"]) {
        return NSLocalizedString(@"kWh", nil);
     } else if ([pmModeTypeForRequest isEqualToString:@"HOURS"]) {
         return NSLocalizedString(@"hour", nil);
     }
    return @"";
}
+(NSString *)graphType:(NSString *)type {
    if ([type isEqualToString:NSLocalizedString(@"day", nil)]) {
        return NSLocalizedString(@"hour", nil);
    } else if ([type isEqualToString:NSLocalizedString(@"week", nil)] || [type isEqualToString:NSLocalizedString(@"month", nil)]) {
        return NSLocalizedString(@"day", nil);
    } else if ([type isEqualToString:NSLocalizedString(@"year", nil)]) {
        return NSLocalizedString(@"month", nil);
    }
    return @"";
}

+(UIColor *) setDropDownColor:(UIViewController *)vc {
    if (@available(iOS 13.0, *)) {
        if (vc.overrideUserInterfaceStyle == UIUserInterfaceStyleDark) {
            return GrayBarColor;
        } else {
            return [UIColor whiteColor];
        }
    } else {
        return [UIColor whiteColor];
    }
}

+(NSString *)getTimerPram:(NSString *) timerParam {
    if ([timerParam isEqualToString:@"minute"]) {
        return NSLocalizedString(@"minut", nil);
    } else if ([timerParam isEqualToString:@"hour"]) {
        return NSLocalizedString(@"h", nil);
    }  else if ([timerParam isEqualToString:@"day"]) {
        return NSLocalizedString(@"day_sort", nil);
    }  else if ([timerParam isEqualToString:@"week"]) {
        return NSLocalizedString(@"week_sort", nil);
    }  else if ([timerParam isEqualToString:@"month"]) {
        return NSLocalizedString(@"month_sort", nil);
    }
    return @"";
}

+ (BOOL)isDeviceLanguageRTL {
   return [NSLocale characterDirectionForLanguage:[[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode]]==NSLocaleLanguageDirectionRightToLeft;
}
@end
