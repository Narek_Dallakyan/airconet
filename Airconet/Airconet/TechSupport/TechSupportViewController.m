//
//  TechSupportViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/30/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "TechSupportViewController.h"
#import "CustomAlertView.h"
#import "RequestManager.h"

@interface TechSupportViewController ()<CustomAlertDelegate>
@property (weak, nonatomic) IBOutlet UILabel *mPlsWriteLb;
@property (weak, nonatomic) IBOutlet UITextView *mMessageTetxV;
@property (weak, nonatomic) IBOutlet UIButton *mSendBtn;
@end
int wordCount;

@implementation TechSupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self setBackground:self];
    [self setupView];
    self.navigationItem.hidesBackButton = NO;

}

-(void)setupView {
    self.mMessageTetxV.layer.cornerRadius = 10.f;
    self.mMessageTetxV.layer.borderWidth = 3.f;
    self.mMessageTetxV.layer.borderColor = [BaseColor CGColor];
    self.mMessageTetxV.textContainerInset = UIEdgeInsetsMake(20, 35, 0, 35);
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationItem.hidesBackButton = NO;
    [self setNavigationStyle];
    self.navigationController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(backToMain)];
    self.navigationController.navigationBar.topItem.title = @"";
}
-(void)setNavigationStyle {
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self setNavigationStyle:BaseColor tintColor:[UIColor whiteColor] titleText:NSLocalizedString(@"tech_supp", nil) titleColor:NavTitleColor];
      for (UIView *view in self.navigationController.navigationBar.subviews) {
            if ([view isKindOfClass:[DevicesNavigationBar class]]) {
                [view removeFromSuperview];
            }
        }
}
-(void)backToMain {
    [self goToMainPage];
}

-(void)deletAlert {
    for(UIView *subView in self.view.subviews) {
          [subView setUserInteractionEnabled:YES];
          [subView setAlpha:1.0];
          if([subView isKindOfClass:[CustomAlertView class]]){
              [subView removeFromSuperview];
          }
      }
}

-(void)delayAlert {
    double delayInSeconds = 2.0;
       dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
       dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
         [self deletAlert];
           [self goToMainPage];
           
       });
}
- (IBAction)send:(UIButton *)sender {
    if (self.mMessageTetxV.text.length > 0) {
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostcallme andParameters:@{@"content":self.mMessageTetxV.text}  success:^(id response) {
           
        [self showAlert:NSLocalizedString(@"send_success", nil)
                     ok:@"" cencel:@"" delegate:self];
        [self delayAlert];
           
       } failure:^(NSError *error) {
           [self showAlert:NSLocalizedString(@"failed_to_send", nil)
                               ok:@"" cencel:@"" delegate:self];
                  [self delayAlert];
       }];
    } else {
        [self showAlert:NSLocalizedString(@"fill_field", nil)
                                      ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
    }
    
}
- (IBAction)tapGesture:(UITapGestureRecognizer *)sender {
    [self.mMessageTetxV resignFirstResponder];
}

#pragma mark: UITextView delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@" "]) {
        wordCount++;
    }
    if (wordCount >= 100) {
        return NO;
    }
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }

    return YES;
}

#pragma mark : Custom Alert View delegate
-(void)handelOk {
    
}

@end
