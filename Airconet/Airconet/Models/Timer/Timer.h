//
//  Timer.h
//  Airconet
//
//  Created by Karine Karapetyan on 14-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceVisualComp.h"

NS_ASSUME_NONNULL_BEGIN

@interface Timer : NSObject

@property (assign, nonatomic) NSInteger tId;
@property (assign, nonatomic) NSInteger onOff;
@property (assign, nonatomic) NSInteger days;
@property (strong, nonatomic) NSString * startTime;
@property (assign, nonatomic) NSInteger temperature;
@property (assign, nonatomic) NSInteger mode;
@property (assign, nonatomic) NSInteger fan;
@property (assign, nonatomic) BOOL enabled;
@property (strong, nonatomic) NSMutableArray * daysArr;
@property (strong, nonatomic) NSMutableArray * airConDevices;

@property (strong, nonatomic) DeviceVisualComp * deviceVisualComp;


-(Timer *)getTimerObj:(NSDictionary *)dic;
-(NSDictionary *)defaultTimer;
+(NSDictionary *)getDictionaryByObj:(Timer *)timer;
+(DeviceVisualComp *)setDeviceVisualCompanent:(Timer *) timer;
@end


/*"id": 0,
   "onOff": 0,
   "days": 0,
   "startTime": "",
   "temperature": 0,
   "mode": 0,
   "fan": 0,
   "airConDevices": {},
 
 
 
 {
         airConDevices =         (
                         {
                 airQuality = "<null>";
                 airQualityLastConnection = "<null>";
                 airSensor = 0;
                 baseWatt = 500;
                 connectionAirQuality = 0;
                 damageDateTime = "<null>";
                 deviceType = AC;
                 id = 11;
                 index = 1;
                 inverter = 0;
                 lastOperation = "<null>";
                 location = "<null>";
                 mac = 840D8E85AB2D;
                 manufacturer = "";
                 maxPoint = 32;
                 mfqWatt = 0;
                 minPoint = 16;
                 model = "Air Conditioner";
                 motionSensorObj = "<null>";
                 name = "test3 reconnect";
                 pir = 0;
                 powerMeter =                 {
                     id = 102;
                     mac = 84F3EB69A82F;
                     masterEnabled = 0;
                     name = "power meter PM";
                     ph1Details =                     {
                         ampere = "12.68";
                         frequency = 50;
                         id = 182;
                         kva = "3.25";
                         kwh = 5555;
                         lastUpdateDate = 1583863627000;
                         pf = "0.82";
                         volt = 225;
                     };
                     ph1Watt = 5678;
                     ph2Details =                     {
                         ampere = "12.68";
                         frequency = 50;
                         id = 183;
                         kva = "3.25";
                         kwh = 5555;
                         lastUpdateDate = 1583863627000;
                         pf = "0.82";
                         volt = 225;
                     };
                     ph2Watt = 5678;
                     ph3Details =                     {
                         ampere = "12.68";
                         frequency = 50;
                         id = 184;
                         kva = "3.25";
                         kwh = 6688;
                         lastUpdateDate = 1583863627000;
                         pf = "0.82";
                         volt = 225;
                     };
                     ph3Watt = 1250;
                     publicIp = "<null>";
                     totalDetails =                     {
                         ampere = "12.68";
                         frequency = 50;
                         id = 190;
                         kva = "66.66";
                         kwh = 7777;
                         lastUpdateDate = 1583863627000;
                         pf = "0.82";
                         volt = 195;
                     };
                     totalWatt = 8888;
                     type = 3PH;
                 };
                 powerMeterPhaseNumber = 0;
                 privateIp = "192.168.1.20";
                 publicIp = "171.96.189.154";
                 realtimeStatus = "<null>";
                 registDateTime = 1580291290000;
                 screenSize = "<null>";
                 seriesNumber = 222;
                 state = 1;
                 sumhours = 0;
                 timeZone = "Asia/Bangkok";
                 type = GR;
                 warrantyExpireDate = "<null>";
                 warrantyYear = 0;
             }
         );
         days =         (
             1,
             2,
             4,
             8,
             16,
             32,
             64
         );
         enabled = 1;
         fan = 1;
         id = 1095;
         mode = 11;
         onOff = 85;
         startTime = "09:00";
         temperature = 25;
     }
 )

 
 
 
 */
NS_ASSUME_NONNULL_END
