//
//  Timer.m
//  Airconet
//
//  Created by Karine Karapetyan on 14-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "Timer.h"
#import "APublicDefine.h"

@implementation Timer

-(NSDictionary *)defaultTimer {
    return  @{ @"id": @(0),
               @"onOff": @(85),
               @"days": @[@1,
                          @2,
                          @4,
                          @8,
                          @16,
                          @32,
                          @64],
               @"startTime": @"12:00",
               @"temperature": @(25),
               @"mode": @(17),
               @"fan": @(2),
               @"airConDevices": @[]};
}

-(Timer *)getTimerObj:(NSDictionary *)dic {
    
    Timer * timer = [[Timer alloc] init];
    timer.tId = [[dic valueForKey:@"id"] integerValue];
    timer.onOff = [[dic valueForKey:@"onOff"] integerValue];
    timer.startTime = [dic valueForKey:@"startTime"];
    timer.temperature = [[dic valueForKey:@"temperature"] integerValue];
    timer.mode = [[dic valueForKey:@"mode"] integerValue];
    timer.fan = [[dic valueForKey:@"fan"] integerValue];
    timer.enabled = [[dic valueForKey:@"enabled"] boolValue];
    timer.daysArr = [[dic valueForKey:@"days"] mutableCopy];
    timer.airConDevices = [[dic valueForKey:@"airConDevices"] mutableCopy];
    timer.deviceVisualComp = [[DeviceVisualComp alloc] init];
    timer.deviceVisualComp = [Timer setDeviceVisualCompanent:timer];
     for (NSNumber *num in timer.daysArr) {
            timer.days += [num intValue];
          }
    if (!timer.daysArr) {
        timer.daysArr = [NSMutableArray array];
    }
    if (!timer.airConDevices) {
           timer.airConDevices = [NSMutableArray array];
       }
    return timer;
}
+(NSDictionary *)getDictionaryByObj:(Timer *)timer {
  //  @"enabled": [NSNumber numberWithBool:timer.enabled],

       return  @{ @"id": @(timer.tId),
                   @"onOff": @(timer.onOff),
                   @"days": @(timer.days),
                   @"startTime": timer.startTime,
                   @"temperature": @(timer.temperature),
                   @"mode": @(timer.mode),
                   @"fan": @(timer.fan),
                   @"airConDevices": timer.airConDevices};
}

+(DeviceVisualComp *)setDeviceVisualCompanent:(Timer *) timer {
    DeviceVisualComp * deviceVisual = [[DeviceVisualComp alloc] init];
    switch (timer.mode) {
           case 11:
           case 17:
               // cold mode icon
               deviceVisual.modeColor = ColdColor;
               deviceVisual.fanImage = [UIImage imageNamed:@"blue_fan"];
              // deviceVisual.turnOnOffImg = [UIImage imageNamed:@"cold_mode"];
               deviceVisual.modeImage = [UIImage imageNamed:@"cold"];
            deviceVisual.modeBigImage  = [UIImage imageNamed:@"cold_big"];

               break;
           case 22:
           case 34:
               // hot mode icon
               deviceVisual.modeColor = HotColor;
               deviceVisual.fanImage = [UIImage imageNamed:@"red_fan_up"];
              // deviceVisual.turnOnOffImg = [UIImage imageNamed:@"heat_mode"];
               deviceVisual.modeImage  = [UIImage imageNamed:@"hot"];
            deviceVisual.modeBigImage  = [UIImage imageNamed:@"hot_big"];

               break;
           case 33:
           case 51:
               // auto mode icon
               deviceVisual.modeColor = AutoColor;
               deviceVisual.fanImage = [UIImage imageNamed:@"purple_fan_up"];
               //deviceVisual.turnOnOffImg = [UIImage imageNamed:@"auto_mode"];
               deviceVisual.modeImage  = [UIImage imageNamed:@"auto_A"];
               deviceVisual.modeBigImage  = [UIImage imageNamed:@"auto_big"];
               break;
           case 44:
           case 68:
               // fan mode icon
               deviceVisual.modeColor = FanColor;
               deviceVisual.fanImage = [UIImage imageNamed:@"green_fan_up"];
               //deviceVisual.turnOnOffImg = [UIImage imageNamed:@"fan_mode"];
                deviceVisual.modeImage  = [UIImage imageNamed:@"green_fan_up"];
            deviceVisual.modeBigImage  = [UIImage imageNamed:@"green_fan_up"];

               break;
           case 55:
           case 85:
               // dry mode icon
               deviceVisual.modeColor = DryColor;
               deviceVisual.fanImage = [UIImage imageNamed:@"yellow_fan_up"];
            //deviceVisual.turnOnOffImg = [UIImage imageNamed:@"dry_mode"];
               deviceVisual.modeImage  = [UIImage imageNamed:@"dry"];
            deviceVisual.modeBigImage  = [UIImage imageNamed:@"dry_big"];

               break;
       }
    return deviceVisual;
}

@end
