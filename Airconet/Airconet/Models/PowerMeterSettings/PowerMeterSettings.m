//
//  PowerMeterSettings.m
//  Airconet
//
//  Created by Karine Karapetyan on 06-05-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "PowerMeterSettings.h"

@implementation PowerMeterSettings


 /* (  {
         baseBill =         {
             day = 16;
             id = 3;
             month = 6;
             monthYear = 2019;
             price = 30;
             user =             {
                 accountExpired = 0;
                 accountLocked = 0;
                 activatedEnabled = 1;
                 address = "EKON Street";
                 company = "<null>";
                 contact = "L3 EKON updated";
                 credentialsExpired = 0;
                 deviceCountry =                 {
                     id = 3;
                     name = Italy;
                 };
                 email = "developer96dev@outlook.com";
                 enabled = 1;
                 firstName = Mr;
                 fullName = "Mr Ekon";
                 id = 67;
                 lastCallTime = 1587734688000;
                 lastErrorTime = 1588816602000;
                 lastName = Ekon;
                 level = 3;
                 linked = 0;
                 numOfAirCons = 0;
                 numOfpmCons = 0;
                 numOfswCons = 0;
                 originalPassword = 1234567;
                 parentId = 70;
                 passwordHint = "<null>";
                 payFlag = 1;
                 phone = 0989962862;
                 registerTime = 1584944092000;
                 roles =                 (
                                         {
                         description = "Single User Role";
                         id = 4;
                         name = "ROLE_USER";
                     }
                 );
                 userImg = "<null>";
                 userType = SINGLE;
                 username = "Mr Ekon";
                 utcDate = "2020-03-23T06:14:51.511841Z";
                 version = 134;
                 website = "L3 EKON ";
             };
         };
         daysLength = 30;
     },
         {
         baseBill =         {
             day = 1;
             id = 4;
             month = 7;
             monthYear = 2019;
             price = 5;
             user =             {
                 accountExpired = 0;
                 accountLocked = 0;
                 activatedEnabled = 1;
                 address = "EKON Street";
                 company = "<null>";
                 contact = "L3 EKON updated";
                 credentialsExpired = 0;
                 deviceCountry =                 {
                     id = 3;
                     name = Italy;
                 };
                 email = "developer96dev@outlook.com";
                 enabled = 1;
                 firstName = Mr;
                 fullName = "Mr Ekon";
                 id = 67;
                 lastCallTime = 1587734688000;
                 lastErrorTime = 1588816602000;
                 lastName = Ekon;
                 level = 3;
                 linked = 0;
                 numOfAirCons = 0;
                 numOfpmCons = 0;
                 numOfswCons = 0;
                 originalPassword = 1234567;
                 parentId = 70;
                 passwordHint = "<null>";
                 payFlag = 1;
                 phone = 0989962862;
                 registerTime = 1584944092000;
                 roles =                 (
                                         {
                         description = "Single User Role";
                         id = 4;
                         name = "ROLE_USER";
                     }
                 );
                 userImg = "<null>";
                 userType = SINGLE;
                 username = "Mr Ekon";
                 utcDate = "2020-03-23T06:14:51.511841Z";
                 version = 134;
                 website = "L3 EKON ";
             };
         };
         daysLength = 31;
     },
         {
         baseBill =         {
             day = 1;
             id = 5;
             month = 8;
             monthYear = 2019;
             price = 6;
             user =             {
                 accountExpired = 0;
                 accountLocked = 0;
                 activatedEnabled = 1;
                 address = "EKON Street";
                 company = "<null>";
                 contact = "L3 EKON updated";
                 credentialsExpired = 0;
                 deviceCountry =                 {
                     id = 3;
                     name = Italy;
                 };
                 email = "developer96dev@outlook.com";
                 enabled = 1;
                 firstName = Mr;
                 fullName = "Mr Ekon";
                 id = 67;
                 lastCallTime = 1587734688000;
                 lastErrorTime = 1588816602000;
                 lastName = Ekon;
                 level = 3;
                 linked = 0;
                 numOfAirCons = 0;
                 numOfpmCons = 0;
                 numOfswCons = 0;
                 originalPassword = 1234567;
                 parentId = 70;
                 passwordHint = "<null>";
                 payFlag = 1;
                 phone = 0989962862;
                 registerTime = 1584944092000;
                 roles =                 (
                                         {
                         description = "Single User Role";
                         id = 4;
                         name = "ROLE_USER";
                     }
                 );
                 userImg = "<null>";
                 userType = SINGLE;
                 username = "Mr Ekon";
                 utcDate = "2020-03-23T06:14:51.511841Z";
                 version = 134;
                 website = "L3 EKON ";
             };
         };
         daysLength = 31;
     },
         {
         baseBill =         {
             day = 1;
             id = 6;
             month = 9;
             monthYear = 2019;
             price = 7;
             user =             {
                 accountExpired = 0;
                 accountLocked = 0;
                 activatedEnabled = 1;
                 address = "EKON Street";
                 company = "<null>";
                 contact = "L3 EKON updated";
                 credentialsExpired = 0;
                 deviceCountry =                 {
                     id = 3;
                     name = Italy;
                 };
                 email = "developer96dev@outlook.com";
                 enabled = 1;
                 firstName = Mr;
                 fullName = "Mr Ekon";
                 id = 67;
                 lastCallTime = 1587734688000;
                 lastErrorTime = 1588816602000;
                 lastName = Ekon;
                 level = 3;
                 linked = 0;
                 numOfAirCons = 0;
                 numOfpmCons = 0;
                 numOfswCons = 0;
                 originalPassword = 1234567;
                 parentId = 70;
                 passwordHint = "<null>";
                 payFlag = 1;
                 phone = 0989962862;
                 registerTime = 1584944092000;
                 roles =                 (
                                         {
                         description = "Single User Role";
                         id = 4;
                         name = "ROLE_USER";
                     }
                 );
                 userImg = "<null>";
                 userType = SINGLE;
                 username = "Mr Ekon";
                 utcDate = "2020-03-23T06:14:51.511841Z";
                 version = 134;
                 website = "L3 EKON ";
             };
         };
         daysLength = 30;
     },
         {
         baseBill =         {
             day = 1;
             id = 7;
             month = 10;
             monthYear = 2019;
             price = 8;
             user =             {
                 accountExpired = 0;
                 accountLocked = 0;
                 activatedEnabled = 1;
                 address = "EKON Street";
                 company = "<null>";
                 contact = "L3 EKON updated";
                 credentialsExpired = 0;
                 deviceCountry =                 {
                     id = 3;
                     name = Italy;
                 };
                 email = "developer96dev@outlook.com";
                 enabled = 1;
                 firstName = Mr;
                 fullName = "Mr Ekon";
                 id = 67;
                 lastCallTime = 1587734688000;
                 lastErrorTime = 1588816602000;
                 lastName = Ekon;
                 level = 3;
                 linked = 0;
                 numOfAirCons = 0;
                 numOfpmCons = 0;
                 numOfswCons = 0;
                 originalPassword = 1234567;
                 parentId = 70;
                 passwordHint = "<null>";
                 payFlag = 1;
                 phone = 0989962862;
                 registerTime = 1584944092000;
                 roles =                 (
                                         {
                         description = "Single User Role";
                         id = 4;
                         name = "ROLE_USER";
                     }
                 );
                 userImg = "<null>";
                 userType = SINGLE;
                 username = "Mr Ekon";
                 utcDate = "2020-03-23T06:14:51.511841Z";
                 version = 134;
                 website = "L3 EKON ";
             };
         };
         daysLength = 31;
     },
         {
         baseBill =         {
             day = 1;
             id = 8;
             month = 11;
             monthYear = 2019;
             price = 9;
             user =             {
                 accountExpired = 0;
                 accountLocked = 0;
                 activatedEnabled = 1;
                 address = "EKON Street";
                 company = "<null>";
                 contact = "L3 EKON updated";
                 credentialsExpired = 0;
                 deviceCountry =                 {
                     id = 3;
                     name = Italy;
                 };
                 email = "developer96dev@outlook.com";
                 enabled = 1;
                 firstName = Mr;
                 fullName = "Mr Ekon";
                 id = 67;
                 lastCallTime = 1587734688000;
                 lastErrorTime = 1588816602000;
                 lastName = Ekon;
                 level = 3;
                 linked = 0;
                 numOfAirCons = 0;
                 numOfpmCons = 0;
                 numOfswCons = 0;
                 originalPassword = 1234567;
                 parentId = 70;
                 passwordHint = "<null>";
                 payFlag = 1;
                 phone = 0989962862;
                 registerTime = 1584944092000;
                 roles =                 (
                                         {
                         description = "Single User Role";
                         id = 4;
                         name = "ROLE_USER";
                     }
                 );
                 userImg = "<null>";
                 userType = SINGLE;
                 username = "Mr Ekon";
                 utcDate = "2020-03-23T06:14:51.511841Z";
                 version = 134;
                 website = "L3 EKON ";
             };
         };
         daysLength = 30;
     },
         {
         baseBill =         {
             day = 1;
             id = 9;
             month = 12;
             monthYear = 2019;
             price = 10;
             user =             {
                 accountExpired = 0;
                 accountLocked = 0;
                 activatedEnabled = 1;
                 address = "EKON Street";
                 company = "<null>";
                 contact = "L3 EKON updated";
                 credentialsExpired = 0;
                 deviceCountry =                 {
                     id = 3;
                     name = Italy;
                 };
                 email = "developer96dev@outlook.com";
                 enabled = 1;
                 firstName = Mr;
                 fullName = "Mr Ekon";
                 id = 67;
                 lastCallTime = 1587734688000;
                 lastErrorTime = 1588816602000;
                 lastName = Ekon;
                 level = 3;
                 linked = 0;
                 numOfAirCons = 0;
                 numOfpmCons = 0;
                 numOfswCons = 0;
                 originalPassword = 1234567;
                 parentId = 70;
                 passwordHint = "<null>";
                 payFlag = 1;
                 phone = 0989962862;
                 registerTime = 1584944092000;
                 roles =                 (
                                         {
                         description = "Single User Role";
                         id = 4;
                         name = "ROLE_USER";
                     }
                 );
                 userImg = "<null>";
                 userType = SINGLE;
                 username = "Mr Ekon";
                 utcDate = "2020-03-23T06:14:51.511841Z";
                 version = 134;
                 website = "L3 EKON ";
             };
         };
         daysLength = 31;
     },
         {
         baseBill =         {
             day = 1;
             id = 10;
             month = 1;
             monthYear = 2020;
             price = 10;
             user =             {
                 accountExpired = 0;
                 accountLocked = 0;
                 activatedEnabled = 1;
                 address = "EKON Street";
                 company = "<null>";
                 contact = "L3 EKON updated";
                 credentialsExpired = 0;
                 deviceCountry =                 {
                     id = 3;
                     name = Italy;
                 };
                 email = "developer96dev@outlook.com";
                 enabled = 1;
                 firstName = Mr;
                 fullName = "Mr Ekon";
                 id = 67;
                 lastCallTime = 1587734688000;
                 lastErrorTime = 1588816602000;
                 lastName = Ekon;
                 level = 3;
                 linked = 0;
                 numOfAirCons = 0;
                 numOfpmCons = 0;
                 numOfswCons = 0;
                 originalPassword = 1234567;
                 parentId = 70;
                 passwordHint = "<null>";
                 payFlag = 1;
                 phone = 0989962862;
                 registerTime = 1584944092000;
                 roles =                 (
                                         {
                         description = "Single User Role";
                         id = 4;
                         name = "ROLE_USER";
                     }
                 );
                 userImg = "<null>";
                 userType = SINGLE;
                 username = "Mr Ekon";
                 utcDate = "2020-03-23T06:14:51.511841Z";
                 version = 134;
                 website = "L3 EKON ";
             };
         };
         daysLength = 31;
     },
         {
         baseBill =         {
             day = 1;
             id = 11;
             month = 2;
             monthYear = 2020;
             price = 10;
             user =             {
                 accountExpired = 0;
                 accountLocked = 0;
                 activatedEnabled = 1;
                 address = "EKON Street";
                 company = "<null>";
                 contact = "L3 EKON updated";
                 credentialsExpired = 0;
                 deviceCountry =                 {
                     id = 3;
                     name = Italy;
                 };
                 email = "developer96dev@outlook.com";
                 enabled = 1;
                 firstName = Mr;
                 fullName = "Mr Ekon";
                 id = 67;
                 lastCallTime = 1587734688000;
                 lastErrorTime = 1588816602000;
                 lastName = Ekon;
                 level = 3;
                 linked = 0;
                 numOfAirCons = 0;
                 numOfpmCons = 0;
                 numOfswCons = 0;
                 originalPassword = 1234567;
                 parentId = 70;
                 passwordHint = "<null>";
                 payFlag = 1;
                 phone = 0989962862;
                 registerTime = 1584944092000;
                 roles =                 (
                                         {
                         description = "Single User Role";
                         id = 4;
                         name = "ROLE_USER";
                     }
                 );
                 userImg = "<null>";
                 userType = SINGLE;
                 username = "Mr Ekon";
                 utcDate = "2020-03-23T06:14:51.511841Z";
                 version = 134;
                 website = "L3 EKON ";
             };
         };
         daysLength = 29;
     },
         {
         baseBill =         {
             day = 1;
             id = 12;
             month = 3;
             monthYear = 2020;
             price = 10;
             user =             {
                 accountExpired = 0;
                 accountLocked = 0;
                 activatedEnabled = 1;
                 address = "EKON Street";
                 company = "<null>";
                 contact = "L3 EKON updated";
                 credentialsExpired = 0;
                 deviceCountry =                 {
                     id = 3;
                     name = Italy;
                 };
                 email = "developer96dev@outlook.com";
                 enabled = 1;
                 firstName = Mr;
                 fullName = "Mr Ekon";
                 id = 67;
                 lastCallTime = 1587734688000;
                 lastErrorTime = 1588816602000;
                 lastName = Ekon;
                 level = 3;
                 linked = 0;
                 numOfAirCons = 0;
                 numOfpmCons = 0;
                 numOfswCons = 0;
                 originalPassword = 1234567;
                 parentId = 70;
                 passwordHint = "<null>";
                 payFlag = 1;
                 phone = 0989962862;
                 registerTime = 1584944092000;
                 roles =                 (
                                         {
                         description = "Single User Role";
                         id = 4;
                         name = "ROLE_USER";
                     }
                 );
                 userImg = "<null>";
                 userType = SINGLE;
                 username = "Mr Ekon";
                 utcDate = "2020-03-23T06:14:51.511841Z";
                 version = 134;
                 website = "L3 EKON ";
             };
         };
         daysLength = 31;
     },
         {
         baseBill =         {
             day = 1;
             id = 1;
             month = 4;
             monthYear = 2020;
             price = 10;
             user =             {
                 accountExpired = 0;
                 accountLocked = 0;
                 activatedEnabled = 1;
                 address = "EKON Street";
                 company = "<null>";
                 contact = "L3 EKON updated";
                 credentialsExpired = 0;
                 deviceCountry =                 {
                     id = 3;
                     name = Italy;
                 };
                 email = "developer96dev@outlook.com";
                 enabled = 1;
                 firstName = Mr;
                 fullName = "Mr Ekon";
                 id = 67;
                 lastCallTime = 1587734688000;
                 lastErrorTime = 1588816602000;
                 lastName = Ekon;
                 level = 3;
                 linked = 0;
                 numOfAirCons = 0;
                 numOfpmCons = 0;
                 numOfswCons = 0;
                 originalPassword = 1234567;
                 parentId = 70;
                 passwordHint = "<null>";
                 payFlag = 1;
                 phone = 0989962862;
                 registerTime = 1584944092000;
                 roles =                 (
                                         {
                         description = "Single User Role";
                         id = 4;
                         name = "ROLE_USER";
                     }
                 );
                 userImg = "<null>";
                 userType = SINGLE;
                 username = "Mr Ekon";
                 utcDate = "2020-03-23T06:14:51.511841Z";
                 version = 134;
                 website = "L3 EKON ";
             };
         };
         daysLength = 30;
     },
         {
         baseBill =         {
             day = 6;
             id = 2;
             month = 5;
             monthYear = 2020;
             price = 30;
             user =             {
                 accountExpired = 0;
                 accountLocked = 0;
                 activatedEnabled = 1;
                 address = "EKON Street";
                 company = "<null>";
                 contact = "L3 EKON updated";
                 credentialsExpired = 0;
                 deviceCountry =                 {
                     id = 3;
                     name = Italy;
                 };
                 email = "developer96dev@outlook.com";
                 enabled = 1;
                 firstName = Mr;
                 fullName = "Mr Ekon";
                 id = 67;
                 lastCallTime = 1587734688000;
                 lastErrorTime = 1588816602000;
                 lastName = Ekon;
                 level = 3;
                 linked = 0;
                 numOfAirCons = 0;
                 numOfpmCons = 0;
                 numOfswCons = 0;
                 originalPassword = 1234567;
                 parentId = 70;
                 passwordHint = "<null>";
                 payFlag = 1;
                 phone = 0989962862;
                 registerTime = 1584944092000;
                 roles =                 (
                                         {
                         description = "Single User Role";
                         id = 4;
                         name = "ROLE_USER";
                     }
                 );
                 userImg = "<null>";
                 userType = SINGLE;
                 username = "Mr Ekon";
                 utcDate = "2020-03-23T06:14:51.511841Z";
                 version = 134;
                 website = "L3 EKON ";
             };
         };
         daysLength = 31;
     }
 )*/
@end
