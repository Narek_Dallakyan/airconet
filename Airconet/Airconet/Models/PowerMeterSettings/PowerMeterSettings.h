//
//  PowerMeterSettings.h
//  Airconet
//
//  Created by Karine Karapetyan on 06-05-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PowerMeterSettings : NSObject
@property (assign, nonatomic) BOOL masterPm;
@property (assign, nonatomic) BOOL masterPmOffline;
@property (strong, nonatomic) NSNumber * countryId;
@property (strong, nonatomic) NSString * countryName;
@property (strong, nonatomic) NSNumber * sqm;
@property (strong, nonatomic) NSNumber * whId;
@property (strong, nonatomic) NSNumber * maxPower;
@property (strong, nonatomic) NSNumber * volt;
@property (strong, nonatomic) NSMutableDictionary * workingHours;
@property (strong, nonatomic) NSMutableArray * countryArr;
@property (strong, nonatomic) NSMutableArray * baseElectricBillArr;

@end

NS_ASSUME_NONNULL_END
