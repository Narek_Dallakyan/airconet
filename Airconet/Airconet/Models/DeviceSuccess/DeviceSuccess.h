//
//  DeviceSuccess.h
//  Airconet
//
//  Created by Karine Karapetyan on 09-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Conditioner : NSObject
@property (assign, nonatomic) int count;
@property (assign, nonatomic) BOOL showRemoteControll;
@property (assign, nonatomic) BOOL modelTypes;
@property (assign, nonatomic) BOOL vrfAddress;

@end
@interface Switch : NSObject
@property (assign, nonatomic) int count;

@end
@interface PowerMeter : NSObject
@property (assign, nonatomic) int count;
@property (strong, nonatomic) NSString* faz;

@end
@interface DeviceSuccess : NSObject
@property (strong, nonatomic) Conditioner * conditioner;
@property (strong, nonatomic) Switch * swit;
@property (strong, nonatomic) PowerMeter * powerMeter;


@end

NS_ASSUME_NONNULL_END
