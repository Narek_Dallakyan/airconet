//
//  NavigationBar.m
//  Airconet
//
//  Created by Karine Karapetyan on 20-04-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "NavigationBar.h"
#import "APublicDefine.h"

@implementation NavigationBar

-(NavigationBar *)getNavigationBarObj:(NSDictionary *)dic
                                 dateMode:(NSString *)dateMode
                           isMasterPm:(BOOL) ismasterPm {
    NavigationBar * navBar = [[NavigationBar alloc] init];
    navBar.dateMode = dateMode;
    navBar.isMasterPm = ismasterPm;
    if (!dic) {
        navBar.pmMode = @(0);
        navBar.errorNumber = @(0);
    } else {
        navBar.pmMode = [dic valueForKey:@"pmMode"];
        navBar.errorNumber = [dic valueForKey:@"errorNumber"];
    }
    return navBar;
}

+(UIImage *)leafImg:(NSNumber *)value {
    switch ([value integerValue]) {
        case 1://0,176,80
            return [UIImage imageNamed:@"leaf1"];
        case 2://143,210,67
            return [UIImage imageNamed:@"leaf2"];
        case 3://166,210,137
            return [UIImage imageNamed:@"leaf3"];
        case 4://251,155,0
            return [UIImage imageNamed:@"leaf4"];
        case 5://.253.195, 0
            return [UIImage imageNamed:@"leaf5"];
        case 6://254,17,0
            return [UIImage imageNamed:@"leaf6"];
        default://191.9.0
            return [UIImage imageNamed:@"leaf7"];
    }
}
+(UIColor *)leafColor:(NSNumber *)value {
    switch ([value integerValue]) {
        case 1:
            return RGB(0,176,80);
        case 2:
            return RGB(143,210,67);
        case 3:
            return RGB(166,210,137);
        case 4:
            return RGB(251,155,0);
        case 5:
            return RGB(253, 195, 0);
        case 6:
            return RGB(254,17,0);
        default:
            return RGB(191,9,0);
    }
}
@end
