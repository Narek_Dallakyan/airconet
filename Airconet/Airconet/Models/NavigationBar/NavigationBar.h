//
//  NavigationBar.h
//  Airconet
//
//  Created by Karine Karapetyan on 20-04-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NavigationBar : NSObject
@property (strong, nonatomic) NSString * dateModeForRequest;
@property (strong, nonatomic) NSString * dateMode;
@property (strong, nonatomic) NSString * pmModeTypeForRequest;
@property (strong, nonatomic) NSString * pmModeType;
@property (strong, nonatomic) NSString * masterPmMac;
@property (strong, nonatomic) NSString * innerPmMac;

@property (strong, nonatomic) NSNumber * pmMode;
@property (strong, nonatomic) NSNumber * errorNumber;
@property (assign, nonatomic) BOOL isMasterPm;
@property (assign, nonatomic) BOOL isLinkedPm;
@property (assign, nonatomic) BOOL isLinkedSensorPm;


@property (strong, nonatomic) UIColor * leafColor;
@property (assign, nonatomic) UIImage * leafImg;
@property (strong, nonatomic) NSNumber * maxGraphValue;

+(UIColor *)leafColor:(NSNumber *)value;
+(UIImage *)leafImg:(NSNumber *)value;
@end

NS_ASSUME_NONNULL_END
