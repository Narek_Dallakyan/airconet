//
//  Sensor.h
//  Airconet
//
//  Created by Karine Karapetyan on 04-05-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Sensor : NSObject
@property (strong, nonatomic) NSString * mac;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * publicIp;
@property (strong, nonatomic) NSString * privateIp;
@property (strong, nonatomic) NSString * type;
@property (strong, nonatomic) NSString * model;
@property (strong, nonatomic) NSString * manufacturer;
@property (strong, nonatomic) NSString * seriesNumber;
@property (strong, nonatomic) NSString * deviceType;
@property (assign, nonatomic) NSInteger  order;
@property (strong, nonatomic) NSNumber * dId;
@property (strong, nonatomic) NSString * timeZone;
@property (strong, nonatomic) NSNumber * maxPoint;
@property (strong, nonatomic) NSNumber * minPoint;
@property (strong, nonatomic) NSNumber * baseWatt;
@property (strong, nonatomic) NSNumber * index;


-(Sensor *)getSensorObj:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
