//
//  Sensor.m
//  Airconet
//
//  Created by Karine Karapetyan on 04-05-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "Sensor.h"

@implementation Sensor

-(Sensor *)getSensorObj:(NSDictionary *)dic {
    
    Sensor * dev = [[Sensor alloc] init];
    dev.mac = [dic valueForKey:@"mac"];
    dev.name = [dic valueForKey:@"name"];
    dev.publicIp = [dic valueForKey:@"publicIp"];
    dev.privateIp = [dic valueForKey:@"privateIp"] ;
    dev.model = [dic valueForKey:@"model"];
    dev.manufacturer = [dic valueForKey:@"manufacturer"];
    dev.dId = [dic valueForKey:@"id"];
    dev.timeZone = [dic valueForKey:@"timeZone"];
    dev.minPoint = [dic valueForKey:@"minPoint"];
    dev.maxPoint = [dic valueForKey:@"maxPoint"];
    dev.baseWatt = [dic valueForKey:@"baseWatt"];
    dev.index = [dic valueForKey:@"index"];

    if ([[[dic valueForKey:@"type"]class] isEqual:[NSNull class]]) {
         dev.type = @"" ;
    } else {
      dev.type = [dic valueForKey:@"type"] ;
    }
    if ([[[dic valueForKey:@"deviceType"]class] isEqual:[NSNull class]]) {
         dev.deviceType = @"" ;
    } else {
      dev.deviceType = [dic valueForKey:@"deviceType"] ;
    }
    return dev;
}
@end
