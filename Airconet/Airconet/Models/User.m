//
//  User.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/29/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "User.h"

@implementation User

+(id) shared {
    static User *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
      sharedManager = [[self alloc] init];
    });
    return sharedManager;
}
@end
