//
//  User.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/29/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface User : NSObject
    @property (strong, nonatomic) NSString * email;
    @property (strong, nonatomic) NSString * password;
@end

NS_ASSUME_NONNULL_END
