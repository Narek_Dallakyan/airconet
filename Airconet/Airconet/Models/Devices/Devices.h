//
//  Devices.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/12/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeviceSetup.h"

NS_ASSUME_NONNULL_BEGIN

@interface Devices : NSObject
@property (strong, nonatomic) NSString * mac;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * publicIp;
@property (strong, nonatomic) NSString * privateIp;
@property (strong, nonatomic) NSString * type;
@property (strong, nonatomic) NSString * model;
@property (strong, nonatomic) NSString * manufacturer;
@property (strong, nonatomic) NSString * seriesNumber;
@property (strong, nonatomic) NSString * deviceType;
@property (assign, nonatomic) NSInteger  order;
@property (strong, nonatomic) NSNumber * dId;
@property (strong, nonatomic) NSString * timeZone;
@property (strong, nonatomic) NSNumber * maxPoint;
@property (strong, nonatomic) NSNumber * minPoint;
@property (strong, nonatomic) NSNumber * baseWatt;
@property (strong, nonatomic) NSNumber * index;
@property (assign, nonatomic) BOOL masterPm;
@property (assign, nonatomic) BOOL pir;




@property (assign, nonatomic) BOOL isCheckDevice;
@property (assign, nonatomic) BOOL isWarning;
@property (strong, nonatomic) DevicePM * powerMeter;




-(Devices *)getDeviceObj:(NSDictionary *)dic;
+(NSString *)getDeviceType:(NSString *)type;
+(NSString *)getDeviceModel:(NSString *)model;
+(NSDictionary *)getDeviceJson:(Devices *)device;
+(NSDictionary *)getDeviceJson:(NSString *)mac
                     privateIp:(NSString *)privateIp
                         index:(NSNumber *)index;
@end

NS_ASSUME_NONNULL_END
