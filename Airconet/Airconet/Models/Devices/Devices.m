//
//  Devices.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/12/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "Devices.h"
#import "APublicDefine.h"

@implementation Devices



-(Devices *)getDeviceObj:(NSDictionary *)dic {
    
    Devices * dev = [[Devices alloc] init];
    dev.mac = [dic valueForKey:@"mac"];
    dev.name = [dic valueForKey:@"name"];
    dev.publicIp = [dic valueForKey:@"publicIp"];
    dev.privateIp = [dic valueForKey:@"privateIp"] ;
    dev.model = [dic valueForKey:@"model"];
    dev.manufacturer = [dic valueForKey:@"manufacturer"];
    dev.dId = [dic valueForKey:@"id"];
    dev.timeZone = [dic valueForKey:@"timeZone"];
    dev.minPoint = [dic valueForKey:@"minPoint"];
    dev.maxPoint = [dic valueForKey:@"maxPoint"];
    dev.baseWatt = [dic valueForKey:@"baseWatt"];
    dev.pir = [[dic valueForKey:@"pir"] boolValue];
    
    dev.index = [dic valueForKey:@"index"];

    if ([[[dic valueForKey:@"type"]class] isEqual:[NSNull class]]) {
         dev.type = @"" ;
    } else {
      dev.type = [dic valueForKey:@"type"] ;
    }
    if ([[[dic valueForKey:@"deviceType"]class] isEqual:[NSNull class]]) {
         dev.deviceType = @"" ;
    } else {
      dev.deviceType = [dic valueForKey:@"deviceType"] ;
    }
    dev.powerMeter = [self setPm:dic];
    return dev;
}


-(DevicePM *)setPm:(NSDictionary *)dic {
    DevicePM * pm = [[DevicePM alloc] init];
    if ([dic valueForKey:@"powerMeter"] && ![[dic valueForKey:@"powerMeter"] isEqual:[NSNull null] ]) {
        NSDictionary * pmDic = [dic valueForKey:@"powerMeter"];
        pm.mac = [pmDic valueForKey:@"mac"];
        pm.name = [pmDic valueForKey:@"name"];
        pm.pmId = [pmDic valueForKey:@"id"];
        pm.publicIp = [pmDic valueForKey:@"publicIp"];
        pm.type = [pmDic valueForKey:@"type"];
        pm.phaseNumber = [pmDic valueForKey:@"phaseNumber"];
    }
    return pm;
}

+(NSDictionary *)getDeviceJson:(Devices *)device {
    if (!device.publicIp) {
        device.publicIp = @"";
    }
    if (!device.type) {
        device.type = device.deviceType;
    }
    return @{@"name":device.name,
             @"publicIp":device.publicIp,
             @"privateIp":device.privateIp,
             @"mac":device.mac,
             @"type":device.type,
             @"model":device.model,
             @"manufacturer":device.manufacturer,
             @"deviceType":device.deviceType,
             @"id": device.dId,
             @"index": device.index
             
    };
}
+(NSDictionary *)getDeviceJson:(NSString *)mac
                     privateIp:(NSString *)privateIp
                         index:(NSNumber *)index{
    
    return @{@"name":@"",
    @"seriesNumber":@"",
    @"publicIp":@"",
    @"privateIp":privateIp,
    @"mac":mac,
    @"type":@"1",
    @"location":@"1",
    @"state":@0,
    @"realtimeStatus":@"1",
    @"screenSize":@1,
    @"registDateTime":@1575572400000,
    @"damageDateTime":@1577146628000,
    @"warrantyExpireDate":@1356984000000,
    @"lastOperation":@"1",
    @"model":@"",
    @"baseWatt":@15000,
    @"mfqWatt":@1,
    @"inverter":[NSNumber numberWithBool:YES],
    @"airSensor":[NSNumber numberWithBool:YES],
    @"manufacturer":@"",
    @"maxPoint":@32,
    @"minPoint":@16,
    @"motionSensorObj":[[NSNull alloc] init],
    @"powerMeterPhaseNumber":@3,
    @"deviceType":@"AC",
    @"airQualityLastConnection":[[NSNull alloc] init],
    @"connectionAirQuality":[NSNumber numberWithBool:NO],
    @"timeZone":@"Asia/Dubai",
    @"warrantyYear":@2000,
    @"sumhours":@1,
    @"index":@1,
    @"pir":[NSNumber numberWithBool:NO]};
}

+(NSString *)getDeviceType:(NSString *)type {
    //@[@"Air Conditioner", @"AC + Sensor", @"Thermostat",@"Switch", @"Power Meter"]

    if ([type isEqualToString:[DeviceTypeList objectAtIndex:0]] ||
        [type isEqualToString:[DeviceTypeList objectAtIndex:1]] ||
        [type isEqualToString:[DeviceTypeList objectAtIndex:2]]) {
        return @"AC";
    } else if ([type isEqualToString:[DeviceTypeList objectAtIndex:3]]) {
        return @"SW";
    } else if ([type isEqualToString:[DeviceTypeList objectAtIndex:4]]) {
        return @"PM";
    }
    return @"";
}

+(NSString *)getDeviceModel:(NSString *)model {
    //@[@"Air Conditioner", @"AC + Sensor", @"Thermostat",@"Switch", @"Power Meter"]

     if ([model isEqualToString:[DeviceTypeList objectAtIndex:1]]) {
        return @"sensor";
    } else if ([model isEqualToString:[DeviceTypeList objectAtIndex:2]]) {
        return @"thermostat";
    } else if ([model isEqualToString:[DeviceTypeList objectAtIndex:3]]) {
        return @"switch";
    } else if ([model isEqualToString:[DeviceTypeList objectAtIndex:4]]) {
        return @"power meter";
    }
    return @"air conditioner";
}





@end
