//
//  DeviceVisualComp.h
//  Airconet
//
//  Created by Karine Karapetyan on 02-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeviceVisualComp : NSObject

@property (strong, nonatomic) UIColor*  modeColor;
@property (strong, nonatomic) UIImage*  fanImage;
@property (strong, nonatomic) UIImage * turnOnOffImg;
@property (strong, nonatomic) UIImage*  modeImage;
@property (strong, nonatomic) UIImage*  modeBigImage;
@property (assign, nonatomic) NSInteger modeImageIndex;


@property (assign, nonatomic) BOOL isError;
@property (assign, nonatomic) BOOL isWarning;
@property (assign, nonatomic) BOOL isTimer;
@end

NS_ASSUME_NONNULL_END
