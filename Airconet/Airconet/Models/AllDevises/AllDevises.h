//
//  AllDevises.h
//  Airconet
//
//  Created by Karine Karapetyan on 24-02-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DeviceVisualComp.h"
#import "ErrorMessage.h"

NS_ASSUME_NONNULL_BEGIN

@interface AllDevises : NSObject
@property (strong, nonatomic) NSString * deviceType;
@property (assign, nonatomic) NSInteger envTemp;
@property (assign, nonatomic) NSInteger envTempShow;
@property (assign, nonatomic) NSInteger fan;
@property (assign, nonatomic) NSInteger devId;
@property (strong, nonatomic) NSString * mac;
@property (assign, nonatomic) NSInteger mode;
@property (assign, nonatomic) NSInteger onoff;
@property (assign, nonatomic) NSInteger tgtTemp;
@property (strong, nonatomic) NSString * type;
@property (strong, nonatomic) NSString * model;
@property (assign, nonatomic) BOOL light;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSNumber * maxPoint;
@property (strong, nonatomic) NSNumber * minPoint;
@property (strong, nonatomic) NSMutableArray * temperatureList;
@property (strong, nonatomic) NSString * efficiencyRate;
@property (assign, nonatomic) BOOL hasDevicePowerMeter;


@property(strong, nonatomic) DeviceVisualComp * deviceVisual;
@property (strong, nonatomic) ErrorMessage * errorMessage;

+(NSDictionary *) dictionaryWithObject:(AllDevises *) device;
+(NSDictionary *) dictionaryForDeviceUpdate:(AllDevises *) device;
+(NSNumber *)getMode:(long)mode;
-(AllDevises *)getDeviceObj:(NSDictionary *)dic;

@end

NS_ASSUME_NONNULL_END
