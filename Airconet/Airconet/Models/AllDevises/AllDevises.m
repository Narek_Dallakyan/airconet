//
//  AllDevises.m
//  Airconet
//
//  Created by Karine Karapetyan on 24-02-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "AllDevises.h"
#import "APublicDefine.h"
#import "RequestManager.h"
#import "AKeysDefine.h"

@implementation AllDevises

+(NSDictionary *) dictionaryWithObject:(AllDevises *) device {
    NSDictionary * dic = @{@"deviceType" : device.deviceType,
                           @"envTemp" : @(device.envTemp),
                           @"envTempShow" : @(device.envTempShow),
                           @"fan" : @(device.fan),
                           @"id" : @(device.devId),
                           @"mac" : device.mac,
                           @"mode" : @(device.mode),
                           @"onoff" :@(device.onoff),
                           @"tgtTemp" : @(device.tgtTemp),
                           @"type" :device.type,
                           @"light" : @(1)};
    return dic;
}

+(NSDictionary *) dictionaryForDeviceUpdate:(AllDevises *) device {
    if([device.mac isKindOfClass:[NSNull class]]) {
        device.mac = @"";
    }
    NSDictionary * dic = @{@"mac" : device.mac,
                           @"onoff" :@(device.onoff),
                           @"mode" : @(device.mode),
                           @"fan" : @(device.fan),
                           @"envTemp" : @(device.envTemp),
                           @"envTempShow" : @(device.envTempShow),
                           @"tgtTemp" : @(device.tgtTemp)};
    return dic;
}

+(NSNumber *)getMode:(long)mode {
    NSNumber *modeNum = nil;
       if (mode == 11 || mode == 17) {
           modeNum = @0x11;
       } else if (mode == 22 || mode == 34) {
           modeNum = @0x22;
       } else if (mode == 33 || mode == 51) {
           modeNum = @0x33;
       } else if (mode == 44 || mode == 68) {
           modeNum = @0x44;
       } else if (mode == 55 || mode == 85) {
           modeNum = @0x55;
       }
    return modeNum;
}

-(AllDevises *)getDeviceObj:(NSDictionary *)dic {
    
    
    AllDevises * dev = [[AllDevises alloc] init];
    dev.deviceType = [dic valueForKey:@"deviceType"] ? [dic valueForKey:@"deviceType"] : @"";
    dev.fan = [[dic valueForKey:@"fan"] integerValue];
    dev.devId = [[dic valueForKey:@"devId"] integerValue];
    dev.mac = [dic valueForKey:@"mac"];
    dev.mode = [[dic valueForKey:@"mode"] integerValue];
    dev.onoff = [[dic valueForKey:@"onoff"] integerValue];
    dev.tgtTemp = [[dic valueForKey:@"tgtTemp"] integerValue];
    dev.type = [dic valueForKey:@"type"];
    dev.light = [[dic valueForKey:@"light"] boolValue];
    dev.temperatureList = [TemperatureList mutableCopy];
    dev.efficiencyRate = @"";
    dev.deviceVisual = [[DeviceVisualComp alloc] init];
    if ([[[dic valueForKey:@"envTempShow"] class] isEqual:[NSNull class]]) {
        dev.envTempShow = 0;
    } else {
        dev.envTempShow =  [[dic valueForKey:@"envTempShow"] integerValue];
    }
    if ([[[dic valueForKey:@"envTemp"]class] isEqual:[NSNull class]]) {
        dev.envTemp = 0;
    }  else {
           dev.envTemp =  [[dic valueForKey:@"envTemp"] integerValue];
       }
    dev.deviceVisual = [self setDeviceVisualCompanent:dev];
    return dev;
}

-(DeviceVisualComp *)setDeviceVisualCompanent:(AllDevises *) device {
    DeviceVisualComp * deviceVisual = [[DeviceVisualComp alloc] init];
    switch (device.mode) {
           case 11:
           case 17:
               // cold mode icon
               deviceVisual.modeColor = ColdColor;
               deviceVisual.fanImage = [UIImage imageNamed:@"blue_fan"];
               deviceVisual.turnOnOffImg = [UIImage imageNamed:@"cold_mode"];
               deviceVisual.modeImage = [UIImage imageNamed:@"cold"];
            deviceVisual.modeBigImage  = [UIImage imageNamed:@"cold_big"];
            deviceVisual.modeImageIndex = 0;
               break;
           case 22:
           case 34:
               // hot mode icon
               deviceVisual.modeColor = HotColor;
               deviceVisual.fanImage = [UIImage imageNamed:@"red_fan_up"];
               deviceVisual.turnOnOffImg = [UIImage imageNamed:@"heat_mode"];
               deviceVisual.modeImage  = [UIImage imageNamed:@"hot"];
            deviceVisual.modeBigImage  = [UIImage imageNamed:@"hot_big"];
            deviceVisual.modeImageIndex = 1;
               break;
           case 33:
           case 51:
               // auto mode icon
               deviceVisual.modeColor = AutoColor;
               deviceVisual.fanImage = [UIImage imageNamed:@"purple_fan_up"];
               deviceVisual.turnOnOffImg = [UIImage imageNamed:@"auto_mode"];
               deviceVisual.modeImage  = [UIImage imageNamed:@"auto_A"];
               deviceVisual.modeBigImage  = [UIImage imageNamed:@"auto_big"];
                deviceVisual.modeImageIndex = 2;
               break;
           case 44:
           case 68:
               // fan mode icon
               deviceVisual.modeColor = FanColor;
               deviceVisual.fanImage = [UIImage imageNamed:@"green_fan_up"];
               deviceVisual.turnOnOffImg = [UIImage imageNamed:@"fan_mode"];
                deviceVisual.modeImage  = [UIImage imageNamed:@"green_fan_up"];
            deviceVisual.modeBigImage  = [UIImage imageNamed:@"green_fan_up"];
            deviceVisual.modeImageIndex = 3;

               break;
           case 55:
           case 85:
               // dry mode icon
               deviceVisual.modeColor = DryColor;
               deviceVisual.fanImage = [UIImage imageNamed:@"yellow_fan_up"];
            deviceVisual.turnOnOffImg = [UIImage imageNamed:@"dry_mode"];
               deviceVisual.modeImage  = [UIImage imageNamed:@"dry"];
            deviceVisual.modeBigImage  = [UIImage imageNamed:@"dry_big"];
            deviceVisual.modeImageIndex = 4;
               break;
       }
     if ( device.onoff == 86 || device.onoff == 170 || device.onoff == 85 || device.onoff == 85){ //is off
         deviceVisual.isWarning = NO;
     } else {
         deviceVisual.isWarning = YES;

     }
    deviceVisual.isError = NO;
    deviceVisual.isTimer = NO;
    
    return deviceVisual;
}

@end
