//
//  Group.m
//  Airconet
//
//  Created by Karine Karapetyan on 28-02-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "Group.h"
#import "APublicDefine.h"

@implementation Group


+(NSDictionary *) dictionaryWithObject:(Group *) group {
    NSDictionary * dic = @{@"fan" : @(group.fan),
                           @"id" : @(group.GrId),
                           @"mode" : @(group.mode),
                           @"name" : group.name,
                           @"onoff" :@(group.onoff),
                           @"tgtTemp" : @(group.tgtTemp),
                           @"indexNum": @(group.indexNum) };
    return dic;
}

-(Group *)getGroupObj:(NSDictionary *)dic {
    Group * gr = [[Group alloc] init];
    gr.fan = [[dic valueForKey:@"fan"] integerValue];
    gr.GrId = [[dic valueForKey:@"id"] integerValue];
    gr.indexNum = [[dic valueForKey:@"indexNum"] integerValue];
    gr.mode = [[dic valueForKey:@"mode"] integerValue];
    gr.onoff = [[dic valueForKey:@"onoff"] integerValue];
    gr.tgtTemp = [[dic valueForKey:@"tgtTemp"] integerValue];
    gr.name = [dic valueForKey:@"name" ];
    gr.groupVisual = [[GroupVisualComp alloc] init];
    gr.groupVisual = [self setGroupVisualCompanent:gr];
    return gr;
}
-(GroupVisualComp *)setGroupVisualCompanent:(Group *) group {
    GroupVisualComp * groupVisual = [[GroupVisualComp alloc] init];
    switch (group.mode) {
           case 11:
           case 17:
               // cold mode icon
               groupVisual.modeColor = ColdColor;
               groupVisual.fanImage = [UIImage imageNamed:@"blue_fan"];
               groupVisual.modeImage = [UIImage imageNamed:@"cold"];
            groupVisual.modeBigImage  = [UIImage imageNamed:@"cold_big"];
            groupVisual.modeImageIndex = 0;
               break;
           case 22:
           case 34:
               // hot mode icon
               groupVisual.modeColor = HotColor;
               groupVisual.fanImage = [UIImage imageNamed:@"red_fan_up"];
               groupVisual.modeImage  = [UIImage imageNamed:@"hot"];
            groupVisual.modeBigImage  = [UIImage imageNamed:@"hot_big"];
            groupVisual.modeImageIndex = 1;
               break;
           case 33:
           case 51:
               // auto mode icon
               groupVisual.modeColor = AutoColor;
               groupVisual.fanImage = [UIImage imageNamed:@"purple_fan_up"];
               groupVisual.modeImage  = [UIImage imageNamed:@"auto_A"];
               groupVisual.modeBigImage  = [UIImage imageNamed:@"auto_big"];
            groupVisual.modeImageIndex = 2;
               break;
           case 44:
           case 68:
               // fan mode icon
               groupVisual.modeColor = FanColor;
               groupVisual.fanImage = [UIImage imageNamed:@"green_fan_up"];
                groupVisual.modeImage  = [UIImage imageNamed:@"green_fan_up"];
            groupVisual.modeBigImage  = [UIImage imageNamed:@"green_fan_up"];
            groupVisual.modeImageIndex = 3;
               break;
           case 55:
           case 85:
               // dry mode icon
               groupVisual.modeColor = DryColor;
               groupVisual.fanImage = [UIImage imageNamed:@"yellow_fan_up"];
               groupVisual.modeImage  = [UIImage imageNamed:@"dry"];
               groupVisual.modeBigImage  = [UIImage imageNamed:@"dry_big"];
            groupVisual.modeImageIndex = 4;
               break;
       }
     if ( group.onoff == 86 || group.onoff == 170 || group.onoff == 85 || group.onoff == 55){ //is off
         groupVisual.isWarning = NO;
     } else {
         groupVisual.isWarning = YES;

     }
    return groupVisual;
}
@end
