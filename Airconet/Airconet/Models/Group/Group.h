//
//  Group.h
//  Airconet
//
//  Created by Karine Karapetyan on 28-02-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupVisualComp.h"

NS_ASSUME_NONNULL_BEGIN

@interface Group : NSObject
@property (assign, nonatomic) NSInteger fan;
@property (assign, nonatomic) NSInteger GrId;
@property (assign, nonatomic) NSInteger indexNum;
@property (assign, nonatomic) NSInteger mode;
@property (strong, nonatomic) NSString * name;
@property (assign, nonatomic) NSInteger onoff;
@property (assign, nonatomic) NSInteger tgtTemp;
@property (strong, nonatomic) GroupVisualComp * groupVisual;

//@property (assign, nonatomic) BOOL isTimer;
//@property (strong, nonatomic) UIColor*  modeColor;
//@property (strong, nonatomic) UIImage*  fanImage;
////@property (strong, nonatomic) UIImage * turnOnOffImg;
//@property (strong, nonatomic) UIImage*  modeImage;
////@property (assign, nonatomic) BOOL isError;
//@property (assign, nonatomic) BOOL isWarning;

+(NSDictionary *) dictionaryWithObject:(Group *) device;
-(Group *)getGroupObj:(NSDictionary *)dic;

@end

//{
//attachment =     (
//            {
//        fan = 2;
//        id = 299;
//        indexSum = 1;
//        mode = 51;
//        name = "group 23";
//        onoff = 85;
//        tgtTemp = 32;
//    },

NS_ASSUME_NONNULL_END
