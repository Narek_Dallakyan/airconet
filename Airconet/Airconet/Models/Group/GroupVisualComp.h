//
//  GroupVisualComp.h
//  Airconet
//
//  Created by Karine Karapetyan on 03-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupVisualComp : NSObject

@property (assign, nonatomic) BOOL isTimer;
@property (strong, nonatomic) UIColor*  modeColor;
@property (strong, nonatomic) UIImage*  fanImage;
@property (strong, nonatomic) UIImage*  modeImage;
@property (strong, nonatomic) UIImage*  modeBigImage;
@property (assign, nonatomic) NSInteger modeImageIndex;
@property (assign, nonatomic) BOOL isWarning;

@end

NS_ASSUME_NONNULL_END
