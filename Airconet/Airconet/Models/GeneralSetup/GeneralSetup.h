//
//  GeneralSetup.h
//  Airconet
//
//  Created by Karine Karapetyan on 02-04-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GeneralSetup : NSObject
@property (strong, nonatomic) NSNumber * t1h;
@property (strong, nonatomic) NSNumber * t1l;
@property (assign, nonatomic) BOOL isAdminLock;
@end

NS_ASSUME_NONNULL_END
