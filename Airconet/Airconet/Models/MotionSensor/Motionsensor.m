//
//  Motionsensor.m
//  Airconet
//
//  Created by Karine Karapetyan on 16-05-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "Motionsensor.h"

@implementation AwayConfig
@end

@implementation ArriveConfig
@end

@implementation MotionsensorForDevice
@end

@implementation Motionsensor

-(Motionsensor *)getMotionSensorObj:(NSDictionary *)dic {
    Motionsensor * ms = [[Motionsensor alloc] init];
    ms.enabled = [[dic valueForKey:@"enabled"] boolValue];
    ms.delayAwayCountdown = [dic valueForKey:@"delayAwayCountdown"];
    ms.sleepTimeTo = [dic valueForKey:@"sleepTimeTo"];
    ms.sleepTimeFrom = [dic valueForKey:@"sleepTimeFrom"];
    ms.disabledSleepTime =  [[dic valueForKey:@"disabledSleepTime"] boolValue];
    ms.msId = [dic valueForKey:@"id"];
    return ms;
}

+(NSDictionary *)getSetupConfigParam:(Motionsensor *)ms {
    NSString * enabled = @"false";
    NSString * disabledSleepTime = @"false";
    if (ms.enabled) {
        enabled = @"true";
    }
    if (ms.disabledSleepTime) {
        disabledSleepTime = @"true";
    }
    return @{@"id": ms.msId,
    @"enabled": enabled,
    @"delayAwayCountDown": ms.delayAwayCountdown,
    @"disabledSleepTime": disabledSleepTime,
    @"sleepTimeFrom": ms.sleepTimeFrom,
    @"sleepTimeTo":ms.sleepTimeTo };
}

+(NSDictionary *)getAwayConfigParam:(AwayConfig *)awayConf {
    return @{@"id": awayConf.ayId,
             @"motionSensorCommand" : awayConf.motionSensorCommand,
             @"roomTmpFrom" : awayConf.roomTmpFrom,
             @"roomTmpTo": awayConf.roomTmpTo,
             @"lastingHour": awayConf.lastingHour};
}

+(NSDictionary *)getArriveConfigParam:(ArriveConfig *)ariveConfig {
    return @{@"id": ariveConfig.arrId,
             @"motionSensorCommand": ariveConfig.motionSensorCommand,
             @"tmp": ariveConfig.tmp,
             @"mode": ariveConfig.mode,
             @"fan": ariveConfig.fan};
}

@end
