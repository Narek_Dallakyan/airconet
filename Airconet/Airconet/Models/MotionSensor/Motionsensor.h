//
//  Motionsensor.h
//  Airconet
//
//  Created by Karine Karapetyan on 16-05-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface AwayConfig : NSObject
@property (strong, nonatomic) NSNumber * ayId;
@property (strong, nonatomic) NSNumber * roomTmpFrom;
@property (strong, nonatomic) NSNumber * roomTmpTo;
@property (strong, nonatomic) NSNumber * lastingHour;
@property (strong, nonatomic) NSString * motionSensorCommand;

@end

@interface ArriveConfig : NSObject
@property (strong, nonatomic) NSNumber * arrId;
@property (strong, nonatomic) NSNumber * fan;
@property (strong, nonatomic) NSNumber * mode;
@property (strong, nonatomic) NSNumber * tmp;
@property (strong, nonatomic) NSString * motionSensorCommand;

@end

@interface MotionsensorForDevice : NSObject
@property (strong, nonatomic) NSString * status;
@property (strong, nonatomic) NSString * commandMode;
@property (strong, nonatomic) NSString * workingTime;
@property (strong, nonatomic) NSString * timeParam;
@end

@interface Motionsensor : NSObject

//Setup config
@property (assign, nonatomic) BOOL enabled;
@property (assign, nonatomic) BOOL disabledSleepTime;
@property (strong, nonatomic) NSNumber * msId;
@property (strong, nonatomic) NSNumber * delayAwayCountdown;
@property (strong, nonatomic) NSString * sleepTimeFrom;
@property (strong, nonatomic) NSString * sleepTimeTo;
@property (strong, nonatomic) AwayConfig * awayConfig;
@property (strong, nonatomic) ArriveConfig * arriveConfig;


-(Motionsensor *)getMotionSensorObj:(NSDictionary *)dic;
+(NSDictionary *)getSetupConfigParam:(Motionsensor *)ms;
+(NSDictionary *)getAwayConfigParam:(AwayConfig *)awayConf;
+(NSDictionary *)getArriveConfigParam:(ArriveConfig *)ariveConfig;
@end

NS_ASSUME_NONNULL_END
