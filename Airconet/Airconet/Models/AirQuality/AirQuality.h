//
//  AirQuality.h
//  Airconet
//
//  Created by Karine Karapetyan on 25-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AirQuality : NSObject

@property (strong, nonatomic) NSString * mac;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * typeGas;
@property (strong, nonatomic) NSString * typeCo2;
@property (assign, nonatomic) NSInteger  numberGas;
@property (assign, nonatomic) NSInteger  numberCo2;
@property (assign, nonatomic) BOOL isConnect;

+(UIColor *)getAirColor:(NSInteger)value;
+(UIImage *)getHeartBitImg:(NSInteger)value;
@end

NS_ASSUME_NONNULL_END
