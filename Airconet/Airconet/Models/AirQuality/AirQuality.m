//
//  AirQuality.m
//  Airconet
//
//  Created by Karine Karapetyan on 25-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "AirQuality.h"
#import "APublicDefine.h"

@implementation AirQuality

-(AirQuality *)getAirQualityObj:(NSDictionary *)dic {
    AirQuality * airQuality = [[AirQuality alloc] init];
    airQuality.mac = [dic valueForKey:@"mac"];
    airQuality.name = [dic valueForKey:@"name"];
    airQuality.typeGas = [dic valueForKey:@"typeGas"];
    airQuality.typeCo2 = [dic valueForKey:@"typeCo2"];
    return airQuality;
}

+(UIColor *)getAirColor:(NSInteger)value {
    switch (value ) {
        case 1:
            return RGB(0, 176, 80);
            break;
           case 2:
             return RGB(168, 208, 141);
            break;
            case 3:
             return RGB(225, 225, 0);
            break;
            case 4:
             return RGB(225, 192, 0);
            break;
        default:
             return RGB(225, 0, 0);
            break;
    }
}

+(UIImage *)getHeartBitImg:(NSInteger)value {
    switch (value) {
        case 1:
            return Image(@"hert_bit1");
            break;
           case 2:
             return Image(@"hert_bit2");
            break;
            case 3:
             return Image(@"hert_bit3");
            break;
            case 4:
             return Image(@"hert_bit4");
            break;
        default:
             return Image(@"hert_bit5");
            break;
    }
}
@end
