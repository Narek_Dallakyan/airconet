//
//  PowerMeter.h
//  Airconet
//
//  Created by Karine Karapetyan on 30-04-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBar.h"

NS_ASSUME_NONNULL_BEGIN
@interface Electric : NSObject
@property (nonatomic, strong) NSString * acMac;
@property (nonatomic, assign) CGFloat graphValue;
@property (nonatomic, assign) CGFloat kwHour;
@property (nonatomic, assign) CGFloat priceHr;
@property (nonatomic, assign) CGFloat pf;
@property (nonatomic, strong) UIColor * pfColor;
@property (nonatomic, assign) BOOL isOnline;

+(UIColor *)getPfColor:(CGFloat)pfValue;
@end

@interface EnergyUsagePerDevice : NSObject
@property (nonatomic, strong) NSMutableArray * nameList;
@property (nonatomic, strong) NSMutableArray * valueList;
@end

@interface DevicesProportion : NSObject
@property (nonatomic, strong) NSNumber * pmModeDivideHour;
@property (nonatomic, strong) NSNumber * pmModeDivideHourAndSqm;
@property (nonatomic, strong) NSNumber * pmModeTotal;
@property (nonatomic, strong) NSMutableArray * proportionEnergyGraphArr;

@end

@interface ProportionEnergyGraph : NSObject
@property (nonatomic, strong) NSString * deviceName;
@property (nonatomic, strong) NSNumber * percent;
@end


@interface EfficiencyEnergyRate : NSObject
@property (nonatomic, strong) NSNumber * acErrorNumber;
@property (nonatomic, strong) NSNumber * allErrorNumber;
@end

@interface TotalEnergyUsage : NSObject
@property (nonatomic, strong) NSMutableArray * acValueList;
@property (nonatomic, strong) NSMutableArray * swValueList;
@property (nonatomic, strong) NSMutableArray * othersValueList;
@end

//@interface ElectricBill : NSObject
//@property (nonatomic, strong) NSNumber * month;
//@property (nonatomic, strong) NSNumber * value;
//@end

//acMac = "";
//graphValue = "0.978";
//kwHour = "0.978";
//pf = 0;
//priceHr = "68.45999999999999";

@interface MainPowerMeter : NSObject
@property (nonatomic, strong) NavigationBar * navBar;
@property (nonatomic, strong) Electric * electric;
@property (nonatomic, strong) EnergyUsagePerDevice * energyUsagePerDev;
@property (nonatomic, strong) EfficiencyEnergyRate * efficiencyEnergyRate;
@property (nonatomic, strong) DevicesProportion * devicesProportion;
@property (nonatomic, strong) TotalEnergyUsage * totalEnergyUsage;
@property (nonatomic, strong) NSMutableArray * electricBillArr;
@property (nonatomic, assign) BOOL isThereElectricBill;
@property (nonatomic, strong) NSString * pmMac;

@end

NS_ASSUME_NONNULL_END

/*
{
    efficiencyEnergyRateAppDto =     {
        acErrorNumber = 0;
        allErrorNumber = 1;
    };
    proportionEnergyDto =     {
        pmModeDivideHour = 0;
        pmModeDivideHourAndSqm = "1.6667";
        pmModeTotal = 20;
        proportionEnergyGraphDataDtoList =         (
                        {
                deviceName = bbb;
                percent = 50;
            },
                        {
                deviceName = kkky;
                percent = 50;
            },
                        {
                deviceName = hhhh;
                percent = 0;
            },
                        {
                deviceName = sensor;
                percent = 0;
            },
                        {
                deviceName = "test 33";
                percent = 0;
            },
                        {
                deviceName = Others;
                percent = 0;
            }
        );
    };
}
*/
