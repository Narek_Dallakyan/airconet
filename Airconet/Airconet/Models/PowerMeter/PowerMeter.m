//
//  PowerMeter.m
//  Airconet
//
//  Created by Karine Karapetyan on 30-04-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "PowerMeter.h"
#import "APublicDefine.h"

@implementation Electric

+(UIColor *)getPfColor:(CGFloat)pfValue {
    if (pfValue >= 0 && pfValue <= 0.4) {
        return RGB(193, 0, 4);
    } else if (pfValue >= 0.5 && pfValue <= 0.7) {
        return RGB(255, 252, 57);
    } else if (pfValue >= 0.8 && pfValue <= 1.0) {
        return RGB(85, 129, 58);
    }
    return nil;
}
@end

@implementation EnergyUsagePerDevice
@end

@implementation DevicesProportion
@end

@implementation ProportionEnergyGraph
@end

@implementation EfficiencyEnergyRate
@end

@implementation TotalEnergyUsage
@end

//@implementation ElectricBill
//@end

@implementation MainPowerMeter
@end
