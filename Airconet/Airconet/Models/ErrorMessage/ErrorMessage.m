//
//  ErrorMessage.m
//  Airconet
//
//  Created by Karine Karapetyan on 17-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "ErrorMessage.h"
#import "APublicDefine.h"

@implementation ErrorMessage

-(ErrorMessage *)getErrorMessageObj:(NSDictionary *)dic {
    ErrorMessage * errMessage = [[ErrorMessage alloc] init];
    errMessage.errId = [dic valueForKey:@"id"];
    errMessage.errDescription = [dic valueForKey:@"description"];
    errMessage.descriptionHebrew = [dic valueForKey:@"descriptionHebrew"];
    errMessage.errorCode = [dic valueForKey:@"errorCode"];
    errMessage.errorNumber = [dic valueForKey:@"errorNumber"];
    errMessage.startTime = [dic valueForKey:@"startTime"];
    errMessage.startTimeTimezone = [dic valueForKey:@"startTimeTimezone"];
    errMessage.h250 = [[dic valueForKey:@"h250"] mutableCopy];
    errMessage.turnOffTimes = [[dic valueForKey:@"turnOffTimes"] mutableCopy];
    if ([dic valueForKey:@"ppm"] &&
        ![[dic valueForKey:@"ppm"] isKindOfClass:[NSNull class]] &&
        [[dic valueForKey:@"ppm"] length] > 0) {
        errMessage.ppm = [dic valueForKey:@"ppm"];
    }
    
    if (![[dic valueForKey:@"mac"]  isKindOfClass:[NSNull class]] && [[dic valueForKey:@"mac"] length] > 0)  {
        errMessage.mac = [dic valueForKey:@"mac"];
        errMessage.color = RedColor;
        errMessage.name = [dic valueForKey:@"name"];
    }  else {
        errMessage.color = GreenAlertColor;
        errMessage.name = [dic valueForKey:@"headerUserMessage"];
    }
    errMessage.date = [self getDateString:[dic valueForKey:@"startTime"]];
    return errMessage;
}

-(UIColor *)getErrColor:(NSString*)color {
    if ([color isEqualToString:@"red"]) {
        return RedColor;
    } else if ([color isEqualToString:@"green"]) {
        return GreenAlertColor;
    } else if ([color isEqualToString:@"blue"]) {
        return ColdColor;
    }
    return nil;
}

-(NSString *)getDateString:(NSNumber *)time {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy, HH:mm"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time.doubleValue/1000];
    // Divided by 1000 (i.e. removed three trailing zeros) ^^^^^^^^
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    return formattedDateString;
}

@end

@implementation H250

-(H250 *)getH250Obj:(NSDictionary *)dic {
    H250 * h250 = [[H250 alloc] init];
    h250.hId = [dic valueForKey:@"id"];
    h250.name = [dic valueForKey:@"name"];
    h250.mac = [dic valueForKey:@"mac"];
    h250.sum = [dic valueForKey:@"sum"];
    
    return h250;
}
@end
