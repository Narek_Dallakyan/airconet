//
//  ErrorMessage.h
//  Airconet
//
//  Created by Karine Karapetyan on 17-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface H250 : NSObject

@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * mac;
@property (strong, nonatomic) NSNumber * hId;
@property (strong, nonatomic) NSNumber * sum;

-(H250 *)getH250Obj:(NSDictionary *)dic ;

@end


@interface ErrorMessage : NSObject

@property (strong, nonatomic) NSNumber * badge;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * errDescription;
@property (strong, nonatomic) NSString * descriptionHebrew;
@property (strong, nonatomic) NSString * errorCode;
@property (strong, nonatomic) NSString * errorNumber;
@property (strong, nonatomic) NSString * mac;
@property (strong, nonatomic) NSString * ppm;
@property (strong, nonatomic) NSNumber * startTime;
@property (strong, nonatomic) NSNumber * startTimeTimezone;
@property (strong, nonatomic) NSNumber * errId;
@property (strong, nonatomic) NSMutableArray * h250;
@property (strong, nonatomic) NSMutableArray * turnOffTimes;

@property (strong, nonatomic) NSString * date;
@property (strong, nonatomic)  UIColor * color;

-(ErrorMessage *)getErrorMessageObj:(NSDictionary *)dic;

    /*                    {
                accountName = mo;
                ackTime = "<null>";
                alertType = OPEN;
                appCode = "Hazard HIGH room temperature (2 Out of 3)";
                clearTime = "<null>";
                cleared = 0;
                color = red;
                comment = "<null>";
                content = "<null>";
                deleted = 0;
                description = Unknown;
                descriptionHebrew = "\U05dc\U05d0 \U05d9\U05d3\U05d5\U05e2";
                deviceType = AC;
                errorCode = "Hazard HIGH room temperature (2 Out of 3)";
                errorGroup = "<null>";
                errorNumber = T2H;
                fixTime = "<null>";
                holdTime = "<null>";
                id = 1257;
                mac = 840D8E85AB2D;
                model = "Air Conditioner";
                name = "test3 reconnect";
                phaseNumber = 0;
                phone = 123456;
                received = 0;
                serialNumber = 222;
                severity = 2000;
                startTime = 1584109274000;
                startTimeTimezone = 1584057600000;
                stopTime = "<null>";
                technician = "<null>";
                userId = 20;
            }
        );
        h250 =         (
                        {
                id = 0;
                mac = 840D8E85AB2D;
                name = "test3 reconnect";
                sum = 4944259;
            }
        );
        turnOffTimes =         (
        );
    };
    reason = "";
    result = 0;
}
     */
@end

NS_ASSUME_NONNULL_END
