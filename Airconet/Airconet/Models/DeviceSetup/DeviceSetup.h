//
//  DeviceSetup.h
//  Airconet
//
//  Created by Karine Karapetyan on 26-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface DevicePM : NSObject

@property (strong, nonatomic) NSString * mac;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * pmId;
@property (strong, nonatomic) NSString * publicIp;
@property (strong, nonatomic) NSString * type;
@property (strong, nonatomic) NSNumber * phaseNumber;

@end

@interface DeviceSetup : NSObject

@property (strong, nonatomic) NSString * mac;
@property (strong, nonatomic) NSString * dId;
@property (assign, nonatomic) BOOL keepSetPointInRange;
@property (assign, nonatomic) NSInteger  maxPoint;
@property (assign, nonatomic) NSInteger  minPoint;
@property (assign, nonatomic) NSInteger  workHour;
@property (strong, nonatomic) NSNumber * baseWatt;
@property (strong, nonatomic) DevicePM * powerMeter;

+(NSMutableDictionary *)getSetPointDic:(DeviceSetup *)deviceSetup ;
@end

NS_ASSUME_NONNULL_END
