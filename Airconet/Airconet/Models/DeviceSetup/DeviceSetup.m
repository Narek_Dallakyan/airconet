//
//  DeviceSetup.m
//  Airconet
//
//  Created by Karine Karapetyan on 26-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "DeviceSetup.h"

@implementation DeviceSetup

+(NSMutableDictionary *)getSetPointDic:(DeviceSetup *)deviceSetup {
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    [dic setValue: deviceSetup.mac forKey:@"mac"];
    [dic setValue: @(deviceSetup.minPoint) forKey:@"min"];
    [dic setValue: @(deviceSetup.maxPoint) forKey:@"max"];
    if (deviceSetup.keepSetPointInRange) {
        [dic setValue: @"true" forKey:@"enableSetPoint"];
    } else {
        [dic setValue: @"false" forKey:@"enableSetPoint"];
    }
    
    
    return dic;
}

@end

@implementation DevicePM
 @end
