//
//  QXBaseNetWorkManager.h
//  CommunityShare
//
//  Created by Elvis on 15/7/9.
//  Copyright (c) 2015年 Elvis. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface QXBaseNetWorkManager : AFHTTPSessionManager

+ (instancetype)shareNetWorkManager;

@end
