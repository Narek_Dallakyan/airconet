//
//  QXBaseNetWorkManager.m
//  CommunityShare
//
//  Created by Elvis on 15/7/9.
//  Copyright (c) 2015年 Elvis. All rights reserved.
//

#import "QXBaseNetWorkManager.h"
#import "AVersionConfig.h"

static QXBaseNetWorkManager *_sharedClient = nil;

@implementation QXBaseNetWorkManager

+ (instancetype)shareNetWorkManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //_sharedClient = [[QXBaseNetWorkManager alloc] initWithBaseURL:[NSURL URLWithString:ABaseURLString]];
        
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _sharedClient = [[QXBaseNetWorkManager alloc] initWithSessionConfiguration:sessionConfiguration];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:sessionConfiguration];
        manager = [manager initWithBaseURL:[NSURL URLWithString:ABaseURLString]];
        //_sharedClient = [[QXBaseNetWorkManager alloc] initWithSessionConfiguration:sessionConfiguration];
        _sharedClient = [_sharedClient initWithBaseURL:[NSURL URLWithString:ABaseURLString]];
        
        AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey withPinnedCertificates:[AFSecurityPolicy certificatesInBundle:[NSBundle mainBundle]]];
        _sharedClient.securityPolicy = securityPolicy;

    });
    
    return _sharedClient;
}

@end
