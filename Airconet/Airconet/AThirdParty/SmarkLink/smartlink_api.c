
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
//#include <sysctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>

#include "smartlink_debug.h"
#include "smartlink_api.h"

#ifndef ipaddr_t
typedef union {
    uint32_t    addr;
    uint8_t     data[4];
} ipaddr_t;
#endif

#define MIN(a,b) ((a)<(b)?(a):(b))

#define SL_PROTOL                   (1)
#define MAX_SSID_LEN                (32)
#define MAX_PASSWORD_LEN            (64)

typedef struct {
    char ssid[MAX_SSID_LEN+1];
    char password[MAX_PASSWORD_LEN+1];
} smartlink_info_t;

static pthread_t pid_smartlink;
//static int fd_sock;

static const uint8_t CRC8Table[]={
  0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
  157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
  35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
  190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
  70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
  219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
  101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
  248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
  140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
  17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
  175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
  50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
  202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
  87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
  233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
  116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53
};

unsigned char calc_crc8(unsigned char *p, int len)
{
    unsigned char crc8 = 0;
    for( ; len > 0; len--){
        crc8 = CRC8Table[crc8^*p];
        p++;
    }
    return(crc8);
}

static void packet_en(uint32_t* v, uint32_t n, const uint32_t* k) {
#define MX (z>>5^y<<2) + (y>>3^z<<4)^(sum^y) + (k[p&3^e]^z)

    uint32_t z = v[n - 1], y = v[0], sum = 0, e, DELTA = 0x9e3779b9;
    uint32_t p, q;

	q = 6 + 52 / n;
	while (q-- > 0) {
		sum += DELTA;
		e = (sum >> 2) & 3;
		for (p = 0; p < n - 1; p++)
            (void)(y = v[p + 1]), z = v[p] += MX;
		y = v[0];
		z = v[n - 1] += MX;
	}
}

static int packet_data(const char *ssid, const char *password, uint8_t *out) {
	static const uint32_t _key[4] = { 0x13FE27FD, 0x936726CE, 0xAC5858B0, 0x9A6534CA };
	uint8_t data[128];
	uint8_t checksum;
	int len, pos = 0, i;

	memset(data, 0, sizeof(data));

	// pack ssid
	len = MIN(strlen(ssid), MAX_SSID_LEN);
	data[pos++] = len;
	memcpy(&data[pos], ssid, len);
	pos += len;

	// pack password
	len = MIN(strlen(password), MAX_PASSWORD_LEN);
	data[pos++] = len;
    if(len != 0) memcpy(&data[pos], password, len);
	pos += len;
    LOG_HEX("plain", data, pos);

	// 补足4的整数倍
	pos = ((pos + 3)/4)*4;

	// 加密
	packet_en((uint32_t *)data, pos/4, _key);
    LOG_HEX("encrypt", data, pos);

	// 采用smartlink协议封包
	*out++ = SL_PROTOL;
	*out++ = pos;

	for (i = 0; i < pos; i++) {
		*out++ = data[i];
	}

	checksum = calc_crc8(data, pos);
	*out++ = checksum;

    LOGI("checksum: %d\n", checksum);

	return pos;
}

#if 0
void handle_signal_quit(int arg){
	LOGV("handle signal quit");

    if(-1 != fd_sock){
        close(fd_sock);
        fd_sock = -1;
    }

    pthread_exit((void *)0);
}
#endif

static void *thread_smartlink(void *arg) {
#define PACKDATA(val) payload_len[j++] = ((val)>>4)+1;payload_len[j++] = ((val)&0x0f)+1
	uint8_t data[128];
    struct sockaddr_in peeraddr;
    ipaddr_t ipaddr;
    int i, j, len;
    uint8_t payload[1/*protocol*/+ 1/*length*/+ 128/*maxdata*/+ 1/*checksum*/];
	uint8_t *payload_len = NULL;
	int payload_len_total;
	uint8_t one_packet[3];
    int err = AIRM2M_SMARTLINKJNI_ERR_NONE;
    smartlink_info_t *info = (smartlink_info_t *)arg;
    int fd_sock = -1;
    int so_broadcast = 1;

    LOGI("smartlink thread run");

    memset(&peeraddr, 0, sizeof(peeraddr));
    memset(data, 0, sizeof(data));

    peeraddr.sin_family = AF_INET;
    peeraddr.sin_port = htons(51111); // 随便写个端口

    ipaddr.data[0] = 239;
    len = packet_data(info->ssid, info->password, payload);
    free(info);

    len += (3+1);
    payload_len_total = (len/2)*10;
    payload_len = calloc(1, payload_len_total);
    for(i = 0, j = 0; i < len/2; i++){
    	payload_len[j++] = 0;payload_len[j++] = 0; // sync word
    	PACKDATA(i);
    	PACKDATA(payload[i*2]);
    	PACKDATA(payload[i*2+1]);

    	one_packet[0] = i;
    	one_packet[1] = payload[i*2];
    	one_packet[2] = payload[i*2+1];
    	PACKDATA(calc_crc8(one_packet,3));
    }

    fd_sock = socket(AF_INET, SOCK_DGRAM, 0);

    if(-1 == fd_sock){
        LOGI("create socket error: %s\n", strerror(errno));
        goto _smartlink_thread_exit;
    }

    if (setsockopt(fd_sock, SOL_SOCKET, SO_BROADCAST, &so_broadcast, sizeof(so_broadcast)) < 0) {
        LOGI("set broadcast error: %s\n", strerror(errno));
        return (void *)err;
    }
    
    /*
    int sndbuf_len = 10*1024;
    if (setsockopt(fd_sock, SOL_SOCKET, SO_SNDBUF, &sndbuf_len, sizeof(int)) < 0) {
        LOGI("set send buffer length error: %s\n", strerror(errno));
    }
    */
    
    i = 0;
    while(1){
		for(j = 0; j < payload_len_total; j++) {
			/* 将数据写在239.x1.x2.x3 的组播mac地址中, x1为数据顺序 x2 x3分别为数据内容 */
			ipaddr.data[1] = i;
			ipaddr.data[2] = payload[i*2];
			ipaddr.data[3] = payload[i*2+1];
			peeraddr.sin_addr.s_addr = ipaddr.addr;

			if(sendto(fd_sock, data, payload_len[j], 0, (struct sockaddr *)&peeraddr, sizeof(struct sockaddr_in)) < 0){
				LOGI("send error: %s\n", strerror(errno));
                usleep(2000);
            } else {
                usleep(2000);
            }

			i++;
			i %= len/2;
		}

		//usleep(1000);
    	//sleep(1);
        if(!pid_smartlink) break;
    }

_smartlink_thread_exit:
    LOGI("smartlink thread exit");
    
    if (payload_len) {
        free(payload_len);
    }
    
    if (fd_sock == -1) {
        close(fd_sock);
    }

    return (void *)err;
}

int airm2m_smartlink_open(const char *ssid, const char *password){
    smartlink_info_t *info;
	int result = 0;

	LOGI("smartlink open: %s %s\n", ssid, password);

    if(pid_smartlink && pthread_kill(pid_smartlink, 0) != ESRCH){ // 线程存在 正在进行smartlink配置
        result = AIRM2M_SMARTLINKJNI_ERR_BUSY;
        goto _out;
    }

	if(!ssid) { // ssid为空 参数错误
        result = AIRM2M_SMARTLINKJNI_ERR_PARAM;
        LOGE("param: %s %s\n", ssid, password);
        goto _out;
	}

    info = calloc(1, sizeof(smartlink_info_t));
    strncpy(info->ssid, ssid, MAX_SSID_LEN);
    strncpy(info->password, password, MAX_PASSWORD_LEN);

	if(pthread_create(&pid_smartlink, NULL, thread_smartlink, info) != 0){
        result = AIRM2M_SMARTLINKJNI_ERR_SYS;
        LOGE("create thread error: %s\n", strerror(errno));
        goto _out;
    }

    usleep(100); //  wait for thread run

	LOGI("smartlink open success");

_out:
	return result;
}

void airm2m_smartlink_close(void){
    if(pthread_kill(pid_smartlink, 0) != ESRCH){
        void *status;
        int ret;
        pthread_t pid;

        LOGI("smartlink close");

        pid = pid_smartlink;

        pid_smartlink = 0;

    #if 0
        if((ret = pthread_kill(pid_smartlink, SIGQUIT)) != 0){
            LOGE("pthread kill error %s\n", strerror(errno));
        }
    #elif 0
        if((ret = pthread_cancel(pid_smartlink)) != 0){
            LOGE("pthread kill error %s\n", strerror(errno));
        }
    #endif

        if((ret = pthread_join(pid, &status)) != 0){
            LOGE("pthread join error %s\n", strerror(errno));
        }
    }
}

