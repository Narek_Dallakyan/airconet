
#ifndef _SMARTLINK_DEBUG_H_
#define _SMARTLINK_DEBUG_H_

//#define ENV_IOS

#if defined(ENV_ANDROID)
#define LOG_TAG "AIRM2M_SMARTLINK_JNI"

#include <jnilogger.h>
#elif defined(ENV_IOS)
#define LOGV		printf
#define LOGI		printf
#define LOGD		printf
#define LOGW		printf
#define LOGE		printf
#else
#define LOGV(...)
#define LOGI(...)
#define LOGD(...)
#define LOGW(...)
#define LOGE(...)
#endif

#define SMARTLINK_DEBUG

#define HEX_TO_ASCII(dec) ((dec) < 0x0a ? ((dec) + '0') : ((dec) - 0xa + 'A'))

#ifdef SMARTLINK_DEBUG
#define LOG_HEX(str, data, len) do{ \
        uint8_t __buf[1024+1]; \
        int __i; \
        for (__i = 0; __i < len; __i++){ \
            __buf[__i*2] = HEX_TO_ASCII((data[__i]&0xf0)>>4); \
            __buf[__i*2+1] = HEX_TO_ASCII(data[__i]&0x0f); \
        } \
        __buf[__i*2] = '\0'; \
        LOGV(str " %d %s\r\n", len, __buf); \
    }while(0)
#else
#define LOG_HEX(str, data, len)
#endif

#endif
