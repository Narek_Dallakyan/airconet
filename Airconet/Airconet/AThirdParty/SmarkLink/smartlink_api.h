//
//  smartlink_api.h
//  ekonair
//
//  Created by HuJin Qiang on 15/7/12.
//  Copyright (c) 2015年 ekon. All rights reserved.
//

#ifndef ekonair_smartlink_api_h
#define ekonair_smartlink_api_h

#define AIRM2M_SMARTLINKJNI_ERR_NONE            (0)
#define AIRM2M_SMARTLINKJNI_ERR_PARAM           (-1) // 参数错误 比如SSID是空的
#define AIRM2M_SMARTLINKJNI_ERR_BUSY            (-2) // 当前已在进行SMARTLINK 请先停止
#define AIRM2M_SMARTLINKJNI_ERR_SYS             (-3) // 系统错误,比如创建线程失败

int airm2m_smartlink_open(const char *ssid, const char *password);

void airm2m_smartlink_close(void);

#endif
