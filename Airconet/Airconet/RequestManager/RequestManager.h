//
//  RequestManager.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/24/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RequestManager : NSObject

+(RequestManager * ) sheredMenage;
- (void)postJsonDataWithUrl:(NSString *)urlString
              andParameters:(NSDictionary *)parms
                    success:(void (^)(id))success
                    failure:(void (^)(NSError *))failuer;
- (void)postJsonWithContentType:(NSString *)urlString
                  andParameters:(NSDictionary *)parms
                        success:(void (^)(id))success
                        failure:(void (^)(NSError *))failuer;
- (void)getJsonDataWithUrl:(NSString *)urlString
             andParameters:(NSDictionary *)parms
              isNeedCookie:(BOOL) isNeedCookie
                   success:(void (^)(id response))success
                   failure:(void (^)(NSError *error))failure;
- (void)getDataWithUrl:(NSString *)urlString
           success:(void (^)(id response))success
           failure:(void (^)(NSError *error))failure;
+ (void)setCookie;
+ (NSHTTPCookie *)getCookie;
@end

NS_ASSUME_NONNULL_END
