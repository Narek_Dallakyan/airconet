//
//  RequestManager.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/24/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "RequestManager.h"
#import "QXBaseNetWorkManager.h"
#import "ACommonTools.h"
#import "BaseViewController.h"
#import "CLRequestTaskQueue.h"
//#import "APublicDefine.h"
//#import "AKeysDefine.h"


@interface RequestManager ()
@property (nonatomic, strong) CLRequestTaskQueue * requestTasks;

@end


@implementation RequestManager

static NSHTTPCookie * _sessonCookie = nil;
//lasat request time in seconds
static long lastRequestTime = 0;

+(RequestManager *)sheredMenage {
    static RequestManager *sharedInstance=nil;
    static dispatch_once_t  oncePredecate;

    dispatch_once(&oncePredecate,^{
        sharedInstance=[[RequestManager alloc] init];

     });
    return sharedInstance;
}

- (void)postJsonDataWithUrl:(NSString *)urlString
              andParameters:(NSDictionary *)parms
                    success:(void (^)(id))success
                    failure:(void (^)(NSError *))failuer {
    
    NSLog(@"postJsonDataWithUrl:(NSString *)urlString andParameters:(NSDictionary *)parms success:(void (^)(id))success failure:(void (^)(NSError *))failuer = urlString = %@\n ", urlString );

    
    QXBaseNetWorkManager *manager = [QXBaseNetWorkManager shareNetWorkManager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.securityPolicy setValidatesDomainName:NO];
    
    NSString *preUrl = [urlString componentsSeparatedByString:@"?"].firstObject;
    if ([preUrl isEqualToString:APostdevsaveDevice] ||
        [preUrl isEqualToString:APostdevsetHvac] ||
        [preUrl isEqualToString:APostUpdateDevice] ||
        [preUrl isEqualToString:APostMotionSensorAway] ||
        [preUrl isEqualToString:APostMotionSensorArrive] ||
        [preUrl isEqualToString:APostMotionSensorConfig]) {
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
    } else {
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    }
    
    NSTimeZone *defaultTimeZone = [NSTimeZone defaultTimeZone];
    [manager.requestSerializer setValue:defaultTimeZone.name forHTTPHeaderField:@"X-Timezone"];
    [manager.requestSerializer setValue:@"Ekon" forHTTPHeaderField:@"X-Client"];


    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
   // NSLog(@"ABaseViewController::postJsonDataWithUrl request URL = %@%@ .. Parms = %@",manager.baseURL, urlString, parms);
     [manager POST:urlString parameters:parms headers:nil progress:^(NSProgress *uploadProgress) {
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
       // NSLog(@"ABaseViewController::postJsonDataWithUrl success  response  URL = %@ .. Object = %@",urlString,task.response);
        NSDictionary *dict = nil;
        if ( [urlString containsString:APostResetWorkingHours] || [urlString containsString:APostLink] ||
            [urlString containsString: APostDeleteLink] || [urlString containsString: APostRefreshBasewatt]) {
        
        NSString * boolResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
       // NSLog(@"ABaseViewController::postJsonDataWithUrl response  URL = %@ .. Object = %@",urlString,dict);
            success(boolResponse);
        } else {
        if (responseObject) {
            dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
           // NSLog(@"ABaseViewController::postJsonDataWithUrl response  URL = %@ .. Object = %@",urlString,dict);
        }
        success(dict);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
       // NSLog(@"ABaseViewController::postJsonDataWithUrl falied: %@",error);
        failuer(error);
        
    }];
}
- (void)postJsonWithContentType:(NSString *)urlString
              andParameters:(NSDictionary *)parms
                    success:(void (^)(id))success
                    failure:(void (^)(NSError *))failuer {
    
     NSLog(@"postJsonWithContentType:(NSString *)urlString andParameters:(NSDictionary *)parms success:(void (^)(id))success failure:(void (^)(NSError *))failuer = urlString = %@\n ", urlString);
    QXBaseNetWorkManager *manager = [QXBaseNetWorkManager shareNetWorkManager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.securityPolicy setValidatesDomainName:NO];
    
    NSString *preUrl = [urlString componentsSeparatedByString:@"?"].firstObject;
    if ([preUrl isEqualToString:APostdevsaveDevice] || [preUrl isEqualToString:APostdevsetHvac] || [preUrl isEqualToString:APostTimerUpdate] || [preUrl isEqualToString:APostEnabledOrDisabled] || [preUrl isEqualToString:APostTimerIsExist] || [preUrl isEqualToString:APostHazardRoomTemp] || [preUrl isEqualToString:APostHolidayAddRemove] || [preUrl isEqualToString:APostSystemWorkingHour]) {
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
    } else {
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    }
    id parameter;
    if ([preUrl isEqualToString:APostEnabledOrDisabled]) {
        parameter = @[parms];
    } else if ([preUrl isEqualToString:APostHolidayAddRemove]) {
        parameter = [parms valueForKey:@"selectedDate"];
    } else {
        parameter = parms;
    }
    
    NSTimeZone *defaultTimeZone = [NSTimeZone defaultTimeZone];
    [manager.requestSerializer setValue:defaultTimeZone.name forHTTPHeaderField:@"X-Timezone"];
    [manager.requestSerializer setValue:@"Ekon" forHTTPHeaderField:@"X-Client"];
    [manager.requestSerializer setValue:@"identity" forHTTPHeaderField:@"Accept-Encoding"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
   // NSLog(@"ABaseViewController::postJsonDataWithUrl request URL = %@%@ .. Parms = %@",manager.baseURL, urlString, parms);
    NSURLSessionDataTask *operationReq = [manager POST:urlString parameters:parameter headers:nil progress:^(NSProgress *uploadProgress) {
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
        //NSLog(@"ABaseViewController::postJsonDataWithUrl success  response  URL = %@ .. Object = %@",urlString,task.response);
        if ( [urlString containsString:APostTimerIsExist] ) {
            
            NSString * boolResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            //NSLog(@"AppDelegate::getJsonDataWithUrl response  URL = %@",urlString);
            success(boolResponse);
        } else {
            NSDictionary *dict = nil;
            if (responseObject) {
                dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
                //NSLog(@"ABaseViewController::postJsonDataWithUrl response  URL = %@ .. Object = %@",urlString,dict);
            }
            if (!dict) {
                success(@{});
            } else {
            success(dict);
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //NSLog(@"ABaseViewController::postJsonDataWithUrl falied: %@",error);
        failuer(error);
        
    }];
    [self.requestTasks addTask:operationReq];
}


- (void)getJsonDataWithUrl:(NSString *)urlString
             andParameters:(NSDictionary *)parms
              isNeedCookie:(BOOL)isNeedCookie
                   success:(void (^)(id response))success
                   failure:(void (^)(NSError *error))failure  {
    
       NSLog(@"getJsonDataWithUrl:(NSString *)urlString andParameters:(NSDictionary *)parms isNeedCookie:(BOOL)isNeedCookie success:(void (^)(id response))success failure:(void (^)(NSError *error))failure= urlString = %@\n ", urlString);
    QXBaseNetWorkManager *manager = [QXBaseNetWorkManager shareNetWorkManager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.securityPolicy setValidatesDomainName:NO];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSTimeZone *defaultTimeZone = [NSTimeZone defaultTimeZone];
    [manager.requestSerializer setValue:defaultTimeZone.name forHTTPHeaderField:@"X-Timezone"];
    [manager.requestSerializer setValue:@"Ekon" forHTTPHeaderField:@"X-Client"];
    [manager.requestSerializer setValue:@"Ekon" forHTTPHeaderField:@"User-agent"];
    [manager.requestSerializer setValue:@"false" forHTTPHeaderField:@"isServer"];
    
    if (isNeedCookie) {
        [RequestManager  getCookie];
    }
   NSURLSessionDataTask *operationReq = [manager GET:urlString parameters:parms headers:nil progress:^(NSProgress *uploadProgress) {
      //  NSLog(@"AppDelegate::getJsonDataWithUrl URL = %@  progress: %@",urlString,uploadProgress);
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString * strResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
      //  NSLog(@"AppDelegate::getJsonDataWithUrl success  URL = %@",urlString);
        NSDictionary *dict = nil;
        if (responseObject) {
            dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
           // NSLog(@"AppDelegate::getJsonDataWithUrl response  URL = %@",urlString);
            success(dict);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
    [self.requestTasks addTask:operationReq];

}

- (void)getDataWithUrl:(NSString *)urlString
           success:(void (^)(id response))success
           failure:(void (^)(NSError *error))failure  {
    
    NSLog(@"getDataWithUrl:(NSString *)urlString success:(void (^)(id response))success failure:(void (^)(NSError *error))failure = urlString = %@\n ", urlString);
    QXBaseNetWorkManager *manager = [QXBaseNetWorkManager shareNetWorkManager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.securityPolicy setValidatesDomainName:NO];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSTimeZone *defaultTimeZone = [NSTimeZone defaultTimeZone];
    [manager.requestSerializer setValue:defaultTimeZone.name forHTTPHeaderField:@"X-Timezone"];
    [manager.requestSerializer setValue:@"Ekon" forHTTPHeaderField:@"X-Client"];
    [manager.requestSerializer setValue:@"Ekon" forHTTPHeaderField:@"User-agent"];
    [manager.requestSerializer setValue:@"false" forHTTPHeaderField:@"isServer"];

    NSURLSessionDataTask *operationReq = [manager GET:urlString parameters:@{} headers:nil progress:^(NSProgress *uploadProgress) {
        //NSLog(@"AppDelegate::getJsonDataWithUrl URL = %@  progress: %@",urlString,uploadProgress);
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        //NSLog(@"AppDelegate::getJsonDataWithUrl success  URL = %@",urlString);
        NSDictionary *dict = nil;
        if (responseObject) {
            if ( [urlString containsString:AGetIsThereAnyTimer] || [urlString containsString:AGetWorkingHour] || [urlString containsString:AGetTotalAlert] || [urlString containsString:AGetMaxPower] || [urlString containsString:AGetVoltage] || [urlString containsString:AGetRefreshBasewattSensor] || [urlString containsString:AGetAllOnOff]) {
                NSString * boolResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                //NSLog(@"AppDelegate::getJsonDataWithUrl response  URL = %@",urlString);
                success(boolResponse);
            } else {
            dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
            //NSLog(@"AppDelegate::getJsonDataWithUrl response  URL = %@",urlString);
            success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
    [self.requestTasks addTask:operationReq];

}



+ (void)setCookie
{
    NSData *cookieData = [NSKeyedArchiver archivedDataWithRootObject:[[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    [ACommonTools setObjectValue:cookieData forKey:AKeyPersistentCookie];
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    for (NSHTTPCookie * cookie in cookies){
        if([cookie.name rangeOfString:@"JSESSIONID"].location != NSNotFound){
            //NSLog(@"got session Cookie = %@ ", cookie.value);
            _sessonCookie = cookie;
            break;
        }
    }
}

+ (NSHTTPCookie *)getCookie   {
    NSHTTPCookie *cookie = _sessonCookie;
    long currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
    //over time
    if(currentTime - lastRequestTime > 300)
        cookie = NULL;
    
    if (cookie == NULL) {
        NSData *cookieData = [ACommonTools getObjectValueForKey:AKeyPersistentCookie];
        if(cookieData != NULL && [cookieData length] > 0){
            NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData:cookieData];
            NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            if(cookies != NULL){
                for (NSHTTPCookie *cookie in cookies) {
                    [cookieStorage setCookie: cookie];
                   // NSLog(@"ABaseViewController::getCookie setcookie %@", cookie);
                }
            }
        }
    }
    
    //NSLog(@"ABaseViewController::getCookie = %@ isSession %d", cookie, currentTime - lastRequestTime < 300);
    
    lastRequestTime = currentTime;
    return cookie;
}
//    NSHTTPCookie *cookie = _sessonCookie;
//    long currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
//    //over time
//    if(currentTime - lastRequestTime > 300)
//        cookie = NULL;
//    if (cookie == NULL) {
//        NSData *cookieData = [ACommonTools getObjectValueForKey:AKeyPersistentCookie];
//        if(cookieData != NULL && [cookieData length] > 0){
//            NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData:cookieData];
//          //  NSError *error = nil;
//           // NSArray *cookies = [NSKeyedUnarchiver unarchivedObjectOfClass:[NSArray class] fromData:cookieData error:&error];
//            NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//            if(cookies != NULL){
//                for (NSHTTPCookie *cookie in cookies) {
//                        [cookieStorage setCookie: cookie];
//                    NSLog(@"ABaseViewController::getCookie setcookie %@", cookie);
//                }
//            }
//        }
//    }
//    NSLog(@"ABaseViewController::getCookie = %@ isSession %d", cookie, currentTime - lastRequestTime < 300);
//    lastRequestTime = currentTime;
//    return cookie;
//}


@end
