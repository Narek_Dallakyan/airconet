//
//  OCHttpClient.h
//  iAirCon
//
//  Created by Carl on 16/6/30.
//  Copyright © 2016年 Ekon Tech. All rights reserved.
//

#import <Foundation/Foundation.h>

//typealias CompletionJSONHandler = (NSURLRequest, NSHTTPURLResponse?, JSON, NSError?) -> Void
//typealias CompletionStringHandler = (NSURLRequest, NSHTTPURLResponse?, String?, NSError?) -> Void
//typealias CompletionHandler = (NSURLRequest, NSHTTPURLResponse?, AnyObject?, NSError?) -> Void
//typealias CompletioDatanHandler = (NSURLRequest, NSHTTPURLResponse?, NSData?, NSError?) -> Void

//typedef void(^CompletionJSONHandler)(NSURLRequest * request, NSHTTPURLResponse *respones,JSON *json, NSError *error);
typedef void(^CompletionStringHandler)(NSURLRequest * request, NSHTTPURLResponse *respones,NSString *respString, NSError *error);
typedef void(^CompletionHandler)(NSURLRequest * request, NSHTTPURLResponse *respones,id respPbj, NSError *error);
typedef void(^CompletioDatanHandler)(NSURLRequest * request, NSHTTPURLResponse *respones,NSData *respData, NSError *error);


@interface OCHttpClient : NSObject

+ (instancetype)sharedInstance;

- (void)loginWithUsername:(NSString *)username password:(NSString *)password rememberme:(NSString *)rememberme completionHandler:(CompletionStringHandler)block;


@end
