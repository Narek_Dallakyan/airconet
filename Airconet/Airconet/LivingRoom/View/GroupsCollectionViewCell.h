//
//  GroupsCollectionViewCell.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/6/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *mTitleLb;
@property (weak, nonatomic) IBOutlet UIImageView *mWarningImg;
@property (weak, nonatomic) IBOutlet UIView *mLineV;

@end

NS_ASSUME_NONNULL_END
