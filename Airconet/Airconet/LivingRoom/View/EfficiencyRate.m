//
//  EfficiencyRate.m
//  Airconet
//
//  Created by Karine Karapetyan on 19-05-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "EfficiencyRate.h"
#import "APublicDefine.h"
#import "AKeysDefine.h"
#import "NavigationBar.h"
#import "RequestManager.h"
#import "Devices.h"
#import "BaseViewController.h"

@interface EfficiencyRate () {
NavigationBar * navBar;
    Devices *device;
}
@end

@implementation EfficiencyRate

-(void)awakeFromNib {
    [super awakeFromNib];
    navBar = [[NavigationBar alloc] init];
    if (SCREEN_Width < 370) {
        [_mDayWeekMothBtn.titleLabel setFont:[UIFont fontWithName:@"Calibri" size:17]];
        [_mPriceBtn.titleLabel setFont:[UIFont fontWithName:@"Calibri" size:17]];
        [_mLeafBtn.titleLabel setFont:[UIFont fontWithName:@"Calibri" size:17]];
    }

    if (_isPowerMeter) {
        navBar = _navBarObj;
        [self getPMInfo];
    } else if (self.mac){
        [self getDeviceFull];
    } else if (self.grId) {
        [self getGroupPMInfo];
    }
}

#pragma mark __ GET
-(void)getDeviceFull {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * urlFormar = [NSString stringWithFormat:@"%@/%@", AGetDevice, self.mac];

        [[RequestManager sheredMenage] getDataWithUrl:urlFormar success:^(id  _Nonnull response) {
           // NSLog(@"AGetDevice response  = %@\n", response);
            self->device = [[[Devices alloc] init] getDeviceObj:response];
            self->navBar.dateModeForRequest = @"DAY";
            self->navBar.pmModeTypeForRequest = @"HOURS";
            self->navBar.dateMode = [BaseViewController getDateMode:self->navBar.dateModeForRequest];
            self->navBar.pmModeType = [BaseViewController getPmMode:self->navBar.pmModeTypeForRequest];

            if (self->device.powerMeter.mac) {
                self->navBar.isLinkedPm = YES;
                self->navBar.innerPmMac = self->device.powerMeter.mac;
            }
           
            [self getEfficiencyRateData:self.mac];
            [self.delegate updateNavigationObj:self->navBar];
        } failure:^(NSError * _Nonnull error) {
            //NSLog(@"AGetDevice response error  = %@\n", error.localizedDescription);
        }];
    });
}
-(void)getGroupPMInfo {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * urlFormar = [NSString stringWithFormat:@"%@?groupId=%@", AGetGroupPmInfo, self.grId];
        [[RequestManager sheredMenage] getDataWithUrl:urlFormar success:^(id  _Nonnull response) {
            NSLog(@"AGetGroupPmInfo response  = %@\n", response);
            self->navBar.dateModeForRequest = @"DAY";
            self->navBar.pmModeTypeForRequest = @"HOURS";
            self->navBar.dateMode = [BaseViewController getDateMode:self->navBar.dateModeForRequest];
            self->navBar.pmModeType = [BaseViewController getPmMode:self->navBar.pmModeTypeForRequest];
            self->navBar.isLinkedPm = [[response valueForKey:@"hasUserLinkedPM"] boolValue];
            [self getEfficiencyRateData:@""];
            [self.delegate updateNavigationObj:self->navBar];
        } failure:^(NSError * _Nonnull error) {
            //NSLog(@"AGetGroupPmInfo response error  = %@\n", error.localizedDescription);
        }];
    });
}
 
-(void)getPMInfo {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    [[RequestManager sheredMenage] getDataWithUrl:AGetPmInfo success:^(id  _Nonnull response) {
           // NSLog(@"AGetPmInfo response  = %@\n", response);
            self->navBar.isMasterPm = [[response valueForKey:@"hasUserMasterPM"] boolValue];
            self->navBar.isLinkedPm = [[response valueForKey:@"hasUserLinkedPM"] boolValue];
            [self getEfficiencyRateData:nil];
               } failure:^(NSError * _Nonnull error) {
                  // NSLog(@"AGetPmInfo response error  = %@\n", error.localizedDescription);
               }];
       });
}
-(void)getEfficiencyRateData:(NSString *)mac {
//    parameter --> pmMac,
    //    parameter --> timeRange (sra valuener@ sranq en (REALTIME,DAY,WEEK,MONTH,YEAR))
    //    parameter --> powerMeterMode (sra valuener@ sranq en (COST,KWH,HOURS))
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * urlFormar;
        if (self.isPowerMeter) {
            urlFormar = [NSString stringWithFormat:@"%@?timeRange=%@&powerMeterMode=%@", AGetNavigationDeviceMode, self->navBar.dateModeForRequest, self->navBar.pmModeTypeForRequest];
        } else {
            if(self.mac) {
                urlFormar = [NSString stringWithFormat:@"%@?deviceMac=%@&timeRange=%@&powerMeterMode=%@", AGetNavigationDeviceMode,mac, self->navBar.dateModeForRequest, self->navBar.pmModeTypeForRequest ];
            } else { //group
                urlFormar = [NSString stringWithFormat:@"%@?groupId=%@&timeRange=%@&powerMeterMode=%@", AGetGroupDeviceMode,self.grId, self->navBar.dateModeForRequest, self->navBar.pmModeTypeForRequest ];
            }
    }

        [[RequestManager sheredMenage] getDataWithUrl:urlFormar success:^(id  _Nonnull response) {
           // NSLog(@"AGetNavigationDeviceMode response  = %@\n", response);
            self->navBar.errorNumber = [response valueForKey:@"errorNumber"];
            self->navBar.pmMode = [response valueForKey:@"pmMode"];
            [self updateEfficiencyRateInfo];
           
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"AGetNavigationDeviceMode response error  = %@\n", error.localizedDescription);
        }];
    });
}

-(void)updateEfficiencyRateInfo{
    navBar.dateMode = [BaseViewController getDateMode:navBar.dateModeForRequest];
    navBar.pmModeType = [BaseViewController getPmMode:navBar.pmModeTypeForRequest];
    if (navBar.isLinkedPm && !self.isPowerMeter) {
        navBar.leafColor = [NavigationBar leafColor: navBar.errorNumber];
        navBar.leafImg = [NavigationBar leafImg: navBar.errorNumber];
        [self.mLeafBtn setHidden:NO];
        [self.mLeafBtn setImage:navBar.leafImg forState:UIControlStateNormal];
        [self.mLeafBtn setTitle:[navBar.errorNumber stringValue] forState:UIControlStateNormal];
        [self.mLeafBtn setTitleColor:navBar.leafColor forState:UIControlStateNormal];
    } else {
        [self.mLeafBtn setHidden:YES];
    }
    
    [self.mDayWeekMothBtn setTitle:navBar.dateMode forState:UIControlStateNormal];
    if ([TimerWithoutOnlineList indexOfObject:navBar.dateMode] >= 0 && [TimerWithoutOnlineList indexOfObject:navBar.dateMode] <= TimerWithoutOnlineList.count-1 ) {
        [self.mDayWeekMothPickV selectRow:[TimerWithoutOnlineList indexOfObject:navBar.dateMode] inComponent:0 animated:YES];
    }
    if ([PriceList indexOfObject:navBar.pmModeType] >= 0 && [PriceList indexOfObject:navBar.pmModeType] <= PriceList.count-1 ) {
        [self.mPricePickV selectRow:[PriceList indexOfObject:navBar.pmModeType] inComponent:0 animated:YES];
    }
    
    if ([navBar.pmModeType isEqualToString:NSLocalizedString(@"hour", nil)]) {
        if ([[ navBar.pmMode stringValue] length] > 3) {
            [self.mPriceBtn setTitle:[NSString stringWithFormat: @"%.1f%@", [ navBar.pmMode floatValue], NSLocalizedString(@"h", nil)] forState:UIControlStateNormal];
        } else {
            [self.mPriceBtn setTitle:[NSString stringWithFormat: @"%@%@", navBar.pmMode, NSLocalizedString(@"h", nil)] forState:UIControlStateNormal];
        }
    } else if ([navBar.pmModeType isEqualToString:NSLocalizedString(@"kWh", nil)]) {
        if ([[ navBar.pmMode stringValue] length] > 3) {
            [self.mPriceBtn setTitle:[NSString stringWithFormat: @"%.1f%@", [ navBar.pmMode floatValue], NSLocalizedString(@"kw_h", nil)] forState:UIControlStateNormal];
        } else {
            [self.mPriceBtn setTitle:[NSString stringWithFormat: @"%@%@", navBar.pmMode, NSLocalizedString(@"kw_h", nil)] forState:UIControlStateNormal];
        }
    } else {
        if ([[navBar.pmMode stringValue] length] > 3) {
            [self.mPriceBtn setTitle:[NSString stringWithFormat: @"%@%.1f",NSLocalizedString(@"$", nil), [ navBar.pmMode floatValue]] forState:UIControlStateNormal];
        } else {
            [self.mPriceBtn setTitle:[NSString stringWithFormat: @"%@%@",NSLocalizedString(@"$", nil), navBar.pmMode] forState:UIControlStateNormal];
        }
    }
    
    if ([navBar.errorNumber integerValue] == 0 ) {
        [self.mLeafBtn setHidden:YES];
    }
   // [self setNeedsDisplay];
}

- (IBAction)dayWeekMoth:(UIButton *)sender {
    [sender setHidden:YES];
       [self.mDayWeekMothPickV setHidden:NO];
}

- (IBAction)leaf:(UIButton *)sender {
    [self.delegate handlerPowerMeter];

}

- (IBAction)price:(UIButton *)sender {
    if (navBar.isLinkedPm ) {
           [self.mPriceBtn setHidden:YES];
           [self.mPricePickV setHidden:NO];
       }
}


#pragma mark: PickerView Delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    [pickerView.subviews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
        subview.hidden = (CGRectGetHeight(subview.frame) < 1.0);
    }];
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView.tag == 1) {
        return [PriceList count];
    }  else  {
        return [TimerWithoutOnlineList count];
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView.tag == 1) {
        return [PriceList objectAtIndex:row];
    }
    return [TimerWithoutOnlineList objectAtIndex:row];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
       NSInteger currentRow = [pickerView selectedRowInComponent:0];
    // if ([pickerView isEqual: self.mTimerPickV]) {
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0,70, 60)];
    label.font = [UIFont fontWithName:@"Calibri" size:19.f];
    label.textColor = GrayTitleColor;
    label.textAlignment = NSTextAlignmentCenter;
    if (pickerView.tag == 0) {
        label.text = [TimerWithoutOnlineList objectAtIndex:row];
    } else {
        label.text = [PriceList objectAtIndex:row];
    }
    if (row == currentRow) {
        if(self.isPowerMeter) {
            label.textColor = [UIColor whiteColor];
        } else {
            label.textColor = ColdColor;
        }
    }
    return label;

}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return  20;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
       if (pickerView.tag == 0) {
           [self.mDayWeekMothBtn setHidden:NO];
           [self.mDayWeekMothPickV setHidden:YES];
           [self.mDayWeekMothBtn setTitle:[TimerWithoutOnlineList objectAtIndex:row] forState:UIControlStateNormal];
          // NSLog(@"mTimerBtn title = %@\n", [TimerWithoutOnlineList objectAtIndex:row]);
           navBar.dateMode = [TimerWithoutOnlineList objectAtIndex:row];
           navBar.dateModeForRequest = [TimerForRequestWithoutOnlineList objectAtIndex:row];
           [self getEfficiencyRateData:self.mac];
           [self.delegate updateNavigationObj:navBar];
       } else  {
           [self.mPriceBtn setHidden:NO];
           [self.mPricePickV setHidden:YES];
           [self.mPriceBtn setTitle:[PriceList objectAtIndex:row] forState:UIControlStateNormal];
           //NSLog(@"mPriceBtn title = %@\n", [PriceList objectAtIndex:row]);
           
           navBar.pmModeType = [PriceList objectAtIndex:row];
           navBar.pmModeTypeForRequest = [PriceForRequestList objectAtIndex:row];
           [self getEfficiencyRateData:self.mac];
           [self.delegate updateNavigationObj:navBar];
       }
}



@end
