//
//  BarChartView.h
//  Airconet
//
//  Created by Karine Karapetyan on 23-05-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltdn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Airconet-Swift.h"
#import "DayAxisValueFormatter.h"

NS_ASSUME_NONNULL_BEGIN
@interface GraphCompanents : NSObject
@property (assign, nonatomic) NSInteger xAxisElement;
@property (assign, nonatomic) int numberOfVerticalElements;
@property (assign, nonatomic) double range;
@property (assign, nonatomic) double visibleXRange;
@property (assign, nonatomic) CGFloat barWidth;
@property (strong, nonatomic) NSArray * horizontalElementList;

@end


@interface BarChartCustomView : UIView
@property (nonatomic, strong) IBOutlet BarChartView * chartView;
@property (nonatomic, strong) NSArray * graphDataArr;
@property (nonatomic, strong) NSArray * graphDateArr;
@property (nonatomic, strong) NSString * dateMode;
@property (nonatomic, strong) NSString * pmModeType;
@property (nonatomic, strong) NSString * viewType;
@end

@interface DayAxisValueFormatterOverride:DayAxisValueFormatter
@property (weak, nonatomic) BarLineChartViewBase * chart;
@property (strong, nonatomic) NSArray * lastMonthValues;
@property (assign, nonatomic) NSInteger  index;

 // weak var chart: BarLineChartViewBase?
//
//    var lastMonthValues: [DayValue] = []
@end
NS_ASSUME_NONNULL_END
