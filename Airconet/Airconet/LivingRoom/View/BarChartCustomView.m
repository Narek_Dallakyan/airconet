//
//  BarChartView.m
//  Airconet
//
//  Created by Karine Karapetyan on 23-05-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BarChartCustomView.h"
#import "APublicDefine.h"

@implementation GraphCompanents
@end

@implementation DayAxisValueFormatterOverride

-(id)init:(BarLineChartViewBase*)chart lastMonthValues:(NSArray *)lastMonthValues {
    self = [super init];
     if(self != nil) {
         self.chart = chart;
         self.index = 0;
         self.lastMonthValues = lastMonthValues;
     }
     return self;
}
- (NSString *)stringForValue:(double)value
                        axis:(ChartAxisBase *)axis {
    self.index = (int)value;
    if (self.index > self.lastMonthValues.count) {
        self.index = 1;
    }
    NSString * str = [self.lastMonthValues objectAtIndex:self.index-1];
   // NSLog(@"str = %@\n", str);
    return str;
}
@end


@interface BarChartCustomView ()  {
    GraphCompanents * graph;
}

@end
@implementation BarChartCustomView
-(void)awakeFromNib {
    [super awakeFromNib];
    _chartView.noDataText = NSLocalizedString(@"no_chart_data", nil);
    _chartView.noDataTextColor = [UIColor grayColor];
    _chartView.noDataFont = [UIFont fontWithName:@"calibri" size:13];
     [[_chartView getAxis: AxisDependencyLeft] setDrawLimitLinesBehindDataEnabled:NO];
    if (graph) {
        if (![_viewType isEqualToString:@"PM"]) {
            _graphDateArr = [self getxAxisLableList];
        } else {
            [self getPMGraph];
        }
        _chartView.xAxis.valueFormatter = [[DayAxisValueFormatterOverride alloc] init:_chartView lastMonthValues:_graphDateArr];
        [self setDataCount:(int)_graphDateArr.count range:graph.range];
        
    } else if (self.graphDataArr.count > 0) {
            [self setupView];
    }
    
}

-(void)setupView {
    graph = [[GraphCompanents alloc] init];
    if (![_viewType isEqualToString:@"PM"]) {
               _graphDateArr = [self getxAxisLableList];
    } else {
        [self getPMGraph];
    }
        _chartView.chartDescription.enabled = NO;
        _chartView.maxVisibleCount = 200;
        _chartView.pinchZoomEnabled = NO;
        _chartView.borderColor = [UIColor clearColor];
        _chartView.backgroundColor =  [UIColor clearColor];//
        _chartView.gridBackgroundColor = [UIColor clearColor];
        [_chartView setScaleEnabled:NO];
        _chartView.dragEnabled = NO;
        _chartView.drawGridBackgroundEnabled = NO;
        [[_chartView getAxis:AxisDependencyLeft] setDrawGridLinesEnabled:NO];
        [_chartView setVisibleXRangeMaximum:10];
        _chartView.scaleXEnabled = YES;
        _chartView.scaleYEnabled = NO;
        _chartView.dragXEnabled = YES;
    

        NSNumberFormatter *leftAxisFormatter = [[NSNumberFormatter alloc] init];
          leftAxisFormatter.minimumFractionDigits = 0;
          leftAxisFormatter.maximumFractionDigits = 1;
          leftAxisFormatter.negativeSuffix = @" ";
          leftAxisFormatter.positiveSuffix = @" ";
        
        ChartYAxis *leftAxis = _chartView.leftAxis;
        leftAxis.labelFont = [UIFont systemFontOfSize:10.f];
        leftAxis.valueFormatter = [[ChartDefaultAxisValueFormatter alloc] initWithFormatter:leftAxisFormatter];
        leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
        leftAxis.spaceTop = 0.15;
        leftAxis.drawTopYLabelEntryEnabled = YES;//n
        leftAxis.drawBottomYLabelEntryEnabled = YES;//n
        leftAxis.labelTextColor = [UIColor grayColor];//n
        leftAxis.axisLineColor = [UIColor clearColor];
        
        ChartYAxis *rightAxis = _chartView.rightAxis;
        rightAxis.enabled = YES;
        rightAxis.gridColor = [UIColor grayColor];//n
        rightAxis.axisLineColor = [UIColor clearColor];//n
        rightAxis.labelTextColor = [UIColor clearColor];//n
        rightAxis.zeroLineColor = [UIColor clearColor];//n
        rightAxis.labelFont = [UIFont systemFontOfSize:10.f];
    
    ChartXAxis *xAxis = _chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    if (SCREEN_Width < 370) {
        xAxis.labelFont = [UIFont systemFontOfSize:8.f];
    }else {
    xAxis.labelFont = [UIFont systemFontOfSize:10.f];
    }
    xAxis.labelCount = _graphDateArr.count ;
    xAxis.granularity = 1.0; // only intervals of 1 day //n
    xAxis.spaceMin = 0.7;//0.15;//n
    xAxis.valueFormatter = [[DayAxisValueFormatterOverride alloc] init:_chartView lastMonthValues:_graphDateArr];
    xAxis.labelTextColor = [UIColor grayColor];//n
    xAxis.gridColor = [UIColor clearColor];//n
    xAxis.drawGridLinesEnabled = NO; //n
    _chartView.legend.enabled = NO;
   

    [self setDataCount:(int)_graphDateArr.count range:graph.range];
}

- (void)setDataCount:(int)count range:(double)range
{
    double start = 1.0;
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    for (int i = start; i <= count; i++){
        double val =[[_graphDataArr objectAtIndex:i-1] doubleValue];
        [yVals addObject:[[BarChartDataEntry alloc] initWithX:i y:val]];
    }
    
    BarChartDataSet *set1 = nil;
    if (_chartView.data.dataSetCount > 0) {
        set1 = (BarChartDataSet *)_chartView.data.dataSets[0];
        if (![_viewType isEqualToString:@"PM"]) {
            set1.barBorderColor = [UIColor redColor];
            set1.barBorderWidth = 2;
        }
        [set1 replaceEntries: yVals];
        [_chartView.data notifyDataChanged];
        [_chartView notifyDataSetChanged];

    } else {
        set1 = [[BarChartDataSet alloc] initWithEntries:yVals label:@"The year 2017"];
        if (![_viewType isEqualToString:@"PM"]) {
            [set1 setColors:@[[UIColor orangeColor]]];
        } else {
            [set1 setColors:@[[UIColor colorWithRed:0.0f/255.0f green:112.0f/255.0f blue:192.0f/255.0f alpha:1.0]]];
        }
        set1.drawIconsEnabled = NO;
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        if (![_viewType isEqualToString:@"PM"]) {
            set1.barBorderColor = [UIColor redColor];
            set1.barBorderWidth = 2;
        }
        [dataSets addObject:set1];
        
        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        [data setValueFont:[UIFont fontWithName:@"Calibri" size:10.f]];
        
        data.barWidth = graph.barWidth;
        _chartView.data = data;
    }
    [_chartView setNeedsDisplay];
    if (![_viewType isEqualToString:@"PM"]) {
        [self handleOption:@"toggleBarBorders" forChartView:_chartView];
        [_chartView setVisibleXRangeMaximum:graph.visibleXRange];//change to visible
    } else {
        [_chartView setVisibleXRangeMaximum:7];//change to visible
    }
    [_chartView moveViewToX:count];
}


- (void)handleOption:(NSString *)key forChartView:(ChartViewBase *)chartView {
    
    if ([key isEqualToString:@"toggleBarBorders"]) {
        for (id<IBarChartDataSet, NSObject> set in chartView.data.dataSets) {
            if ([set conformsToProtocol:@protocol(IBarChartDataSet)]){
                set.barBorderWidth = set.barBorderWidth == 1.0 ? 0.0 : 1.0;
                set.barBorderColor = [UIColor  redColor];
            }
        }
        [chartView setNeedsDisplay];
    }
    if ([key isEqualToString:@"toggleAutoScaleMinMax"]) {
        BarLineChartViewBase *barLineChart = (BarLineChartViewBase *)chartView;
        barLineChart.autoScaleMinMaxEnabled = !barLineChart.isAutoScaleMinMaxEnabled;
        
        [chartView notifyDataSetChanged];
    }
}


-(void)getPMGraph {
    graph.barWidth = 0.5;
    graph.range = _graphDataArr.count;
    graph.visibleXRange = _graphDataArr.count;
}

-(NSMutableArray * )getxAxisLableList {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[[NSDate alloc] init]];
    [components setHour:-[components hour]];
    [components setMinute:-[components minute]];
    [components setSecond:-[components second]];
    NSDate *today = [cal dateByAddingComponents:components toDate:[[NSDate alloc] init] options:0];
    
    if ([self.dateMode isEqual:NSLocalizedString(@"week", nil)]) {
        
        NSMutableArray * weekArr = [NSMutableArray array];
        for (int i = 6; i >= 0 ; i--) {
            [components setHour:(i*-24)];
            [components setMinute:0];
            [components setSecond:0];
            NSDate *yesterday = [cal dateByAddingComponents:components toDate: today options:0];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat=@"EEE";
            NSString *dayString = [[dateFormatter stringFromDate:yesterday] capitalizedString];
            [weekArr addObject:dayString];
        }
        graph.barWidth = 0.3;
        graph.range = 24;
        graph.visibleXRange = 15;
        [self isNotHour];
        return  weekArr;
        
    } else if ([self.dateMode isEqual:NSLocalizedString(@"month", nil)]) {
        NSMutableArray * monthArr = [NSMutableArray array];
        for (int i = 29; i >= 0 ; i--) {
            [components setHour:(i*-24)];
            [components setMinute:0];
            [components setSecond:0];
            NSDate *yesterday = [cal dateByAddingComponents:components toDate: today options:0];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat=@"dd";
            NSString *dayString = [[dateFormatter stringFromDate:yesterday] capitalizedString];
            [monthArr addObject:dayString];
        }
        graph.barWidth = 0.7;
        graph.range = 24;
        graph.visibleXRange = 10;
        [self isNotHour];
        return monthArr;
        
    } else if ([self.dateMode isEqual:NSLocalizedString(@"year", nil)]) { //year+
        NSDateComponents *components1 = [cal components:( NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour ) fromDate:[[NSDate alloc] init]];
        
        [components1 setMonth:1];
        NSDate *today = [cal dateByAddingComponents:components1 toDate:[[NSDate alloc] init] options:0];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat=@"MMM";
        NSMutableArray * yearArr = [NSMutableArray array];
        for (int i = -2; i < 10 ; i++) {
            [components1 setMonth:i];
            NSDate *yesterday = [cal dateByAddingComponents:components1 toDate: today options:0];
            NSString *dayString = [[dateFormatter stringFromDate:yesterday] capitalizedString];
            [yearArr addObject:dayString];
        }
        graph.barWidth = 0.5;
        graph.visibleXRange = 10;
        graph.range =  [[self.graphDataArr valueForKeyPath:@"@max.self"] doubleValue]+10;
        [self isNotHour];

        return yearArr;
    } else {
        NSMutableArray * dayArr = [NSMutableArray array];
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"HH:mm"];
        
        for (int i = 23; i > 0 ; i--) {
            NSDate *minusOneHr = [[NSDate date] dateByAddingTimeInterval:-60*60 * i];
            NSString *dayString = [[outputFormatter stringFromDate:minusOneHr] capitalizedString];
            [dayArr addObject:dayString];
        }
        NSString *todayString = [[outputFormatter stringFromDate:[NSDate date]] capitalizedString];
        [dayArr addObject:todayString];
        graph.barWidth = 0.5;
        graph.range =  60;
        graph.visibleXRange = 10;
        [self isNotHour];

        return dayArr;
    }
}

-(void)isNotHour {
    if (![self.pmModeType isEqualToString:@"Hour"]) {
        graph.range =  [[self.graphDataArr valueForKeyPath:@"@max.self"] doubleValue]+10;
    }
}
@end
