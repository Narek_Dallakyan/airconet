//
//  GroupsNavigationView.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/6/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "GroupsNavigationView.h"
#import "GroupsCollectionViewCell.h"
#import "APublicDefine.h"
#import "Devices.h"

@implementation GroupsNavigationView
-(void)awakeFromNib {
    [super awakeFromNib];
    self.mOnOffBackgroundV.layer.borderWidth = 2.f;
    self.mOnOffBackgroundV.layer.cornerRadius = 8.f;
    self.mOnOffBackgroundV.layer.borderColor =  [GrayTitleColor CGColor];
 }



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_devicesOfGroupArr count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GroupsCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"GroupsCell" forIndexPath:indexPath];

    Devices * dev = [_devicesOfGroupArr objectAtIndex:indexPath.row];
    [cell.mWarningImg setHidden:YES];
    cell.mLineV.hidden = NO;
    cell.mTitleLb.text = dev.name;
    if (_devicesOfGroupArr.count == 1) {
        cell.mLineV.hidden = YES;
    } else if (_devicesOfGroupArr.count == 2) {
        if (indexPath.row == 0) {
            cell.mLineV.hidden = NO;
        } else {
            cell.mLineV.hidden = YES;
        }
    }
    if (dev.isWarning) {
        [cell.mWarningImg setHidden:NO];
    }    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(150, 30);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (_devicesOfGroupArr.count == 1) {
            return UIEdgeInsetsMake(0, SCREEN_Width/2-75, 0, 0);
    } else if (_devicesOfGroupArr.count == 2) {
            return UIEdgeInsetsMake(0, SCREEN_Width/2-150, 0, 0);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (IBAction)on:(UIButton *)sender {

      //  [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //[sender setBackgroundColor:self.mOffBtn.backgroundColor];
       // [self.mOffBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
       // [self.mOffBtn setBackgroundColor:[UIColor whiteColor]];
        [self.delegate handlerOnOff:YES];
}

- (IBAction)off:(UIButton *)sender {
//        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [sender setBackgroundColor:self.mOnBtn.backgroundColor];
//        [self.mOnBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
//        [self.mOnBtn setBackgroundColor:[UIColor whiteColor]];
        [self.delegate handlerOnOff:NO];
}
@end
