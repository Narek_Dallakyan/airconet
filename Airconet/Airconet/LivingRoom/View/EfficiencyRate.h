//
//  EfficiencyRate.h
//  Airconet
//
//  Created by Karine Karapetyan on 19-05-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBar.h"


NS_ASSUME_NONNULL_BEGIN

@protocol EfficiencyRateDelegate <NSObject>

@optional
-(void)handlerPowerMeter;
-(void)updateNavigationObj:(NavigationBar *)navBarObj;
@end

@interface EfficiencyRate : UIView
@property (strong, nonatomic)id<EfficiencyRateDelegate> delegate;
@property (strong, nonatomic) NSString * mac;
@property (strong, nonatomic) NSNumber * grId;

@property (assign, nonatomic) BOOL isPowerMeter;
@property (strong, nonatomic) NavigationBar * navBarObj;
@property (weak, nonatomic) IBOutlet UIButton *mDayWeekMothBtn;
@property (weak, nonatomic) IBOutlet UIPickerView * mDayWeekMothPickV;
@property (weak, nonatomic) IBOutlet UIPickerView *mPricePickV;
@property (weak, nonatomic) IBOutlet UIButton *mPriceBtn;
@property (weak, nonatomic) IBOutlet UIButton *mLeafBtn;
@end

NS_ASSUME_NONNULL_END
