//
//  BarGraphView.h
//  Airconet
//
//  Created by Karine Karapetyan on 03-05-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface Graph : NSObject
@property (assign, nonatomic) NSInteger horizontalElement;
@property (assign, nonatomic) int numberOfVerticalElements;
@property (assign, nonatomic) int showVerticalElements;
@property (assign, nonatomic) CGFloat verticalDtaSpaces;
@property (assign, nonatomic) CGFloat barWidth;
@property (assign, nonatomic) CGFloat spaceBetweenBars;
@property (assign, nonatomic) CGFloat barRandValue;
@property (strong, nonatomic) NSArray * horizontalElementList;
@property (assign, nonatomic) BOOL scrollFinish;

@end



NS_ASSUME_NONNULL_END
