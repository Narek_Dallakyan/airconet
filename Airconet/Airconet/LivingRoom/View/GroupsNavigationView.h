//
//  GroupsNavigationView.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/6/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol GroupsNavigationViewDelegate <NSObject>

-(void)handlerOnOff:(BOOL)isOn;

@end
@interface GroupsNavigationView : UIView
@property(weak, nonatomic) id <GroupsNavigationViewDelegate> delegate;

@property (assign, nonatomic) BOOL isOn;
@property (strong, nonatomic) NSMutableArray * devicesOfGroupArr;

@property (weak, nonatomic) IBOutlet UIView * mGroupCustomNavSubview;
@property (weak, nonatomic) IBOutlet UICollectionView * mGroupsCollactionV;
@property (weak, nonatomic) IBOutlet UIView * mOnOffBackgroundV;
@property (weak, nonatomic) IBOutlet UIButton * mOffBtn;
@property (weak, nonatomic) IBOutlet UIButton * mOnBtn;
- (IBAction)on:(UIButton *)sender;
- (IBAction)off:(UIButton *)sender;
@end

NS_ASSUME_NONNULL_END
