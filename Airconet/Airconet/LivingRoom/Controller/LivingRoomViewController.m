//
//  LivingRoomViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 2/4/20.
//  Copyright © 2020Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "LivingRoomViewController.h"
#import "OpenMessageViewController.h"
#import "WMGaugeView.h"
#import "GroupsNavigationView.h"
#import "RequestManager.h"
#import "ACommonTools.h"
#import "Devices.h"
#import "NavigationBar.h"
#import "BarGraphView.h"
#import "AirQuality.h"
#import "PowerMeter.h"
#import "EfficiencyRate.h"
#import "BarChartCustomView.h"
#import "Motionsensor.h"



#define RGB(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
@interface LivingRoomViewController ()<GroupsNavigationViewDelegate, EfficiencyRateDelegate> {
    AirQuality * airQuality;
    MotionsensorForDevice * motionSensor;
    NavigationBar * navBar;

    MainPowerMeter * powerMeter;
    UIButton * clockBtn;
    NSMutableArray * devicesOfGroupArr;
    NSMutableArray * temperatureList;
    Devices * device;
}

@property (strong, nonatomic) IBOutlet EfficiencyRate *mEfficencyRateV;
@property (strong, nonatomic) IBOutlet UILabel *mPfLb;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mSpeedometrTopLayoutV;
@property (weak, nonatomic) IBOutlet UIView *mCustomNavSubview;
@property (weak, nonatomic) IBOutlet UIView *mCustomNavigationBarV;
@property (weak, nonatomic) IBOutlet UIButton *mTurnOnOffBtn;
@property (weak, nonatomic) IBOutlet GroupsNavigationView *mGroupsNavigationV;
@property (strong, nonatomic) IBOutlet BarChartCustomView *mBarGraphV;
@property (weak, nonatomic) IBOutlet WMGaugeView *mFirstGuageContentV;
@property (strong, nonatomic) IBOutlet UIView *mAirQualityBackV;
@property (strong, nonatomic) IBOutlet UIButton *mAirHeartBtn;
@property (strong, nonatomic) IBOutlet UILabel *mAirLb;

@property (weak, nonatomic) IBOutlet UIScrollView *mScrollV;
@property (weak, nonatomic) IBOutlet UIButton *mErrorBtn;
@property (weak, nonatomic) IBOutlet UIPickerView *mTemperaturePickV;
@property (weak, nonatomic) IBOutlet UIPickerView *mFanPickV;
@property (weak, nonatomic) IBOutlet UIPickerView *mModePickV;
@property (weak, nonatomic) IBOutlet UIButton *mFanBtn;
@property (weak, nonatomic) IBOutlet UIButton *mGradusBtn;
@property (weak, nonatomic) IBOutlet UIButton *mModeBtn;
@property (weak, nonatomic) IBOutlet UIButton *mHomeTemperatureBtn;
@property (weak, nonatomic) IBOutlet UIView *mOffBackgrV;
@property (weak, nonatomic) IBOutlet UIView *mwarningBackgrV;
@property (weak, nonatomic) IBOutlet UIImageView *mWarningImg;
@property (weak, nonatomic) IBOutlet UILabel *mNetworkErrorLb;
@property (weak, nonatomic) IBOutlet UILabel *mCheckInetLb;
@property (weak, nonatomic) IBOutlet UILabel *mCheckDevLb;
@property (weak, nonatomic) IBOutlet UIView *mGroupNavLineV;
//@property (weak, nonatomic) IBOutlet UIButton *mErrTurnOffBtn;
@property (weak, nonatomic) IBOutlet UILabel *mHLb;
@property (weak, nonatomic) IBOutlet UILabel *mWkHLb;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mGraphTopLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mScrollContenierHeightLayout;
@property (strong, nonatomic) IBOutlet UIImageView *mGaugeWarningImg;
@property (weak, nonatomic) IBOutlet UILabel *mGraphLb;
@property (strong, nonatomic) IBOutlet UIView *mMotionBackV;
@property (strong, nonatomic) IBOutlet UIImageView *mMotionUserImg;
@property (strong, nonatomic) IBOutlet UIImageView *mMotionTempImg;
@property (strong, nonatomic) IBOutlet UILabel *mMotionTempLb;
@property (strong, nonatomic) IBOutlet UIImageView *mHomeImg;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mFanTrailingLayoutV;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mModeLeadingLayoutV;

@end
@implementation LivingRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([BaseViewController isDeviceLanguageRTL]) { //Arabic, Hebrew
        _mModeLeadingLayoutV.constant = 15;
        _mFanTrailingLayoutV.constant = 20;
    }
    temperatureList = [NSMutableArray array];
    if (self.currDevice) {
        temperatureList = _currDevice.temperatureList;
    } else {
        temperatureList = [TemperatureList mutableCopy];
    }
}

-(void)viewDidLayoutSubviews {
    self.mScrollV.scrollEnabled = YES;
    self.mScrollContenierHeightLayout.constant =self.mScrollV.frame.size.height;
    self.mScrollV.contentSize = CGSizeMake(self.view.frame.size.width,self.mBarGraphV.frame.origin.y + self.mBarGraphV.frame.size.height+50);
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self setBackground:self];
    [self setupView];
}

-(void)viewDidAppear:(BOOL)animated  {
    [super viewDidAppear:YES];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(updateByFeedback:) name:NotificationLivingRoom object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(updateByFeedback:) name:NotificationAirQuality object:nil];
    
    self.mScrollV.contentOffset = CGPointMake(0, 0);
}
-(void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)setupView {
    self.mGroupsNavigationV.delegate = self;
    [self.mHomeTemperatureBtn setImageEdgeInsets:UIEdgeInsetsMake(-5, 0, 5, 0)];//(top, left, bottom, right)
    self.mEfficencyRateV.delegate = self;
    if (self.isDevice) {
        [self.navigationController.navigationBar setBarTintColor:self.currDevice.deviceVisual.modeColor];
        [self setNavigationStyle:self.currDevice.deviceVisual.modeColor];


        airQuality = [[AirQuality alloc] init];
        motionSensor = [[MotionsensorForDevice alloc] init];
        powerMeter = [[MainPowerMeter alloc] init];
        self.mEfficencyRateV.mac = _currDevice.mac;
        [self.mEfficencyRateV awakeFromNib];
        [self performSelectorInBackground:@selector(getAllDevices) withObject:nil];

        [self.mCustomNavigationBarV setHidden:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.currDevice.deviceType isEqualToString:@"SW"]) {
                [self setSWInfo];
            } else {
                [self setDeviceInfo];
            }
            self.mTurnOnOffBtn.layer.cornerRadius = self.mTurnOnOffBtn.frame.size.height/2;
            self.mTurnOnOffBtn.layer.borderColor = self.currDevice.deviceVisual.modeColor.CGColor;
            self.mTurnOnOffBtn.layer.borderWidth = 3.f;
            [self.mTurnOnOffBtn setBackgroundColor:self.view.backgroundColor];
        });
        [self showGuage:NO];
    } else {
        self.mEfficencyRateV.grId =  [NSNumber numberWithInteger:_currGroup.GrId];
        [self.mEfficencyRateV awakeFromNib];
        [self setNavigationStyleForGRoup];
        [self setGroupInfo];
    }
    [self setRightBarButton];
    
}
-(void)setNavigationStyle:(UIColor *)color{
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self setNavigationStyle:color tintColor:[UIColor whiteColor] titleText:_currDevice.name titleColor:[UIColor whiteColor]];
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if ([view isKindOfClass:[DevicesNavigationBar class]]) {
            [view removeFromSuperview];
        }
    }
}

-(void)setNavigationStyleForGRoup {
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if ([view isKindOfClass:[DevicesNavigationBar class]]) {
            [view removeFromSuperview];
        }
    }
    if ([self isDarkMode]) {
        [self setNavigationStyle:RGB(38, 38, 38) tintColor:[UIColor whiteColor] titleText:_currGroup.name titleColor:[UIColor whiteColor]];
    } else {
        [self setNavigationStyle:[UIColor whiteColor] tintColor:RGB(68, 68, 68) titleText:_currGroup.name titleColor:RGB(68, 68, 68)];
    }
}

-(void)setRightBarButton {
    UIImage* timerImage = [UIImage imageNamed:@"timer"];
    clockBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, timerImage.size.width, timerImage.size.height)];
    [clockBtn setBackgroundImage:timerImage forState:UIControlStateNormal];
    [clockBtn addTarget:self action:@selector(clock)
       forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithCustomView:clockBtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    [clockBtn setHidden:!self.currDevice.deviceVisual.isTimer];
}

#pragma mark: SET DEVICE INFO
-(void)setDeviceInfo {
    
    if (self.currDevice.deviceVisual.isWarning) {
        [self setWarning];
    } else   if (self.currDevice.onoff == 86 || self.currDevice.onoff == 170) {
        [self.mOffBackgrV setHidden:NO];
       // [self.mErrTurnOffBtn setHidden:NO];
       // [self.mErrorBtn setHidden:YES];
        [self.mErrorBtn setHidden:!_currDevice.deviceVisual.isError];
        
    } else if (self.currDevice.onoff == 85 || self.currDevice.onoff == 55)  {
        [self.mOffBackgrV setHidden:YES];
       // [self.mErrTurnOffBtn setHidden:YES];
       // [self.mErrorBtn setHidden:NO];
        [self setNavigationStyle:self.currDevice.deviceVisual.modeColor];
        [self.mErrorBtn setHidden:!self.currDevice.deviceVisual.isError];
        
    }
    
    [self.mModeBtn setImage:self.currDevice.deviceVisual.modeBigImage forState:UIControlStateNormal];
    if ([DevisesModeBigImgList indexOfObject:self.currDevice.deviceVisual.modeBigImage] >= 0 && [DevisesModeBigImgList indexOfObject:self.currDevice.deviceVisual.modeBigImage] <= DevisesModeBigImgList.count-1) {
         [self.mModePickV selectRow:[DevisesModeBigImgList indexOfObject:self.currDevice.deviceVisual.modeBigImage] inComponent:0 animated:NO];
    }
   
    [self.mTurnOnOffBtn setImage:self.currDevice.deviceVisual.turnOnOffImg forState:UIControlStateNormal];
    [self.mFanBtn setImage:self.currDevice.deviceVisual.fanImage forState:UIControlStateNormal];
    [self.mFanBtn setTitleColor:self.currDevice.deviceVisual.modeColor forState:UIControlStateNormal];
    [self.mGradusBtn setTitleColor:self.currDevice.deviceVisual.modeColor forState:UIControlStateNormal];
    [self.mGradusBtn setTitle:[NSString stringWithFormat:@"%li°", self.currDevice.tgtTemp] forState:UIControlStateNormal];
    [self.navigationController.navigationBar setBarTintColor:self.currDevice.deviceVisual.modeColor];
    [self.mCustomNavSubview setBackgroundColor:self.currDevice.deviceVisual.modeColor];
    self.mTurnOnOffBtn.layer.borderColor = self.currDevice.deviceVisual.modeColor.CGColor;
    if (self.currDevice.fan == 0) {
        [self.mFanBtn setTitle:@"A"forState:UIControlStateNormal];
        [self.mFanBtn.titleLabel setFont:[UIFont fontWithName:@"Calibri" size:35.f] ];
        
    } else {
        [self.mFanBtn setTitle:[NSString stringWithFormat:@"%li",self.currDevice.fan]
                      forState:UIControlStateNormal];
        [self.mFanBtn.titleLabel setFont:[UIFont fontWithName:@"Calibri" size:40.f] ];
    }
    
    if (self.currDevice.envTempShow) {
        [self.mHomeTemperatureBtn setTitle:[NSString stringWithFormat:@"%li°", self.currDevice.envTempShow] forState:UIControlStateNormal];
    } else {
        [self.mHomeTemperatureBtn setHidden:YES];
    }
    [clockBtn setHidden:!self.currDevice.deviceVisual.isTimer];
    
}

-(void)setSWInfo {
    [self.mTemperaturePickV setHidden:YES];
    [self.mHomeTemperatureBtn setHidden:YES];
    [self.mGradusBtn setHidden:YES];
    [self.mModePickV setHidden:YES];
    [self.mModeBtn setHidden:YES];
    [self.mFanBtn setHidden:YES];
    [self.mFanPickV setHidden:YES];
    [self.mHomeImg setHidden:YES];
    self.mSpeedometrTopLayoutV.constant = -400;

    if (self.currDevice.deviceVisual.isWarning) {
        [self setWarning];
    } else   if (self.currDevice.onoff == 86 || self.currDevice.onoff == 170) {
        [self.mOffBackgrV setHidden:NO];
       // [self.mErrTurnOffBtn setHidden:NO];
       // [self.mErrorBtn setHidden:YES];
        [self.mErrorBtn setHidden:!_currDevice.deviceVisual.isError];
        self.currDevice.deviceVisual.modeColor = GrayDevColor;
        self.currDevice.deviceVisual.turnOnOffImg = [UIImage imageNamed:@"gray_lampa"];
        
    } else if (self.currDevice.onoff == 85 || self.currDevice.onoff == 55)  {
        [self.mOffBackgrV setHidden:YES];
      //  [self.mErrTurnOffBtn setHidden:YES];
       // [self.mErrorBtn setHidden:NO];
        [self setNavigationStyle:self.currDevice.deviceVisual.modeColor];
        [self.mErrorBtn setHidden:!self.currDevice.deviceVisual.isError];
        self.currDevice.deviceVisual.modeColor = YellowLightDevColor;
        self.currDevice.deviceVisual.turnOnOffImg = [UIImage imageNamed:@"yellow_lamp"];
    }
   

    [self.mTurnOnOffBtn setImage:self.currDevice.deviceVisual.turnOnOffImg forState:UIControlStateNormal];
    [self.navigationController.navigationBar setBarTintColor:self.currDevice.deviceVisual.modeColor];
    [self.mCustomNavSubview setBackgroundColor:self.currDevice.deviceVisual.modeColor];
    self.mTurnOnOffBtn.layer.borderColor = self.currDevice.deviceVisual.modeColor.CGColor;
    [clockBtn setHidden:!self.currDevice.deviceVisual.isTimer];
    
}
-(void)showGuage:(BOOL)show {
    if (show) {
        [self.mFirstGuageContentV setHidden:NO];
        [self.mHLb setHidden:NO];
        [self.mWkHLb setHidden:NO];
            if (SCREEN_Width < 370) {
                self.mGraphTopLayout.constant = 0;
            } else {
                self.mGraphTopLayout.constant = -80;
            }
    } else {
        [self.mFirstGuageContentV setHidden:YES];
        [self.mHLb setHidden:YES];
        [self.mWkHLb setHidden:YES];
        if (SCREEN_Width < 370) {
            self.mGraphTopLayout.constant = -230;
            
        } else {
            self.mGraphTopLayout.constant = -300;
        }
    }
}

-(void)setWarning {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mwarningBackgrV setHidden:NO];
        [self.mErrorBtn setHidden:!self.currDevice.deviceVisual.isError];
        [self.mTurnOnOffBtn setImage:[UIImage imageNamed:@"off"] forState:UIControlStateNormal];
        [self.navigationController.navigationBar setBarTintColor:GrayDevColor];
        [self.mCustomNavSubview setBackgroundColor:GrayDevColor];
        self.mTurnOnOffBtn.layer.borderColor = GrayDevColor.CGColor;
        if (self.currDevice.envTemp) {
            [self.mHomeTemperatureBtn setTitle:[NSString stringWithFormat:@"%li°", self.currDevice.envTemp] forState:UIControlStateNormal];
        } else {
            [self.mHomeTemperatureBtn setHidden:YES];
        }
    });
}

-(void)setAirQuality {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mAirQualityBackV setHidden:NO];
        [self.mAirHeartBtn setImage:[AirQuality getHeartBitImg:self->airQuality.numberGas] forState:UIControlStateNormal];
        [self.mAirLb setTextColor:[AirQuality getAirColor:self->airQuality.numberGas] ];
        [self.mAirLb setText:[NSString stringWithFormat:@"%li", (long)self->airQuality.numberGas]];
    });
}
-(void) setMotionSensor {
    [self.mMotionBackV setHidden:NO];
    if ([motionSensor.status isEqualToString:@"AWAY"]) {
        [self.mMotionUserImg setImage:Image(@"user_circle")];
        if ([motionSensor.commandMode isEqualToString:@"FUNC_MODE"]) {
            [self.mMotionTempImg setHidden:NO];
        } else {
            [self.mMotionTempImg setHidden:YES];
        }
    } else if ([motionSensor.status isEqualToString:@"COME"]) {
        [self.mMotionUserImg setImage:Image(@"white_user")];
    }
    if (![motionSensor.workingTime isKindOfClass:[NSNull class]]) {
        [self.mMotionTempLb setText:motionSensor.workingTime];
    }
}

-(void)updateDeviceObj:(AllDevises *)device {
    NSDictionary * newDic = [AllDevises dictionaryWithObject:device];
    BOOL err = self.currDevice.deviceVisual.isError;
    BOOL time = self.currDevice.deviceVisual.isTimer;
    BOOL warr = self.currDevice.deviceVisual.isWarning;
    self.currDevice = [[[AllDevises alloc] init] getDeviceObj:newDic];
    self.currDevice.deviceVisual.isError = err;
    self.currDevice.deviceVisual.isWarning = warr;
    self.currDevice.deviceVisual.isTimer = time;
    
}

#pragma mark:Get Requests

-(void)getAirQuality {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] getJsonDataWithUrl:APostAirQuality andParameters:@{@"mac":self.currDevice.mac}
                                             isNeedCookie:NO success:^(id  _Nonnull response) {
            self->airQuality.mac = self.currDevice.mac;
            if ([[response valueForKey:@"type"] isEqual:@"gas"]) {
                self->airQuality.typeGas = [response valueForKey:@"type"];
                self->airQuality.numberGas = [[response valueForKey:@"number"] integerValue];
                [self setAirQuality];
            }
            [self getMotionSensor];
        } failure:^(NSError * _Nonnull error) {
            //NSLog(@"AirQuality response  error = %@\n", error.localizedDescription);
        }];
    });
}

-(void)getMotionSensor {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *formatUrl = [NSString stringWithFormat:@"%@?mac=%@",AGetMotionSensorByDeviceMac, self->_currDevice.mac];
        [[RequestManager sheredMenage] getJsonDataWithUrl:formatUrl andParameters:@{}
                                             isNeedCookie:NO success:^(id  _Nonnull response) {
         // NSLog(@"AGetMotionSensorByDeviceMac response  error = %@\n", response);
            self->motionSensor.status = [response valueForKey:@"status"];
            self->motionSensor.commandMode = [response valueForKey:@"commandMode"];
            self->motionSensor.timeParam = [response valueForKey:@"timeParam"];
            self->motionSensor.workingTime = [NSString stringWithFormat:@"%@%@",[response valueForKey:@"workingTime"], [BaseViewController getTimerPram:self->motionSensor.timeParam]];
            
            [self setMotionSensor];

        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"AGetMotionSensorByDeviceMac response  error = %@\n", error.localizedDescription);
        }];
    });
}


-(void)getGraphData {
     NSString *formatUrl = [NSString stringWithFormat:@"%@?mac=%@&timeRange=%@&powerMeterMode=%@",AGetTotalEnergyGraph, _currDevice.mac, navBar.dateModeForRequest, navBar.pmModeTypeForRequest];
    if (!_isDevice) {
        formatUrl = [NSString stringWithFormat:@"%@?groupId=%ld&timeRange=%@&powerMeterMode=%@",AGetGroupTotalEnergyGraph, (long)_currGroup.GrId, navBar.dateModeForRequest, navBar.pmModeTypeForRequest];
    }
   
    [[RequestManager sheredMenage] getJsonDataWithUrl:formatUrl
                                        andParameters:@{}
                                         isNeedCookie:NO
                                              success:^(id  _Nonnull response) {
       // NSLog(@"AGetTotalEnergyGraph %@ response = %@\n", formatUrl, response);
        self.mBarGraphV.graphDataArr = response;
        self.mBarGraphV.dateMode = self->navBar.dateMode;
        self.mBarGraphV.pmModeType = self->navBar.pmModeType;
        [self.mGraphLb setText:[BaseViewController graphType:self->navBar.dateMode]];
        [self.mBarGraphV awakeFromNib];
        
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"%@ error = %@\n", formatUrl, error.localizedDescription);
        // [self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
        
    }];
}
#pragma mark -- PM Electric
- (void)getAllDevices {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestManager sheredMenage] getJsonDataWithUrl:AGetdevdevices
                                            andParameters:@{}  isNeedCookie:NO
                                                  success:^(id response) {
            //NSLog(@"AGetdevdevices result = %@ \n",response);
            NSArray *array = [response objectForKey:@"values"];
            for (NSDictionary * dic in array) {
                if ([[dic valueForKey:@"mac"] isEqualToString:self.currDevice.mac]) {
                    if ([[dic valueForKey:@"model"] caseInsensitiveCompare:@"sensor"] == NSOrderedSame) {
                        self.currDevice.model = @"sensor";
                        [self getAirQuality];
                    }
                    if (![[dic valueForKey:@"powerMeter"] isKindOfClass:[NSNull class]]) {
                        self->powerMeter.pmMac = [dic valueForKeyPath:@"powerMeter.mac"];
                        self->navBar.isLinkedPm = YES;
                        [self getPmElectric];
                    }
                    break;
                }
            }
        } failure:^(NSError *error) {
        }];
    });
}

-(void)isOfflineElectric{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * url = [NSString stringWithFormat:@"%@%@", AGetIsOnline,  self->navBar.masterPmMac];
        [[RequestManager sheredMenage] getJsonDataWithUrl:url andParameters:@{}
                                             isNeedCookie:NO success:^(id  _Nonnull response) {
            self->powerMeter.electric.isOnline = YES;
            
        } failure:^(NSError * _Nonnull error) {
            NSString * notFound = @"not found (404)";
            if ( [error.localizedDescription containsString:notFound]) {
                self->powerMeter.electric.isOnline = NO;
            }
        }];
    });
}


-(void)getPmElectric {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * urlFormat = [NSString stringWithFormat:@"%@?pmMac=%@", AGetPmElectric, self->powerMeter.pmMac];
        [[RequestManager sheredMenage] getDataWithUrl:urlFormat success:^(id  _Nonnull response) {
           // NSLog(@"AGetPmElectric response  = %@\n", response);
            self->powerMeter.electric = [[Electric alloc] init];
            self->powerMeter.electric.acMac = [response valueForKey:@"acMac"];
            self->powerMeter.electric.graphValue = [[response valueForKey:@"graphValue"] floatValue];
            self->powerMeter.electric.kwHour = [[response valueForKey:@"kwHour"] floatValue];
            self->powerMeter.electric.priceHr = [[response valueForKey:@"priceHr"] floatValue];
            self->powerMeter.electric.pf = [[response valueForKey:@"pf"] floatValue];
            self->powerMeter.electric.pfColor = [Electric getPfColor:self->powerMeter.electric.pf];
            self->powerMeter.electric.isOnline = YES;
            [self addGuage];
            [self showGuage:YES];
            [self performSelector:@selector(isOfflineElectric)];
        } failure:^(NSError * _Nonnull error) {
           // NSLog(@"AGetNavigationDeviceMode response error  = %@\n", error.localizedDescription);
        }];
    });
}

#pragma mark: SET GROUP INFO
-(void)updateGroupObj:(Group *)group {
    NSDictionary * newDic = [Group dictionaryWithObject:group];
    self.currGroup = [[[Group alloc] init] getGroupObj:newDic];
}
-(void)setGroupInfo {
    // [self setGroupOnOff];
    [self showGuage:NO];
    [self.mGroupsNavigationV setHidden:NO];
    [self.mCustomNavigationBarV setHidden:YES];
    [self.mGroupNavLineV setHidden:NO];
    [self.mModeBtn setImage:_currGroup.groupVisual.modeBigImage forState:UIControlStateNormal];
    
    [self.mGradusBtn setTitle:[NSString stringWithFormat:@"%li",_currGroup.tgtTemp] forState:UIControlStateNormal];
    [self.mFanBtn setImage:_currGroup.groupVisual.fanImage forState:UIControlStateNormal];
    [self.mFanBtn setTitleColor:_currGroup.groupVisual.modeColor forState:UIControlStateNormal];
    [self.mGradusBtn setTitleColor:_currGroup.groupVisual.modeColor forState:UIControlStateNormal];
    [self.mGradusBtn setTitle:[NSString stringWithFormat:@"%li°", _currGroup.tgtTemp] forState:UIControlStateNormal];
    if (self.currGroup.fan == 0) {
        [self.mFanBtn setTitle:@"A"forState:UIControlStateNormal];
        [self.mFanBtn.titleLabel setFont:[UIFont fontWithName:@"Calibri" size:35.f] ];
    } else {
        [self.mFanBtn setTitle:[NSString stringWithFormat:@"%li",_currGroup.fan] forState:UIControlStateNormal];
        [self.mFanBtn.titleLabel setFont:[UIFont fontWithName:@"Calibri" size:40.f] ];
        
    }
    [self getCurrentGroupsDevices];
    // [self.mErrorBtn setHidden:!self.currDevice.isError];
}

-(void)setGroupOnOff {
    if (_currGroup.onoff == 85) {
        //[self.mGroupsNavigationV.mOnBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //  [self.mGroupsNavigationV.mOnBtn setBackgroundColor:[UIColor whiteColor]];
        //[self.mGroupsNavigationV.mOnBtn setBackgroundColor:_currGroup.groupVisual.modeColor];
        //[self.mGroupsNavigationV.mOffBtn setBackgroundColor:[UIColor whiteColor]];
        // [self.mGroupsNavigationV.mOffBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
        
        
    } else {
        [self.mGroupsNavigationV.mOffBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.mGroupsNavigationV.mOffBtn setBackgroundColor:[UIColor whiteColor]];
        // [self.mGroupsNavigationV.mOffBtn setBackgroundColor:_currGroup.groupVisual.modeColor];
        [self.mGroupsNavigationV.mOnBtn setBackgroundColor:[UIColor whiteColor]];
        [self.mGroupsNavigationV.mOnBtn setTitleColor:PlaceholderColor forState:UIControlStateNormal];
    }
}


-(void)addGuage {
    self.mFirstGuageContentV.style = [WMGaugeViewStyle3D new];
    self.mFirstGuageContentV.maxValue = 12.0;//max gauge value
    self.mFirstGuageContentV.showRangeLabels = YES;
    self.mFirstGuageContentV.rangeValues = @[@12];//graph's color range
    self.mFirstGuageContentV.rangeColors = @[[UIColor orangeColor] ];//grap's colors
//     self.mFirstGuageContentV.unitOfMeasurement = @"0.0Kw/h";
//    self.mFirstGuageContentV.showUnitOfMeasurement = YES;
    self.mFirstGuageContentV.scaleDivisionsWidth = 0.008;
    self.mFirstGuageContentV.scaleSubdivisionsWidth = 0.006;
    self.mFirstGuageContentV.rangeLabelsFontColor = [UIColor blackColor];
    self.mFirstGuageContentV.rangeLabelsWidth = 0.045;//gauge's width
    self.mFirstGuageContentV.rangeLabelsFont = [UIFont fontWithName:@"Calibri" size:0.04];
    [self gaugeUpdate];
}

-(void)gaugeUpdate
{
    self.mFirstGuageContentV.value = powerMeter.electric.graphValue;
    [self.mPfLb setHidden:NO];
    self.mPfLb.text = NSLocalizedString(@"kWh", nil);
    [self.mPfLb setTextColor:self.mWkHLb.textColor];
    
    [self.mWkHLb setText:[NSString stringWithFormat:@"%.1f%@", powerMeter.electric.kwHour, NSLocalizedString(@"kw_h", nil)]];
    [self.mHLb setText:[NSString stringWithFormat:@"%.1f%@", powerMeter.electric.priceHr, NSLocalizedString(@"$_h", nil)]];
    if (powerMeter.electric.isOnline) {
        [self.mGaugeWarningImg setHidden:YES];
        //[self.mPfLb setHidden:NO];
    } else {
        if ([_currDevice.model isEqualToString:@"sensor"]) {
           // [self.mPfLb setHidden:YES];
        }
        [self.mGaugeWarningImg setHidden:NO];
    }
}

#pragma mark:UPDATE
-(void)getCurrentGroupsDevices {
    NSString *formatUrl = [NSString stringWithFormat:@"%@/%@",AGetgroupdevices,@(_currGroup.GrId)];
    [[RequestManager sheredMenage] getJsonDataWithUrl:formatUrl
                                        andParameters:@{}
                                         isNeedCookie:NO
                                              success:^(id  _Nonnull response) {
       // NSLog(@"%@ response = %@\n", formatUrl, response);
        NSNumber *code = [response objectForKey:AResultCode];
        if (code.intValue == 0) {
            NSArray *array = [response objectForKey:AAttachment];
            [self setCurrentDevices:array];
        }
    } failure:^(NSError * _Nonnull error) {
        //NSLog(@"%@ error = %@\n", formatUrl, error.localizedDescription);
        // [self showAlert:@"Operation Failure!" ok:@"OK" cencel:@"" delegate:self];
    }];
}

-(void)setCurrentDevices:(NSArray *)dataArr {
    devicesOfGroupArr = [NSMutableArray array];
    for (int i = 0; i < dataArr.count; i++) {
        [devicesOfGroupArr addObject:[[[Devices alloc]init] getDeviceObj:[dataArr objectAtIndex:i]]];
        for (AllDevises * allDev in _allDevises) {
            if ([allDev.mac isEqualToString:[[dataArr objectAtIndex:i] valueForKey:@"mac"]] && allDev.deviceVisual.isWarning) {
                [[devicesOfGroupArr objectAtIndex:i] setIsWarning:YES];
                break;
            } else {
                [[devicesOfGroupArr objectAtIndex:i] setIsWarning:NO];
            }
        }
    }
    [self.mGroupsNavigationV setDevicesOfGroupArr:devicesOfGroupArr];
    [self.mGroupsNavigationV.mGroupsCollactionV reloadData];
}

-(void)updateGeneralInfo  {
    NSMutableDictionary *dict = [[AllDevises  dictionaryForDeviceUpdate:self.currDevice] mutableCopy];
   // NSLog(@"Dic = %@\n", dict);
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostdevsetHvac andParameters:dict success:^(id _Nonnull response) {
       // NSLog(@"response = %@\n", response);
        dispatch_async(dispatch_get_main_queue(), ^{
                    [self setDeviceInfo];
        });
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"error = %@\n", error.localizedDescription);
        //error
    }];
}
-(void)updateOnOff:(AllDevises *)currDevice on:(NSString * )on  {
    NSDictionary *dict = [AllDevises  dictionaryWithObject:currDevice];
    //NSLog(@"Dic = %@\n", dict);
    [[RequestManager sheredMenage] getDataWithUrl:[NSString stringWithFormat:@"%@/%@?on=%@", AGetdevswitchHvac, currDevice.mac, on] success:^(id  _Nonnull response) {
       // NSLog(@"response = %@\n", response);
        
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"error = %@\n", error.localizedDescription);
        
    }];
}

- (void)updateSelectedDeviceStatus {
    NSString *deviceMac = _currDevice.mac;
    //NSLog(@"updateSelectedDeviceStatus currentSelect mac: %@", deviceMac);
    NSString *formatUrl = [NSString stringWithFormat:@"%@/%@",AGetdevqueryHvac,deviceMac];
    [[RequestManager sheredMenage] getDataWithUrl:formatUrl success:^(id response) {
        NSNumber *code = [response objectForKey:@"returnCode"];
       // NSLog(@"updateSelectedDeviceStatus query status return: %i", code.intValue);
        if (code.intValue == 0) {
        } else if (code.intValue == -72) {
            [self setDeviceOnline:NO];
        } else {
            [self setDeviceOnline:YES];
        }
    } failure:^(NSError *error) {
    }];
}
-(void)updateDeviceStatus:(AllDevises *)device {
    if(device.onoff == 170) {
        device.onoff = 86;
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
 
        self.currDevice = device;
        if ([self.currDevice.deviceType isEqualToString:@"SW"]) {
            [self setSWInfo];
        } else {
            [self setDeviceInfo];
        }
    });
    
}
- (void)setDeviceOnline:(BOOL)isOpen {
}


#pragma mark: Update Group
-(void)updateGroupOnOff:(Group *)currGroup on:(NSString * )on  {
    NSDictionary * param = @{@"on": on, @"groupId": @(currGroup.GrId)};
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostgroupswitchGroup andParameters:param success:^(id _Nonnull response) {
       // NSLog(@"response = %@\n", response);
        [self updateGroupOnOffBtn:on];
        
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"error = %@\n", error.localizedDescription);
    }];
}
-(void)updateGroupOnOffBtn:(NSString *)on {
    UIButton * onOffBtn = [[UIButton alloc] init];
    NSString * title;
    if ([on isEqualToString:@"true"]) {
        onOffBtn = _mGroupsNavigationV.mOnBtn;
        title = NSLocalizedString(@"on", nil);
    } else {
        onOffBtn = _mGroupsNavigationV.mOffBtn;
        title = NSLocalizedString(@"off", nil);;
        
    }
    [onOffBtn setImage:[UIImage imageNamed:@"little_check"] forState:UIControlStateNormal];
    [onOffBtn  setTitle:@"" forState:UIControlStateNormal];
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [onOffBtn setImage:nil forState:UIControlStateNormal];
        [onOffBtn setTitle:title forState:UIControlStateNormal];
    });
}
-(void)updateGroupTemp:(Group *)currGroup  {
    NSDictionary * param = @{@"tgtTemp": @(currGroup.tgtTemp), @"groupId": @(currGroup.GrId)};
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostgroupsetGroupTgtTemp andParameters:param success:^(id _Nonnull response) {
       // NSLog(@"response = %@\n", response);
        [self updateGroupOnOffBtn:@"true"];
        
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"error = %@\n", error.localizedDescription);
    }];
}

-(void)updateGroupFan:(Group *)currGroup  {
    NSDictionary * param = @{@"fan": @(currGroup.fan), @"groupId": @(currGroup.GrId)};
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostgroupsetGroupFan andParameters:param success:^(id _Nonnull response) {
       // NSLog(@"response = %@\n", response);
        [self updateGroupOnOffBtn:@"true"];
        
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"error = %@\n", error.localizedDescription);
    }];
}

-(void)updateGroupMode:(Group *)currGroup  {
    NSDictionary * param = @{@"mode": @(currGroup.mode), @"groupId": @(currGroup.GrId)};
    [[RequestManager sheredMenage] postJsonDataWithUrl:APostgroupsetGroupMode andParameters:param success:^(id _Nonnull response) {
       // NSLog(@"response = %@\n", response);
        [self updateGroupOnOffBtn:@"true"];
    } failure:^(NSError * _Nonnull error) {
       // NSLog(@"error = %@\n", error.localizedDescription);
    }];
}

//check if dont find the device it means doesnt have communication .show warning
- (BOOL)findDeviceFromLocalData:(NSString *)mac {
    for (int i = 0; i <= _allDevises.count; i++) {
        AllDevises * dev  = [_allDevises objectAtIndex:i];
        if ([mac caseInsensitiveCompare:dev.mac] == NSOrderedSame) {
            return  YES;
        } else {
            if ([_currDevice.mac isEqualToString:dev.mac]) {
                _currDevice.deviceVisual.isWarning = YES;
            }
            [dev.deviceVisual setIsWarning:YES];
            [_allDevises replaceObjectAtIndex:i withObject:dev];
        }
    }
    return NO;
}

#pragma mark: ACTIONS
-(void) clock {
    //check if Admin don't login
     if ([ACommonTools getBoolValueForKey:KeyIsUnLockMenu]) {
        [self pushViewControllerWithIdentifier:@"TimerViewController" controller:self];
     }
}

- (IBAction)airQuality:(UIButton *)sender {
}
- (IBAction)turnOnOff:(UIButton *)sender {
    if (!self.currDevice.deviceVisual.isWarning) {
        if (self.currDevice.onoff == 85 ||  self.currDevice.onoff == 55) {
            self.currDevice.onoff = 86;
            [self updateOnOff:self.currDevice on:@"false"];
            [self.mOffBackgrV setHidden:NO];
        } else {
            self.currDevice.onoff = 85;
            [self updateOnOff:self.currDevice on:@"true"];
            [self.mOffBackgrV setHidden:YES];
        }
        [self.mErrorBtn setHidden:!_currDevice.deviceVisual.isError];
    }
}


- (IBAction)error:(UIButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    OpenMessageViewController * vc = [storyboard   instantiateViewControllerWithIdentifier:@"OpenMessageViewController"];
    vc.currErrMessage = _currDevice.errorMessage;
    //vc.deviceMac = [_currDevice mac];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)temperature:(UIButton *)sender {
    
    
}
- (IBAction)mode:(UIButton *)sender {
    UIImage *img = [sender imageForState:UIControlStateNormal];
//NSLog(@"[DevisesModeBigImgList indexOfObject:img] = %i\n", [DevisesModeBigImgList indexOfObject:img]);
    if ([DevisesModeBigImgList indexOfObject:img] >= 0 && [DevisesModeBigImgList indexOfObject:img] <= DevisesModeBigImgList.count-1) {
        [self.mModePickV selectRow:[DevisesModeBigImgList indexOfObject:img] inComponent:0 animated:NO];
    }
    [sender setHidden:YES];
    [self.mModePickV setHidden:NO];
}

- (IBAction)fan:(UIButton *)sender {
     if ([FanModesList indexOfObject:sender.titleLabel.text] >= 0 && [FanModesList indexOfObject:sender.titleLabel.text] <= FanModesList.count-1) {
         [self.mFanPickV selectRow:[FanModesList indexOfObject:sender.titleLabel.text]inComponent:0 animated:NO];
     }
    [sender setHidden:YES];
    [self.mFanPickV setHidden:NO];
}
- (IBAction)gradus:(UIButton *)sender {
     if ([temperatureList indexOfObject:sender.titleLabel.text] >= 0 && [temperatureList indexOfObject:sender.titleLabel.text] <= temperatureList.count-1) {
         [self.mTemperaturePickV selectRow:[temperatureList indexOfObject:sender.titleLabel.text]inComponent:0 animated:NO];
     }
    [sender setHidden:YES];
    [self.mTemperaturePickV setHidden:NO];
}

#pragma mark: EfficencyRate Delegate
-(void) handlerPowerMeter {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController * vc = [storyboard   instantiateViewControllerWithIdentifier:@"PowerMeterViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)updateNavigationObj:(NavigationBar *)navBarObj {
   // if (_currDevice) {
        navBar = navBarObj;
        [self performSelectorInBackground:@selector(getGraphData) withObject:nil];
   // }
}
#pragma mark: PickerView Delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    [pickerView.subviews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
        subview.hidden = (CGRectGetHeight(subview.frame) < 1.0);
    }];
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView.tag == 1) {
        return [temperatureList count];
    } else if (pickerView.tag == 2) {
        return [FanModesList count];
    } else if (pickerView.tag == 3) {
        return [DevisesModeBigImgList count];
    } else if (pickerView.tag == 4) {
        return [PriceList count];
    } else  {
        return [TimerWithoutOnlineList count];
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView.tag == 1) {
        return [temperatureList objectAtIndex:row];
    } else if (pickerView.tag == 2) {
        return [FanModesList objectAtIndex:row];
    } else if (pickerView.tag == 3) {
        return @"";
    } else if (pickerView.tag == 4) {
        return [PriceList objectAtIndex:row];
    }  else
        return [TimerWithoutOnlineList objectAtIndex:row];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    NSInteger currentRow = [pickerView selectedRowInComponent:0];
    if (pickerView.tag == 1) {//Temperature
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150, 90)];
        label.font = [UIFont fontWithName:@"Calibri" size:80.f];
        label.textColor = GrayTitleColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.text = [temperatureList objectAtIndex:row];
        if (row == currentRow) {
            if (self.isDevice)  {
                [label setTextColor:self.currDevice.deviceVisual.modeColor];
            } else {
                [label setTextColor:self.currGroup.groupVisual.modeColor];
            }
        }
        return label;
    } else if (pickerView.tag == 2) {// Fan
        UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 50.f)];
        [btn setTitle:FanModesList[row] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"gray_fan"] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:@"Calibri" size:40.f];
        if([FanModesList[row] isEqual:@"A"]) {
            btn.titleLabel.font = [UIFont fontWithName:@"Calibri" size:35.f];
        }
        [btn setTitleColor:GrayDevColor forState:UIControlStateNormal];
        [pickerView addSubview:btn];
        if (row == currentRow) {
            if (self.isDevice)  {
                [btn setTitleColor:self.currDevice.deviceVisual.modeColor forState:UIControlStateNormal];
                [btn setImage:self.currDevice.deviceVisual.fanImage forState:UIControlStateNormal];
            } else {
                [btn setTitleColor:self.currGroup.groupVisual.modeColor forState:UIControlStateNormal];
                [btn setImage:self.currGroup.groupVisual.fanImage forState:UIControlStateNormal];
            }
            //[btn setTitleColor:self.mFanBtn.titleLabel.textColor forState:UIControlStateNormal];
           // [btn setImage:self.mFanBtn.imageView.image forState:UIControlStateNormal];
        }
        return btn;
    } else //if (pickerView.tag == 3){// Mode
    {
        UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 50.f)];
        if (row == 0) {
            [btn setImage: [UIImage imageNamed:@"auto_big"] forState:UIControlStateNormal];
        } else {
            [btn setImage:[DevisesModeBigImgList objectAtIndex:row]
                 forState:UIControlStateNormal];
        }
       // NSLog(@"image row = %i\n", row);
        [pickerView addSubview:btn];
        if (row == currentRow) {
            [btn setImage:self.mModeBtn.imageView.image forState:UIControlStateNormal];
        }
        return btn;
    }
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    if (pickerView.tag == 1)  {
        return  90;
    } else if (pickerView.tag == 5 || pickerView.tag == 4) {
        return 30;
    }
    return  45;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    NSLog(@"CALL didSelectRow!!!!!\n");
    if (pickerView.tag == 1) { //Temperature
        UILabel *labelSelected = (UILabel*)[pickerView viewForRow:row forComponent:component];
        if (self.isDevice)  {
            [labelSelected setTextColor:self.currDevice.deviceVisual.modeColor];
        } else {
            [labelSelected setTextColor:self.currGroup.groupVisual.modeColor];
        }
        [self.mGradusBtn setHidden:NO];
        [self.mTemperaturePickV setHidden:YES];
        [self.mGradusBtn setTitle:[temperatureList objectAtIndex:row] forState:UIControlStateNormal];
        NSInteger temp = [[temperatureList objectAtIndex:row] integerValue];
        if (self.isDevice) {
            [self.currDevice setTgtTemp:temp];
        } else {
            [self.currGroup setTgtTemp:temp];
            [self updateGroupTemp:self.currGroup ];
        }
        
    } else if (pickerView.tag == 2) {//Fan
        [self.mFanBtn setHidden:NO];
        [self.mFanPickV setHidden:YES];
        [self.mFanBtn setTitle:[FanModesList objectAtIndex:row] forState:UIControlStateNormal];
        NSInteger fan;
        if ([self.mFanBtn.titleLabel.text isEqualToString:@"A"]) {
            fan = 0;
        } else {
            fan = (long)[[FanModesList objectAtIndex:row] integerValue];
        }
        if (self.isDevice) {
            [self.currDevice setFan:fan];
        } else {
            [self.currGroup setFan:fan];
            [self updateGroupFan:self.currGroup];
        }
    } else if (pickerView.tag == 3){ //Mode
        [self.mModeBtn setHidden:NO];
        [self.mModePickV setHidden:YES];
        [self.mModeBtn setImage:[DevisesModeBigImgList objectAtIndex:row] forState:UIControlStateNormal];
        if (self.isDevice) {
            [self.currDevice setMode:[BaseViewController getModeValue:(int)row]];
            self.currDevice.deviceVisual.modeBigImage = [DevisesModeBigImgList objectAtIndex:row];
        } else {
            [self.currGroup setMode:[BaseViewController getModeValue:(int)row]];
            self.currGroup.groupVisual.modeBigImage = [DevisesModeBigImgList objectAtIndex:row];
            [self updateGroupMode:_currGroup];
        }
    }
        if (self.isDevice) {
            [self updateDeviceObj:self.currDevice];
            [self updateGeneralInfo];
        }
        else {
            [self updateGroupObj:self.currGroup];
            [self setGroupInfo];
        }
}

#pragma mark : Group NAvigation View Delegate
-(void)handlerOnOff:(BOOL)isOn {
    if (isOn) {
        _currGroup.onoff = 85;
        [self updateGroupOnOff:self.currGroup on:@"true"];
    } else {
        _currGroup.onoff = 86;
        [self updateGroupOnOff:self.currGroup on:@"false"];
    }
}

-(void)updateGroupDeviceWarning:(NSDictionary *)dic {
    for (int i = 0; i < devicesOfGroupArr.count; i++) {
        if ([[[devicesOfGroupArr objectAtIndex:i] mac] isEqualToString:[dic valueForKey:@"mac"]]  && [[[devicesOfGroupArr objectAtIndex:i] deviceVisual] isWarning]) {
            [[[devicesOfGroupArr objectAtIndex:i] deviceVisual] setIsWarning:NO ];
            break;
        }
    }
    [self.mGroupsNavigationV setDevicesOfGroupArr:devicesOfGroupArr];
    [self.mGroupsNavigationV.mGroupsCollactionV reloadData];
}


#pragma mark - Notification
-(void) updateByFeedback:(NSNotification *) notification
{
    NSDictionary * dict = notification.userInfo;
  if (dict != nil) {
       // NSLog(@"webSocketreceiveMessage = %@",dict);
        if (self.isDevice) {
            if ([dict objectForKey:DeviceStatus]) {
                NSDictionary * currDic = [dict objectForKey:DeviceStatus];
                if ([[currDic valueForKey:@"mac"] isEqual:_currDevice.mac]) {
                    
                     if ([[currDic valueForKey:@"onoff"] isEqual:@86] || [[currDic valueForKey:@"onoff"] isEqual:@170]) {
                        [_currDevice setOnoff:86];
                         [_currDevice setEnvTempShow:[[currDic valueForKey:@"envTempShow"] integerValue]];
                        [self updateDeviceStatus:_currDevice];
                        
                    } else {
                        AllDevises * device = [[[AllDevises alloc] init] getDeviceObj:currDic];
                        device.deviceVisual.isError = _currDevice.deviceVisual.isError;
                        device.deviceVisual.isTimer = _currDevice.deviceVisual.isTimer;
                        if ( [currDic objectForKey:@"errorCode"]) {
                         device.deviceVisual.isError = YES;
                         device.errorMessage = [[ErrorMessage alloc] getErrorMessageObj:currDic];
                        }
                        [self updateDeviceStatus:device];
                    }
                }
            }
            else if ([[dict valueForKey:@"type"] isEqual:@"gas"] && [[dict valueForKey:@"mac"] isEqual:_currDevice.mac]) {//Air Quality
                    [airQuality setTypeGas:[dict valueForKey:@"type"]];
                    [airQuality setNumberGas:[[dict valueForKey:@"number"]integerValue]];
                    [self setAirQuality];
            }//speedometer
            else if ([dict valueForKey:@"graphValue"] && [[dict valueForKey:@"acMac"] isEqual:_currDevice.mac] ) {
                powerMeter.electric.graphValue = [[dict valueForKey:@"graphValue"] floatValue];
                powerMeter.electric.pf = [[dict valueForKey:@"pf"] floatValue];
                powerMeter.electric.kwHour = [[dict valueForKey:@"kwHour"] floatValue];
                powerMeter.electric.priceHr = [[dict valueForKey:@"priceHr"] floatValue];
                powerMeter.electric.pfColor = [Electric getPfColor:powerMeter.electric.pf];
                powerMeter.electric.isOnline = YES;
                [self addGuage];
                
            }
            else if ([dict valueForKey:@"commandMode"] && [dict valueForKey:@"status"] && [[dict valueForKeyPath:@"device.mac"] isEqual:_currDevice.mac]) {
                motionSensor.commandMode = [dict valueForKey:@"commandMode"];
                motionSensor.status = [dict valueForKey:@"status"];
                motionSensor.workingTime = [NSString stringWithFormat:@"%@%@",[dict valueForKey:@"workingTime"], [BaseViewController getTimerPram:[dict valueForKey:@"timeParam"]]];
                [self setMotionSensor];
                
            } else if ([dict valueForKey:@"workingTime"]  && [[dict valueForKey:@"mac"] isEqual:_currDevice.mac]) {
                motionSensor.workingTime = [NSString stringWithFormat:@"%@%@",[dict valueForKey:@"workingTime"], [BaseViewController getTimerPram:[dict valueForKey:@"timeParam"]]];
                [self setMotionSensor];
                
            }
        } else { //Group
            if ([dict objectForKey:DeviceStatus]) {
                NSDictionary * currDic = [dict objectForKey:DeviceStatus];
                [self updateGroupDeviceWarning:currDic];
            }
        }
    }
}
@end



