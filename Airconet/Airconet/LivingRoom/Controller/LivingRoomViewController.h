//
//  LivingRoomViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 2/4/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "BaseViewController.h"
#import "AllDevises.h"
#import "Group.h"

NS_ASSUME_NONNULL_BEGIN

@interface LivingRoomViewController : BaseViewController
@property (assign, nonatomic)BOOL isDevice;
@property (strong, nonatomic) NSMutableArray * allDevises;
@property (strong, nonatomic) AllDevises * currDevice;
@property (strong, nonatomic) Group * currGroup;

@end

NS_ASSUME_NONNULL_END
