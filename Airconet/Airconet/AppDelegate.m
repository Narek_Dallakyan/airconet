//
//  AppDelegate.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/23/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <UserNotifications/UserNotifications.h>

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "ACommonTools.h"
#import "APublicDefine.h"
#import "RequestManager.h"
#import "QXBaseNetWorkManager.h"
#import "MainPageViewController.h"
#import "MainViewController.h"
#import "LeftViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "BaseViewController.h"
#import "QXBaseNetWorkManager.h"
#import "Firebase.h"


// define macro
 #define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
 #define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface AppDelegate () <UNUserNotificationCenterDelegate, FIRMessagingDelegate>  {
    CLLocationManager *_locationManagerSystem;
    BOOL isOpenNotification;
    BOOL loginStart;


}
@property (nonatomic, strong) LoginViewController * loginvc;


@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [FIRApp configure];//n
    [FIRMessaging messaging].delegate = self; //n
    [self checkAdminLock];
    UIStoryboard *mainSB= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.loginvc = mainSB.instantiateInitialViewController;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
       NSString *documentsDirectory = [paths objectAtIndex:0];
       NSString *path = [documentsDirectory stringByAppendingPathComponent:@"logfile.txt"];
    if (path.length > 0 && [ACommonTools getBoolValueForKey:@"isSentLogfile"]) {
         NSError *error;
         [fileManager removeItemAtPath:path error:&error];
        [ACommonTools setBoolValue:NO forKey:@"isSentLogfile"];
    }
    
      [UNUserNotificationCenter currentNotificationCenter].delegate = self;
      UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
          UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
      [[UNUserNotificationCenter currentNotificationCenter]
          requestAuthorizationWithOptions:authOptions
          completionHandler:^(BOOL granted, NSError * _Nullable error) {
          }];
    
    [application registerForRemoteNotifications]; //n
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self userLocationAuth];
    }); //n

//    if ( ![ACommonTools getObjectValueForKey:@"firebaseToken"]) {
//            if ([ACommonTools getObjectValueForKey:AKeyCurrentUserName]) {
//                [self startLogin];//n
//            } else {
//                self.window.rootViewController = self.loginvc;
//            }
//    }
    
    return YES;
}


- (void)userLocationAuth {
    if (![self getUserLocationAuth]) {
        _locationManagerSystem = [[CLLocationManager alloc]init];
        [_locationManagerSystem requestWhenInUseAuthorization];
    }
}
- (BOOL)getUserLocationAuth {
    BOOL result = NO;
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusNotDetermined:
            break;
        case kCLAuthorizationStatusRestricted:
            break;
        case kCLAuthorizationStatusDenied:
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
            result = YES;
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            result = YES;
            break;
            
        default:
            break;
    }
    return result;
}
#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options  API_AVAILABLE(ios(13.0)){
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];

}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions  API_AVAILABLE(ios(13.0)){
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}

- (void)startLogin {
    loginStart = YES;
    __block BOOL finish = NO;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:[ACommonTools getObjectValueForKey:AKeyCurrentUserName] forKey:@"username"];
    [dict setValue:[ACommonTools getObjectValueForKey:AKeyCurrentUserPwd] forKey:@"password"];
    [dict setValue:@"true" forKey:@"remember-me"];
    [dict setValue:@"fasle" forKey:@"isServer-me"];
    [dict setValue:@"" forKey:@"token"];
    [dict setValue:@"tRue" forKey:@"isDebug"];
    [dict setValue:@"IOS" forKey:@"os_type"];
    if (self.firebaseToken.length > 0) {
        [dict setValue:self.firebaseToken forKey:@"firebase-token"];
    }

    [[RequestManager sheredMenage] postJsonDataWithUrl:APostjspringsecuritycheck
                                         andParameters:dict
                                               success:^(id response) {
        [RequestManager setCookie];
        [ACommonTools setBoolValue:YES forKey:AKeyLastLoginSuccess];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"MainPageViewController"]]];
        
        MainViewController *mainViewController =  [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
        mainViewController.rootViewController = navigationController;
        NSUInteger val = 0;
        [mainViewController setupWithType:val];
        if (@available(iOS 13.0, *)) {
            self.window =  [BaseViewController keyWindow];
        }
        self.window.rootViewController = mainViewController;
        finish = YES;
    } failure:^(NSError *error) {
        NSLog(@"AppDelegate::startLogin falied: %@",error);
        self.window.rootViewController = self.loginvc;
        [ACommonTools setBoolValue:NO forKey:AKeyLastLoginSuccess];
        finish = YES;
    }];
}

-(void)checkAdminLock {
    //if Admin unlock and remember, keep admin unlock else lock admin
    if ([ACommonTools getBoolValueForKey:KeyIsAdminLock] && ![ACommonTools getBoolValueForKey:KeyRememberAdmin]) {
        NSLog(@"KeyIsUnLockMenu ======  NO\n")
        [ACommonTools setBoolValue:NO forKey:KeyIsUnLockMenu];
    } else{
        [ACommonTools setBoolValue:YES forKey:KeyIsUnLockMenu];
        NSLog(@"KeyIsUnLockMenu ====== YES\n");

    }
}

#pragma mark: Notification

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{

    NSLog(@"deviceToken: %@", deviceToken);
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSLog(@"deviceToken tocken: %@", token);
    [[FIRMessaging messaging] setAPNSToken:deviceToken];
    [FIRMessaging messaging].autoInitEnabled = YES;
    
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                        NSError * _Nullable error) {
      if (error != nil) {
       // NSLog(@"Error fetching remote instance ID: %@", error);
      } else {
        NSLog(@"Remote instance ID token: %@", result.token);
        NSString* message =
          [NSString stringWithFormat:@"Remote InstanceID token: %@", result.token];
        self.firebaseToken = result.token;
          [ACommonTools setObjectValue:self.firebaseToken forKey:@"firebaseToken"];
          if ([ACommonTools getObjectValueForKey:AKeyCurrentUserName]  && [ACommonTools getObjectValueForKey:AKeyCurrentUserPwd]) {
              if (!self.notificationDic) {
                  [self startLogin];
              }
          } else {
              self.window.rootViewController = self.loginvc;
          }
          NSLog(@"FierBase tocken = %@\n", message);
      }
    }];
    
}

-(void)applicationDidBecomeActive:(UIApplication *)application {
    NSLog(@"applicationDidBecomeActive\n");
            [[RequestManager sheredMenage] getJsonDataWithUrl:AGetCurrentUserId
                                                   andParameters:@{}
                                                    isNeedCookie:YES
                                                         success:^(id response) {
                   [ACommonTools setBoolValue:YES forKey:AKeyLastLoginSuccess];
                if (self.notificationDic || [self isReachThreeHours] /*|| !self->loginStart*/) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
                [navigationController setViewControllers:@[[storyboard instantiateViewControllerWithIdentifier:@"MainPageViewController"]]];
                
                MainViewController *mainViewController =  [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
                mainViewController.rootViewController = navigationController;
                NSUInteger val = 0;
                [mainViewController setupWithType:val];
                if (@available(iOS 13.0, *)) {
                    self.window =  [BaseViewController keyWindow];
                }
                self.window.rootViewController = mainViewController;
            }
               } failure:^(NSError *error) {
                   NSLog(@"AppDelegate::startLogin falied: %@",error);
                   [ACommonTools setBoolValue:NO forKey:AKeyLastLoginSuccess];
               }];

}

-(void)applicationDidEnterBackground:(UIApplication *)application {
  NSLog(@"applicationDidEnterBackground\n");
    NSDate * now = [NSDate date];
    [ACommonTools setObjectValue:now forKey:@"EnterBackgroundDate"];
}

-(BOOL)isReachThreeHours {
    if ([ACommonTools getObjectValueForKey:@"EnterBackgroundDate"]) {
         NSDate * now = [NSDate date];
         NSDate * date = [ACommonTools getObjectValueForKey:@"EnterBackgroundDate"];
         
        NSTimeInterval secondsInEightHours = 3 * 60 * 60;
         NSDate *dateThreeHoursAhead = [date dateByAddingTimeInterval:secondsInEightHours];
         NSComparisonResult result = [now compare:dateThreeHoursAhead];
         if(result == NSOrderedDescending) {
             NSLog(@"dateThreeHoursAhead is in the past");
             return YES;
         }
         return NO;
    }
    return NO;
}


-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    NSLog(@"Notification userInfo = %@\n", userInfo);
}


// Receive displayed notifications for iOS 10 devices.
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
  NSDictionary *userInfo = notification.request.content.userInfo;

  // Print full message.
  NSLog(@"%@", userInfo);
  // Change this to your preferred presentation option
  completionHandler(UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionAlert);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler {
  NSDictionary *userInfo = response.notification.request.content.userInfo;
// Print full message.
  NSLog(@"%@", userInfo);
    self.notificationDic = userInfo;
  completionHandler();
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
      NSLog(@"FCM registration token: %@", fcmToken);
    NSLog(@"FCM messaging messaging: %@", messaging);

        // Notify about received token.
        NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
        [[NSNotificationCenter defaultCenter] postNotificationName:
         @"FCMToken" object:nil userInfo:dataDict];
       
}

- (NSString *)hexadecimalStringFromData:(NSData *)deviceToken {
  NSUInteger dataLength = deviceToken.length;
  if (dataLength == 0) {
    return nil;
  }

  const unsigned char *dataBuffer = (const unsigned char *)deviceToken.bytes;
  NSMutableString *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
  for (NSInteger index = 0; index < dataLength; ++index) {
    [hexString appendFormat:@"%02x", dataBuffer[index]];
  }
  return [hexString copy];
}

@end


