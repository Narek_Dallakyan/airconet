//
//  ACommonTools.m
//  iAirCon
//
//  Created by Elvis on 16/6/30.
//  Copyright © 2016年 Elvis. All rights reserved.
//

#import "ACommonTools.h"
#import <SystemConfiguration/CaptiveNetwork.h>

@interface ACommonTools ()

@end

@implementation ACommonTools

+ (void)setObjectValue:(id)obj forKey:(NSString *)key
{
    [self removeValueForKey:key];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:obj forKey:key];
    [userDefault synchronize];
}

+ (id)getObjectValueForKey:(NSString *)key
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    id value = [userDefault objectForKey:key];
    return value;
}

+ (void)setBoolValue:(BOOL)obj forKey:(NSString *)key
{
    [self removeValueForKey:key];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:obj forKey:key];
    [userDefault synchronize];
}

+ (BOOL)getBoolValueForKey:(NSString *)key
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    BOOL value = [userDefault boolForKey:key];
    return value;
}

+ (void)removeValueForKey:(NSString *)key
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if([userDefault valueForKey:key]) {
        [userDefault removeObjectForKey:key];
        [userDefault synchronize];
    }
}

+ (id)getCustomObjectForKey:(NSString *)key
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData * dataobject = [userDefault objectForKey:key];
   // id object = [NSKeyedUnarchiver unarchiveObjectWithData:dataobject];
    NSError *error = nil;
    id object = [NSKeyedUnarchiver unarchivedObjectOfClass:[NSString class] fromData:dataobject error:&error];
    return object;
}

+ (NSString *)getWifiName
{
    NSString *wifiName = nil;
    CFArrayRef wifiInterfaces = CNCopySupportedInterfaces();
    if (!wifiInterfaces) {
        return nil;
    }
    
    NSArray *interfaces = (__bridge NSArray *)wifiInterfaces;
    for (NSString *interfaceName in interfaces)
    {
        CFDictionaryRef dictRef = CNCopyCurrentNetworkInfo((__bridge CFStringRef)(interfaceName));
        if (dictRef) {
            NSDictionary *networkInfo = (__bridge NSDictionary *)dictRef;
           // NSLog(@"network info -> %@", networkInfo);
            wifiName = [networkInfo objectForKey:(__bridge NSString *)kCNNetworkInfoKeySSID];
            CFRelease(dictRef);
        }
    }
    
    CFRelease(wifiInterfaces);
    return wifiName;
}

+ (NSString *)getWifiMacIp
{
    NSString *wifiSSID = @"MOBILE_DATA";
    CFArrayRef wifiInterfaces = CNCopySupportedInterfaces();
    if (!wifiInterfaces) {
        return nil;
    }
    
    NSArray *interfaces = (__bridge NSArray *)wifiInterfaces;
    for (NSString *interfaceName in interfaces)
    {
        CFDictionaryRef dictRef = CNCopyCurrentNetworkInfo((__bridge CFStringRef)(interfaceName));
        if (dictRef) {
            NSDictionary *networkInfo = (__bridge NSDictionary *)dictRef;
           // NSLog(@"network info -> %@", networkInfo);
            NSString *wifiMacIp = [networkInfo objectForKey:(__bridge NSString *)kCNNetworkInfoKeyBSSID];
            CFRelease(dictRef);
            
            NSArray *listItems = [wifiMacIp componentsSeparatedByString:@":"];
            
            wifiSSID = [[NSString alloc] init];
            int i = 0;
            for (NSString * xx in listItems) {
               // NSLog(@"wifiMacIp number: %@", xx);
                if(xx.length < 2)
                    wifiSSID = [wifiSSID stringByAppendingString:@"0"];
                
                wifiSSID = [wifiSSID stringByAppendingString:xx];
                if(i++ < 5)
                wifiSSID = [wifiSSID stringByAppendingString:@":"];
            }
        }
    }
    
    CFRelease(wifiInterfaces);
    return wifiSSID;
}

+ (void) runAfterDelay: (void(^)(void))callback forTotalSeconds: (double)delayInSeconds{
    
//    (void(^)(void)) *task = dispatch_block_create(DISPATCH_BLOCK_BARRIER, { print("output after 5 seconds") })
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if(callback){
            callback();
        }
    });
}

+ (dispatch_cancelable_block_t) cancelableDelayedRun: (void(^)(void))callback forTotalSeconds: (double)delayInSeconds{
    
    if (callback == nil)
        return nil;
    
    // First we have to create a new dispatch_cancelable_block_t and we also need to copy the block given (if you want more explanations about the __block storage type, read this: https://developer.apple.com/library/ios/documentation/cocoa/conceptual/Blocks/Articles/bxVariables.html#//apple_ref/doc/uid/TP40007502-CH6-SW6
    __block dispatch_cancelable_block_t cancelableBlock = nil;
    __block dispatch_block_t originalCallback = [callback copy];
    
    // This block will be executed in NOW() + delay
    dispatch_cancelable_block_t delayBlock = ^(BOOL cancel){
        if (cancel == NO && originalCallback)
            dispatch_async(dispatch_get_main_queue(), originalCallback);
        
        // We don't want to hold any objects in the memory
        originalCallback = nil;
        cancelableBlock = nil;
    };
    
    cancelableBlock = [delayBlock copy];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        // We are now in the future (NOW() + delay). It means the block hasn't been canceled so we can execute it
        if (cancelableBlock)
            cancelableBlock(NO);
    });
    
    return cancelableBlock;
}
+ (void)cancelDelayedRun: (dispatch_cancelable_block_t)callback{
    if (callback == nil)
        return;
    callback(YES);
}

static NSString * _currentWifiSSID;
+ (NSString *)currentWifiSSID { return _currentWifiSSID; }
+ (void)setCurrentWifiSSID:(NSString *)currentWifiSSID { _currentWifiSSID = currentWifiSSID; }

@end
