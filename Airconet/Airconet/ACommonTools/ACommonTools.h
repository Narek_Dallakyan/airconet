//
//  ACommonTools.h
//  iAirCon
//
//  Created by Elvis on 16/6/30.
//  Copyright © 2016年 Elvis. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^dispatch_cancelable_block_t)(BOOL cancel);

@interface ACommonTools : NSObject

/*
 * UserDefault存储,不支持自定义对象 */
+ (void)setObjectValue:(id)obj forKey:(NSString *)key;

+ (id)getObjectValueForKey:(NSString *)key;

+ (void)setBoolValue:(BOOL)obj forKey:(NSString *)key;

+ (BOOL)getBoolValueForKey:(NSString *)key;

+ (void)removeValueForKey:(NSString *)key;

+ (void) runAfterDelay: (void(^)(void))callback forTotalSeconds: (double)delayInSeconds;

//可以取消的延迟执行
+ (dispatch_cancelable_block_t) cancelableDelayedRun: (void(^)(void))callback forTotalSeconds: (double)delayInSeconds;

//取消延迟执行
//+ (void)cancelableDelayedRun: (dispatch_cancelable_block_t)callback forTotalSeconds: (double)delayInSeconds;

// 获取当前wifi名称
+ (NSString *)getWifiName;
// 获取当前wifi Mac
+ (NSString *)getWifiMacIp;

@property (class) NSString *currentWifiSSID;

@end
