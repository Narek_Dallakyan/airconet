//
//  WaitForConnectView.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/28/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "WaitForConnectView.h"

@implementation WaitForConnectView

-(void)awakeFromNib {
    [super awakeFromNib];
    self.mActivityInd.transform =  CGAffineTransformMakeScale(2.7, 2.7);
}

@end
