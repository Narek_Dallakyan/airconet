//
//  AddDeviceView.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/28/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol AAddACViewDelegate <NSObject>

- (void)scanClicked;
- (void)scanDidEnd;

@end

@interface AddDeviceView : UIView
@property (weak, nonatomic) IBOutlet UILabel *mFirstLb;
@property (weak, nonatomic) IBOutlet UILabel *mSecondLb;
@property (weak, nonatomic) IBOutlet UILabel *mThirdLb;
@property (weak, nonatomic) IBOutlet UITextField *mRouterSSIDTxtRl;
@property (weak, nonatomic) IBOutlet UITextField *mRouterPasswordTxtFl;
@property (weak, nonatomic) IBOutlet UIButton *mScanBtn;
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, weak) id<AAddACViewDelegate> delegate;

@end


NS_ASSUME_NONNULL_END
