//
//  AddDeviceView.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/28/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "AddDeviceView.h"
#import "APublicDefine.h"
#import "UITextField+Content.h"

@interface AddDeviceView ()

@property (nonatomic, assign) int autoCode;

@end
@implementation AddDeviceView

-(void)awakeFromNib {
    
    [self.mRouterSSIDTxtRl setBlueBorderStyle];
    [self.mRouterPasswordTxtFl setBlueBorderStyle];
    [self.mRouterSSIDTxtRl setImageInLeftView:[UIImage imageNamed:@"router"]];
    [self.mRouterPasswordTxtFl setImageInLeftView:[UIImage imageNamed:@"password"]];
    [self.mRouterSSIDTxtRl setPlaceholderColor:PlaceholderColor text:self.mRouterSSIDTxtRl.placeholder];
    [self.mRouterPasswordTxtFl setPlaceholderColor:PlaceholderColor text:self.mRouterPasswordTxtFl.placeholder];
    [self.mRouterSSIDTxtRl setRightPadding];
    [self addButtonToRightViewTextField];
    [super awakeFromNib];
}

-(void)addButtonToRightViewTextField {
    UIButton * secureBtn = [[UIButton alloc] init];
       [secureBtn addTarget:self action:@selector(secureBtnAction:) forControlEvents:UIControlEventTouchUpInside];
       
       [self.mRouterPasswordTxtFl setButtonInRightView:secureBtn buttonImg:[UIImage imageNamed: @"not_visible"]];
}

-(void)secureBtnAction:(UIButton *)btn {
    if ([btn.imageView.image isEqual:[UIImage imageNamed:@"not_visible"]]) {
        [btn setImage:[UIImage imageNamed:@"visible"] forState:UIControlStateNormal];
        [self.mRouterPasswordTxtFl setSecureTextEntry:NO];
    } else {
        [btn setImage:[UIImage imageNamed:@"not_visible"] forState:UIControlStateNormal];
        [self.mRouterPasswordTxtFl setSecureTextEntry:YES];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.mRouterSSIDTxtRl) {
        textField.returnKeyType = UIReturnKeyNext;
    } else if (textField == self.mRouterPasswordTxtFl) {
        textField.returnKeyType = UIReturnKeyDone;
    }
    textField.text = @"";
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.mRouterSSIDTxtRl) {
        [self.mRouterPasswordTxtFl becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}
-(void)textFieldDidChangeSelection:(UITextField *)textField {
    if ([textField isEqual:self.mRouterSSIDTxtRl]) {
        if (textField.text.length == 0) {
            [textField setRightPadding];
        } else
            [textField deletRightPadding];
    }
}

- (IBAction)scanAction:(UIButton *)sender {
    [self.mRouterPasswordTxtFl  resignFirstResponder];
    [self.mRouterSSIDTxtRl  resignFirstResponder];

    if (self.mRouterPasswordTxtFl.text.length > 0 && self.mRouterSSIDTxtRl.text.length > 0){
          double delayInSeconds = 35.0;
              dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
              dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                 [self.delegate scanDidEnd];
              });
    }
    [self.delegate scanClicked];
}
@end
