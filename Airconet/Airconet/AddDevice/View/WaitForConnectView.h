//
//  WaitForConnectView.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/28/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WaitForConnectView : UIView
@property (weak, nonatomic) IBOutlet UILabel * mWaitForConnLb;
@property (weak, nonatomic) IBOutlet UILabel * mSecondLb;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * mActivityInd;
@end

NS_ASSUME_NONNULL_END
