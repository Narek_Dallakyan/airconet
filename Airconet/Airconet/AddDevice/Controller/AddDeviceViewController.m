//
//  AddDeviceViewController.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/28/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//


// "[isSuc: YES,isCancelled: NO,bssid: cc50e33b0b41,inetAddress: 192.168.1.36]"

#import "AddDeviceViewController.h"
#import "WaitForConnectView.h"
#import "AddDeviceView.h"
#import "APublicDefine.h"
#import "AVersionConfig.h"
#import "GCDAsyncUdpSocket.h"
#import "ACommonTools.h"
#import "RequestManager.h"
#import "Devices.h"
#import "DeviceErrorView.h"
#import "DeviceSuccessfulyAddedViewController.h"

#import "ESPTouchTask.h"
#import "ESPTouchResult.h"
#import "ESP_NetUtil.h"
#import "ESPTouchDelegate.h"
#import "AFNetworking.h"
#import "Reachability.h"
#import "ESP_ByteUtil.h"


#import "ESPTools.h"
#import <MessageUI/MFMailComposeViewController.h>

#import "DeviceSuccess.h"
#import "SRWebSocket.h"


@interface EspTouchDelegateImpl : NSObject<ESPTouchDelegate>

@end

@implementation EspTouchDelegateImpl

-(void) onEsptouchResultAddedWithResult: (ESPTouchResult *) result
{
    NSLog(@"EspTouchDelegateImpl onEsptouchResultAddedWithResult bssid: %@", result.bssid);
    dispatch_async(dispatch_get_main_queue(), ^{
    });
}

@end



@interface AddDeviceViewController ()<AAddACViewDelegate, GCDAsyncUdpSocketDelegate, CustomAlertDelegate, DeviceErrorViewDelegate> {
    NSMutableArray * modulsArray;
    NSMutableArray * devicesArr;
    BOOL isResiveModule;
    int newDreviceCounter;
    DeviceSuccess * deviceSuccess;
    int scanSeconds;
    int sreachCounter;
}

@property (weak, nonatomic) IBOutlet UIScrollView *mScrollV;
@property (weak, nonatomic) IBOutlet WaitForConnectView *mWaitForConnectV;
@property (strong, nonatomic) IBOutlet AddDeviceView *mAddDeviceV;
@property (nonatomic, strong) GCDAsyncUdpSocket *udpSocket;
@property (nonatomic, strong) NSMutableDictionary * devicesDic;
@property (strong, nonatomic) NSTimer *scanSecondTimer;

//- (IBAction)sendEmail:(UIButton *)sender;


// to cancel ESPTouchTask when
@property (atomic, strong) ESPTouchTask *_esptouchTask;

// the state of the confirm/cancel buttonssid


// without the condition, if the user tap confirm/cancel quickly enough,
// the bug will arise. the reason is follows:
// 0. task is starting created, but not finished
// 1. the task is cancel for the task hasn't been created, it do nothing
// 2. task is created
// 3. Oops, the task should be cancelled, but it is running
@property (nonatomic, strong) NSCondition *_condition;

@property (nonatomic, strong) UIButton *_doneButton;
@property (nonatomic, strong) EspTouchDelegateImpl *_esptouchDelegate;

@property (nonatomic, strong)NSDictionary *netInfo;

@end
NSArray * esptouchResults;


@implementation AddDeviceViewController {
    NSMutableArray * newDevicesArray;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    scanSeconds = 35;
    //ESPTouch
    self._condition = [[NSCondition alloc]init];
    self._esptouchDelegate = [[EspTouchDelegateImpl alloc]init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    //程序进入前台并处于活动状态调用
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wifiViewUpdates) name:UIApplicationDidBecomeActiveNotification object:nil];
    //注册Wi-Fi变化通知
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        [self wifiViewUpdates];
    }];
    _devicesDic = [NSMutableDictionary dictionary];
    
    ipAddrDataExt = @"";
    [self getAllDevices];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    scanSeconds = 35;
    sreachCounter = 0;
    
    [self setBackground:self];
    [self setupView];
    self.mWaitForConnectV.hidden = YES;
    self.navigationItem.hidesBackButton = NO;
    modulsArray = [NSMutableArray array];
    
}
-(void)viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBar.topItem.title = NSLocalizedString(@"add_device", nil);
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        [self wifiViewUpdates];
    }];
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.scanSecondTimer invalidate];
    self.scanSecondTimer = nil;
   // NSLog(@"viewWillDisappear === broadcastTimer invalidate!!!!!!!!!!!!!!!!!!!!!!!!!!!\n")
}

- (void)getAllDevices {
    [[RequestManager sheredMenage] getJsonDataWithUrl:AGetdevdevices andParameters:@{}
                                         isNeedCookie:NO success:^(id response) {
        
        NSNumber *code = [response objectForKey:AResultCode];
        if (code.intValue == 0) {
            NSArray *array = [response objectForKey:@"values"];
            NSLog(@"ALL DEVICES = %@\n", array);
            [self setAllDevises:array];
        }
    } failure:^(NSError *error) {
        [self showAlert:NSLocalizedString(@"operetion_failure", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
    }];
}

-(void)setAllDevises:(NSArray *)result {
    devicesArr = [NSMutableArray array];
    for (NSDictionary * device in result) {
        [devicesArr addObject:[[[Devices alloc]init] getDeviceObj:device]];
    }
}

- (NSTimer *) scanSecondTimer {
    if (!_scanSecondTimer) {
        _scanSecondTimer = [NSTimer timerWithTimeInterval:1.f target:self selector:@selector(scanTimerDecrease) userInfo:nil repeats:YES];
    }
    return _scanSecondTimer;
}

-(void)sendBroadcast {
   // NSLog(@"sendBroadcast === broadcastTimer called!!!!!!!!!!!!!!!!!!!!!!!!!!!\n")
    [self performSelector:@selector(startBroadcast) withObject:nil afterDelay:2];
}

-(void)setupView {
    [self setNavigationStyle:BaseColor tintColor:[UIColor whiteColor] titleText:NSLocalizedString(@"add_device", nil) titleColor:NavTitleColor];
    self.navigationController.navigationBar.prefersLargeTitles = NO;
    self.navigationController.navigationBar.hidden = NO;
    self.mAddDeviceV.delegate = self;
    self._esptouchDelegate = [[EspTouchDelegateImpl alloc]init];
    self.netInfo = [self fetchNetInfo];
    NSLog(@"bssid  = %@\n", [_netInfo objectForKey:@"bssid"])
    if ([[_netInfo objectForKey:@"bssid"] length] > 0) {
        NSString * routerPwd = [ACommonTools getObjectValueForKey:[_netInfo objectForKey:@"bssid"]];
        if ([routerPwd length] > 0) {
            self.mAddDeviceV.mRouterPasswordTxtFl.text = routerPwd;
        }
    }
}

-(void)scanTimerDecrease {
    [self.mWaitForConnectV.mSecondLb setText:[NSString stringWithFormat:@"%i%@", scanSeconds, NSLocalizedString(@"second", nil)]];
    scanSeconds --;
    if (scanSeconds <= 0) {
        [self.scanSecondTimer invalidate];
        self.scanSecondTimer = nil;
        scanSeconds = 35;
    }
}

#pragma mark -- Keyboard Fubctions
- (void)keyboardWillShow:(NSNotification *)sender {
    CGFloat height = [[sender.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(self.mScrollV.contentInset.top, 0, height, 0);
    _mScrollV.contentInset = edgeInsets;
    _mScrollV.scrollIndicatorInsets = edgeInsets;
}

- (void)keyboardWillHide:(NSNotification *)sender {
    UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
    _mScrollV.contentInset = edgeInsets;
    _mScrollV.scrollIndicatorInsets = edgeInsets;
}

-(void) showErrorController:(BOOL)isNetworkError  {
    NSLog(@"showErrorController");
    [self nowTime];
    UIWindow * keyWindow = [BaseViewController keyWindow];
    DeviceErrorView * deviceErrorV = [[DeviceErrorView alloc] initWithFrame:[keyWindow bounds] delegate:self];
    deviceErrorV.isNetworkErr = isNetworkError;
    [keyWindow addSubview:deviceErrorV];
    deviceErrorV.mScrollV.contentOffset = CGPointMake(0, 0);
    deviceErrorV.mScrollV.scrollEnabled = YES;
    deviceErrorV.mScrollContenierHeight.constant = self.mScrollV.frame.size.height;
    deviceErrorV.userInteractionEnabled = YES;
//    UIButton * buttons = [deviceErrorV.mTechSupport objectAtIndex:0];
//    deviceErrorV.mScrollV.contentSize = CGSizeMake( [[UIScreen mainScreen] bounds].size.width, buttons.frame.origin.y -88);
    [self.mWaitForConnectV.mActivityInd stopAnimating];
    self.mWaitForConnectV.hidden = YES;
    self->scanSeconds = 35;
}
-(void)handelOk {
    
}


- (NSArray *) executeForResultsWithSsid:(NSString *)apSsid bssid:(NSString *)apBssid password:(NSString *)apPwd taskCount:(int)taskCount broadcast:(BOOL)broadcast {
   // NSLog(@"start ***********    executeForResultsWithSsid   *************************\n");
    [self._condition lock];
    self._esptouchTask = [[ESPTouchTask alloc]initWithApSsid:apSsid andApBssid:apBssid andApPwd:apPwd];
    // set delegate
    [self._esptouchTask setEsptouchDelegate:self._esptouchDelegate];
    [self._esptouchTask setPackageBroadcast:broadcast];
    [self._condition unlock];
    NSArray * esptouchResults = [self._esptouchTask executeForResults:taskCount];
    NSLog(@"ESPViewController executeForResult() result is: %@",esptouchResults);
    return esptouchResults;
}


-(void) nowTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss.SSS"];
    
    NSDate *currentDate = [NSDate date];
    NSString *dateString = [formatter stringFromDate:currentDate];
    NSLog(@"Time is %@!!!!!!!!!!!!!\n", dateString);
}

- (void) smartlinkModules {
    self.netInfo = [self fetchNetInfo];
    __block  NSString *apSsid;
    __block  NSString *apPwd;
    __block NSString *apBssid;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mAddDeviceV.mRouterSSIDTxtRl.text = [self->_netInfo objectForKey:@"ssid"];
    });
    apSsid = self.mAddDeviceV.mRouterSSIDTxtRl.text;
    apPwd = self.mAddDeviceV.mRouterPasswordTxtFl.text;
    apBssid = [self->_netInfo objectForKey:@"bssid"];
    //Temporary after we ned delate it
    [ACommonTools setObjectValue:self.mAddDeviceV.mRouterPasswordTxtFl.text forKey:[self.netInfo objectForKey:@"bssid"]];
    int taskCount = 5;
    BOOL broadcast = YES;
   // NSLog(@"ESPViewController do confirm action...");
    
    dispatch_queue_t  queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSLog(@"ESPViewController do the execute work...");
        // execute the task
        NSArray *esptouchResultArray = [self executeForResultsWithSsid:apSsid bssid:apBssid password:apPwd taskCount:taskCount broadcast:broadcast];
        // show the result to the user in UI Main Thread
        dispatch_async(dispatch_get_main_queue(), ^{
            [self nowTime];
            NSLog(@"esptouchResultArray = %@\n", esptouchResultArray);
            
            ESPTouchResult *firstResult = [esptouchResultArray objectAtIndex:0];
            self->isResiveModule = YES;
            
            // check whether the task is cancelled and no results received
            if (!firstResult.isCancelled) {
                [ACommonTools setObjectValue:self.mAddDeviceV.mRouterPasswordTxtFl.text forKey:[self.netInfo objectForKey:@"bssid"]];
                if ([firstResult isSuc]) {
                    
                    for (int i = 0; i < [esptouchResultArray count]; ++i) {
                        ESPTouchResult * eSPTouchresult = [esptouchResultArray objectAtIndex:i];
                        
                        NSLog(@"ESPTouchResult *resultInArray = %@,  i = %i\n", eSPTouchresult, i);
                        if (eSPTouchresult.bssid.length > 0 && eSPTouchresult.ipAddrData.length > 0) {
                            NSLog(@"ESPTouchResult *resultInArray = %@,  i = %i\n", [esptouchResultArray objectAtIndex:i], i);
                           // NSString * ip = [self getIpAddWithoutEncode:[array objectAtIndex:i]];

                            
                            NSString * ip = [self getIpAddWithoutEncode:[esptouchResultArray objectAtIndex:i]];
                            NSDictionary * dic = @{@"eSPTouchresult" : eSPTouchresult , @"privateIp" : ip};
                            [self->modulsArray addObject:dic];
                        }
                    }
                    NSLog(@"self->modulsArray = %@\n", self->modulsArray);
                    [self setFoundDevice];
                }
            }
        });
    });
    
}

-(NSString *)getIpAddWithoutEncode:(NSString *)result {
    NSString* str = @"";
    str = result.debugDescription;
    NSArray *ary2 = [str componentsSeparatedByString:@"inetAddress: "];
    NSString *ip = [[ary2 objectAtIndex:1] stringByReplacingOccurrencesOfString:@"]" withString:@""];
    NSLog(@"%@", ip);
    return ip;
}
-(void)setFoundDevice {
        for (NSDictionary * dic in modulsArray) {
            ESPTouchResult * result = [dic valueForKey:@"eSPTouchresult"];
            //if new device has a other owner return
NSLog(@"result.bssid = %@\n", result.bssid);
            NSLog(@"result.ipAddrData = %@\n", result.ipAddrData);
            NSString * ip = [dic valueForKey:@"privateIp"];;
            NSLog(@"result.ipAddrData getHexStringByData = %@\n", ip);
        BOOL found = NO;
        for (Devices * dev in newDevicesArray) {
            if ([[result.bssid lowercaseString] isEqualToString:[dev.mac lowercaseString]]) {
                found = YES;
                break;
            }
        }
        if (!found) {
            NSDictionary *paramDic = [Devices getDeviceJson:result.bssid privateIp:ip index:@0];
            [newDevicesArray addObject:[[[Devices alloc]init] getDeviceObj:paramDic]];
        }
    }
}

-(void)goToSuccessfullyConnected:(NSMutableArray *)newDevices{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DeviceSuccessfulyAddedViewController * vc = [storyboard   instantiateViewControllerWithIdentifier:@"DeviceSuccessfulyAddedViewController"];
    [vc setMNewDevicesArr:newDevices];
    [self.navigationController pushViewController:vc animated:YES];
    self->scanSeconds = 35;
}



- (void)wifiViewUpdates {
    self.netInfo = [self fetchNetInfo];
    self.mAddDeviceV.mRouterSSIDTxtRl.text = [_netInfo objectForKey:@"ssid"];
    NSLog(@"ssid = %@\n", [_netInfo objectForKey:@"ssid"]);
    NSLog(@"bssid = %@\n", [_netInfo objectForKey:@"bssid"]);
    NSLog(@"pwd = %@\n", self);
}

- (NSDictionary *)fetchNetInfo
{
   // NSLog(@"start  ************ fetchNetInfo********\n");
    
    NSMutableDictionary *wifiDic = [NSMutableDictionary dictionaryWithCapacity:0];
    wifiDic[@"ssid"] = ESPTools.getCurrentWiFiSsid;
    wifiDic[@"bssid"] = ESPTools.getCurrentBSSID;
    //NSLog(@"finish  ************ fetchNetInfo********\n");
    
    return wifiDic;
}


-(void)isExistNewDevice {
    NSArray * copyNewDevicesArray = [self->newDevicesArray mutableCopy];
    for (int i = 0; i < copyNewDevicesArray.count; i++) {
        NSString * url = [NSString stringWithFormat:@"%@?macs=%@", AGetExistDevice, [[copyNewDevicesArray objectAtIndex:i] mac]];
        [[RequestManager sheredMenage] getDataWithUrl:url success:^(id  _Nonnull response) {
            //NSLog(@"response = %@",response);
            if( [[[response valueForKey:[[copyNewDevicesArray objectAtIndex:i] mac]] valueForKey:@"exist"] boolValue]) {
               // NSLog(@"isExistNewDevice after removeObjectAtIndex:i = %i\n remove mac = %@\n", i, [[copyNewDevicesArray objectAtIndex:i] mac]);
                for (int j = 0; j < self->newDevicesArray.count; j++) {
                    if ([[[[self->newDevicesArray objectAtIndex:j] mac] lowercaseString]  isEqualToString:[[[copyNewDevicesArray objectAtIndex:i] mac] lowercaseString]]) {
                        [self->newDevicesArray removeObjectAtIndex:j];
                        break;
                    }
                }
                //NSLog(@"isExistNewDevice after removeObjectAtIndex:i  self->newDevicesArray  = %@\n",self->newDevicesArray);
            }
            if (i == copyNewDevicesArray.count -1) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self->newDevicesArray.count == 0) {
                        //NSLog(@"Call showErrorController from CheckDeviceOwner\n");
                        [self showErrorController:NO];
                    } else {
                        [self goToSuccessfullyConnected:self->newDevicesArray];
                    }
                });
                
            }
            
        } failure:^(NSError * _Nonnull error) {
            NSLog(@"error = %@",error.localizedDescription);
        }];
    }
}


#pragma mark AddACView

- (void)scanDidEnd
{
    [self nowTime];
    [self closeAddACView];
    [self performSelector:@selector(isConnectDevice) withObject:nil afterDelay:0.5];
    
}
-(void)isConnectDevice {
    if (!isResiveModule) {
        [self.mWaitForConnectV.mActivityInd stopAnimating];
        NSLog(@"Call showErrorController from scanDidEnd\n");
        
        [self showErrorController:YES];
    } else {
        NSLog(@"newDevicesArray = %@\n", newDevicesArray);
        if (self->newDevicesArray.count > 0) {
            [self isExistNewDevice];
        } else {
            [self showErrorController:YES];
        }
    }
}


- (void)scanClicked {
//     newDevicesArray = [NSMutableArray array];
//    Devices * dev = [[Devices alloc] init];
//    dev.mac = @"192.168.5.17";
//    dev.privateIp = @"a4cf12dd509a";
//    [newDevicesArray addObject:dev];
//    [self goToSuccessfullyConnected:newDevicesArray];
    newDevicesArray = [NSMutableArray array];
    [[NSRunLoop mainRunLoop] addTimer:self.scanSecondTimer forMode:NSRunLoopCommonModes];
    NSLog(@"start  ************ scanClicked********\n");
    [self nowTime];
    if (self.mAddDeviceV.mRouterSSIDTxtRl.text.length <= 0 || self.mAddDeviceV.mRouterPasswordTxtFl.text.length <= 0) {
        [self showAlert:NSLocalizedString(@"fill_info_note", nil) ok:NSLocalizedString(@"ok", nil) cencel:@"" delegate:self];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.mWaitForConnectV.hidden = NO;
            self.navigationItem.hidesBackButton = YES;
            [self.mWaitForConnectV.mActivityInd startAnimating];
            [self smartlinkModules];

        });
        [self performSelector:@selector(startBroadcast) withObject:nil afterDelay:2];
    }
}

- (void)startBroadcast {
    u_int16_t serverPort = (u_int16_t)(2000 + (int)arc4random_uniform((u_int32_t)3001));
    self.udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    NSError *error = nil;
    [self.udpSocket bindToPort:serverPort error:&error];
    if (error) {
        return;
    }
    error = nil;
    [self.udpSocket enableBroadcast:YES error:&error];
    if (error) {
        return;
    }
    NSData *data = [@"Are You AirM2M IOT Smart Device?" dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    [self.udpSocket sendData:data toHost:ABaseHostString port:ABasePort withTimeout:ATimeOut tag:(long)0];
    error = nil;
    [self.udpSocket beginReceiving:&error];
    if (error) {
        return;
    }
    [self performSelector:@selector(closeAfterSend) withObject:nil afterDelay:30];
}

- (void)closeAfterSend {
    [self.udpSocket closeAfterSending];
}


#pragma mark GCDAsyncUdpSocketDelegate
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didConnectToAddress:(NSData *)address {
    NSLog(@"didConnect to Address data = %@",address);
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotConnect:(NSError *)error {
    
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag {
    if (tag == 10) {
        [self.udpSocket close];
        return;
    }
    [self performSelector:@selector(udpSendData:) withObject:[NSNumber numberWithLong:tag] afterDelay:2];
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
    if (tag >= 10)
    {
        [self.udpSocket closeAfterSending];
        return;
    }
    
    [self performSelector:@selector(udpSendData:) withObject:[NSNumber numberWithLong:tag] afterDelay:2.5];
}

- (void)udpSendData:(NSNumber *)tag
{
    long t = tag.longValue + 1;
    NSData *data = [@"Are You AirM2M IOT Smart Device?" dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    [self.udpSocket sendData:data toHost:ABaseHostString port:ABasePort withTimeout:ATimeOut tag:t];
}
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext
{
    NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"msg = %@\n", msg);
    NSArray *array = [msg componentsSeparatedByString:@" "];
    if (array.count < 3) {
        return;
    }
    NSString *str1 = [array objectAtIndex:1];
    NSString *privateIP = [array objectAtIndex:2];
    NSArray *ary2 = [str1 componentsSeparatedByString:@"."];
    NSString *mac = [[ary2 objectAtIndex:1] stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSLog(@"udpSocket didReceiveData ipAddrDataExt = %@\n", ipAddrDataExt);
    NSLog(@"udpSocket didReceiveData array = %@\n", array);
    NSLog(@"udpSocket didReceiveData ary2 = %@\n", ary2);
    BOOL found = NO;
    for (Devices * dev in devicesArr){
        NSLog(@"udpSocket didReceiveData devicesArr mac = %@\n", dev.mac);
        NSLog(@"udpSocket didReceiveData curr mac = %@\n", mac);
        
        if ([[dev.mac lowercaseString] isEqualToString:[mac lowercaseString]]) {
            found = YES;
            NSLog(@"udpSocket didReceiveData found = Yes \n privateIP = %@\n mac = %@\n", privateIP, mac);
            break;
        }
    }
    if (!found) {
        isResiveModule = YES;
        BOOL isNew = YES;
        for (int i = 0; i < newDevicesArray.count; i++){
            if ([[[[newDevicesArray objectAtIndex:i] mac]lowercaseString] isEqualToString:[mac lowercaseString]]) {
                [[newDevicesArray objectAtIndex:i] setPrivateIp:privateIP];
                NSLog(@"udpSocket didReceiveData isNew = NO\n index = %i\n ipAddrDataExt = %@\n newDevicesArray = %@\n",i, ipAddrDataExt, newDevicesArray);
                isNew = NO;
                break;
            }
        }
        if (isNew) {
            NSDictionary *paramDic = [Devices getDeviceJson:mac privateIp:privateIP index:@(0)];
            //added on the arr new devices obj
            [newDevicesArray addObject:[[[Devices alloc]init] getDeviceObj:paramDic]];
            NSLog(@"udpSocket didReceiveData isNew = YES\n index = last\n ipAddrDataExt = %@\n newDevicesArray = %@\n", ipAddrDataExt, newDevicesArray);
        }
        ipAddrDataExt = privateIP;
        NSLog(@"udpSocket didReceiveData found = NO \n ipAddrDataExt = %@\n newDevicesArray = %@\n", ipAddrDataExt, newDevicesArray);
    }
}


- (void)closeAddACView {
    [self cancel];
    if (self.udpSocket != nil && !self.udpSocket.isClosed) {
        [self.udpSocket close];
    }
}

- (void) cancel{
    [self._condition lock];
    if (self._esptouchTask != nil) {
        [self._esptouchTask interrupt];
    }
    [self._condition unlock];
}


-(void)hideErrorView {
    UIWindow * keyWindow = [BaseViewController keyWindow];
    for(UIView *subView in keyWindow.subviews) {
        if([subView isKindOfClass:[DeviceErrorView class]]){
            [subView removeFromSuperview];
        }
    }
}
#pragma mark:  Error Delegate
-(void)handlerRetryOk {
    [self hideErrorView];
    scanSeconds = 35;
    self.navigationItem.hidesBackButton = NO;
    isResiveModule = NO;
    
}
-(void)handlerTechSupport {
    [self hideErrorView];
    [self pushViewControllerWithIdentifier:@"TechSupportViewController" controller:self];
}
-(void)handlerBack {
    [self hideErrorView];
    [self.navigationController popViewControllerAnimated:YES];
}


//new
- (IBAction)sendEmail:(UIButton *)sender {
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;

        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:@"logfile.txt"];
        NSData *fileData = [NSData dataWithContentsOfFile:path];

        [mailCont setSubject:@"AirConet Log file!"];
        [mailCont setToRecipients:[NSArray arrayWithObject:@"karapetyankarine87@gmail.com"]];//@"jmaliaga1@gmail.com"]];
        [mailCont addAttachmentData:fileData mimeType:@"application/txt" fileName:@"logfile.text"];

        [self presentViewController:mailCont animated:YES completion:nil];

    }
}
// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog (@"Sending Mail is cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog (@"Sending Mail is Saved");
            break;
        case MFMailComposeResultSent:
            [ACommonTools setBoolValue:YES forKey:@"isSentLogfile"];

            NSLog (@"Your Mail has been sent successfully");
            break;
        case MFMailComposeResultFailed:
            NSLog (@"Message sending failed");
            break;
        default:
            NSLog (@"Your Mail is not Sent");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end

