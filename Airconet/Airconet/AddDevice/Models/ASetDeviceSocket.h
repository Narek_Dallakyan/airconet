//
//  ASetDeviceSocket.h
//  iAirCon
//
//  Created by Elvis on 16/8/3.
//  Copyright © 2016年 Elvis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASetDeviceSocket : NSObject

@property (nonatomic, strong) NSString *ip;
@property (nonatomic, strong) NSString *command;
@property (nonatomic, strong) NSString *configData;

- (void)send;

@end
