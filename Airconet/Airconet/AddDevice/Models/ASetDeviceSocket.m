//
//  ASetDeviceSocket.m
//  iAirCon
//
//  Created by Elvis on 16/8/3.
//  Copyright © 2016年 Elvis. All rights reserved.
//

#import "ASetDeviceSocket.h"
#import "GCDAsyncSocket.h"

@interface ASetDeviceSocket ()

@property (nonatomic, strong) GCDAsyncSocket *socket;

@end

@implementation ASetDeviceSocket

- (void)send
{
    self.socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    NSError *error = nil;
    [self.socket connectToHost:self.ip onPort:80 error:&error];
    if (error) {
       // NSLog(@"SetDeviceSocket failed to connect port");
    }
}

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port
{
    NSString *data = [NSString stringWithFormat:@"POST %@ HTTP/1.1\r\nHost: %@\r\nAccept: application/json\r\nContent-Type: application/json\r\nConnection: Close\r\nAccept-Charset: utf-8,*\r\nUser-Agent: LuaSocket 2.0.2\r\nAccept-Encoding: gzip\r\nContent-Length: %lu\r\n\r\n%@",self.command,self.ip,(unsigned long)[self.configData lengthOfBytesUsingEncoding:NSUTF8StringEncoding],self.configData];
    
    self.configData = @"";
    NSData *nsdata = [data dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    [sock writeData:nsdata withTimeout:60 tag:0];
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    [sock disconnectAfterReading];
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    [sock readDataWithTimeout:60 tag:0];
}

- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutReadWithTag:(long)tag
                 elapsed:(NSTimeInterval)elapsed
               bytesDone:(NSUInteger)length
{
    return 0;
}

- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutWriteWithTag:(long)tag
                 elapsed:(NSTimeInterval)elapsed
               bytesDone:(NSUInteger)length
{
    return 0;
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
    self.socket.delegate = nil;
    self.socket = nil;
}

@end
