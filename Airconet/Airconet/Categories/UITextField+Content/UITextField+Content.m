//
//  UITextField+Content.m
//  Airconet
//
//  Created by Karine Karapetyan on 1/23/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import "UITextField+Content.h"
#import "APublicDefine.h"
#import "BaseViewController.h"

@implementation UITextField (Content)

- (void) addEgdeInsetsOnTextFields {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                                   12, 20)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}
-(void) setRightPadding {
    UIView * paddingView=  [[UIView alloc]initWithFrame:CGRectMake(0, 20, self.frame.size.height-10, self.frame.size.height-1)];
    self.rightView = paddingView;
    self.rightViewMode = UITextFieldViewModeAlways;
}
-(void) setleftPadding {
    UIView * paddingView=  [[UIView alloc]initWithFrame:CGRectMake(50, 0, self.frame.size.height-10, self.frame.size.height-1)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}

-(void)deletRightPadding {
        UIView * paddingView=  [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
        self.rightView = paddingView;
        self.rightViewMode = UITextFieldViewModeAlways;
}

- (void) setBlueBorderStyle {
    self.layer.cornerRadius = 5.5f;
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = 3.f;
    self.layer.borderColor=[BaseColor CGColor];
}
- (void) setBlueBorderStyle:(float)borderWidth {
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = borderWidth;
    self.layer.borderColor=[BaseColor CGColor];
}

- (void) setImageInLeftView:(UIImage *) img {
    UIView * leftViewImg = [[UIImageView alloc] initWithImage:img];
    if ([BaseViewController isDeviceLanguageRTL] && ![img isEqual:[UIImage imageNamed:@"password"]]) { //arabian
        leftViewImg.frame = CGRectMake(-5, 0, self.frame.size.height-10, self.frame.size.height-10);
    } else {
        leftViewImg.frame = CGRectMake(5, 0, self.frame.size.height-10, self.frame.size.height-10);
    }
    leftViewImg.contentMode = UIViewContentModeScaleAspectFit;
    self.leftViewMode = UITextFieldViewModeAlways;
    
     UIView * paddingView =  [[UIView alloc]initWithFrame:CGRectMake(20, 0, self.frame.size.height-10, self.frame.size.height-10)];
      [paddingView addSubview:leftViewImg];
      [self.leftView setFrame:leftViewImg.frame];
      self.leftView = paddingView;
}
- (void) setImageInRightView:(UIImage *) img {
    UIView * rightViewImg = [[UIImageView alloc] initWithImage:img];
    if ([BaseViewController isDeviceLanguageRTL]) { 
        rightViewImg.frame = CGRectMake(-5, 0, self.frame.size.height+10, self.frame.size.height-10);
    }else {
        rightViewImg.frame = CGRectMake(5, 0, self.frame.size.height+10, self.frame.size.height-10);
    }
    rightViewImg.contentMode = UIViewContentModeScaleAspectFit;
    self.rightViewMode = UITextFieldViewModeAlways;
    
    UIView * paddingView=  [[UIView alloc]initWithFrame:CGRectMake(0, 20, self.frame.size.height+10, self.frame.size.height-10)];
      [paddingView addSubview:rightViewImg];
      [self.rightView setFrame:rightViewImg.frame];
      self.rightView = paddingView;

}

- (void) setButtonInRightView:(UIButton *) btn  buttonImg: (UIImage *) img{
    if ([BaseViewController isDeviceLanguageRTL]) { //Arabic, Hebrew
          btn.frame = CGRectMake(5, 0, self.frame.size.height-10, self.frame.size.height-10);
      } else {
          btn.frame = CGRectMake(0, 0, self.frame.size.height-10, self.frame.size.height-10);
      }
    [btn setBackgroundColor: [UIColor clearColor]];
    [btn setImage:img forState:UIControlStateNormal];
    self.rightViewMode = UITextFieldViewModeAlways;
    
    if ([BaseViewController isDeviceLanguageRTL]) { //Arabic, Hebrew
        UIView * paddingView=  [[UIView alloc]initWithFrame:CGRectMake(0, 20, self.frame.size.height+10, self.frame.size.height-10)];
        [paddingView addSubview:btn];
        [self.rightView setFrame:btn.frame];
        self.rightView = paddingView;
    } else {
        self.rightView = btn;
    }
}


- (void) setPlaceholderColor: (UIColor*)color text:(NSString *)text {
    self.attributedPlaceholder =
       [[NSAttributedString alloc] initWithString:text
         attributes:@{
            NSForegroundColorAttributeName: color
         }
       ];
}
    
@end

