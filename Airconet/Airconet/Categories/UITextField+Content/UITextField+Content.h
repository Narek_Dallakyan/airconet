//
//  UITextField+Content.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/23/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField(Content)

- (void) addEgdeInsetsOnTextFields;
- (void) setRightPadding;
- (void) setleftPadding;
- (void) deletRightPadding;
- (void) setBlueBorderStyle;
- (void) setBlueBorderStyle:(float)borderWidth;
- (void) setImageInLeftView:(UIImage *) img;
- (void) setImageInRightView:(UIImage *) img;
- (void) setButtonInRightView:(UIButton *) btn  buttonImg: (UIImage *) img;
- (void) setPlaceholderColor: (UIColor*)color text:(NSString *)text;
@end

NS_ASSUME_NONNULL_END
