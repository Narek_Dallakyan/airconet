//
//  NSDictionary.h
//  Airconet
//
//  Created by Karine Karapetyan on 02-03-20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (Object)
+(NSDictionary *) dictionaryWithPropertiesOfObject:(id) obj;

@end

NS_ASSUME_NONNULL_END
