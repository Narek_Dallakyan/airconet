//
//  BaseViewController.h
//  Airconet
//
//  Created by Karine Karapetyan on 1/23/20.
//  Copyright © 2020 Ekon manufacturer and trading Shanghai Co Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AKeysDefine.h"
#import "APublicDefine.h"
#import "AVersionConfig.h"
#import "CustomAlertView.h"
#import "DevicesNavigationBar.h"
#import "Log.h"
#import "PopupView.h"
#import "PopupRemotControllView.h"
#import "RequestManager.h"


NS_ASSUME_NONNULL_BEGIN

extern NSMutableArray * deviceMacs;
extern NSMutableArray * hiddenDevicesArr;
extern NSMutableArray * mainDevicesArr;
extern  NSString* ipAddrDataExt;

@interface BaseViewController : UIViewController
+(NSMutableAttributedString *) getAttributStringPossition:(NSString *)attribut
                                                     text:(NSString *)text color:(UIColor *)color;

+(UIWindow*)keyWindow;
+(void)setGradient:(UIView *)view;

-(BOOL)prefersStatusBarHidden;
-(void)showAlert:(NSString *)message
              ok:(NSString*)title
          cencel:(NSString *)cencel
        delegate:(id<CustomAlertDelegate>)delegate;
-(void)popupAlert:(NSString *)message
            frame:(CGRect)frame
               ok:(NSString*)title
           cencel:(NSString *)cencel
         delegate:(id<PopupViewDelegate>)delegate ;
-(void)popupRemoteControllAlert:(CGRect)frame
                             ok:(NSString*)title
                       delegate:(id<PopupRemotControllDelegate>)delegate;
-(void)setNavigationStyle:(UIColor *)barTintColor
                tintColor:(UIColor *)tintColor
                titleText:(NSString *)titleText
               titleColor:(UIColor *)titleColor;
-(UILabel *)setNavigationTitle:(NSString *)txt;
-(void)presentViewControllerWithIdentifier:(NSString *)storyboardIdentifier
                                controller:(UIViewController *)viewCont;
-(void)pushViewControllerWithIdentifier:(NSString *)storyboardIdentifier
                             controller:(UIViewController *)viewCont;
-(void)goToMainPage;
-(void)animationPopup:(UIView *)view;
+(NSInteger )getModeValue:(int)index;
-(BOOL)isDarkMode;
-(NSString *)getCurrentBackgroundMode;
-(void)setBackground:(UIViewController *)vc;
-(void)setBackgroundAuto:(UIViewController *)vc;
-(BOOL)isHiddenDevice:(NSString *)mac;
-(BOOL)isHiddenGroupsDevice:(NSArray *)devicemacs;
+(NSInteger)indexOfMode:(UIImage *)img;
+(NSDate *)convertDateFromString:(NSString *)str;
+(NSString *)convertStringFromDate:(NSDate *)date;
+(NSString *)getDateMode:(NSString *)dateModeForRequest;
+(NSString *)getPmMode:(NSString *)pmModeTypeForRequest;
+(NSString *)getModeName:(NSNumber *)value;
+(NSString *)graphType:(NSString *)type;
+(UIColor *) setDropDownColor:(UIViewController *)vc;
+ (BOOL)isDeviceLanguageRTL;
+(NSString *)getTimerPram:(NSString *) timerParam;
@end

NS_ASSUME_NONNULL_END
